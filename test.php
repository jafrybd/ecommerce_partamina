<?php
if (isset($_GET['phpinfo'])) {
	phpinfo(); 
	exit;
}
?>
<!--Original code - JV System Test-->
<html>

  <head>

    <style>
      body, td {
        font-size:70%;
        font-family:verdana, helvetica, arial;
      }

      div.main {
        width:500px;
        text-align:left;
        top:20px;
        position:relative;
        border:2px solid #F0F0F0;
        padding:20px;
      }
      
      }
    </style>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    
    <title>
      Тест системы
    </title>
    
  </head>

  <body>
    <div align="center">
      <div class="main">

        <center>
          <h2>System test</h2>
          This test will check if the module will work on your server. Run it on a real server, not Denwer or localhost.
          <br />
          <br />
          <br />
          <br />
          <br />
        </center>
       
        <table width="100%"> 
          <tr>
            <td>
              <h3>PHP version: </h3>
            </td>
            <td>
              <h3>IonCube Loader: </h3>
            </td>
          </tr>
          <tr>
            <td>
              <?php 
                 if (!defined('PHP_VERSION_ID')) {
                   $version = explode('.', PHP_VERSION);
                   define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
                 }

                if ( (PHP_VERSION_ID >= 50600) && (PHP_VERSION_ID < 70200) )  {
                  $phpversion_str = '<font color="green">' . phpversion() . '</font>';
                  $status_phpversion_str = '<b><font color="green">Good</font></b>';
                } else {
                  $phpversion_str = '<font color="red">' . phpversion() . '</font>';
                  $status_phpversion_str = '<b><font color="red">Bad</font></b>';
                }
              ?>
              <br />
              <b>Need:</b> 5.6.x.x - 7.2.x.x 
              <br />
              <br />
              <b>Your version:</b> <?php echo $phpversion_str; ?>
              <br />
              <br />
              <b>Status:</b> <?php echo $status_phpversion_str; ?> 
            </td>
            <td>
              <?php 
                $ioncube_loader_version = ioncube_loader_version_array();
                
                if ($ioncube_loader_version && ($ioncube_loader_version['major'] >= 9)) {
                  $ioncube_version_str = '<font color="green">ver. ' . $ioncube_loader_version['version'] . '</font>';
                  $status_ioncube_str = '<b><font color="green">Good</font></b>';
                } else {
                  if (isset($ioncube_loader_version['version'])) {
                    $ioncube_version_str = '<font color="red">ver. ' . $ioncube_loader_version['version'] . '</font>';
                  } else {
                    $ioncube_version_str = '<font color="red">Not Installed</font>';
                  }
                  $status_ioncube_str = '<b><font color="red">Bad</font></b>';
                }
                
              ?>
              <br />
              <b>Need:</b> 9.x and higher
              <br />
              <br />
              <b>Your version:</b> <?php echo $ioncube_version_str; ?>
              <br />
              <br />
              <b>Status:</b> <?php echo $status_ioncube_str; ?>
          </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <b>If something is wrong, please email me at <a href="mailto:sv2109@gmail.com">sv2109@gmail.com</a> and ask if the module will work on your server</b>
        <br />
        <br />
        <br />
        <br />
        <a href="<?php echo $_SERVER['PHP_SELF']; ?>?phpinfo=1" target="_blank">PHP info </a>
      </div>
    </div>
  </body>
</html>

<?php
function ioncube_loader_version_array () {
  if (extension_loaded("ionCube Loader")) {
  
    if ( function_exists('ioncube_loader_iversion') ) {
      // Mmmrr
      $ioncube_loader_iversion = ioncube_loader_iversion();
      $ioncube_loader_version_major = (int)substr($ioncube_loader_iversion,0,1);
      $ioncube_loader_version_minor = (int)substr($ioncube_loader_iversion,1,2);
      $ioncube_loader_version_revision = (int)substr($ioncube_loader_iversion,3,2);
      $ioncube_loader_version = "$ioncube_loader_version_major.$ioncube_loader_version_minor.$ioncube_loader_version_revision";
    }
    
    if ( function_exists('ioncube_loader_version') ) {
      $ioncube_loader_version = ioncube_loader_version();
      // for version 10 and more
      if (substr($ioncube_loader_version,0,1) == 1) {
        $ioncube_loader_version_major = (int)substr($ioncube_loader_version,0,2);
        $ioncube_loader_version_minor = (int)substr($ioncube_loader_version,3,1);
      } else {
        $ioncube_loader_version_major = (int)substr($ioncube_loader_version,0,1);
        $ioncube_loader_version_minor = (int)substr($ioncube_loader_version,2,1);
      }
    }
    
    return array(
      'version'=>$ioncube_loader_version, 
      'major'=>$ioncube_loader_version_major, 
      'minor'=>$ioncube_loader_version_minor
    );
    
  } else {
    return false;
  } 
	
}

?>