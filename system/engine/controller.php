<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Controller class
*/
abstract class Controller {
	protected $registry;
    
	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
	
	public function numberConverter($number)
        {
            $bn = ["১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০"];
            $en = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
            $en_number = str_replace($en, $bn, $number);
            return $en_number;
        }
}