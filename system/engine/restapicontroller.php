<?php
error_reporting(E_ALL & ~E_NOTICE);

abstract class RestApiController extends Controller
{
    public static $ocRegistry = null;
    public static $ocVersion = null;

    public $statusCode = 200;
    public $post = array();

    public $allowedHeaders = array("GET", "POST", "PUT", "DELETE");

    public $accessControlAllowHeaders = array(
        "Content-Type",
        "oc-authorization",
        "oc-session-Id",
        "oc-currency",
        "oc-language"
    );

    public $json = array("success" => 1, "error" => array(), "data" => array());

    private $httpVersion = "HTTP/1.1";

    public function auth($method, $language_mod="")
    {
        //home setting
        $this->config->set('home_product_limit',20);
        $this->config->set('home_banner_width',1920);
        $this->config->set('home_banner_hieght',700);
        $this->config->set('home_category_width',100);
        $this->config->set('home_category_hieght',50);
        $this->config->set('featured_module_id',28);

        $this->config->set('ac_rest_decimal_point',2);

        $this->opencartVersion = str_replace(".", "", VERSION);

        static::$ocRegistry = $this->registry;

        /*check rest api is enabled*/
        // if (!$this->config->get('module_ac_rest_api_status')) {
        //     $this->json["error"][] = 'Rest API is disabled. Enable it!';
        //     $this->statusCode = 403;
        //     $this->sendResponse();
        // }

        $this->setConfiguration();
        
        //default language file
        $this->load->language("ac_api/common");
        if($language_mod){
            $this->load->language("ac_api/$language_mod");
        }

        $this->validateMethod($method);
        
        //default model
        $this->load->model("ac_api/common");
    }

    public function sendResponse()
    {
        $this->json['customer'] = $this->customer->isLogged();

        $statusMessage = $this->getHttpStatusMessage($this->statusCode);

        //fix missing allowed OPTIONS header
        $this->allowedHeaders[] = "OPTIONS";

        if ($this->statusCode != 200) {
            if (!isset($this->json["error"])) {
                $this->json["error"][] = $statusMessage;
            }

            if ($this->statusCode == 405 && $_SERVER['REQUEST_METHOD'] !== 'OPTIONS') {
                $this->response->addHeader('Allow: ' . implode(",", $this->allowedHeaders));
            }

            $this->json["success"] = 0;

            //enable OPTIONS header
            if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
                $this->statusCode = 200;
                $this->json["success"] = 1;
                $this->json["error"] = array();
            }
        }

        //staus 0 if have error
        if(isset($this->json["error"]) && !empty($this->json["error"])){
            $this->json["success"] = 0;
            $this->json["data"] = new stdClass;
        }

        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: '. implode(", ", $this->allowedHeaders));
            $this->response->addHeader('Access-Control-Allow-Headers: '. implode(", ", $this->accessControlAllowHeaders));
            $this->response->addHeader('Access-Control-Allow-Credentials: true');
        }

        $this->response->addHeader($this->httpVersion . " " . $this->statusCode . " " . $statusMessage);
        $this->response->addHeader('Content-Type: application/json; charset=utf-8');

        if (defined('JSON_UNESCAPED_UNICODE')) {
            $this->response->setOutput(json_encode($this->json, JSON_UNESCAPED_UNICODE));
        } else {
            $this->response->setOutput($this->rawJsonEncode($this->json));
        }

        $this->response->output();

        die;
    }

    public function getHttpStatusMessage($statusCode)
    {
        $httpStatus = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed'
        );

        return ($httpStatus[$statusCode]) ? $httpStatus[$statusCode] : $httpStatus[500];
    }

    private function setConfiguration()
    {

        //$this->config->set('config_rest_api_key','Bearer fOBLlu6Zv6xHf0jYjeed0eUOjktC73tpNNzOtlJTjx4X7OWj0yHUD57BPa4NUmtnubzHMbpSubqRXwRgWHWMJmdTgCrn1xEZdTnP2Ac7oeqeVYfGFXKRYJUjBVFkKoDRQ02uiudjr3kKQCJKYwcDXIEcCiRKalE6gOc5WHSedvkZHbk5MzOB0B2iuGaM03xLyrLhUyLszf5OSOn6Gg6xvphFAhslJZRKOyGTaVKTZNZigGVzLWlON4jzbyjfEOnE');

        $headers = $this->getRequestHeaders();

        $key = "";

        //set api key
        if (isset($headers['oc-authorization'])) {
            $key = $headers['oc-authorization'];
        }

        /*validate api security key*/
        if (!$this->config->get('config_rest_api_key')) {
            $this->json["error"][] = 'Invalid or missing secret key, please update api key!';
            $this->statusCode = 403;
            $this->sendResponse();
        }

        if ($this->config->get('config_rest_api_key') && ($key != $this->config->get('config_rest_api_key'))) {
            $this->json["error"][] = 'Invalid or missing secret key';
            $this->statusCode = 403;
            $this->sendResponse();
        }

        //set store ID
        if (isset($headers['oc-store-id'])) {
          $this->config->set('config_store_id', $headers['oc-store-id']);
        }

        //set currency
        // if (isset($headers['oc-currency'])) {
        //     $currency = $headers['oc-currency'];
        //     if (!empty($currency)) {
        //         //$this->currency->set($currency);
        //         $this->session->data['currency'] = $currency;
        //     }
        // }

        //set language @tested
        // if (isset($headers['oc-language'])) {

        //     $this->load->model('localisation/language');
        //     $allLanguages = $this->model_localisation_language->getLanguages();
    
        //     $languages = array();

        //     foreach ($allLanguages as $result) {
        //       $languages[$result['code']] = $result;
        //     }

        //     if(count($languages) > 1){
              
        //       $oc_lang = $headers['oc-language'];
              
        //       //check if language code is valid
        //       if(isset($languages[$oc_lang])){

        //         $this->session->data['language'] = $oc_lang;
        //         $this->config->set('config_language', $oc_lang);
    
        //         $this->config->set('config_language_id', $languages[$oc_lang]['language_id']);
    
        //         if (isset($languages[$oc_lang]['directory']) && !empty($languages[$oc_lang]['directory'])) {
        //             $directory = $languages[$oc_lang]['directory'];
        //         } else {
        //             $directory = $languages[$oc_lang]['code'];
        //         }
    
        //         $language = new \Language($directory);
        //         $language->load($directory);
        //         $this->registry->set('language', $language);
        //       }

        //     }
        // }

    }

    private function getRequestHeaders()
    {
        $arh = array();
        $rx_http = '/\AHTTP_/';

        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);

                $rx_matches = explode('_', $arh_key);

                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val) {
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }

                    $arh_key = implode('-', $rx_matches);
                }

                $arh[strtolower($arh_key)] = $val;
            }
        }

        return ($arh);

    }

    public function getPost()
    {
        // $input = file_get_contents('php://input');
        // $post = json_decode($input, true);
        $request_data = $this->request->request;

        if (!is_array($request_data) || empty($request_data)) {
            $this->statusCode = 400;
            $this->json['error'][] = 'Invalid request body, please validate the json object';
            $this->sendResponse();
        }

        return $request_data;
    }

    private function rawJsonEncode($input, $flags = 0) {
        $fails = implode('|', array_filter(array(
            '\\\\',
            $flags & JSON_HEX_TAG ? 'u003[CE]' : '',
            $flags & JSON_HEX_AMP ? 'u0026' : '',
            $flags & JSON_HEX_APOS ? 'u0027' : '',
            $flags & JSON_HEX_QUOT ? 'u0022' : '',
        )));
        $pattern = "/\\\\(?:(?:$fails)(*SKIP)(*FAIL)|u([0-9a-fA-F]{4}))/";
        $callback = function ($m) {
            return html_entity_decode("&#x$m[1];", ENT_QUOTES, 'UTF-8');
        };
        return preg_replace_callback($pattern, $callback, json_encode($input, $flags));
    }

    public function validateData($required_data,$post){
        
        foreach($required_data as $data){
            if(!isset($post[$data])){
                $this->json['error'][] = "$data is required!";
            }
        }

        if(!empty($this->json['error'])){
            $this->statusCode = 400;
            $this->sendResponse();
        }
    }

    public function validateMethod($method=''){
        
        if(strtolower($method) != strtolower($this->request->server['REQUEST_METHOD'])){
            
            $this->statusCode = 405;
            $this->json['error'][] = $this->language->get('method_not_allowed');

            $this->sendResponse();
        }
    }

    public function model($model){
        
        $this->load->model($model);
    }

    //product data formating
    public function genrateProductData($product_info){

        if($product_info){
            if ($product_info['image']) {
            $image = $this->model_tool_image->resize($product_info['image'],1200, 1200);
            } else {
            $image = $this->model_tool_image->resize('placeholder.png', 1200, 1200);
            }
        
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
            $price = false;
            }
        
            if ((float)$product_info['special']) {
            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
            $special = false;
            }
        
            if ($this->config->get('config_tax')) {
            $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
            } else {
            $tax = false;
            }
        
            if ($this->config->get('config_review_status')) {
            $rating = $product_info['rating'];
            } else {
            $rating = false;
            }
        
            return array(
            'product_id'    => $product_info['product_id'],
            'thumb'         => $image,
            'name'          => $product_info['name'],
            'description'   => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
            'price'         => $product_info['price'],
            'special'       => $product_info['special'],
            'price_text'    => $price ,
            'special_text'  => $special,
            'tax'           => $tax,
            'rating'        => $rating,
            'href'          => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
            );
        }

        return array();
    
    }
  

    /*
    Required model 
    $this->model('catalog/product');
    $this->model('tool/image');
    $this->model('catalog/category');
    TODO:add more filter - filter_name,filter_tag,filter_description(boolean),filter_manufacturer_id
    */
    public function getProductData($include_category = false)
    {
        $post = $this->getPost();

        if (isset($post['filter_options']) && $post['filter_options']) {
            $filter = $post['filter_options'];
        } else {
            $filter = '';
        }
        
        if (isset($post['sort']) && $post['sort']) {
            $sort = $post['sort'];
        } else {
            $sort = 'p.sort_order';
        }
        
        if (isset($post['order']) && $post['order']) {
            $order = $post['order'];
        } else {
            $order = 'ASC';
        }
        
        if (isset($post['page']) && $post['page']) {
            $page = ($post['page'])?$post['page']:1;
        } else {
            $page = 1;
        }
        
        if (isset($post['limit']) && $post['limit']) {
            $limit = (int)$post['limit'];
        } else {
            $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
        }
        
        if (isset($post['filter_category_id']) && $post['filter_category_id']) {
            $category_id = $post['filter_category_id'];
        } else {
            $category_id = 0;
        }

        if (isset($post['min_price']) && $post['min_price']) {
            $min_price = $post['min_price'];
        } else {
            $min_price = '';
        }

        if (isset($post['max_price']) && $post['max_price']) {
            $max_price = $post['max_price'];
        } else {
            $max_price = '';
        }

        if (isset($post['search']) && $post['search']) {
            $search = $post['search'];
        } else {
            $search = '';
        }
        
        if (isset($post['tag'])) {
			$tag = $post['tag'];
		} elseif (isset($post['search'])) {
			$tag = $post['search'];
		} else {
			$tag = '';
        }
        
        if (isset($post['description'])) {
			$description = $post['description'];
		} else {
			$description = '';
		}

        $products = array();

        $filter_data = array(
            'filter_name'        => $search,
            'filter_tag'         => $tag,
            'filter_description'  => $description,
            'filter_category_id' => $category_id,
            'filter_filter'      => $filter,
            'min_price'          => $min_price,
            'max_price'          => $max_price,
            'sort'               => $sort,
            'order'              => $order,
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit
        );

        $product_total = $this->model_catalog_product->getTotalProducts($filter_data);
        $results = $this->model_catalog_product->getProducts($filter_data);

        foreach ($results as $result) {
            $formated_data = $this->genrateProductData($result);
            if($formated_data){
                $products[] = $formated_data;
            }
        }

        if($include_category){
            $data['category'] = $this->formatCategoryData($this->model_catalog_category->getCategory($category_id));
        }else{
            $data['category'] = array();
        }

        $data['products'] = $products;

        //Paginiation
        $total_pages = ceil($product_total / $limit);
        $next_page = $page+1;

        $data['pagination'] = array(
        'product_total' => $product_total,
        'total_pages'   => $total_pages,
        'current'       => $page,
        'next'          => ($next_page <= $total_pages)?$next_page:0,
        'limit'         => $limit
        );

        return $data;
    }


    public function getSortByData(){

        $sorts = array();
    
        $sorts[] = array(
            'text'  => $this->language->get('text_default'),
            'value' => 'p.sort_order',
            'order' => 'ASC'
        );

//        $sorts[] = array(
//            'text'  => $this->language->get('text_name_asc'),
//            'value' => 'pd.name',
//            'order' => 'ASC'
//        );
//
//        $sorts[] = array(
//            'text'  => $this->language->get('text_name_desc'),
//            'value' => 'pd.name',
//            'order' => 'DESC'
//        );

        $sorts[] = array(
            'text'  => $this->language->get('text_price_asc'),
            'value' => 'p.price',
            'order' => 'ASC'
        );

        $sorts[] = array(
            'text'  => $this->language->get('text_price_desc'),
            'value' => 'p.price',
            'order' => 'DESC'
          );
          
//        if ($this->config->get('config_review_status')) {
//            $sorts[] = array(
//                'text'  => $this->language->get('text_rating_desc'),
//                'value' => 'rating',
//                'order'  => 'DESC'
//            );
//
//            $sorts[] = array(
//                'text'  => $this->language->get('text_rating_asc'),
//                'value' => 'rating',
//                'order'  => 'ASC'
//            );
//        }
//
//        $sorts[] = array(
//            'text'  => $this->language->get('text_model_asc'),
//            'value' => 'p.model',
//            'order'  => 'ASC'
//        );
//
//        $sorts[] = array(
//            'text'  => $this->language->get('text_model_desc'),
//            'value' => 'p.model',
//            'order'  => 'DESC'
//        );
        
        return $sorts;
      }

    public function formatCategoryData($data)
    {
        $formated_data = array();
    
        if($data){
        
            if ($data['image']) {
                $image = $this->model_tool_image->resize($data['image'],335, 72);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 335, 72);
            }
        
            $formated_data = array(
                'category_id'         => $data['category_id'],
                'parent_id'           => $data['parent_id'],
                'name'                => $data['name'],
                'description'         => $data['description'],
                'image'               => $image,
                'meta_description'    => $data['meta_description'],
                'meta_keyword'        => $data['meta_keyword'],
                'meta_title'          => $data['meta_title']
            );
        
        }
    
        return $formated_data;
    }

    //TODO:Update in image extension
    public function uploadBase64Image($image_data,$prefix="")
    {
        if($image_data){
            
            try{

                $destinationPath = DIR_IMAGE."catalog/product/";
                $image_data_status = stripos($image_data,";base64,");

                if($image_data_status){
                    $image_data = str_replace('data:image/png;base64,', '', $image_data);
                    $image_data = str_replace('data:image/jpeg;base64,', '', $image_data);
                    $image_data = str_replace('data:image/gif;base64,', '', $image_data);
                    $image_data = str_replace(' ', '+', $image_data);
                    $image_data = base64_decode($image_data);
                    $image_name = $prefix."_".uniqid().time().".png";
                    $file = $destinationPath . $image_name;
                    $file_saved_response = file_put_contents($file, $image_data);
    
                    if(!$file_saved_response){
                        $image_name = "Unable to upload image!";
                    }
    
                    return array(
                        'status' => $file_saved_response,
                        'message' => "catalog/product/".$image_name
                    );

                }else{

                    return array(
                        'status' => false,
                        'message' => 'Invalid image data!'
                    );

                }
                

            }catch(\Throwable $th){

                return array(
                    'status' => false,
                    'message' => $th->getMessage()
                );
            }

        }else{

            return array(
                'status' => false,
                'message' => "Image data requuired !"
            );
            
        }

    }

    public function imageLink($image)
    { 
        $image_link = '';
        if($image){
            if ($this->request->server['HTTPS']) {
                $image_link =  $this->config->get('config_ssl') . 'image/' . $image;
            } else {
                $image_link = $this->config->get('config_url') . 'image/' . $image;
            }
        }

        return $image_link;
    }
    
	public function execute_fcm($fcm_tokens, $notificaton, $notification_custom_data = array()) {
        
		$fcm_authorisation_token = $this->config->get('config_fcm_token');
		 
		$headers = array(
				'Authorization: key=' . $fcm_authorisation_token,
				'Content-Type: application/json'
		);

		$notificaton_data = array(
				"title"     => $notificaton["title"],
				"body"      => $notificaton["body"],
				"image"     => "https://medinatech.co/ecommerce/image/catalog/push_notification/push_icon.png"
		);

		$post_data = array(
				'registration_ids' => $fcm_tokens,
				'notification'  => $notificaton_data,
				'data' => $notification_custom_data
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, "https://fcm.googleapis.com/fcm/send" );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $post_data ));
		$json_result = curl_exec($ch );
		curl_close( $ch );

		return $json_result;

    }
    
    public function needLogin()
    {
        if (!$this->customer->isLogged()) {
            $this->statusCode = 400;
            $this->json['error'][] = 'Please login to perform this action !';
            $this->json['need_action'] = 'login';
            $this->sendResponse();
        }
    }
}