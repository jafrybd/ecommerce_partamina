<?php

Class Mailboxvalidator {
	private $log;
	
	
	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
	}
	
	private function mailboxvalidator_single ($email, $api_key) {
		try{
			$Response = $this->Http('http://api.mailboxvalidator.com/v1/validation/single?' . http_build_query(array(
				'key' => $api_key,
				'email' => $email,
				'format' => 'json',
				'source' => 'opencart',
			)));
			if (is_null( $json = json_decode($Response, true)) === FALSE) {
				if ($this->config->get('module_mailboxvalidator_debug')) {
					$this->_getLog()->write(print_r($Response, 1));
				}
				return $json;
			} else {
				// if connection error, let it pass
				return true;
			}
		} catch( Exception $e ) {
			return true;
		}
	}
	
	private function mailboxvalidator_is_valid($api_result) {
		if ( $api_result != '' ) {
			// To update the credits value in settings.
			// $this->db->query("UPDATE oc_setting SET `value` = '" . $this->db->escape($api_result['credits_available']) . "' WHERE `key` = 'module_mailboxvalidator_remaining_credits'");
			if ( $api_result['error_message'] == '' ) {
				if ( $api_result['status'] == 'False' ) {
					return false;
				} else {
					return true;
				}
			} else {
				// If error message occured, let it pass first.
				return true;
			}
		} else {
			// If error message occured, let it pass first.
			return true;
		}
	}
	
	private function mailboxvalidator_is_role($api_result) {
		if ( $api_result != '' ) {
			if ( $api_result['error_message'] == '' ) {
				if ( $api_result['is_role'] == 'False' ) {
					return false;
				} else {
					return true;
				}
			} else {
				// If error message occured, let it pass first.
				return false;
			}
		} else {
			// If error message occured, let it pass first.
			return false;
		}
	}
	
	private function mailboxvalidator_disposable ($email, $api_key) {
		try {
			$Response = $this->Http('http://api.mailboxvalidator.com/v1/email/disposable?' . http_build_query(array(
				'key' => $api_key,
				'email' => $email,
				'format' => 'json',
				'source' => 'opencart',
			)));
			if (is_null( $json = json_decode($Response, true)) === FALSE) {
				if ($this->config->get('module_mailboxvalidator_debug')) {
					$this->_getLog()->write(print_r($Response, 1));
				}
				// To update the credits value in settings.
				// $this->db->query("UPDATE oc_setting SET `value` = '" . $this->db->escape($json['credits_available']) . "' WHERE `key` = 'module_mailboxvalidator_remaining_credits'");
				if ( $json['error_message'] == '' ) {
					if ( $json['is_disposable'] == 'False' ) {
						return false;
					} else {
						return true;
					}
				}else {
					// if connection error, let it pass
					return true;
				}
			} else {
				// if connection error, let it pass
				return true;
			}
		} catch( Exception $e ) {
			return true;
		}
	}
	
	private function mailboxvalidator_free ($email, $api_key) {
		try {
			$Response = $this->Http('http://api.mailboxvalidator.com/v1/email/free?' . http_build_query(array(
				'key' => $api_key,
				'email' => $email,
				'format' => 'json',
				'source' => 'opencart',
			)));
			if (is_null( $json = json_decode($Response, true)) === FALSE) {
				if ($this->config->get('module_mailboxvalidator_debug')) {
					$this->_getLog()->write(print_r($Response, 1));
				}
				// To update the credits value in settings.
				// $this->db->query("UPDATE oc_setting SET `value` = '" . $this->db->escape($json['credits_available']) . "' WHERE key = 'module_mailboxvalidator_remaining_credits'");
				if ( $json['error_message'] == '' ) {
					if ( $json['is_free'] == 'False' ) {
						return false;
					} else {
						return true;
					}
				}else {
					// if connection error, let it pass
					return true;
				}
			} else {
				// if connection error, let it pass
				return true;
			}
		} catch( Exception $e ) {
			return true;
		}
	}
	
	public function validate($email) {
		$api_key = $this->config->get('module_mailboxvalidator_api_key');
		$error_message = '';
		
		// Optional: Different languages for error message
		// $this->load->language('extension/module/mailboxvalidator/error');
		// $language_id = isset($data['language_id']) ? $data['language_id'] : $this->config->get('config_language_id');

		// $language_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE `language_id` = '" . (int)$language_id . "' LIMIT 1");

		// if (version_compare(VERSION, '2.2', '>') == true) {
			// $language_code = $language_query->row['code'];
		// } else {
			// $language_code = $language_query->row['directory'];
		// }
		//Currently only support English
		$language_code = 'en-gb';

		$language = new \Language($language_code);
		$language->load($language_code);
		$language->load('extension/module/mailboxvalidator/error');
		
		// Check the domain blacklisted list, if the domain is in the list, block the attempt.
		if ($this->config->get('module_mailboxvalidator_custom_email_domain_validator')) {
			$blacklisted_domains_array = explode( ';', $this->config->get('module_mailboxvalidator_custom_email_domain_validator') );
			$email_parts = explode( '@', $email );
			
			if ( ( isset( $email_parts[1] ) ) && ( in_array( $email_parts[1], $blacklisted_domains_array ) ) ) {
				$error_message = (($this->config->get('module_mailboxvalidator_block_email_domain_error_message') !== '') && ($this->config->get('module_mailboxvalidator_block_email_domain_error_message') !== '')) ? $this->config->get('module_mailboxvalidator_block_email_domain_error_message') : $language->get('error_block_email_domain');
				return $error_message;
			}
		}
		
		$single_result = (($this->config->get('module_mailboxvalidator_valid_email_validator')) || ($this->config->get('module_mailboxvalidator_role_email_validator'))) ? $this->mailboxvalidator_single($email, $api_key) : '';
		// $single_result = ($this->config->get('module_mailboxvalidator_valid_email_validator')) ? $this->mailboxvalidator_single($email, $api_key) : '';
		$is_valid_email = ($this->config->get('module_mailboxvalidator_valid_email_validator')) ? $this->mailboxvalidator_is_valid($single_result) : true;
		$is_role_email = ($this->config->get('module_mailboxvalidator_role_email_validator')) ? $this->mailboxvalidator_is_role($single_result) : true;
		$is_disposable = ($this->config->get('module_mailboxvalidator_disposable_email_validator')) ? $this->mailboxvalidator_disposable($email, $api_key) : '';
		$is_free = ($this->config->get('module_mailboxvalidator_free_email_validator')) ? $this->mailboxvalidator_free($email, $api_key) : '';
		
		
		if ($is_valid_email == false) {
			$error_message = (($this->config->get('module_mailboxvalidator_invalid_email_error_message')) && ($this->config->get('module_mailboxvalidator_invalid_email_error_message') !== '')) ? $this->config->get('module_mailboxvalidator_invalid_email_error_message') : $language->get('error_invalid_email');
			// return $error_message;
		} else if ($is_disposable == true) {
			$error_message = (($this->config->get('module_mailboxvalidator_disposable_email_error_message') !== '') && ($this->config->get('module_mailboxvalidator_disposable_email_error_message') !== '')) ? $this->config->get('module_mailboxvalidator_disposable_email_error_message') : $language->get('error_disposable_email');
			// return $error_message;
		} else if ($is_free == true) {
			$error_message = (($this->config->get('module_mailboxvalidator_free_email_error_message') !== '') && ($this->config->get('module_mailboxvalidator_free_email_error_message') !== '')) ? $this->config->get('module_mailboxvalidator_free_email_error_message') : $language->get('error_free_email');
			// return $error_message;
		} else if ($is_role_email == true) {
			$error_message = (($this->config->get('module_mailboxvalidator_role_based_email_error_message') !== '') && ($this->config->get('module_mailboxvalidator_role_based_email_error_message') !== '')) ? $this->config->get('module_mailboxvalidator_role_based_email_error_message') : $language->get('error_role_email');
			// return $error_message;
		}
		
		if ($error_message != '') {
			return $error_message;
		}
		
		return true;
	}
	
	private function Http($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		$Response = curl_exec($ch);
		curl_close($ch);

		return $Response;
	}
	
	private function _getLog(){
		if (!$this->log) {
			$this->log = new \Log('mbv-debug.log');
		}
		return $this->log;
	}
}