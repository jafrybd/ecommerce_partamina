$(document).ready(function() {
    var currentRequest = null;

    /**
     * Get param from url
     * @param string name
     */
    var getUrlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return (results==null) ? null : (decodeURIComponent(results[1]) || 0);
    }

    $('body').on('change', 'input[name=email]', function(){
        var emailField = $(this);

        if (emailField.val() == '') {
            if (emailField.parent('.input-group').length) {
                emailField.parent('.input-group').next('.text-danger').remove();
            } else if (emailField.next('.text-danger').length) {
                emailField.next('.text-danger').remove();
            }

            return true;
        }

        var action_route = getUrlParam('route');

        if (action_route == 'checkout/checkout') {
            // Checkout guest status if no password
            if (emailField.parents('.collapse').find('[name=password]').length) {
                action_route = 'checkout/register';
            } else {
                action_route = 'checkout/guest';
            }

            // Skip login
            if (emailField.parents('#collapse-checkout-option').length) {
                return true;
            }
        }

        currentRequest = $.ajax({
            type: 'POST',
            url: 'index.php?route=extension/module/email_validation/check&email=' + getUrlParam('email'),
            dataType: 'json',
            data: {
                'email': emailField.val(),
                'action_route': action_route
            },
            beforeSend : function() {
                if (currentRequest != null) {
                    currentRequest.abort();
                }

                if (emailField.parent('.input-group').length) {
                    emailField.parent('.input-group').next('.text-danger').remove();
                } else if (emailField.next('.text-danger').length) {
                    emailField.next('.text-danger').remove();
                }
            },
            success: function(json) {
                var msg;

                if (emailField.parent('.input-group').length) {
                    if (!emailField.parent('.input-group').next('.text-danger').length) {
                        emailField.parent('.input-group').after('<div class="text-danger"></div>')
                    }

                    msg = emailField.parent('.input-group').next('.text-danger')
                } else {
                    if (!emailField.next('.text-danger').length) {
                        emailField.after('<div class="text-danger"></div>')
                    }

                    msg = emailField.next('.text-danger')
                }

                if (json && json['error']) {
                    msg.html(json['error']);
                } else if (msg.length) {
                    msg.remove();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + " " + xhr.statusText + " " + xhr.responseText);
            }
        });
    });
});