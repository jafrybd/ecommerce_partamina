<?php 

$_['error_customer'] = 'Please login to procced!';
$_['no_payment_method']           = 'No payment method found!';
$_['error_customer']              = "Customer not found!" ;
$_['error_payment_address']       = "Payment address not found!";
$_['error_payment_method'] = 'Payment method is not valid!';
$_['error_shipping_method'] = 'Shipping method is not valid!';
$_['error_no_payment'] = 'Payment methods not set!';
$_['error_shipping_address'] = 'Shipping address not set!';
$_['error_no_shipping'] = 'Shipping methods not set!';
$_['error_stock'] = 'Cart is empty!';
$_['error_minimum'] = '%s should have mininum %s quantity';
$_['text_order_success'] = 'Order has been placed successfully!';