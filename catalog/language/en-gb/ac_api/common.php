<?php 
$_['method_not_allowed']  = 'Method not allowed, you request has been rejected!';
$_['text_default']        = 'Default';
$_['text_name_asc']       = 'Name (A - Z)';
$_['text_name_desc']      = 'Name (Z - A)';
$_['text_price_asc']      = 'Price (Low > High)';
$_['text_price_desc']     = 'Price (High > Low)';
$_['text_rating_asc']     = 'Rating (Lowest)';
$_['text_rating_desc']    = 'Rating (Highest)';
$_['text_model_asc']      = 'Model (A - Z)';
$_['text_model_desc']     = 'Model (Z - A)';

$_['profile_update_success']  = 'Profile Updated successfully!';
$_['error_telephone']  = 'Error phone number!';



//Wislist language
$_['wish_list_success']           = 'You have added %s to your wish list!';
$_['wish_list_delete_success']    = '%s has been removed form the wish list!';
$_['product_not_found']           = 'Product not found !';
$_['product_not_in_wishlist']     = 'Product not in wishlist.';
$_['auth_user']                   = 'You are not logged In!';

//Change password
$_['error_old_password']    = 'Password does not match with current password!';
$_['error_password']        = 'Password must be between 4 and 20 characters!';
$_['error_confirm']         = 'Password confirmation does not match password!';
$_['password_text_success'] = 'Success: Your password has been successfully updated.';

//Address
$_['text_address_not_found']  = 'Address not found!';
$_['text_address_update_success']  = 'Address updated successfully!';
$_['error_default_address']  = 'You cant\'t delete default address!';
$_['address_text_delete'] = 'Address deleted successfully!';
$_['default_address_not_valid'] = 'Default address is not valid, please update your address!';
$_['text_address_add_success'] = 'Address has been updated successfully!';

//order
$_['order_not_valid'] = 'Order is not valid!';
$_['invalid_filter'] = 'Invalid filter!';
$_['no_order_found'] = 'No order found!';

//Checkout 
$_['text_order_success'] = 'Order has been placed successfully!';

//Quote Success
$_['get_quote_success'] = 'Rquested has been submitted successfully, we will contact you soon!';
$_['get_quote_error']   = 'Currently we are unable to accept you request, please try again later!';

//Information page
$_['information_not_found'] = 'Page not found';

//Push Notification
$_['push_order_placed_title'] = "Order has been placed successfully!";
$_['push_order_placed_body'] = "Your order #%s has been received.";


