<?php 

$_['register_success'] = 'Congratulations! Your new account has been successfully created!';
$_['register_error'] = 'Unable to register account, please try agan later';


// Error
$_['login_error']          = 'Warning: No match for E-Mail Address and/or Password.';
$_['login_success']        = 'Login successfully!';

$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 11 and 14 characters!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['profile_update_success']        = 'Profile Updated successfully!';
$_['register_login_error'] = 'Congratulations! Your account has been successfully created!, You accout is disabled, we will verify your details soon!';