<?php 

$_['error_required']        = 'Warning: Product option - %s is required!';
$_['text_add_success']      = 'Success: You have added %s to your shopping cart!';
$_['product_not_found']     = 'Product not found!';
$_['cart_not_found']    = 'Card item not found!';
$_['product_removed'] = 'Product has been removed from cart!';
$_['cart_empty'] = 'Cart is empty!';
$_['text_cart_update'] = 'Cart updated!';