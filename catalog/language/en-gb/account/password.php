<?php
// Heading
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_password'] = 'Password';
$_['entry_confirm']  = 'Password Confirm';
$_['current_password'] = 'Current Password';
// Error
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';

$_['error_correctpass']  = 'Password does not match!';

$_['entry_newPassword']  = 'New Password';
