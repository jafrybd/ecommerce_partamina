<?php
$_['heading_title']     = 'Confirm Your Email Address';

$_['button_back']       = 'Back';
$_['button_save']       = 'Save';

$_['text_alert_content'] = '%s is running low on API Credits from &lt;a href=&quot;https://www.zerobounce.net/?ref=opencart-templates&quot; target=&quot;_blank&quot;&gt;Zero Bounce (https://www.zerobounce.net/)&lt;/a&gt; only &lt;strong&gt;%s&lt;/strong&gt; credits left.&amp;nbsp;&lt;br /&gt;&lt;br /&gt;&lt;a href=&quot;https://www.zerobounce.net/members/buycreditsnow/?ref=opencart-templates&quot; target=&quot;_blank&quot;&gt;Top up your credits&lt;/a&gt; before they run out.';
$_['text_heading']      = 'Change Email';
$_['text_intro']        = 'There appears to be a problem validating your email address. Please enter a valid email address or <a href="%s">contact us</a> if you need assistance.';
$_['text_login']        = 'Login';
$_['text_success']      = 'Success: Your email has been changed. You can now login with your new email.';

$_['entry_email']       = 'Email';