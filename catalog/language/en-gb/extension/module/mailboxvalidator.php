<?php
$_['heading_title']     = 'Confirm Your Email Address';

$_['button_back']       = 'Back';
$_['button_save']       = 'Save';

$_['text_heading']      = 'Change Email';
$_['text_intro']        = 'There appears to be a problem validating your email address. Please enter a valid email address or <a href="%s">contact us</a> if you need assistance.';
$_['text_login']        = 'Login';
$_['text_success']      = 'Success: Your email has been changed. You can now login with your new email.';

$_['entry_email']       = 'Email';