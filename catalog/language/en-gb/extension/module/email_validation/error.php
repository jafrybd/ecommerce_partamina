<?php

// Error
$_['error_api_key']          = 'Warning: You must enter a API Key.';
$_['error_banned']           = 'Warning: Your email address domain is not allowed';
$_['error_catch_all']        = 'Warning: Your email address appears to be a catch-all which is not allowed';
$_['error_default']          = 'There was a problem validating your email address';
$_['error_domain_age']       = 'Warning: Your email address does not appear to be from a long-established domain';
$_['error_do_not_mail']      = 'Warning: Your email address is not valid';
$_['error_email']            = 'Warning: Your email address does not appear to be valid';
$_['error_exists']           = 'Warning: Your email address is already registered! <a href="%s">Login</a> OR <a href="%s">forgotten password</a>';
$_['error_free_mail']        = 'Warning: Your email address appears to be from a free provider which is not allowed';
$_['error_invalid']          = 'Warning: Your email address does not appear to be valid';
$_['error_permission']       = 'Warning: You do not have permission to modify Email Validation module!';
$_['error_required']         = 'Warning: Your email address is required';
$_['error_typo']             = 'Warning: Your email address is not valid, did you mean <a href="#" onclick="getElementById(\'input-email\').value=this.innerHTML;return false;">%s</a>?';
