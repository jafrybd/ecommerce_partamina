<?php
// Heading
$_['heading_title']        = 'Customer Support';

// Text
$_['text_list']       			     = 'list';
$_['text_faq']       			     = 'FAQ';
$_['text_subject']      		     = 'New Request';
$_['text_ticket']       		     = 'Ticket list';
$_['text_account']       		     = 'Account';
$_['tab_general']        		     = 'General';
$_['tab_upload']         			 = 'Attachments';
$_['text_comment']      			 = 'Comments';
$_['text_support']      			 = 'Support';
$_['text_view']       				 = 'View';
$_['button_save']      				 = 'Submit';
$_['button_save']      				 = 'Submit';
$_['text_subject_mail_Request']      = 'Request';
$_['text_comment_subject']       	  = 'You have received a new comment';
$_['support_id']       	  = 'Ticket Number';



$_['text_ticket']        = 'Ticket List';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Submit your Request';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] 			= 'Customer Group';
$_['entry_subject']      			= 'Subject';
$_['entry_choose_department']       = 'Choose Department';
$_['entry_message']         		 = 'Message';
$_['entry_file']     				 = 'Attachments';
$_['entry_department']            	  = 'Department';
$_['entry_customer']      			  = 'Customer';
$_['entry_date_added']     			 = 'Date';
$_['entry_status']     				 = 'Status';
$_['entry_email']      				 = ' Email';
$_['entry_city']         			  = 'City';
$_['entry_country']       			 = 'Country';
$_['entry_zone']          			 = 'Region / State';
$_['entry_newsletter']    			 = 'Subscribe';
$_['entry_password']     			  = 'Password';
$_['entry_confirm']      			  = 'Password Confirm';
$_['entry_comment']       			 = 'Comment';
//columns

$_['column_department']        = 'Department';
$_['column_subject1']        = 'Subject';
$_['column_message']        = 'Message';
$_['column_date_added']        = 'Date added';
$_['column_action']        = 'Action';
$_['column_status']        = 'Status';
// Error
$_['error_subject']      = 'Subject must be between 1 and 32 characters!';
$_['error_choose_department_id']       = 'Choose department';
$_['error_message']          = 'Message must be between 10 and 2000 characters!';
$_['error_file']      = 'Telephone must be between 3 and 32 characters!';