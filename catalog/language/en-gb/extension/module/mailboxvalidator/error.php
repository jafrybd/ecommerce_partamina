<?php

// Error
$_['error_invalid_email'] = 'Invalid email detected. Please enter a valid email address.';
$_['error_disposable_email'] = 'Disposable email detected. Please enter a non-disposable email address.';
$_['error_free_email'] = 'Free email detected. Please enter a non-free email address.';
$_['error_role_email'] = 'Role based email detected. Please enter a non role-based email address.';