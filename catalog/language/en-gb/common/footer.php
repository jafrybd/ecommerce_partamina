<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Developed By <a href="http://medinatech.co">Medina Tech</a> %s &copy; %s';

//address
$_['text_siteAddress']   = 'Sonargaon Link Road, 4 Karwanbazar, AJ Tower';
$_['text_email']   = 'cmo@pertaminabd.com';

$_['text_authorizedBy']   = 'Petro Product Company (Authorized Distributor of Pertamina Lubricants)';


