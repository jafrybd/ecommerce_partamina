<?php
// Text
$_['text_subject']  = '%s - Change password request';
$_['text_greeting'] = 'Your password has successfully changed.';
$_['text_ip']       = 'The IP used to make this request was:';