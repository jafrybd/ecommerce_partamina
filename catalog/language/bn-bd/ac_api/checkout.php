<?php

$_['error_customer'] = 'এগিয়ে যেতে লগইন করুন!';
$_['no_payment_method']           = 'কোন পেমেন্ট পদ্ধতি পাওয়া যায় নি!';
$_['error_customer']              = "গ্রাহক পাওয়া গেল না!" ;
$_['error_payment_address']       = "অর্থ প্রদানের ঠিকানা পাওয়া যায় নি!";
$_['error_payment_method'] = 'পেমেন্ট পদ্ধতি বৈধ নয়!';
$_['error_shipping_method'] = 'শিপিং পদ্ধতি বৈধ নয়!';
$_['error_no_payment'] = 'অর্থ প্রদানের পদ্ধতি সেট করা হয়নি!';
$_['error_shipping_address'] = 'শিপিং ঠিকানা সেট করা হয়নি!';
$_['error_no_shipping'] = 'শিপিংয়ের পদ্ধতি সেট করা হয়নি!';
$_['error_stock'] = 'Cart খালি !';
$_['error_minimum'] = '%s সর্বনিম্ন %s পরিমাণ থাকা উচিত';
$_['text_order_success'] = 'অর্ডার সফলভাবে স্থাপন করা হয়েছে!';