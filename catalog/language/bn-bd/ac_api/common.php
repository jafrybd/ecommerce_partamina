<?php 
$_['method_not_allowed']  = 'পদ্ধতি অনুমোদিত নয়, আপনি অনুরোধ বাতিল হয়েছে!';
$_['text_default']        = 'ডিফল্ট';
$_['text_name_asc']       = 'নাম (এ - জেড)';
$_['text_name_desc']      = 'নাম (জেড - এ)';
$_['text_price_asc']      = 'দাম (কম> উচ্চ)';
$_['text_price_desc']     = 'মূল্য (উচ্চ> নিম্ন)';
$_['text_rating_asc']     = 'রেটিং (সর্বনিম্ন)';
$_['text_rating_desc']    = 'রেটিং (সর্বোচ্চ)';
$_['text_model_asc']      = 'মডেল (এ - জেড)';
$_['text_model_desc']     = 'মডেল (জেড - এ)';
$_['profile_update_success']  = 'প্রোফাইল সফলভাবে আপডেট করা হয়েছে!';
$_['error_telephone']  = 'ত্রুটি ফোন নম্বর!';


//Wislist language
$_['wish_list_success']           = 'আপনি আপনার ইচ্ছার তালিকায় %s যোগ করেছেন!';
$_['wish_list_delete_success']    = '%s ইচ্ছার তালিকা থেকে সরানো হয়েছে!';
$_['product_not_found']           = 'পণ্য পাওয়া যায় নি!';
$_['product_not_in_wishlist']     = 'পণ্য ইচ্ছার তালিকায় নেই।';
$_['auth_user']                   = 'আপনি লগইন নেই!';

//Change password
$_['error_old_password']    = 'পাসওয়ার্ড বর্তমান পাসওয়ার্ডের সাথে মেলে না!';
$_['error_password']        = 'পাসওয়ার্ড ৪ এবং ২০ টি অক্ষরের মধ্যে হওয়া আবশ্যক!';
$_['error_confirm']         = 'পাসওয়ার্ড নিশ্চিতকরণ পাসওয়ার্ড মেলে না!';
$_['password_text_success'] = 'সাফল্য: আপনার পাসওয়ার্ড সফলভাবে আপডেট করা হয়েছে.';

//Address
$_['text_address_not_found']  = 'ঠিকানা পাওয়া যায় নি!';
$_['text_address_update_success']  = 'ঠিকানা সফলভাবে আপডেট হয়েছে!';
$_['error_default_address']  = 'আপনি ডিফল্ট ঠিকানা মুছতে পারবেন না!';
$_['address_text_delete'] = 'ঠিকানা সফলভাবে মোছা হয়েছে!';
$_['default_address_not_valid'] = 'ডিফল্ট ঠিকানা বৈধ নয়, দয়া করে আপনার ঠিকানা আপডেট করুন!';
$_['text_address_add_success'] = 'ঠিকানা সফলভাবে আপডেট করা হয়েছে!';

//order
$_['order_not_valid'] = 'অর্ডার বৈধ নয়!';
$_['invalid_filter'] = 'অবৈধ ফিল্টার!';
$_['no_order_found'] = 'কোন অর্ডার পাওয়া যায় নি!';

//Checkout 
$_['text_order_success'] = 'অর্ডার সফলভাবে স্থাপন করা হয়েছে!';

//Quote Success
$_['get_quote_success'] = 'অনুরোধ সফলভাবে জমা দেওয়া হয়েছে, আমরা শীঘ্রই আপনার সাথে যোগাযোগ করব!';
$_['get_quote_error']   = 'বর্তমানে আমরা আপনার অনুরোধ গ্রহণ করতে অক্ষম, দয়া করে পরে আবার চেষ্টা করুন!';

//Information page
$_['information_not_found'] = 'পৃষ্ঠা খুঁজে পাওয়া যায়নি';

//Push Notification
$_['push_order_placed_title'] = "অর্ডার সফলভাবে স্থাপন করা হয়েছে!";
$_['push_order_placed_body'] = "আপনার অর্ডার # %s পেয়েছে।";


