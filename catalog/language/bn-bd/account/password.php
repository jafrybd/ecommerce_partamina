<?php

// Heading 

$_['heading_title']  = 'পাসওয়ার্ড পরিবর্তন করুন';



// Text

$_['text_account']   = 'অ্যাকাউন্ট';

$_['text_password']  = 'আপনার পাসওয়ার্ড';

$_['text_success']   = 'অভিনন্দন ,আপনার পাসওয়ার্ড সফলভাবে আপডেট করা হয়েছে।';



// Entry

$_['entry_password'] = 'পাসওয়ার্ড ';

$_['entry_confirm']  = 'পাসওয়ার্ড নিশ্চিত করুন ';

$_['entry_newPassword']  = 'নতুন পাসওয়ার্ড';

// Error

$_['error_password'] = 'পাসওয়ার্ড ৪ এবং ২০ অক্ষরের মধ্যে হতে হবে!';
$_['error_correctpass']  = 'পাসওয়ার্ড মেলে না!';
$_['error_confirm']  = 'নিশ্চিতকরণ পাসওয়ার্ড এক নয়!';

?>