<?php
// Text
$_['text_home']           = 'মুল পাতা';
$_['text_wishlist']       = 'পছন্দের তালিকা (%s)';
$_['text_shopping_cart']  = 'শপিং কার্ট';
$_['text_search']         = 'অনুসন্ধান';
$_['text_welcome']        = 'স্বাগতম <a href="%s">লগইন</a> অথবা <a href="%s">রেজিষ্ট্রেশন</a>।';
$_['text_logged']         = 'হ্যালো!<a href="%s"> %s </a> <b>(</b> <a href="%s">লগ আউট</a> <b>)</b>';
$_['text_account']        = 'আমার অ্যাকাউন্ট';
$_['text_order']         = 'অর্ডার ইতিহাস';
$_['text_transaction']   = 'লেনদেন';
$_['text_download']      = 'ডাউনলোড';
$_['text_logout']        = 'প্রস্থান';
$_['text_checkout']      = 'চেকআউট';
$_['text_search']        = 'অনুসন্ধান করুন';
$_['text_all']           = 'সব দেখাও';

$_['text_register']      = 'নিবন্ধন';
$_['text_login']         = 'প্রবেশ করুন';
$_['text_language']         = 'ভাষা';
$_['text_authorized']         = 'পার্টামিনা লুব্রিক্যান্টগুলির অনুমোদিত বিতরণকারী';
?>