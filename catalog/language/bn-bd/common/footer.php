<?php
// Text
$_['text_information']  = 'তথ্য';
$_['text_service']      = 'গ্রাহক সেবা';
$_['text_extra']        = 'অতিরিক্ত';
$_['text_contact']      = 'যোগাযোগ';
$_['text_return']       = 'ফেরত';
$_['text_sitemap']      = 'সাইটম্যাপ';
$_['text_manufacturer'] = 'ব্র্যান্ড';
$_['text_voucher']      = 'গিফট ভাউচার';
$_['text_affiliate']    = 'অ্যাফিলিয়েট';
$_['text_special']      = 'স্পেশাল';
$_['text_account']      = 'আমার অ্যাকাউন্ট';
$_['text_order']        = 'আমার অর্ডার সমূহ';
$_['text_wishlist']     = 'পছন্দের তালিকা ';
$_['text_newsletter']   = 'সদস্যপদ / নিউজলেটার সদস্যপদ ত্যাগ';
$_['text_powered']      = '';

//address
$_['text_siteAddress']   = 'সোনারগাঁ লিংক রোড, ৪ কারওয়ানবাজার, এজে টাওয়ার';
$_['text_email']   = 'cmo@pertaminabd.com';

$_['text_authorizedBy']   = 'পেট্রো প্রোডাক্ট সংস্থা (পার্টামিনা লুব্রিক্যান্টের অনুমোদিত বিতরণকারী)';
?>