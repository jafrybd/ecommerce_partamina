<?php
// Heading
$_['heading_title']  = 'যোগাযোগ';

// Text 
$_['text_storeLocation']  = 'বসুন্ধরা , ঢাকা ';
$_['text_storeAddress']     = 'পেট্রো পণ্য লিমিটেড';
$_['text_location']  = 'আমাদের অবস্থান';
$_['text_contact']   = 'যোগাযোগ';
$_['text_address']   = 'ঠিকানা ';
$_['text_email']     = 'ইমেইল ';
$_['text_telephone'] = 'টেলিফোন ';
$_['text_fax']       = 'ফ্যাক্স ';
$_['text_message']   = '<p> আপনার জিজ্ঞাসা সফলভাবে  পাঠানো হয়েছে!</p>';

// Entry Fields
$_['entry_name']     = 'প্রথম নাম';
$_['entry_email']    = 'ইমেইল ';
$_['entry_enquiry']  = 'অনুসন্ধান';
$_['entry_captcha']  = 'নিচের বাক্সে কোডটি লিখুন';
$_['button_submit']     = 'জমা দিন';
// Email
$_['email_subject']  = 'অনুসন্ধান %s';

// Errors
$_['error_name']     = 'নাম ৩ এবং ৩২ অক্ষরের মধ্যে হতে হবে!';
$_['error_email']    = 'ইমেইল ঠিকানাটি বৈধ বলে মনে হচ্ছেনা!';
$_['error_enquiry']  = 'অনুসন্ধান ১০ এবং ৩০০০ অক্ষরের মধ্যে হতে হবে!';
$_['error_captcha']  = 'যাচাইকরণ কোড চিত্র মেলে না!';

//subject question
$_['text_subject']  = 'বিষয় নির্বাচন করুন';
$_['text_orderStatus']  = 'অর্ডার স্থিতি এবং বিতরণ সময়সীমা';
$_['text_trackOrder']  = 'আমি কিভাবে আমার অর্ডার ট্র্যাক করতে পারি?    ';
$_['text_orderRecive']  = 'অর্ডার পুনরুদ্ধার করতে কতক্ষণ সময় লাগে?';
$_['text_OrderNotFound']  = 'আমার অর্ডার বিলম্বিত। আমার কি করা উচিৎ?';
$_['text_orderLate']  = 'আমার অর্ডার আসতে দেরি হচ্ছে কেন?';
$_['text_orderDelivery']  = 'আমার অর্ডার ডেলিভারি হতে কত দিন লাগবে?';
$_['text_orderReturn']  = 'প্রত্যাবর্তন এবং প্রত্যর্পণ';
$_['text_orderReturning']  = 'আমি কীভাবে আমার পণ্য ফেরত দিতে পারি?';
$_['text_orderNotReturned']  = 'আমাকে এখনও ফেরত দেওয়া হয়নি';
$_['text_orderRefund']  = 'ফেরতের নীতি কী?';

$_['text_orderGetRefund']  = 'আমি কখন আমার ফেরত পাব?';
$_['text_orderRefundProcess']  = 'অর্থ ফেরতের প্রক্রিয়া কী?';
$_['text_orderPlacement']  = 'অর্ডার প্লেসমেন্ট';
$_['text_orderHowToPlace']  = 'অর্ডার কিভাবে রাখবেন?';
$_['text_orderHowToPay']  = 'আমি কিভাবে মূল্য পরিষোধ করব ?';
$_['text_orderCancel']  = 'কিভাবে আমার অর্ডার বাতিল করব?';
$_['text_orderplace']  = 'অর্ডার প্লেস করার উপায় কি?';
$_['text_ordercancel']  = 'অর্ডার ক্যানসেল করার উপায় কি?';
?>
