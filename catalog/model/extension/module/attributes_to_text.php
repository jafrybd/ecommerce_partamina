<?php

class ModelExtensionModuleAttributesToText extends Model {

	public function getText($product_id, $options) {

		$text = "";
		$this->load->model('catalog/product');

		$attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);

		$attr_arr = array();
		foreach ($attribute_groups as $group) {
			foreach ($group['attribute'] as $attribute) {
				if (isset($options[$attribute['attribute_id']])) {
					if ($options[$attribute['attribute_id']]['show'] == 1) {
						$attr_arr[] = $attribute['text'];
					} else if ($options[$attribute['attribute_id']]['show'] == 2 && in_array($attribute['text'], explode(',', $options[$attribute['attribute_id']]['replace']))) {
						$attr_arr[] = $attribute['name'];
					}
				}
			}
		}

		if ($attr_arr) {
			$separator = isset($options['separator']) ? $options['separator'] : "/";
			$text = implode($attr_arr, $separator);
		}
		if (isset($options['cut'])) {
			$dots = strlen($text) > $options['cut'] ? '..' : '';
			$text = utf8_substr($text, 0, $options['cut']) . $dots;
		}
		return $text;
	}
}