<?php
class ModelExtensionModuleEmailValidation extends Model {

	public function addEmailValidation($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "email_validation SET `email` = '" . $this->db->escape($data['email']) . "', `status` = '" . $this->db->escape($data['status']) . "', `sub_status` = '" . $this->db->escape($data['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($data['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($data['domain_age_days']) . "', `customer_id` = " . (int)$data['customer_id'] . ", `date_added` = NOW()");

		return $this->db->getLastId();
	}

	public function editEmailValidation($email_validation_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "email_validation SET `status` = '" . $this->db->escape($data['status']) . "', `sub_status` = '" . $this->db->escape($data['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($data['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($data['domain_age_days']) . "', `customer_id` = " . (int)$data['customer_id'] . ", `date_modified` = NOW() WHERE `email_validation_id` = " . (int)$email_validation_id);

		$affected = $this->db->countAffected();

		return $affected > 0 ? $affected : false;
	}

	public function getEmailValidationToken($email_validation_id) {
		$token = token(32);

		$this->db->query("UPDATE `" . DB_PREFIX . "email_validation` SET `token` = '" . $this->db->escape($token) . "', `token_date` = NOW() WHERE email_validation_id = " . (int)$email_validation_id);

		return $token;
	}

	public function clearEmailValidation($email_validation_id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "email_validation` SET `token` = NULL, `token_date` = NULL WHERE email_validation_id = " . (int)$email_validation_id);
	}

	public function getEmailValidation($email_validation_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email_validation_id` = '" . (int)$email_validation_id . "' LIMIT 1");

		return $query->row;
	}

	public function getEmailValidationByCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `customer_id` = '" . (int)$customer_id . "' LIMIT 1");

		return $query->row;
	}

	public function getEmailValidationByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email` = '" . $this->db->escape($email) . "' LIMIT 1");

		return $query->row;
	}

	public function getEmailValidationByToken($email, $token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email` = '" . $this->db->escape($email) . "' AND `token` = '" . $this->db->escape($token) . "' AND `token_date` > SUBDATE(NOW(), INTERVAL 1 DAY) LIMIT 1");

		return $query->row;
	}

	public function deleteEmailValidation($email_validation_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "email_validation WHERE `email_validation_id` = " . (int)$email_validation_id);
	}

	public function editCustomerEmail($customer_id, $email) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET email = '" . $this->db->escape($email) . "' WHERE customer_id = " . (int)$customer_id);
	}
}