<?php
class ModelAcApiCommon extends Model {

  public function getModule($module_id){
    
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE module_id = '".(int)$module_id."' ");

    return $query->row;
  }

	public function getBanner($banner_id) {
    //code
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . DB_PREFIX . "banner_image bi ON (b.banner_id = bi.banner_id) WHERE b.banner_id = '" . (int)$banner_id . "' AND b.status = '1' AND bi.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC");
    
    return $query->rows;
  }

  public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
  }
  
  public function countProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT count(*) as total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "'");

		return $query->row['total'];
  }

  public function getProductById($product_id){
    
    $product = array();

    $product_info = $this->model_catalog_product->getProduct($product_id);
    if ($product_info) {

      if ($product_info['image']) {
        $image = $this->model_tool_image->resize($product_info['image'],1200, 1200);
      } else {
        $image = $this->model_tool_image->resize('placeholder.png', 1200, 1200);
      }

      if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
      } else {
        $price = false;
      }

      if ((float)$product_info['special']) {
        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
      } else {
        $special = false;
      }

      if ($this->config->get('config_tax')) {
        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
      } else {
        $tax = false;
      }

      if ($this->config->get('config_review_status')) {
        $rating = $product_info['rating'];
      } else {
        $rating = false;
      }

      $product = array(
        'product_id'    => $product_info['product_id'],
        'thumb'         => $image,
        'name'          => $product_info['name'],
        'description'   => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
        'price'         => $product_info['price'],
        'special'       => $product_info['special'],
        'price_text'    => $price ,
        'special_text'  => $special,
        'tax'           => $tax,
        'rating'        => $rating,
        'href'          => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
      );
    }

    return $product;
  }
  
  public function getFeaturedProducts($limit=10){

    $featured_products = array();
    $featured_id = $this->config->get('featured_module_id');
    $featured = $this->getModule($featured_id);

    if(isset($featured['setting'])){
      $setting = json_decode($featured['setting'],1);
      $product_ids = (isset($setting['product']))?$setting['product']:0;

      if(!empty($product_ids)){
        
        $products = array_slice($setting['product'], 0, (int)$setting['limit']);
        foreach ($products as $product_id) {
          $product_data = $this->getProductById($product_id);
          if($product_data){
            $featured_products[] = $product_data;
          }
        }

      }
    }

    return $featured_products;
  }

  public function addProductReview($data)
  {
    //ALTER TABLE `oc_review` ADD `images` TEXT NULL AFTER `status`;
    //add product review
    $this->db->query("INSERT INTO " . DB_PREFIX . "review SET product_id = '".(int)$data['product_id']."', customer_id = '".(int)$data['customer_id']."', author = '".$this->db->escape($data['author'])."', text = '".$this->db->escape($data['review'])."', rating = '".(int)$data['rating']."', images = '".$this->db->escape(isset($data['review_images']) ? json_encode($data['review_images']) : '')."', date_added = NOW()");

    return $this->db->getLastId();

  }

  public function getOrderStatuses($order_status_ids)
  {
    $order_status_ids = implode(",",$order_status_ids);
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id IN (" . $order_status_ids . ") AND language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY sort ASC");

    return $query->rows;
  }

  public function getTotalOrders($filter='') 
  {
    $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o WHERE customer_id = '" . (int)$this->customer->getId() . "' AND o.order_status_id > '0' AND o.store_id = '" . (int)$this->config->get('config_store_id') . "'";
    if($filter){
      $sql .= " AND o.order_status_id IN($filter) ";
    }
		$query = $this->db->query($sql);

		return $query->row['total'];
		
	}

  public function getOrders($start = 0, $limit = 20, $filter='') 
  {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 1;
    }

    $have_delivery_history = "(SELECT COUNT(ab.id) FROM `assigned_boys` ab WHERE ab.order_id = o.order_id ) as is_trackable";

    $sql = "SELECT o.order_id, o.firstname, o.lastname, o.order_status_id, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value, $have_delivery_history FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND o.order_status_id > '0' AND o.store_id = '" . (int)$this->config->get('config_store_id') . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "'" ;
    
    if($filter){
      $sql .= " AND o.order_status_id IN($filter) ";
    }

    $sql .= " ORDER BY o.order_id DESC LIMIT " . (int)$start . "," . (int)$limit;

		$query = $this->db->query($sql);

		return $query->rows;
  }
  
  public function confirmPassword($email, $password)
  {
    $customer_query = $this->db->query("SELECT count(*) as total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "')");

    return $customer_query->row['total'];
  }

  public function getProductImage($product_id)
  {
    $product_info = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" .(int)$product_id. "' ")->row;
    if($product_info){
      return $product_info['image'];
    }
    return '';
  }

  //Save product quote
  public function saveQuote($data)
  {

    $this->db->query("INSERT INTO product_quotes SET full_name = '" . $this->db->escape($data['name']) . "', phone = '" . $this->db->escape($data['phone']) . "', email = '" . $this->db->escape($data['email']) . "', product_data = '" . $this->db->escape($data['product_data']) . "' ");

    return $this->db->getLastId();
  }

  //get ALl Quotes
  public function allQuote()
  {

    $results = $this->db->query("SELECT * FROM product_quotes WHERE 1");

    $quotes = array();
    foreach($results->rows as $result){
      $quotes[] = array(
        'id' => $result['id'],
        'full_name' => $result['full_name'],
        'phone' => $result['phone'],
        'email' => $result['email'],
        'product_data' => json_decode(html_entity_decode($result['product_data'])),
        'date_added' => $result['date_added'],
        'answered' => (int)$result['answered']
      );
    }

    return $quotes;
  }

  //Information pages
  public function getInformation($information_id) 
  {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE i.information_id = '" . (int)$information_id . "' AND id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1'");

		return $query->row;
	}

  public function getInformations() 
  {
		$query = $this->db->query("SELECT i.information_id,id.title FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

		return $query->rows;
	}
  
  public function updateFirebaseToken($customer_id, $firebasetoken)
  { 
    //Remove if attached to other account
    $this->db->query("UPDATE oc_customer SET firebase_token = '' WHERE firebase_token = '" .$this->db->escape($firebasetoken). "' ");

    $this->db->query("UPDATE oc_customer SET firebase_token = '" . $this->db->escape($firebasetoken) . "' WHERE customer_id = '" .(int)$customer_id. "' ");
    
    return $this->db->countAffected();
  }

  public function getOrderDeliveryHistory($order_id)
  {
    $query = $this->db->query("SELECT ab.*,(SELECT name FROM `users` u WHERE u.id = ab.user_id LIMIT 1) as delivery_boy_name FROM `assigned_boys` ab WHERE order_id = $order_id ORDER BY updated_at DESC"); 
    return $query->rows;
  }

  public function addCustomer($data) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		// if ($customer_group_info['approval']) {
		// 	$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int)$customer_id . "', type = 'customer', date_added = NOW()");
		// }
		
		return $customer_id;
  }
  
  public function loggedInDevices($customer_id)
  {
    $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "session WHERE customer_id = '". (int)$customer_id ."' ");

    return $query->row['total'];
  }

}