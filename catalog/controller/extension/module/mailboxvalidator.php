<?php
class ControllerExtensionModuleMailboxValidator extends Controller {
	private $error = array();
	private $handle;
	
	public function index() {
		if ($this->customer->isLogged()) {
			$this->customer->logout();
		}
		if (! defined(DIR_LOGS)) {
			define('DIR_LOGS', 'C:/Apache24/storage/logs/');
		}
		$this->load->language('extension/module/mailboxvalidator');
		$this->document->setTitle($this->language->get('heading_title'));
		if ((! empty($this->request->get['email'])) || (! empty($this->request->post['email']))) {
			if (! empty($this->request->get['email'])) {
				$email = $this->request->get['email'];
			} else if (! empty($this->request->post['email'])) {
				$email = $this->request->post['email'];
			}
		} else {
			
			$this->handle = fopen(DIR_LOGS . 'mbv-debug.log', 'a');
			fwrite($this->handle, date('Y-m-d G:i:s') . ' - ' . " Email is empty....\n");
			fclose($this->handle);
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_heading'),
			'href' => $this->url->link('extension/module/mailboxvalidator', 'email=' . $this->request->get['email'] . '&token=' . $this->request->get['token'], true)
		);

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if ($this->error) {
			foreach($this->error as $i => $msg) {
				$data['error_' . $i] = $msg;
			}
		}

		if (!empty($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		// } elseif ($email_validation_info) {
			// $data['email'] = $email_validation_info['email'];
		} else {
			$data['email'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_intro'] = $this->language->get('text_intro');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_back'] = $this->language->get('button_back');

		$data['action'] = $this->url->link('extension/module/mailboxvalidator', 'email=' . $this->request->get['email'] . '&token=' . $this->request->get['token'], true);

		$data['back'] = $this->url->link('account/login', '', true);

		// $this->document->addScript('catalog/view/javascript/module/email_validation.js');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/mailboxvalidator', $data));
	}
	
	public function valid() {
		if (!$this->config->get('module_mailboxvalidator_status')) {
			return true;
		}
		if (empty($this->request->post['email'])) {
			return false;
		}
		$this->load->library('mailboxvalidator');
		$email = $this->request->post['email'];
		$result = $this->mailboxvalidator->validate($email);
		if ($result != '') {
			return $result;
		}
		
		return false;
	}
}