<?php
class ControllerExtensionModuleGetsupport extends Controller {
	private $error = array();

	public function index() {
	

		$this->load->language('extension/module/frequently');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('extension/module/support');


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_support'),
			'href' => $this->url->link('extension/module/getsupport', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));
		$data['text_your_details'] = $this->language->get('text_your_details');
		

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_choose_department'] = $this->language->get('entry_choose_department');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_file'] = $this->language->get('entry_file');
		$data['text_select'] = $this->language->get('text_select');
		
		$data['entry_password'] = $this->language->get('entry_password');


		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['subject'])) {
			$data['error_subject'] = $this->error['subject'];
		} else {
			$data['error_subject'] = '';
		}

		if (isset($this->error['choose_department'])) {
			$data['error_choose_department'] = $this->error['choose_department'];
		} else {
			$data['error_choose_department'] = '';
		}

		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

	
		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('extension/moudule/support', '', true);


		if (isset($this->request->post['subject'])) {
			$data['subject'] = $this->request->post['subject'];
		} else {
			$data['subject'] = '';
		}

		if (isset($this->request->post['choose_department_id'])) {
			$data['choose_department_id'] = $this->request->post['choose_department_id'];
		} else {
			$data['choose_department_id'] = '';
		}

		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} else {
			$data['message'] = '';
		}
		if (isset($this->request->post['import'])) {
			$data['import'] = $this->request->post['import'];
		} else {
			$data['import'] = '';
		}

	
		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} else {
			$data['company'] = '';
		}

	



		$this->load->model('extension/module/support');

		$data['groups'] = $this->model_extension_module_support->getGroups();

		$detaildesc=$this->model_extension_module_support->getGeneral();
		
		
		foreach($detaildesc as $detail){
			$data['descriptions'][]=array(
			
			'faq_id'=>$detail['faq_id'],
			'question'=>$detail['question'],
			'answer'=> html_entity_decode($detail['answer'],ENT_QUOTES,'UTF-8')
			
			
			);
			
			
		
		}
		
		
$data['setting']=$this->model_extension_module_support->getModuleSetting('setting');
		

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/frequently_que', $data));
	}

	
	
	
	
	
	public function getFaq(){
		$this->load->language('extension/module/frequently');
			$this->document->setTitle($this->language->get('heading_title'));
			$this->load->model('extension/module/support');
			
			$data['description']=$this->model_extension_module_support->getDescriptions($this->request->get['group_id']);
			
			$json = array();
			$htmlf='';
			$chk=0;
			if(!empty($data['description'])){
				
			 foreach ($data['description'] as $desc) {
		
			$htmlf .='<div class="panel m565 panel-default panel-help">
            <a href="#opret-produkt'.$desc['faq_id'].'"';
			if($chk==0){
			$htmlf .='class="" aria-expanded="true"';	
			}
			else{ 
				$htmlf .='class="collapsed 5522" aria-expanded="false"';	
			}
			$htmlf .='data-toggle="collapse" data-parent="#help-accordion-1">
              <div class="panel-heading">
                <h3>'.$desc['question'].'</h3>
              </div>
            </a>';
			
			if($chk==0){
			$htmlf .= '<div id="opret-produkt'.$desc['faq_id'].'" class="collapse in">';	
			}
			else{ 
				$htmlf .= '<div id="opret-produkt'.$desc['faq_id'].'" class="collapse">';	
			}
           $htmlf .='<div class="panel-body">
                <p>'. html_entity_decode($desc['answer'],ENT_QUOTES,
				'UTF-8').'</p>
              </div>
            </div>
          </div>'; 
			$chk++;}	
			
			}
			 else{
				 $htmlf .='<div ><h3> no result</h3></div>';
				 
				 
			 }
			 
			 
			 
			 
			$this->session->data['success'] = $this->language->get('text_success');
		$url = '';
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($htmlf));
		

	}
	


}