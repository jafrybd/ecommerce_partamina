<?php
class ControllerExtensionModuleTicket extends Controller {
	private $error = array();

	public function index() {
	 
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('extension/moudle/ticket', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('extension/module/support');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('extension/module/support');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		$customer_id=$this->customer->getId();
		
			$uploads =   '../upload/catalog/attachments/'; //path to upload file
			
			if(isset($this->request->files['import'])){
			$fiup=$this->request->files['import']['tmp_name'];
			
			$fiupName=$this->request->files['import']['name'];
			
			$support_id = $this->model_extension_module_support->addCustomerReport($this->request->post,$customer_id);
			
				$i=0;
				foreach($fiup as $test){	
						$extension=explode(".",$fiupName[$i]);	
					 move_uploaded_file($test,$uploads.time().'.'.$extension[1]);
						$fileext =($extension[0].'.'.time().'.'.$extension[1]);
						
						$this->model_extension_module_support->addFile($fileext,$support_id);
						
						sleep(5);
						$i++;
				}
				
			}
		else{
			echo "file";
			
		}

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $customer_id,
					'name'        => $this->request->post['subject'] . ' ' . $this->request->post['choose_department']
				);

				$this->model_account_activity->addActivity('register', $activity_data);
			}

			$this->response->redirect($this->url->link('extension/module/support_success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

			$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_faq'),
			'href' => $this->url->link('extension/module/getsupport', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_ticket'),
			'href' => $this->url->link('extension/module/ticket', '', true)
		);

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['products'] = array();

		$filter_data = array(
		
			
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

	
		$logId= $this->customer->getId();
		
		$ticket_total = $this->model_extension_module_support->getTotalTickets($filter_data,$logId);

		
			
		
		
		$results = $this->model_extension_module_support->getTickets($filter_data,$logId);

		foreach ($results as $result) {
		
   $department=$this->model_extension_module_support->getDepartment($result['department']);
			
		$data['department']=$department;

		$status=$this->model_extension_module_support->getStatusName($result['status_id']);
			
		$data['status']=$status;


			$data['tickets'][] = array(
				'support_id' => $result['support_id'],
				
				'subject'       => $result['subject'],
				'department'      => $department[0]['department'],
				'message'      => $result['message'],
				'status'     => $status[0]['name'],
				'date_added'=> $result['date_added'],
				'view'       => $this->url->link('extension/module/ticket/view','&support_id=' . $result['support_id'] . $url, true)
				
			);
		
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		
		
		$data['column_subject'] = $this->language->get('column_subject1');
		$data['column_department'] = $this->language->get('column_department');
		$data['column_message'] = $this->language->get('column_message');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_ticket'] = $this->language->get('text_ticket');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_subject'] = $this->language->get('text_subject');
       $data['entry_comment'] = $this->language->get('entry_comment');
		
		$data['entry_status'] = $this->language->get('entry_status');
	     $data['button_save'] = $this->language->get('button_save');
	
		


	

	

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		
		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $ticket_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/ticket',  $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($ticket_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ticket_total - $this->config->get('config_limit_admin'))) ? $ticket_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ticket_total, ceil($ticket_total / $this->config->get('config_limit_admin')));

		
		

		$data['sort'] = $sort;
		$data['order'] = $order;
        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/ticket_list', $data));
	}

	
		public function view() {
			$this->load->language('extension/module/support');
			
		$this->load->model('extension/module/support');
		
		$data['users'] = array();
	
		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_view') : $this->language->get('text_view');
		$data['text_low'] = $this->language->get('text_low');
		$data['text_owner'] = $this->language->get('text_owner');
		$data['text_user'] = $this->language->get('text_user');
		
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_subject'] = $this->language->get('text_subject');
		$data['text_comment_subject'] = $this->language->get('text_comment_subject');
	
	
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_department'] = $this->language->get('entry_department');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['support_id'] = $this->language->get('support_id');
		

		
		$data['button_comment'] = $this->language->get('button_comment');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_upload'] = $this->language->get('tab_upload');
	
	

		$url = '';



		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', true)
		);

			$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_faq'),
			'href' => $this->url->link('extension/module/getsupport', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_ticket'),
			'href' => $this->url->link('extension/module/ticket', '', true)
		);
			

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_view'),
			'href' => $this->url->link('extension/module/ticket/view','&support_id=' . $this->request->get['support_id'] . $url, true)
		);


		if (!isset($this->request->get['support_id'])) {
			$data['action'] = $this->url->link('extension/module/ticket/view',$url, true);
			
		} else {
			$data['action'] = $this->url->link('extension/module/ticket/view','&support_id=' . $this->request->get['support_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/ticket',$url, true);

		if (isset($this->request->get['support_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
		
		$support_info = $this->model_extension_module_support->getTicketsview($this->request->get['support_id']);
		
			foreach($support_info as $info){
			$data['user_info'][]=array(
			'email'=>$info['email'],
			'telephone'=>$info['telephone'],
			'firstname'=>$info['firstname'].''.$info['lastname']
			);
		
			}
	    $data['statusData']=$this->model_extension_module_support->getstatus();

		  $comment_info1 = $this->model_extension_module_support->getComment($this->request->get['support_id']);
	
				foreach($comment_info1 as $com_info){
			
					$data['CommentData'][] = array(
						'comment' =>$com_info['comment'],
						'name' =>$com_info['name'],
						'status'=>$com_info['status'],
						'support_id'=>$com_info['support_id'],
						'datetime'=>$com_info['date_added']
						); 
		 
				} 
		 
		
		  $data['file_info'] = $this->model_extension_module_support->getSupportFile($this->request->get['support_id']); 
	
			 
	}
		
		if (isset($this->request->get['support_id']) && ($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
		$name = $this->customer->getfirstname();
			
		$data['statusData']=$this->model_extension_module_support->getstatus();
	
		$comment_info = $this->model_extension_module_support->addComment($this->request->get['support_id'],$this->request->post,$name);
		
			
			$support_info = $this->model_extension_module_support->getTicketsview($this->request->get['support_id']);
			foreach($support_info as $info){
			$data['user_info'][]=array(
			'email'=>$info['email'],
			'telephone'=>$info['telephone'],
			'firstname'=>$info['firstname'].' '.$info['lastname']
			
			);
		
			
			
			}
		$data['file_info'] = $this->model_extension_module_support->getSupportFile($this->request->get['support_id']); 
	
		 $comment_info1 = $this->model_extension_module_support->getComment($this->request->get['support_id']);

			foreach($comment_info1 as $com_info){
			
				$data['CommentData'][] = array(
				'comment' =>$com_info['comment'],
				'name' =>$com_info['name'],
				'status'=>$com_info['status'],
				'support_id'=>$com_info['support_id'],
				'datetime'=>$com_info['date_added']
			
			); 
			
			}
		 
		
		$url = '';
	
        if (isset($this->request->get['support_id'])) {
	
		 $this->url->link('extension/module/ticket/view', '&support_id=' . $this->request->get['support_id'] . $url, true);
			
		} else {
			$this->response->redirect($this->url->link('extension/module/ticket' , true));
		}
	
	
	
	}
		
		 if (isset($this->request->post['comment'])) {		
			$data['comment'] = $this->request->post['comment'];
		} elseif (!empty($comment_info)) {
			$data['comment'] = $comment_info['comment'];
		}  else {
			$data['comment'] = '';
		}
		  if (isset($this->request->post['firstname'])) {		
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($comment_info)) {
			$data['firstname'] = $comment_info['firstname'];
		}  else {
			$data['firstname'] = '';
		}
		
		if (isset($this->request->post['subject'])) {
			$data['subject'] = $this->request->post['subject'];
		} elseif (!empty($support_info)) {
			$data['subject'] = $support_info[0]['subject'];
		} else {
			$data['subject'] = '';
		}
		if (isset($this->request->post['date_added'])) {
			$data['date_added'] = $this->request->post['date_added'];
		} elseif (!empty($support_info)) {
			$data['date_added'] = $support_info[0]['date_added'];
		} else {
			$data['date_added'] = '';
		}
		if (isset($this->request->post['department'])) {
			$data['department'] = $this->request->post['department'];
		} elseif (!empty($support_info)) {
			$data['department']    =  $support_info[0]['department'];
		} else {
			$data['department'] = '';
		}

		if (isset($this->request->post['customer'])) {
			$data['customer'] = $this->request->post['customer'];
		} elseif (!empty($support_info)) {
			$data['customer'] = $support_info[0]['firstname'].$support_info[0]['lastname'];
		} else {
			$data['customer'] = '';
		}
		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} elseif (!empty($support_info)) {
			$data['message'] = $support_info[0]['message'];
		} else {
			$data['message'] = '';
		}
	if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($support_info)) {
			$data['status'] = $support_info[0]['name'];
			
		} else {
			$data['status'] = '';
		}

		

		$this->load->model('setting/store');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/view', $data));
		


}
	

}

	