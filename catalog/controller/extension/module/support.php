<?php
class ControllerExtensionModuleSupport extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('extension/module/support', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('extension/module/support');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('extension/module/support');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		$customer_id=$this->customer->getId();
		
		
			
			 $support_id = $this->model_extension_module_support->addCustomerReport($this->request->post,$customer_id);
			

			$uploads =   './admin/attachments/'; //path to upload file
			
			if($this->request->files['import']['name'][0]!=''){
				
			 $fiup=$this->request->files['import']['tmp_name'];
			
			 $fiupName=$this->request->files['import']['name'];
				$i=0;
				foreach($fiup as $test){	
						$extension=explode(".",$fiupName[$i]);	
					 move_uploaded_file($test,$uploads.$extension[0].'.'.time().'.'.$extension[1]);
						$fileext =($extension[0].'.'.time().'.'.$extension[1]);
					
						$this->model_extension_module_support->addFile($fileext,$support_id);
						sleep(5);
						$i++;
				}
				
			}
		else{
			echo "file";
			
		}
			

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $customer_id,
					'name'        => $this->request->post['subject'] . ' ' . $this->request->post['choose_department']
				);

				$this->model_account_activity->addActivity('register', $activity_data);
			}

			$this->response->redirect($this->url->link('extension/module/support_success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
	   $data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_faq'),
			'href' => $this->url->link('extension/module/getsupport', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_support'),
			'href' => $this->url->link('extension/module/support', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));
		$data['text_your_details'] = $this->language->get('text_your_details');
		

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['text_comment_subject'] = $this->language->get('text_comment_subject');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_choose_department'] = $this->language->get('entry_choose_department');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_file'] = $this->language->get('entry_file');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_subject_mail_Request'] = $this->language->get('text_subject_mail_Request');
		
		$data['entry_password'] = $this->language->get('entry_password');
		


		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['subject'])) {
			$data['error_subject'] = $this->error['subject'];
		} else {
			$data['error_subject'] = '';
		}

		if (isset($this->error['choose_department_id'])) {
			$data['error_choose_department_id'] = $this->error['choose_department_id'];
		} else {
			$data['error_choose_department_id'] = '';
		}

		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

	
		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('extension/module/support', '', true);

		$data['customer_groups'] = array();


		

		if (isset($this->request->post['subject'])) {
			$data['subject'] = $this->request->post['subject'];
		} else {
			$data['subject'] = '';
		}

		if (isset($this->request->post['choose_department_id'])) {
			$data['choose_department_id'] = $this->request->post['choose_department_id'];
		} else {
			$data['choose_department_id'] = '';
		}

		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} else {
			$data['message'] = '';
		}
		if (isset($this->request->post['import'])) {
			$data['import'] = $this->request->post['import'];
		} else {
			$data['import'] = '';
		}

	



		$this->load->model('extension/module/support');

		$data['departments'] = $this->model_extension_module_support->getDepartment();

		
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

	

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		if (isset($this->request->post['newsletter'])) {
			$data['newsletter'] = $this->request->post['newsletter'];
		} else {
			$data['newsletter'] = '';
		}

		

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/support', $data));
	}

	private function validate() {
		if ((utf8_strlen(trim($this->request->post['subject'])) < 1) || (utf8_strlen(trim($this->request->post['subject'])) > 32)) {
			$this->error['subject'] = $this->language->get('error_subject');
		}
		if ((utf8_strlen(trim($this->request->post['message'])) < 10) || (utf8_strlen(trim($this->request->post['message'])) > 2000)) {
			$this->error['message'] = $this->language->get('error_message');
		}
		if ($this->request->post['choose_department_id'] == '') {
         $this->error['choose_department_id'] = $this->language->get('error_choose_department_id');
        }
		
		
		return !$this->error;
	}

}