<?php
class ControllerApiGetQuote extends Controller {
    
    public function index() {
        $json =array();
        
        if( !isset($this->request->post['name']) ||
            !isset($this->request->post['email']) ||
            !isset($this->request->post['phone']) ||
            !isset($this->request->post['product']) ||
            !isset($this->request->post['quantity'])) {
                
            $json['success'] = false;
		    $json['message'] = "required inputs are missing";
		    
		    $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $name = $this->request->post['name'];
        $email = $this->request->post['email'];
        $phone = $this->request->post['phone'];
        $model = $this->request->post['product'];
        $quantity = $this->request->post['quantity'];
        
        try {
            // $payload = array("email"=>$this->request->post['email'], "password"=>$this->request->post['password'], "returnSecureToken"=>true);

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, "catalog/view/theme/default/template/information/customer_service.php");
            // curl_setopt($ch, CURLOPT_POST, 1);// set post data to true
            // curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));   // post data
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $result = curl_exec($ch);
            // curl_close ($ch);
            
            $formcontent="From: $name\nEmail: $email\nPhone Number: $phone\nMessage: I am looking for $quantity of $model !";
            $recipient = "jafrybd42@gmail.com";
            $subject = "Bulk Quotation Request";
            $mailheader = "From: $email \r\n";
            
            $headers = "From: $email\r\n";
            $headers .= "Reply-To: $email\r\n";
            $headers .= "Return-Path: $email\r\n";
            $headers .= "CC: $recipient\r\n";
            $headers .= "BCC: $email\r\n";
            
            
            if ( mail($recipient, $subject, $formcontent, $headers) ) {
                $json['success'] = true;
        		$json['message'] = "The request is successful";
        		$json['data']['message'] = "The email has been sent!";
        
        		$this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            } else {
                $json['success'] = false;
        		$json['message'] = "The email has failed!";
        
        		$this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
		$json['success'] = false;
		$json['message'] = "unknown error";

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

}