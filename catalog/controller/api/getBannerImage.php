<?php
class ControllerApiGetBannerImage extends Controller {
    
    public function getBanners() {
        
        // $banner_id = $this->request->post['banner_id'];
        $banner_id = 7;
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . 
		DB_PREFIX . "banner_image bi ON (b.banner_id = bi.banner_id) WHERE b.banner_id = '" . (int)$banner_id . "' AND b.status = '1' ORDER BY bi.banner_image_id ASC " );
		
		return $query->rows;
	}
	
	public function getTotalBanners($banner_id) {
        
		$query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		
		$result = $query->rows;
		
        return $result[0]["COUNT(*)"];
	}

	public function index() {
	    
	   // $limit = 100;
    //     if(isset($this->request->post['limit'])) {
    //         $limit = $this->request->post['limit'];
    //     }
        
    //     $banner_id = 7;
        
    //     $tpc = (int)$this->getTotalBanners($banner_id);
    //     $total_page_count = (float)($tpc / $limit);
    //     if($total_page_count > (int)$total_page_count)
    //         $total_page_count = (int)$total_page_count + 1;
        
    //     $page = $this->request->post['page'];

	    $json =array();
        
        // $this->load->model('design/banner');
        $result = $this->getBanners();
        $counter = 0;
        
        foreach ($result as $results) {
            $counter++;
			$data['banners'][] = array(
				'id'     => $results['banner_image_id'],
				'title' => $results['title'],
				'link'       => $results['link'],
				'image'     => $results['image']
			);
		}
		if($counter == 0) {
		    $json['success'] = "false";
    	    $json['message'] = "No data found";
		}
    	$json['success'] = true;
    	$json['message'] = "The request is successful";
    // 	$json['total_page_count'] = (int)$total_page_count;
        $json['data'] = $data['banners'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
}