<?php
class ControllerApiCustomerService extends Controller {
    
    public function index() {
        $json =array();
        
        if( !isset($this->request->post['name']) ||
            !isset($this->request->post['email']) ||
            !isset($this->request->post['subject']) ||
            !isset($this->request->post['enquiry'])) {
                
            $json['success'] = false;
		    $json['message'] = "required inputs are missing";
		    
		    $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $name = $this->request->post['name'];
        $email = $this->request->post['email'];
        $subject = $this->request->post['subject'];
        $enquiry = $this->request->post['enquiry'];
        
        try {
            // $payload = array("email"=>$this->request->post['email'], "password"=>$this->request->post['password'], "returnSecureToken"=>true);

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, "catalog/view/theme/default/template/information/customer_service.php");
            // curl_setopt($ch, CURLOPT_POST, 1);// set post data to true
            // curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));   // post data
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $result = curl_exec($ch);
            // curl_close ($ch);
            
            $to = "jafry.mondol@medinatech.co"; // this is your Email address
            $from = $email; // this is the sender's Email address
        
            $name = $name;
            $subject = $subject;
            $subject2 = "Copy of your form submission";
            $message = $name . " wrote the following:" . "\n\n" . $enquiry;
            $message2 = "Here is a copy of your message " . $name . "\n\n" . $enquiry;
            // $headers = 'From: jafrybd42@gmail.com' . "\r\n";
            // $headers .= 'Reply-To: '.$to.'';
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8\r\n";
            $headers .= "From: <".$from. ">" ;
            $headers2 = "From:" . $to;
            mail($to,$subject,$message,$headers);
            mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
            //echo "Mail Sent. Thank you " . $name . ", we will contact you shortly.";
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data']['message'] = "Mail Sent. Thank you " . $name . ", we will contact you shortly.";
    
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
		$json['success'] = false;
		$json['message'] = "unknown error";

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

}