<?php
class ControllerApiProductReturn extends Controller {
    
    public function getOrderDetails() {
        
        $customer_id = $this->request->post['customer_id'];
        $product_id = $this->request->post['product_id'];
        $order_id = $this->request->post['order_id'];
        
        $this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']);
        
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int)$customer_id . "' AND order_status_id > '0'");
		
		$order_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "' AND product_id = '" . (int)$product_id . "'");
        
        //return "success";
        
		return array(
				'order_id'                => $order_query->row['order_id'],
				'customer_id'             => $customer_info['customer_id'],
				'firstname'               => $customer_info['firstname'],
				'lastname'                => $customer_info['lastname'],
				'telephone'               => $customer_info['telephone'],
				'email'                   => $customer_info['email'],
				'date_added'              => $order_query->row['date_added'],
				'product_id'              => $order_product_query->row['product_id'],
				'quantity'                => $order_product_query->row['quantity'],
				'name'                    => $order_product_query->row['name'],
				'model'                   => $order_product_query->row['model']
			);
	}
    
    public function addReturnProduct($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "return` SET order_id = '" . (int)$data['order_id'] 
		. "', customer_id = '" . (int)$data['customer_id'] 
		. "', firstname = '" . $this->db->escape($data['firstname']) 
		. "', lastname = '" . $this->db->escape($data['lastname']) 
		. "', email = '" . $this->db->escape($data['email']) 
		. "', telephone = '" . $this->db->escape($data['telephone']) 
		. "', product = '" . $this->db->escape($data['name']) 
		. "', model = '" . $this->db->escape($data['model']) 
		. "', quantity = '" . (int)$data['quantity'] 
		. "', opened = '" . (int)$data['opened'] 
		. "', return_reason_id = '" . (int)$data['return_reason_id'] 
		. "', return_status_id = '" . (int)$data['return_status_id'] 
		. "', comment = '" . $this->db->escape($data['comment']) 
		. "', date_ordered = '" . $this->db->escape($data['date_added']) 
		. "', date_added = NOW(), date_modified = NOW()");

		return $this->db->getLastId();
	}
    
    public function index() {
        
        $json =array();
        $data =array();
        
        $customer_id = $this->request->post['customer_id'];
        $order_id = $this->request->post['order_id'];
        $product_id = $this->request->post['product_id'];
        $opened = $this->request->post['opened'];
        $return_reason_id = $this->request->post['return_reason_id'];
        $return_status_id = 1;
        $comment = $this->request->post['comment'];
        
        $result = $this->getOrderDetails();
        //echo "pre";
        //print_r($result);
        
        $data['order_id'] = $result['order_id'];
			$data['customer_id'] = $result['customer_id'];
			$data['firstname'] = $result['firstname'];
			$data['lastname'] = $result['lastname'];
			$data['telephone'] = $result['telephone'];
			$data['email'] = $result['email'];
			$data['date_added'] = $result['date_added'];
			$data['product_id'] = $result['product_id'];
			$data['quantity'] = $result['quantity'];
			$data['name'] = $result['name'];
			$data['model'] = $result['model'];
		$data['opened'] = $opened;
		$data['return_reason_id'] = $return_reason_id;
		$data['return_status_id'] = $return_status_id;
		$data['comment'] = $comment;
        
        $result2 = $this->addReturnProduct($data);
		
		/*$data['order_id'] = $order_id;
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . $customer_group_id . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

		$result = $query->rows;
		
		$hasResults = 0;
		
		foreach ($result as $results) {
		    $hasResults += 1;
    			$data['discounts'][] = array(
    				'product_discount_id'     => $results['product_discount_id'],
    				'customer_group_id' => $results['customer_group_id'],
    				'quantity'       => $results['quantity'],
    				'priority'     => $results['priority'],
    				'price'     => $results['price'],
    				'date_start'     => $results['date_start'],
    				'date_end'     => $results['date_end']
    			);
    		}
    	if($hasResults > 0) {
    	    $json['discounts'] = $data['discounts'];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	*/
    	$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput($result2);
        return;
	}
	
}