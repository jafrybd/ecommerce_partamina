<?php
class ControllerApiTest extends Controller {

	public function getBestSellerProducts() {
	    
		$product_data = array();

		$query = $this->db->query("SELECT op.product_id, SUM(op.quantity) AS total FROM oc_order_product op LEFT JOIN `oc_order` o ON (op.order_id = o.order_id) LEFT JOIN `oc_product` p ON (op.product_id = p.product_id) LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' GROUP BY op.product_id ORDER BY total DESC limit 10");
		
		if($query->num_rows > 0) {
		    $json =array();
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		
		    $json['data'] = $query->rows;
		} else {
		    $json =array();
    		$json['success'] = false;
    		$json['message'] = "no data found";
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
		
	}
}


