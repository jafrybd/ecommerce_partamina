<?php
class ControllerApiPushNotification extends Controller {
    
    public function getDeviceIds() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE uid <> '' ");
		return $query->rows;
	}
	
	public function index() {
	    //$info_id = $this->request->post['$info_id'];
	    $json =array();
        $result = $this->getDeviceIds();
        
        $counter = 0;
        foreach ($result as $results) {
            $result = $this->sendFCM("demo message from 221b", $results['uid']);
            $json[$counter] = array(
                'response' => json_decode($result),
                'id' =>$results['uid']
                );
            $counter++;
    	}
        // $result = $this->sendFCM("221b", "Jic3JP2fSaabwlkEbgKrOcZoxS72");
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function sendFCM( $data, $id ) {
	    
        // API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAApnBoCFM:APA91bEMXciNZ9V4PAzeVIV9mRr4Jr8u5gGXaB0D0vDNxgiWmdcn22h0xOzbnxSxBOmi-im0lz7WXZSb2A8CUSuzmBVcrb7W7SDhG5-AlRNDWROsLOS_o7GgVC_PMvF_hFMvmvbvKjTe' );     
         
        //$registrationIds = array( $_GET['id'] );
        // $regids = array("e3U6usHUR1aVnpd3lKvg7K:APA91bGVzeEMSQA3lxPb5qmykcVdoRGpxeRsqjpaXBlr4uS_oKeiwqMNXXnp6QSn1bH4EZ3feeZa7kCfP_68o864pjEjFeyRBKXSKtoS9qeQB86tao-nUGOCJG6pPSx29_H5haQYOYhI");
        // $regids = "e3U6usHUR1aVnpd3lKvg7K:APA91bGVzeEMSQA3lxPb5qmykcVdoRGpxeRsqjpaXBlr4uS_oKeiwqMNXXnp6QSn1bH4EZ3feeZa7kCfP_68o864pjEjFeyRBKXSKtoS9qeQB86tao-nUGOCJG6pPSx29_H5haQYOYhI";
        // $regids = "d1DQQqQUSYy_oMmZLT4p2u:APA91bFlMZCtjQoi2crGZVCMsY_aeEmQ958U7Q6lEUEod4v7i6ZwRQJIaStjMRLcVdDM0VLdxgrjiL-MkRV5R8GuaSCkNK_S21WFubkTuJ8eF9PUwI_mYGJa5yffO6pMfvI8nEoqyBdf";
        $regids = $id;
    
        //$registrationIds = $ids;
        $registrationIds = $regids;
        //echo json_encode($registrationIds);     
        // prep the bundle
        $msg = array(
            'text' => $data,
            'title' => 'HWTitle',
            'subtitle'  => 'HWSubtitle',
            'tickerText'    => 'HW Notification from OC',
            'vibrate'   => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
         
        $fields = array(
            'to' => $registrationIds,
            'notification'  => $msg
        );


        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
         
        //echo $result . '<br>';
        return $result;
    }
	
}