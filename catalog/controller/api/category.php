<?php
class ControllerApiCategory extends Controller {
    
//     public function getTotalCategories($parent_category_id) {
        
// 		$query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_category_id . "'");
		
// 		$result = $query->rows;
		
//         return $result[0]["COUNT(*)"];
// 	}
	
	public function getParentCategories() {

        $this->load->model('catalog/category');
        $json = array();

        $filter_data = array();
        $results = $this->model_catalog_category->getCategories(0);
        
        foreach ($results as $result) { 
            $data['Parent_Categories'][] = array(
                'category_id' => $result['category_id'],
                'parent_id' => $result['parent_id'],
                'thumb' => $result['image'],
                'name' => $result['name'],
                'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'store_id' => $result['store_id'],
                'child_categories' => $this->pushChildCategoryIntoParent($result['category_id'])
            );
        }
        
        $json['success'] = true;
		$json['message'] = "The request is successful";
    		
        $json['data']['Parent_Categories'] = $data['Parent_Categories'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}
	
	private function pushChildCategoryIntoParent($parent_id=null){
	    $this->load->model('catalog/category');
            $json = array();
    
            $filter_data = array();
            $results = $this->model_catalog_category->getCategories($parent_id);
            $data = array();
            $count = 0;
            if(!empty($results)){
                
                foreach ($results as $result) { 
                    $count++;
                    $data[] = array(
                        'category_id' => $result['category_id'],
                        'parent_id' => $result['parent_id'],
                        'thumb' => $result['image'],
                        'name' => $result['name'],
                        'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                        'meta_title' => $result['meta_title'],
                        'meta_description' => $result['meta_description'],
                        'store_id' => $result['store_id'],
                    );
                }
                
                return $data;
                
            }
            
            return $data[] = array();
                
	}
	
	public function getChildCategoriesOfCategory() {
        
        if(isset($this->request->post['category_id'])) {
            $parent_category_id = $this->request->post['category_id'];
    		$this->load->model('catalog/category');
            $json = array();
    
            $filter_data = array();
            $results = $this->model_catalog_category->getCategories($parent_category_id);
            $count = 0;
            foreach ($results as $result) { 
                $count++;
                $data['Child_Categories'][] = array(
                    'category_id' => $result['category_id'],
                    'parent_id' => $result['parent_id'],
                    'thumb' => $result['image'],
                    'name' => $result['name'],
                    'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'meta_title' => $result['meta_title'],
                    'meta_description' => $result['meta_description'],
                    'store_id' => $result['store_id'],
                );
            }
            if($count == 0) {
                $json['success'] = true;
        		$json['message'] = "no data found";
            		
                $json['data'] = array();
                
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
        		
            $json['data']['Child_Categories'] = $data['Child_Categories'];
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        else {
            $json['success'] = false;
    		$json['message'] = "category_id is required";
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
	}

	public function getProductsUnderCategory() {
	    
	    if(isset($this->request->post['category_id'])) {
	        
            $category_id = $this->request->post['category_id'];
    	    
    		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
    
    		$results = $query->rows;
    		
    		$count = 0;
    		
    		foreach ($results as $result) {
    		    $count++;
    			$data['Products_In_Category'][] = array(
    				'product_id'     => $result['product_id']
    			);
    		}
            if($count == 0) {
                $json['success'] = true;
        		$json['message'] = "no data found";
            		
                $json['data'] = array();
                
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
        		
            $json['data']['Products_In_Category'] = $data['Products_In_Category'];
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
	    }
	    else {
            $json['success'] = false;
    		$json['message'] = "category_id is required";
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
	}
	
//     public function index()
//     {
//         $this->load->language('api/cart');
//         $this->load->model('catalog/category');
//         $this->load->model('catalog/product');
//         $this->load->model('tool/image');
//         $json = array();
        
//         if (isset($this->request->get['product_id'])) {
//             $json['product_category'] = array();
//             $data['category'] = array();
//             $results = $this->model_catalog_product->getCategories($this->request->get['product_id']);
            
//             foreach ($results as $result) {
//     			$data['category'][] = array(
//     				'product_id'     => $result['product_id'],
//     				'category_id'     => $result['category_id']
//     			);
//     		}
//             $json['product_category'] = $data['category'];
//             $this->response->addHeader('Content-Type: application/json');
//             $this->response->setOutput(json_encode($json));
//             return;
//         }
        
//         if (isset($this->request->post['parent_category_id'])) {
            
//             // $limit = 2;
//             // if(isset($this->request->post['limit'])) {
//             //     $limit = $this->request->post['limit'];
//             // }
            
//             // $parent_category_id = $this->request->post['parent_category_id'];
            
//             // $tpc = (int)$this->getTotalCategories($parent_category_id);
//             // $total_page_count = (float)($tpc / $limit);
//             // if($total_page_count > (int)$total_page_count)
//             //     $total_page_count = (int)$total_page_count + 1;
            
//             // $page = $this->request->post['page'];
            
//     		// $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_category_id . "' LIMIT " . ((((int)$page) - 1) * ((int)$limit)) . ", " . (int)$limit );
//     		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_category_id . "'" );
    		
//     		$results = $query->rows;
		
//             $data['category'] = array();
//             //$results = $this->model_catalog_category->getCategories($parent_category_id);
            
//             foreach ($results as $result) { 
//                 $data['Categories'][] = array(
//                     'category_id' => $result['category_id'],
//                     'parent_id' => $parent_category_id,
//                     'thumb' => $result['image'],
//                     'name' => $result['name'],
//                     'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
//                     'meta_title' => $result['meta_title'],
//                     'meta_description' => $result['meta_description'],
//                     'store_id' => $result['store_id'],
//                 );
//             }
            
//             $json['success'] = true;
//     		$json['message'] = "The request is successful";
//     		// $json['total_page_count'] = (int)$total_page_count;
//             $json['data'] = $data['Categories'];
            
//             $this->response->addHeader('Content-Type: application/json');
//             $this->response->setOutput(json_encode($json));
//             return;
//         }
        
//         //$json['Categories'] = array();
//         $filter_data = array();
//         //$results = $this->model_catalog_category->getCategories($this->request->post['category_id']);
//         $results = $this->model_catalog_category->getCategories();
        
//         /*$json['Categories'] = $results;
//         $this->response->addHeader('Content-Type: application/json');
//         $this->response->setOutput(json_encode($results));*/
        
        
//         foreach ($results as $result) { 
//             $data['Categories'][] = array(
//                 'category_id' => $result['category_id'],
//                 'parent_id' => $result['parent_id'],
//                 'thumb' => $result['image'],
//                 'name' => $result['name'],
//                 'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
//                 'meta_title' => $result['meta_title'],
//                 'meta_description' => $result['meta_description'],
//                 'store_id' => $result['store_id'],
//             );
//         }
        
//         $json['success'] = true;
// 		$json['message'] = "The request is successful";
    		
//         $json['data']['Categories'] = $data['Categories'];
        
//         $this->response->addHeader('Content-Type: application/json');
//         $this->response->setOutput(json_encode($json));
        
//     }
}