<?php
class ControllerApiGetCustomer extends Controller {
	public function index() {
		$this->load->language('api/customer');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {

			// Customer
			if ($this->request->post['customer_id']) {
			    
			    try {
			        $this->load->model('account/customer');
    
    				$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']);
    
    				if (!$customer_info || !$this->customer->login($customer_info['email'], '', true)) {
    					//$json['error']['warning'] = $this->language->get('error_customer');
    					$json['success'] = false;
                		$json['message'] = $this->language->get('error_customer');
            		
                        $this->response->addHeader('Content-Type: application/json');
                        $this->response->setOutput(json_encode($json));
                        return;
    				}

            		$json['success'] = true;
            		$json['message'] = "The request is successful";
            		
            		$json['data'] = $customer_info;
            		
            		$this->response->addHeader('Content-Type: application/json');
        		    $this->response->setOutput(json_encode($json));
        		    return;
			    }
			    catch (Exception $e) {
			        
            		$json['success'] = false;
            		$json['message'] = $e->getMessage();
        		
                    $this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode($json));
                    return;
                }
				
			}
			
			$json['success'] = false;
    		$json['message'] = "unknown error occured";
            		
            $this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		}
	}
}