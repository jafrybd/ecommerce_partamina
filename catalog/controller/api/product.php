<?php
class ControllerApiProduct extends Controller
{
    public function index() {
        $this->load->language('api/cart');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $json = array();
        
        if (isset($this->request->get['product_id']) && isset($this->request->get['product_review'])) {
            
            $json['product_review'] = array();
            $this->load->model('catalog/review');
            $data['reviews'] = array();
            $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);
            
            foreach ($results as $result) {
    			$data['reviews'][] = array(
    				'author'     => $result['author'],
    				'text'       => nl2br($result['text']),
    				'rating'     => (int)$result['rating'],
    				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
    			);
    		}
            $json['product_review'] = $data['reviews'];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        if (isset($this->request->get['product_id'])) {
            $json['product'] = array();
            
			$product_id = (int)$this->request->get['product_id'];
			
			$product_info = $this->model_catalog_product->getProduct($product_id);
			$related_products = $this->getSimilarProducts($this->request->get['product_id']);

    		$data['product'][] = array(
                'product_id'       => $product_info['product_id'],
				'name'             => $product_info['name'],
				'description'      => $product_info['description'],
				'meta_title'       => $product_info['meta_title'],
				'meta_description' => $product_info['meta_description'],
				'meta_keyword'     => $product_info['meta_keyword'],
				'tag'              => $product_info['tag'],
				'model'            => $product_info['model'],
				'sku'              => $product_info['sku'],
				'upc'              => $product_info['upc'],
				'ean'              => $product_info['ean'],
				'jan'              => $product_info['jan'],
				'isbn'             => $product_info['isbn'],
				'mpn'              => $product_info['mpn'],
				'location'         => $product_info['location'],
				'quantity'         => $product_info['quantity'],
				'stock_status'     => $product_info['stock_status'],
				'image'            => $product_info['image'],
				'manufacturer_id'  => $product_info['manufacturer_id'],
				'manufacturer'     => $product_info['manufacturer'],
				'price'            => $product_info['price'],
				'special'          => $product_info['special'],
				'reward'           => $product_info['reward'],
				'points'           => $product_info['points'],
				'tax_class_id'     => $product_info['tax_class_id'],
				'date_available'   => $product_info['date_available'],
				'weight'           => $product_info['weight'],
				'weight_class_id'  => $product_info['weight_class_id'],
				'length'           => $product_info['length'],
				'width'            => $product_info['width'],
				'height'           => $product_info['height'],
				'length_class_id'  => $product_info['length_class_id'],
				'subtract'         => $product_info['subtract'],
				'rating'           => $product_info['rating'],
				'reviews'          => $product_info['reviews'],
				'minimum'          => $product_info['minimum'],
				'sort_order'       => $product_info['sort_order'],
				'status'           => $product_info['status'],
				'date_added'       => $product_info['date_added'],
				'date_modified'    => $product_info['date_modified'],
				'viewed'           => $product_info['viewed'],
				'related_products' => $this->getSimilarProducts($product_info['product_id'])
            );
            
            
            $json =array();
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		
            $json['data']['product_details'] = $data['product'];
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$json['products'] = array();
		
		$filter_data = array();
		if(isset($this->request->get['sort'])) {
		    if($this->request->get['sort'] == "price") {
		        $filter_data['sort'] = 'p.price';
		    }
		}
		if(isset($this->request->get['order'])) {
		    if($this->request->get['order'] == "ASC" || $this->request->get['order'] == "DESC") {
		        $filter_data['order'] = $this->request->get['order'];
		    }
		}
        
        $results = $this->model_catalog_product->getProducts($filter_data);
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
            }
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }
            if ((float) $result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }
            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }
            if ($this->config->get('config_review_status')) {
                $rating = (int) $result['rating'];
            } else {
                $rating = false;
            }
            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                'price' => $price,
                'special' => $special,
                'tax' => $tax,
                'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating' => $result['rating'],
                'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
            );
        }
        
        $json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		$json['data']['products'] = $data['products'];
		
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
    }
    
    private function getSimilarProducts($product_id) {
 
        $this->load->model('catalog/product');
        
		$query = $this->db->query("SELECT related_id FROM " . DB_PREFIX . "product_related WHERE product_id = '" . $product_id . "'");

		$result = $query->rows;
		
		$similarProductsList = array();
		
		if(!empty($result)){
		    
		    foreach ($result as $results) {
    		    
    		    $similarProductsList [] = $results['related_id'];
    		    
    		}
		    
		}
		
		$similarData = array();
		
		
		if(!empty($similarProductsList)){
		    
		    foreach($similarProductsList as $key=>$simiproduct){
		    
		        $similarData [] = $this->model_catalog_product->getProduct($simiproduct);
		    
		    }
		    
		    return $similarData;
		    
		}
		
		return $similarData;
 
    }
    
    public function search() {
        $this->load->language('api/cart');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $json = array();
        
        if (isset($this->request->get['filter_name'])) {
			$search = $this->request->get['filter_name'];
		} else {
			$search = '';
		}
		if (isset($this->request->get['category_id'])) {
			$cat_id = $this->request->get['category_id'];
		} else {
			$cat_id = '';
		}
        
        $tag           = $search;
		$description   = '';
		$category_id   = $cat_id;
		$sub_category  = '';
		//$sort          = 'p.sort_order';
		$order         = 'ASC';
		//$page          = 1;
		//$limit         = $this->config->get('module_live_search_limit');
		//$search_result = 0;
		//$error         = false;
        
        $filter_data = array(
			'filter_name'         => $search,
			'filter_tag'          => $tag,
			'filter_description'  => $description,
			'filter_category_id'  => $category_id,
			'filter_sub_category' => $category_id,
			//'sort'                => $sort,
			'order'               => $order,
			//'start'               => ($page - 1) * $limit,
			//'limit'               => $limit
		);
		
		try {
    		$results = $this->model_catalog_product->getProducts($filter_data);
    		
    		$json =array();
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data'] = $results;
    		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
    }
    
    public function getReview() {
        
        $this->load->model('catalog/product');
        $json = array();
        
        //$query = $this->db->query("SELECT r.review_id, r.author, r.rating, r.text, p.product_id, pd.name, p.price, p.image, r.date_added FROM " . DB_PREFIX . "review r LEFT JOIN " . DB_PREFIX . "product p ON (r.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.date_available <= NOW() AND p.status = 1 AND r.status = 1 and pd.language_id = '" . (int)$this->config->get('config_language_id') . "' and p.product_id = '" . (int)$product_id . "' ORDER BY r.date_added DESC");

        if (isset($this->request->get['product_id']) && isset($this->request->get['product_review'])) {
            
            $this->load->model('catalog/review');
            $data['reviews'] = array();
            $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);
            
            $count = 0;
            
            foreach ($results as $result) {
                $count++;
    			$data['reviews'][] = array(
    				'author'     => $result['author'],
    				'text'       => nl2br($result['text']),
    				'rating'     => (int)$result['rating'],
    				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
    			);
    		}
    		
    		if($count ==0) {
    		    $json['success'] = false;
    		    $json['message'] = "no data found";
    		
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
    		}
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
            $json['data'] = $data['reviews'];
            
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
    }
}