<?php
class ControllerApiWishlist extends Controller {
    
    public function getWL($customer_id) {
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$customer_id . "'");
		
		$result = $query->rows;
		
		$json =array();
		
		foreach ($result as $results) {
    			$data['wishlist'][] = array(
    				'product_id'     => $results['product_id']
    			);
    		}
        $json['wishlist'] = $data['wishlist'];
        return $json;

	}
	
	public function getTotalWL($customer_id) {
        
		$query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$customer_id . "'");
		
		$result = $query->rows;
		
        return $result[0]["COUNT(*)"];
	}
    
    public function getWishlist() {
        
         $this->load->language('api/cart');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        
        $limit = 2;
        if(isset($this->request->post['limit'])) {
            $limit = $this->request->post['limit'];
        }
        
        $customer_id = $this->request->get['customer_id'];
        
        /*echo "<pre>";
        print_r($customer_id);
        echo "</pre>";
        
        die("die...");exit;*/
        
        $tpc = (int)$this->getTotalWL($customer_id);
        $total_page_count = (float)($tpc / $limit);
        if($total_page_count > (int)$total_page_count)
            $total_page_count = (int)$total_page_count + 1;
        
        $page = $this->request->get['page'];
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$customer_id . "' LIMIT " . ((((int)$page) - 1) * ((int)$limit)) . ", " . (int)$limit );
		
		$result = $query->rows;
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		$json['total_page_count'] = (int)$total_page_count;
		
		foreach ($result as $results) {
		    
		    //$data['wishlist'][] = array('product_id' => $results['product_id']);
		    
		    /*if((int)$results['product_id'] !== 0){
		        $data['wishlist'][] = array('product_id' => $results['product_id']);
		    }*/
		    
			$data['wishlist'][] = $this->model_catalog_product->getCustomProduct($results['product_id']);
		}
        $json['wishlist'] = $data['wishlist'];
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function setWishlist() {
        
        $customer_id = $this->request->post['customer_id'];
        $product_id = $this->request->post['product_id'];
        try {
		    $query = $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '" . (int)$customer_id . "' , product_id = '" . (int)$product_id . "', date_added = NOW()");
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		
		$json['data'] = $this->getWL($customer_id);
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

}