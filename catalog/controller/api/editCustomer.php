<?php
class ControllerApiEditCustomer extends Controller {
    public function index(){
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode("success"));
    }
    
    public function editEmail(){
        if(!isset($this->request->post['customer_id']) && !isset($this->request->post['email'])) {
            
            $json['success'] = false;
    		$json['message'] = "customer id is requied";
            		
            $this->response->addHeader('Content-Type: application/json');
    	    $this->response->setOutput(json_encode($json));
            return;
        }
        $customer_id = $this->request->post['customer_id'];
        $email = $this->request->post['email'];
        
        try {
        
            $query = "UPDATE " . DB_PREFIX . "customer SET email = '" . $email . "'";
            
            $query = $query . " WHERE customer_id = '" . (int)$customer_id . "' ";
            $result = $this->db->query($query);
        
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		
    		$json['data'] = array();
    		
    		$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		    return;
        }
        catch (Exception $e) {
	        
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    }
    
    public function editName(){
        if(!isset($this->request->post['customer_id']) && !isset($this->request->post['firstname']) && !isset($this->request->post['lastname'])) {
            
            $json['success'] = false;
    		$json['message'] = "customer id, firstname, lastname are requied";
            		
            $this->response->addHeader('Content-Type: application/json');
    	    $this->response->setOutput(json_encode($json));
            return;
        }
        $customer_id = $this->request->post['customer_id'];
        $firstname = $this->request->post['firstname'];
        $lastname = $this->request->post['lastname'];
        
        try {
        
            $query = "UPDATE " . DB_PREFIX . "customer SET firstname = '" . $firstname . "' , lastname = '" . $lastname . "' ";
            
            $query = $query . " WHERE customer_id = '" . (int)$customer_id . "' ";
            $result = $this->db->query($query);
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		
    		$json['data'] = array();
    		
    		$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		    return;
        }
        catch (Exception $e) {
	        
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    }
    
    public function editAddress(){
        $json =array();
        
        if( !isset($this->request->post['address_id']) || 
            !isset($this->request->post['address1']) || 
            !isset($this->request->post['customer_id']) || 
            !isset($this->request->post['firstname']) || 
            !isset($this->request->post['lastname']) || 
            !isset($this->request->post['city']) || 
            !isset($this->request->post['postcode']))
        {
            $json['success'] = false;
		    $json['message'] = "required inputs are missing";
                
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $address_id = $this->request->post['address_id'];
        $customer_id = $this->request->post['customer_id'];
        
        try {
        
            $query = "UPDATE " . DB_PREFIX . "address SET customer_id = '" . $customer_id . "'";
            
            $firstname = (isset($this->request->post['firstname'])) ? $this->request->post['firstname'] : "";
            $query = (isset($this->request->post['firstname'])) ? $query . " , firstname = '" . $firstname . "'" : $query . "";
            
            $lastname = (isset($this->request->post['lastname'])) ? $this->request->post['lastname'] : "";
            $query = (isset($this->request->post['lastname'])) ? $query . " , lastname = '" . $lastname . "'" : $query . "";
            
            $address1 = (isset($this->request->post['address1'])) ? $this->request->post['address1'] : "";
            $query = (isset($this->request->post['address1'])) ? $query . " , address_1 = '" . $address1 . "'" : $query . "";
            
            $address2 = (isset($this->request->post['address2'])) ? $this->request->post['address2'] : "";
            $query = (isset($this->request->post['address2'])) ? $query . " , address_2 = '" . $address2 . "'" : $query . "";
            
            $city = (isset($this->request->post['city'])) ? $this->request->post['city'] : "";
            $query = (isset($this->request->post['city'])) ? $query . " , city = '" . $city . "'" : $query . "";
            
            $postcode = (isset($this->request->post['postcode'])) ? $this->request->post['postcode'] : "";
            $query = (isset($this->request->post['postcode'])) ? $query . " , postcode = '" . $postcode . "'" : $query . "";
            
            $query = $query . " WHERE address_id = '" . (int)$address_id . "' ";
            $result = $this->db->query($query);
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
    }
    
    public function addAddress(){
        $json =array();
        
        if( !isset($this->request->post['address1']) || 
            !isset($this->request->post['customer_id']) || 
            !isset($this->request->post['firstname']) || 
            !isset($this->request->post['lastname']) || 
            !isset($this->request->post['city']) || 
            !isset($this->request->post['postcode']) ) {
                
            $json['success'] = false;
		    $json['message'] = "required inputs are missing";
                
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $customer_id = $this->request->post['customer_id'];
        
        try {
            $query = "INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . $customer_id . "'";
        
            $firstname = (isset($this->request->post['firstname'])) ? $this->request->post['firstname'] : "";
            $query = (isset($this->request->post['firstname'])) ? $query . " , firstname = '" . $firstname . "'" : $query . "";
            
            $lastname = (isset($this->request->post['lastname'])) ? $this->request->post['lastname'] : "";
            $query = (isset($this->request->post['lastname'])) ? $query . " , lastname = '" . $lastname . "'" : $query . "";
            
            $address1 = (isset($this->request->post['address1'])) ? $this->request->post['address1'] : "";
            $query = (isset($this->request->post['address1'])) ? $query . " , address_1 = '" . $address1 . "'" : $query . "";
            
            $address2 = (isset($this->request->post['address2'])) ? $this->request->post['address2'] : "";
            $query = (isset($this->request->post['address2'])) ? $query . " , address_2 = '" . $address2 . "'" : $query . "";
            
            $city = (isset($this->request->post['city'])) ? $this->request->post['city'] : "";
            $query = (isset($this->request->post['city'])) ? $query . " , city = '" . $city . "'" : $query . "";
            
            $postcode = (isset($this->request->post['postcode'])) ? $this->request->post['postcode'] : "";
            $query = (isset($this->request->post['postcode'])) ? $query . " , postcode = '" . $postcode . "'" : $query . "";
            
            $query = $query . " , country_id = '" . 18 . "'";
            
            $this->db->query($query);
            $new_address_id = $this->db->getLastId();
            
            
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data']['address_id'] = $new_address_id;
    		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
    }
    
    public function deleteAddress(){
        $json =array();
        
        if( !isset($this->request->post['address_id']) ) {
            
            $json['success'] = false;
		    $json['message'] = "address_id is missing";
                
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $address_id = $this->request->post['address_id'];
        // $customer_id = $this->request->post['customer_id'];
        
        try{
            $query = "DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . $address_id . "'";
            $result = $this->db->query($query);
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data'] = array();
    		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
    }
    
    public function getAddress()  {
        $json =array();
        
        if( !isset($this->request->post['customer_id']) ) {
            
            $json['success'] = false;
		    $json['message'] = "customer_id is missing";
                
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $customer_id = $this->request->post['customer_id'];
        
        //$query = ;
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . $customer_id . "'");
        
        $result = $query->rows;
        
        $count = 0;
        
        foreach ($result as $results) {
            $count++;
            
			$data['address'][] = array(
			    'address_id'     => $results['address_id'],
				'firstname'     => $results['firstname'],
				'lastname'     => $results['lastname'],
				'address_1'     => $results['address_1'],
				'address_2'     => $results['address_2'],
				'postcode'     => $results['postcode'],
				'city'     => $results['city'],
				'country'     => $results['country_id']
			);
		}
		
		if($count == 0) {
		    $json['success'] = true;
        	$json['message'] = "no data foound";
        	$json['data'] = array();
        	$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		} 
		else {
		    $json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data'] = $data;
    		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
        
    }
    
	public function activate(){
	    $this->load->language('api/customer');
	    
	    // Customer
			if ($this->request->post['customer_id']) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']);

				if (!$customer_info || !$this->customer->login($customer_info['email'], '', true)) {
					$json['error']['warning'] = $this->language->get('error_customer');
					$this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode("error customer"));
                    return;
				}
			}
		$customer_id = $this->request->post['customer_id'];
		$status = 1;
		$result = $this->db->query("UPDATE " . DB_PREFIX . "customer SET status = '" . (int)$status . "' WHERE customer_id = '" . (int)$customer_id . "'");
		$query = $this->db->query("SELECT status FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		//$query->row;
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($query->row));
	}
	
	public function deactivate(){
	    $this->load->language('api/customer');
	    // Customer
			if ($this->request->post['customer_id']) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']);

				if (!$customer_info || !$this->customer->login($customer_info['email'], '', true)) {
					$json['error']['warning'] = $this->language->get('error_customer');
					$this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode("error customer"));
                    return;
				}
			}
		$customer_id = $this->request->post['customer_id'];
		$status = 0;
		$result = $this->db->query("UPDATE " . DB_PREFIX . "customer SET status = '" . (int)$status . "' WHERE customer_id = '" . (int)$customer_id . "'");
		$query = $this->db->query("SELECT status FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		//$query->row;
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($query->row));
	}
}
