<?php
class ControllerApiCustomerLogin extends Controller {
    
    private function getSalt($email) {
	    $query = $this->db->query("SELECT salt FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	    
	    $result = $query->rows;
	    // return $query;
	    if((int)$query->num_rows == 0) {
	        return false;
	    }
	    else {
	        return $query->row["salt"];
	    }
	}
    
    private function checkCredentials($email, $password, $salt) {
	    $query = $this->db->query("SELECT customer_id, COUNT(*) FROM " . DB_PREFIX . "customer WHERE email = '" . $email . "' AND password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "'");
	    
	    $result = $query->rows;
	    // return $query;
	    if($result[0]["COUNT(*)"] == "0") {
	        return false;
	    }
	    else {
	        return $result[0]["customer_id"];
	    }
	}
	
	public function index() {
		//$this->load->language('api/login');

		$json = array();
		
		$r = $this->getSalt($this->request->post['email']);
		//print($r); die();
		
		$r2 = $this->checkCredentials($this->request->post['email'], $this->request->post['password'], $r);
		
		if($r2 == false) {
		    $json =array();
    		$json['success'] = false;
    		$json['message'] = "Email does not exist";
    		
		    $this->response->addHeader('Content-Type: application/json');
    		$this->response->setOutput(json_encode($json));
            return;
		}

		// $this->response->addHeader('Content-Type: application/json');
		// $this->response->setOutput(json_encode($r2));
		// print($r->num_rows); die();
        // return;
        
		$this->load->model('account/api');

		// Login with API Key
		$api_info = $this->model_account_api->login($this->request->post['username'], $this->request->post['key']);

		if ($api_info) {
				
			if (!$json) {
			    $json =array();
        		$json['success'] = true;
        		$json['message'] = "The request is successful";
        		 
		
				// $json['success'] = $this->language->get('text_success');

				$session = new Session($this->config->get('session_engine'), $this->registry);
				$session->start();
				
				$this->model_account_api->addApiSession($api_info['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']);
				
				$session->data['api_id'] = $api_info['api_id'];
				
				// Create Token
				$json['data']['api_taken'] = $session->getId();
				$json['data']['customer_id'] = $r2;
					
			} else {
				$json['error']['key'] = $this->language->get('error_key');
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
}
