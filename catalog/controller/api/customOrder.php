<?php
class ControllerApiCustomOrder extends Controller {
    
	public function add() {
		$this->load->language('api/order');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
	
            $erroMessage = $this->setOrderRequestValidationMessage($this->request->post);
            
                if(!empty($erroMessage)){
                    $json['error'] = $erroMessage['error'];
                    
                    unset($this->session->data['shipping_address']);
        			unset($this->session->data['shipping_method']);
        			unset($this->session->data['payment_address']);
        			unset($this->session->data['payment_method']);
        			unset($this->session->data['customer']);
                    
                    $this->response->addHeader('Content-Type: application/json');
    		        $this->response->setOutput(json_encode($json));
                }
            
                if (!$json) {
                    
                    $customerid         =   $this->customer->getId();
                    $customer_group_id  =   $this->customer->getGroupId();
                    $firstname          =   $this->customer->getFirstName();
                    $lastname           =   $this->customer->getLastName();
                    $email              =   $this->customer->getEmail();
                    $telephone          =   $this->customer->getTelephone();
                    
                    $order_data = array();
        
            		$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            		$order_data['store_id'] = $this->config->get('config_store_id');
            		$order_data['store_name'] = $this->config->get('config_name');
            		$order_data['store_url'] = $this->config->get('config_url');
            		
            		$this->session->data['customer'] = array(
        				'customer_id'       => $customerid,
        				'customer_group_id' => $customer_group_id,
        				'firstname'         => $firstname,
        				'lastname'          => $lastname,
        				'email'             => $email,
        				'telephone'         => $telephone,
        				'custom_field'      => isset($this->request->post['custom_field']) ? $this->request->post['custom_field'] : array()
        			);
                    
                    //set payment address and ship address to session also set the currency code to session
                    $this->setOrderSessionData($firstname,$lastname,$this->request->post);
                    
                    
                    // Customer Details
        			$order_data['customer_id'] = $this->session->data['customer']['customer_id'];
        			$order_data['customer_group_id'] = $this->session->data['customer']['customer_group_id'];
        			$order_data['firstname'] = $this->session->data['customer']['firstname'];
        			$order_data['lastname'] = $this->session->data['customer']['lastname'];
        			$order_data['email'] = $this->session->data['customer']['email'];
        			$order_data['telephone'] = $this->session->data['customer']['telephone'];
        			$order_data['custom_field'] = $this->session->data['customer']['custom_field'];
        
        			// Payment Details
        			$order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
        			$order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
        			$order_data['payment_company'] = $this->session->data['payment_address']['company'];
        			$order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
        			$order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
        			$order_data['payment_city'] = $this->session->data['payment_address']['city'];
        			$order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
        			$order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
        			$order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
        			$order_data['payment_country'] = $this->session->data['payment_address']['country'];
        			$order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
        			$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
        			$order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());
        
        			if (isset($this->session->data['payment_method']['title'])) {
        				$order_data['payment_method'] = $this->session->data['payment_method']['title'];
        			} else {
        				$order_data['payment_method'] = '';
        			}
        
        			if (isset($this->session->data['payment_method']['code'])) {
        				$order_data['payment_code'] = $this->session->data['payment_method']['code'];
        			} else {
        				$order_data['payment_code'] = '';
        			}
        			
        			
        			// Shipping Details
        			if ($this->cart->hasShipping()) {
        				$order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
        				$order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
        				$order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
        				$order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
        				$order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
        				$order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
        				$order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
        				$order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
        				$order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
        				$order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
        				$order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
        				$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
        				$order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());
        
        				if (isset($this->session->data['shipping_method']['title'])) {
        					$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
        				} else {
        					$order_data['shipping_method'] = '';
        				}
        
        				if (isset($this->session->data['shipping_method']['code'])) {
        					$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
        				} else {
        					$order_data['shipping_code'] = '';
        				}
        			} else {
        				$order_data['shipping_firstname'] = '';
        				$order_data['shipping_lastname'] = '';
        				$order_data['shipping_company'] = '';
        				$order_data['shipping_address_1'] = '';
        				$order_data['shipping_address_2'] = '';
        				$order_data['shipping_city'] = '';
        				$order_data['shipping_postcode'] = '';
        				$order_data['shipping_zone'] = '';
        				$order_data['shipping_zone_id'] = '';
        				$order_data['shipping_country'] = '';
        				$order_data['shipping_country_id'] = '';
        				$order_data['shipping_address_format'] = '';
        				$order_data['shipping_custom_field'] = array();
        				$order_data['shipping_method'] = '';
        				$order_data['shipping_code'] = '';
        			}
        			
        			
        			// Products
        			$order_data['products'] = array();
        
        			foreach ($this->cart->getProducts() as $product) {
        				$option_data = array();
        
        				foreach ($product['option'] as $option) {
        					$option_data[] = array(
        						'product_option_id'       => $option['product_option_id'],
        						'product_option_value_id' => $option['product_option_value_id'],
        						'option_id'               => $option['option_id'],
        						'option_value_id'         => $option['option_value_id'],
        						'name'                    => $option['name'],
        						'value'                   => $option['value'],
        						'type'                    => $option['type']
        					);
        				}
        
        				$order_data['products'][] = array(
        					'product_id' => $product['product_id'],
        					'name'       => $product['name'],
        					'model'      => $product['model'],
        					'option'     => $option_data,
        					'download'   => $product['download'],
        					'quantity'   => $product['quantity'],
        					'subtract'   => $product['subtract'],
        					'price'      => $product['price'],
        					'total'      => $product['total'],
        					'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
        					'reward'     => $product['reward']
        				);
        			}
        			
        			// Gift Voucher
        			$order_data['vouchers'] = array();
        
        			if (!empty($this->session->data['vouchers'])) {
        				foreach ($this->session->data['vouchers'] as $voucher) {
        					$order_data['vouchers'][] = array(
        						'description'      => $voucher['description'],
        						'code'             => token(10),
        						'to_name'          => $voucher['to_name'],
        						'to_email'         => $voucher['to_email'],
        						'from_name'        => $voucher['from_name'],
        						'from_email'       => $voucher['from_email'],
        						'voucher_theme_id' => $voucher['voucher_theme_id'],
        						'message'          => $voucher['message'],
        						'amount'           => $voucher['amount']
        					);
        				}
        			}
        			
        			// Order Totals
        			$this->load->model('setting/extension');
        
        			$totals = array();
        			$taxes = $this->cart->getTaxes();
        			$total = 0;
        
        			// Because __call can not keep var references so we put them into an array.
        			$total_data = array(
        				'totals' => &$totals,
        				'taxes'  => &$taxes,
        				'total'  => &$total
        			);
        
        			$sort_order = array();
        
        			$results = $this->model_setting_extension->getExtensions('total');
        
        			foreach ($results as $key => $value) {
        				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        			}
        
        			array_multisort($sort_order, SORT_ASC, $results);
        
        			foreach ($results as $result) {
        				if ($this->config->get('total_' . $result['code'] . '_status')) {
        					$this->load->model('extension/total/' . $result['code']);
        
        					// We have to put the totals in an array so that they pass by reference.
        					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
        				}
        			}
        
        			$sort_order = array();
        
        			foreach ($total_data['totals'] as $key => $value) {
        				$sort_order[$key] = $value['sort_order'];
        			}
        
        			array_multisort($sort_order, SORT_ASC, $total_data['totals']);
        
        			$order_data = array_merge($order_data, $total_data);
        
        			if (isset($this->request->post['comment'])) {
        				$order_data['comment'] = $this->request->post['comment'];
        			} else {
        				$order_data['comment'] = '';
        			}
        
        			if (isset($this->request->post['affiliate_id'])) {
        				$subtotal = $this->cart->getSubTotal();
        
        				// Affiliate
        				$this->load->model('account/customer');
        
        				$affiliate_info = $this->model_account_customer->getAffiliate($this->request->post['affiliate_id']);
        
        				if ($affiliate_info) {
        					$order_data['affiliate_id'] = $affiliate_info['customer_id'];
        					$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
        				} else {
        					$order_data['affiliate_id'] = 0;
        					$order_data['commission'] = 0;
        				}
        
        				// Marketing
        				$order_data['marketing_id'] = 0;
        				$order_data['tracking'] = '';
        			} else {
        				$order_data['affiliate_id'] = 0;
        				$order_data['commission'] = 0;
        				$order_data['marketing_id'] = 0;
        				$order_data['tracking'] = '';
        			}
        			
        			$order_data['language_id'] = $this->config->get('config_language_id');
        			$order_data['currency_id'] = $this->currency->getId($this->session->data['currency']); //4
        			$order_data['currency_code'] = $this->session->data['currency']; //BDT
        			$order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
        			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];
        			
        			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
        				$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        			} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
        				$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        			} else {
        				$order_data['forwarded_ip'] = '';
        			}
        
        			if (isset($this->request->server['HTTP_USER_AGENT'])) {
        				$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        			} else {
        				$order_data['user_agent'] = '';
        			}
        
        			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
        				$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        			} else {
        				$order_data['accept_language'] = '';
        			}
        			
        			$this->load->model('checkout/order');
        			
        			$json['success'] = true;
        			$json['order_id'] = $this->model_checkout_order->addOrder($order_data);
        
        			// Set the order history
        			if (isset($this->request->post['order_status_id'])) {
        				$order_status_id = $this->request->post['order_status_id'];
        			} else {
        				$order_status_id = $this->config->get('config_order_status_id');
        			}
        
        			$this->model_checkout_order->addOrderHistory($json['order_id'], $order_status_id);
        
        			// clear cart since the order has already been successfully stored.
        			$this->cart->clear();
        			
        		    unset($this->session->data['shipping_address']);
        			unset($this->session->data['shipping_method']);
        			unset($this->session->data['payment_address']);
        			unset($this->session->data['payment_method']);
        			unset($this->session->data['customer']);
                }
		
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function setOrderSessionData($firstname,$lastname,$data){
	    
	    $this->load->model('localisation/country');
            
        $country_info = $this->model_localisation_country->getCountry($data['country_id']);
        
        if ($country_info) {
				$country = $country_info['name'];
				$iso_code_2 = $country_info['iso_code_2'];
				$iso_code_3 = $country_info['iso_code_3'];
				$address_format = $country_info['address_format'];
		} else {
			$country = '';
			$iso_code_2 = '';
			$iso_code_3 = '';
			$address_format = '';
		}

		$this->load->model('localisation/zone');

		$zone_info = $this->model_localisation_zone->getZone($data['zone_id']);

		if ($zone_info) {
			$zone = $zone_info['name'];
			$zone_code = $zone_info['code'];
		} else {
			$zone = '';
			$zone_code = '';
		}
		
		$json = array();
	
	    //set shipping address to session
		$this->session->data['shipping_address'] = array(
					'firstname'      => $firstname,
					'lastname'       => $lastname,
					'company'        => $data['company'],
    				'address_1'      => $data['address_1'],
    				'address_2'      => $data['address_2'],
    				'postcode'       => $data['postcode'],
    				'city'           => $data['city'],
    				'zone_id'        => $data['zone_id'],
    				'zone'           => $zone,
    				'zone_code'      => $zone_code,
    				'country_id'     => $data['country_id'],
    				'country'        => $country,
    				'iso_code_2'     => $iso_code_2,
    				'iso_code_3'     => $iso_code_3,
    				'address_format' => $address_format,
    				'custom_field'   => isset($data['custom_field']) ? $data['custom_field'] : array()
				);
				
	
		
        $json['shipping_methods'] = array();
        
        
        //start set shipping methods to session

		$this->load->model('setting/extension');

		$results = $this->model_setting_extension->getExtensions('shipping');

		foreach ($results as $result) {
			if ($this->config->get('shipping_' . $result['code'] . '_status')) {
				$this->load->model('extension/shipping/' . $result['code']);

				$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

				if ($quote) {
					$json['shipping_methods'][$result['code']] = array(
						'title'      => $quote['title'],
						'quote'      => $quote['quote'],
						'sort_order' => $quote['sort_order'],
						'error'      => $quote['error']
					);
				}
			}
		}

		$sort_order = array();

		foreach ($json['shipping_methods'] as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $json['shipping_methods']);

		if ($json['shipping_methods']) {
			$this->session->data['shipping_methods'] = $json['shipping_methods'];
		} else {
			$json['error'] = $this->language->get('error_no_shipping');
		}
		
		
		if ($this->cart->hasShipping()) {
		    
		    $shipping = explode('.', $this->request->post['shipping_method']);
		    
		    $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
		}
		
		//end setting shipping methods
		
		
		
				
			// need revalidate	
		/*$this->session->data['shipping_method'] = [
			'title' => isset($data['shipping_method']['title']) ? $data['shipping_method']['title'] : 'Free Shipping' ,// 'Free Shipping'
			'code' => isset($data['shipping_method']['code']) ? $data['shipping_method']['code'] : 'free.free'   //'free.free'
		];*/
		
		
		//set payement address to session
		$this->session->data['payment_address'] = array(
				'firstname'      => $firstname,
				'lastname'       => $lastname,
				'company'        => $data['company'],
				'address_1'      => $data['address_1'],
				'address_2'      => $data['address_2'],
				'postcode'       => $data['postcode'],
				'city'           => $data['city'],
				'zone_id'        => $data['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $data['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => isset($data['custom_field']) ? $data['custom_field'] : array()
		);
		
		
		    //setting payment methods to session 
		
		        $totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;

				// Because __call can not keep var references so we put them into an array. 
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				$this->load->model('setting/extension');

				$sort_order = array();

				$results = $this->model_setting_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get('total_' . $result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				// Payment Methods
				$json['payment_methods'] = array();

				$this->load->model('setting/extension');

				$results = $this->model_setting_extension->getExtensions('payment');

				$recurring = $this->cart->hasRecurringProducts();

				foreach ($results as $result) {
					if ($this->config->get('payment_' . $result['code'] . '_status')) {
						$this->load->model('extension/payment/' . $result['code']);

						$method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

						if ($method) {
							if ($recurring) {
								if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
									$json['payment_methods'][$result['code']] = $method;
								}
							} else {
								$json['payment_methods'][$result['code']] = $method;
							}
						}
					}
				}

				$sort_order = array();

				foreach ($json['payment_methods'] as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $json['payment_methods']);

				if ($json['payment_methods']) {
					$this->session->data['payment_methods'] = $json['payment_methods'];
				} else {
					$json['error'] = $this->language->get('error_no_payment');
				}
				
		//end setting payment methods
		
		
		//need to revalidate the payment method
		/*$this->session->data['payment_method'] = [
				'title' => isset($data['payment_method']['title']) ? $data['payment_method']['title'] : 'Cash On Delivery' , //'Cash On Delivery'
				'code' => isset($data['payment_method']['code']) ? $data['payment_method']['code'] : 'cod'   //'cod'
		];*/
		
	//This is the original commented out by Halima	
	//	$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];
		
		$this->session->data['payment_method'] = $data['payment_method'];

		$this->session->data['currency'] = $data['currency'];

		return true;
	}
	
	public function setOrderRequestValidationMessage($requestData=[]){
	    
	    if(!isset($requestData['company']) || empty($requestData['company'])){
                return ['error'=>'company is required'];
        }
        if(!isset($requestData['address_1']) || empty($requestData['address_1'])){
            return ['error'=>'address_1 is required'];
        }
        if(!isset($requestData['address_2']) || empty($requestData['address_2'])){
            return ['error'=>'address_2 is required'];
        }
        if(!isset($requestData['postcode']) || empty($requestData['postcode'])){
            return ['error'=>'postcode is required'];
        }
        if(!isset($requestData['city']) || empty($requestData['city'])){
            return ['error'=>'city is required'];
        }
        if(!isset($requestData['zone_id']) || empty($requestData['zone_id'])){
            return ['error'=>'zone_id is required'];
        }
        if(!isset($requestData['country_id']) || empty($requestData['country_id'])){
            return ['error'=>'country_id is required'];
        }
        if(!isset($requestData['payment_method']) || empty($requestData['payment_method'])){
            return ['error'=>'payment method title is required'];
        }
        if(!isset($requestData['shipping_method']) || empty($requestData['shipping_method'])){
            return ['error'=>'payment method code is required'];
        }
        
        if(!isset($requestData['currency']) || empty($requestData['currency'])){
            return ['error'=>'currency is required'];
        }
        if(!isset($requestData['order_status_id']) || empty($requestData['order_status_id'])){
            return ['error'=>'order_status_id is required'];
        }
        
        return [];
	}
	

	public function edit() {
		$this->load->language('api/order');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('checkout/order');

			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$order_info = $this->model_checkout_order->getOrder($order_id);

			if ($order_info) {
				// Customer
				if (!isset($this->session->data['customer'])) {
					$json['error'] = $this->language->get('error_customer');
				}

				// Payment Address
				if (!isset($this->session->data['payment_address'])) {
					$json['error'] = $this->language->get('error_payment_address');
				}

				// Payment Method
				if (!$json && !empty($this->request->post['payment_method'])) {
					if (empty($this->session->data['payment_methods'])) {
						$json['error'] = $this->language->get('error_no_payment');
					} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
						$json['error'] = $this->language->get('error_payment_method');
					}

					if (!$json) {
						$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];
					}
				}

				if (!isset($this->session->data['payment_method'])) {
					$json['error'] = $this->language->get('error_payment_method');
				}

				// Shipping
				if ($this->cart->hasShipping()) {
					// Shipping Address
					if (!isset($this->session->data['shipping_address'])) {
						$json['error'] = $this->language->get('error_shipping_address');
					}

					// Shipping Method
					if (!$json && !empty($this->request->post['shipping_method'])) {
						if (empty($this->session->data['shipping_methods'])) {
							$json['error'] = $this->language->get('error_no_shipping');
						} else {
							$shipping = explode('.', $this->request->post['shipping_method']);

							if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
								$json['error'] = $this->language->get('error_shipping_method');
							}
						}

						if (!$json) {
							$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
						}
					}

					if (!isset($this->session->data['shipping_method'])) {
						$json['error'] = $this->language->get('error_shipping_method');
					}
				} else {
					unset($this->session->data['shipping_address']);
					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
				}

				// Cart
				if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
					$json['error'] = $this->language->get('error_stock');
				}

				// Validate minimum quantity requirements.
				$products = $this->cart->getProducts();

				foreach ($products as $product) {
					$product_total = 0;

					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}

					if ($product['minimum'] > $product_total) {
						$json['error'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);

						break;
					}
				}

				if (!$json) {
					$json['success'] = $this->language->get('text_success');
					
					$order_data = array();

					// Store Details
					$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
					$order_data['store_id'] = $this->config->get('config_store_id');
					$order_data['store_name'] = $this->config->get('config_name');
					$order_data['store_url'] = $this->config->get('config_url');

					// Customer Details
					$order_data['customer_id'] = $this->session->data['customer']['customer_id'];
					$order_data['customer_group_id'] = $this->session->data['customer']['customer_group_id'];
					$order_data['firstname'] = $this->session->data['customer']['firstname'];
					$order_data['lastname'] = $this->session->data['customer']['lastname'];
					$order_data['email'] = $this->session->data['customer']['email'];
					$order_data['telephone'] = $this->session->data['customer']['telephone'];
					$order_data['custom_field'] = $this->session->data['customer']['custom_field'];

					// Payment Details
					$order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
					$order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
					$order_data['payment_company'] = $this->session->data['payment_address']['company'];
					$order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
					$order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
					$order_data['payment_city'] = $this->session->data['payment_address']['city'];
					$order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
					$order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
					$order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
					$order_data['payment_country'] = $this->session->data['payment_address']['country'];
					$order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
					$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
					$order_data['payment_custom_field'] = $this->session->data['payment_address']['custom_field'];

					if (isset($this->session->data['payment_method']['title'])) {
						$order_data['payment_method'] = $this->session->data['payment_method']['title'];
					} else {
						$order_data['payment_method'] = '';
					}

					if (isset($this->session->data['payment_method']['code'])) {
						$order_data['payment_code'] = $this->session->data['payment_method']['code'];
					} else {
						$order_data['payment_code'] = '';
					}

					// Shipping Details
					if ($this->cart->hasShipping()) {
						$order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
						$order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
						$order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
						$order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
						$order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
						$order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
						$order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
						$order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
						$order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
						$order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
						$order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
						$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
						$order_data['shipping_custom_field'] = $this->session->data['shipping_address']['custom_field'];

						if (isset($this->session->data['shipping_method']['title'])) {
							$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
						} else {
							$order_data['shipping_method'] = '';
						}

						if (isset($this->session->data['shipping_method']['code'])) {
							$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
						} else {
							$order_data['shipping_code'] = '';
						}
					} else {
						$order_data['shipping_firstname'] = '';
						$order_data['shipping_lastname'] = '';
						$order_data['shipping_company'] = '';
						$order_data['shipping_address_1'] = '';
						$order_data['shipping_address_2'] = '';
						$order_data['shipping_city'] = '';
						$order_data['shipping_postcode'] = '';
						$order_data['shipping_zone'] = '';
						$order_data['shipping_zone_id'] = '';
						$order_data['shipping_country'] = '';
						$order_data['shipping_country_id'] = '';
						$order_data['shipping_address_format'] = '';
						$order_data['shipping_custom_field'] = array();
						$order_data['shipping_method'] = '';
						$order_data['shipping_code'] = '';
					}

					// Products
					$order_data['products'] = array();

					foreach ($this->cart->getProducts() as $product) {
						$option_data = array();

						foreach ($product['option'] as $option) {
							$option_data[] = array(
								'product_option_id'       => $option['product_option_id'],
								'product_option_value_id' => $option['product_option_value_id'],
								'option_id'               => $option['option_id'],
								'option_value_id'         => $option['option_value_id'],
								'name'                    => $option['name'],
								'value'                   => $option['value'],
								'type'                    => $option['type']
							);
						}

						$order_data['products'][] = array(
							'product_id' => $product['product_id'],
							'name'       => $product['name'],
							'model'      => $product['model'],
							'option'     => $option_data,
							'download'   => $product['download'],
							'quantity'   => $product['quantity'],
							'subtract'   => $product['subtract'],
							'price'      => $product['price'],
							'total'      => $product['total'],
							'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
							'reward'     => $product['reward']
						);
					}

					// Gift Voucher
					$order_data['vouchers'] = array();

					if (!empty($this->session->data['vouchers'])) {
						foreach ($this->session->data['vouchers'] as $voucher) {
							$order_data['vouchers'][] = array(
								'description'      => $voucher['description'],
								'code'             => token(10),
								'to_name'          => $voucher['to_name'],
								'to_email'         => $voucher['to_email'],
								'from_name'        => $voucher['from_name'],
								'from_email'       => $voucher['from_email'],
								'voucher_theme_id' => $voucher['voucher_theme_id'],
								'message'          => $voucher['message'],
								'amount'           => $voucher['amount']
							);
						}
					}

					// Order Totals
					$this->load->model('setting/extension');

					$totals = array();
					$taxes = $this->cart->getTaxes();
					$total = 0;
					
					// Because __call can not keep var references so we put them into an array. 
					$total_data = array(
						'totals' => &$totals,
						'taxes'  => &$taxes,
						'total'  => &$total
					);
			
					$sort_order = array();

					$results = $this->model_setting_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get('total_' . $result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);
							
							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($total_data['totals'] as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $total_data['totals']);

					$order_data = array_merge($order_data, $total_data);

					if (isset($this->request->post['comment'])) {
						$order_data['comment'] = $this->request->post['comment'];
					} else {
						$order_data['comment'] = '';
					}

					if (isset($this->request->post['affiliate_id'])) {
						$subtotal = $this->cart->getSubTotal();

						// Affiliate
						$this->load->model('account/customer');

						$affiliate_info = $this->model_account_customer->getAffiliate($this->request->post['affiliate_id']);

						if ($affiliate_info) {
							$order_data['affiliate_id'] = $affiliate_info['customer_id'];
							$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
						} else {
							$order_data['affiliate_id'] = 0;
							$order_data['commission'] = 0;
						}
					} else {
						$order_data['affiliate_id'] = 0;
						$order_data['commission'] = 0;
					}

					$this->model_checkout_order->editOrder($order_id, $order_data);

					// Set the order history
					if (isset($this->request->post['order_status_id'])) {
						$order_status_id = $this->request->post['order_status_id'];
					} else {
						$order_status_id = $this->config->get('config_order_status_id');
					}
					
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
				}
			} else {
				$json['error'] = $this->language->get('error_not_found');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function delete() {
		$this->load->language('api/order');

		$json = array();

		if (!isset($this->session->data['api_id']) && !isset($this->request->get['order_id'])) {
		    
			$json['success'] = false;
    		$json['message'] = "appi id, order id requied";
            		
            $this->response->addHeader('Content-Type: application/json');
    	    $this->response->setOutput(json_encode($json));
            return;
		} 
		else {
		    
			$this->load->model('checkout/order');

			$order_id = $this->request->get['order_id'];
			
			try {

    			$order_info = $this->model_checkout_order->getOrder($order_id);
    
    			if ($order_info) {
    				$this->model_checkout_order->deleteOrder($order_id);
    				
    				$json['success'] = true;
            		$json['message'] = "The request is successful";
            		
            		$json['data'] = array();
            		
            		$this->response->addHeader('Content-Type: application/json');
        		    $this->response->setOutput(json_encode($json));
        		    return;
    			} 
    			else {
    			    
    			    $json['success'] = false;
            		$json['message'] = $this->language->get('error_not_found');
                    		
                    $this->response->addHeader('Content-Type: application/json');
            	    $this->response->setOutput(json_encode($json));
                    return;
    			}
			}
			catch (Exception $e) {
	        
        		$json['success'] = false;
        		$json['message'] = $e->getMessage();
    		
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }
		}
		
		$json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
	}

	public function info() {

		$this->load->language('api/order');

		$json = array();

		//if (!isset($this->session->data['api_id'])) {
		//	$json['error'] = $this->language->get('error_permission');
		// else {
		
		$this->load->model('checkout/order');

		if (!isset($this->request->get['order_id'])) {
		    
			$json['success'] = false;
    		$json['message'] = "appi id, order id requied";
            		
            $this->response->addHeader('Content-Type: application/json');
    	    $this->response->setOutput(json_encode($json));
            return;
		} 
		
		try {
		    
            $order_id = $this->request->get['order_id'];
    		$order_info = $this->model_checkout_order->getOrder($order_id);
    
    		if ($order_info) {
    		    
    		    $json['success'] = true;
        		$json['message'] = "The request is successful";
        		
        		$json['data'] = $order_info;
        		
        		$this->response->addHeader('Content-Type: application/json');
    		    $this->response->setOutput(json_encode($json));
    		    return;
    		} 
    		else {
    		    
    			$json['success'] = false;
        		$json['message'] = $this->language->get('error_not_found');
                		
                $this->response->addHeader('Content-Type: application/json');
        	    $this->response->setOutput(json_encode($json));
                return;
    		}
		}
		catch (Exception $e) {
	        
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
			
		//}

		$json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
	}

	public function history() {
		$this->load->language('api/order');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Add keys for missing post vars
			$keys = array(
				'order_status_id',
				'notify',
				'override',
				'comment'
			);

			foreach ($keys as $key) {
				if (!isset($this->request->post[$key])) {
					$this->request->post[$key] = '';
				}
			}

			$this->load->model('checkout/order');

			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$order_info = $this->model_checkout_order->getOrder($order_id);

			if ($order_info) {
				$this->model_checkout_order->addOrderHistory($order_id, $this->request->post['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);

				$json['success'] = $this->language->get('text_success');
			} else {
				$json['error'] = $this->language->get('error_not_found');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getTotalOrders($customer_id) {
        
		$query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int)$customer_id . "'");
		
		$result = $query->rows;
		
        return $result[0]["COUNT(*)"];
	}
	
	public function list() {
	    
	    $limit = 2;
        if(isset($this->request->post['limit'])) {
            $limit = $this->request->post['limit'];
        }
        
        $customer_id = $this->request->post['customer_id'];
        
        $tpc = (int)$this->getTotalOrders($customer_id);
        
        if($tpc == 0) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = "no data found";
    		
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $total_page_count = (float)($tpc / $limit);
        if($total_page_count > (int)$total_page_count)
            $total_page_count = (int)$total_page_count + 1;
        
        $page = $this->request->post['page'];
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int)$customer_id . "' LIMIT " . ((((int)$page) - 1) * ((int)$limit)) . ", " . (int)$limit );
		
		$result = $query->rows;
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		$json['total_page_count'] = (int)$total_page_count;
		
		foreach ($result as $results) {
			$data['wishlist'][] = array(
				'order_id'     => $results['order_id']
			);
		}
        $json['data'] = $data['wishlist'];
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function getProductsInOrder() {
	    
	    $order_id = $this->request->post['order_id'];
	    
	    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
	    
	    $query = $this->db->query("SELECT " 
	    . DB_PREFIX . "order.order_id, " 
	    . DB_PREFIX . "order_product.product_id, " 
	    . DB_PREFIX . "order_product.name, " 
	    . DB_PREFIX . "order_product.model, "
	    . DB_PREFIX . "order_product.quantity, "
	    . DB_PREFIX . "order_product.price, "
	    . DB_PREFIX . "order_product.total, "
	    . DB_PREFIX . "order_product.tax, "
	    . DB_PREFIX . "order_product.reward "
	    . " FROM " 
	    . DB_PREFIX . "order INNER JOIN " . DB_PREFIX . "order_product ON " . DB_PREFIX . "order.order_id = " . DB_PREFIX . "order_product.order_id " . 
	    "WHERE " . DB_PREFIX . "order.order_id = '" . (int)$order_id . "'");
		
		if($query->num_rows > 0) {
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		} else {
    		$json['success'] = false;
    		$json['message'] = "no data found";
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$result = $query->rows;
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		
		foreach ($result as $results) {
			$data['products'][] = array(
				'product_id'     => $results['product_id'],
				'name'     => $results['name'],
				'model'     => $results['model'],
				'quantity'     => $results['quantity'],
				'price'     => $results['price'],
				'total'     => $results['total'],
				'tax'     => $results['tax'],
				'reward'     => $results['reward']
			);
		}
        $json['data'] = $data;
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function getPastOrders() {
	    $json =array();
	    
	    $customer_id = $this->request->post['customer_id'];
	    
	    $query = $this->db->query("SELECT * FROM ". DB_PREFIX . "order WHERE customer_id = '" . (int)$customer_id . "' AND order_status_id > 2");
		
		if($query->num_rows > 0) {
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		} else {
    		$json['success'] = false;
    		$json['message'] = "no data found";
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$result = $query->rows;

		foreach ($result as $results) {
			$data['order_ids'][] = array(
				'order_id'     => $results['order_id'],
				'total'     => $results['total']
			);
		}
        $json['data'] = $data;
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function getPendingOrders() {
	    
	    $customer_id = $this->request->post['customer_id'];
	    
	    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int)$customer_id . "' AND order_status_id IN ('1','2')");
		
		if($query->num_rows > 0) {
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		} else {
    		$json['success'] = false;
    		$json['message'] = "no data found";
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$result = $query->rows;
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		
		foreach ($result as $results) {
			$data['order_ids'][] = array(
				'order_id'     => $results['order_id'],
				'total'     => $results['total']
			);
		}
        $json['data'] = $data;
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function getOrderList() {
	    
	    $customer_id = $this->request->post['customer_id'];
	    
	    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int)$customer_id . "' AND order_status_id IN ('1','2','5')");
		
		if($query->num_rows > 0) {
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		} else {
    		$json['success'] = false;
    		$json['message'] = "no data found";
    		
    		$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$result = $query->rows;
		
		$json =array();
		$json['success'] = true;
		$json['message'] = "The request is successful";
		
		foreach ($result as $results) {
			$data['order_ids'][] = array(
				'order_id'     => $results['order_id'],
				'order_status'     => $results['order_status_id'],
				'total'     => $results['total']
			);
		}
        $json['data'] = $data;
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	
}