<?php
class ControllerAPIRegister extends Controller {
	private $error = array();
	
	private function checkDuplicate($email) {
	    $query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	    
	    $result = $query->rows;
	    
	    if($result[0]["COUNT(*)"] == "0") {
	        return true;
	    }
	    else {
	        return false;
	    }
	}

	public function index() {
		$this->load->language('account/register');
		$json =array();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			//$customer_id = $this->model_account_customer->addCustomerFromApp($this->request->post);
			
			$data = $this->request->post;

			if(!$this->checkDuplicate($this->db->escape($data['email']))) {
			    $json['success'] = false;
        		$json['message'] = "Email already exist";
        		$this->response->addHeader('Content-Type: application/json');
        		$this->response->setOutput(json_encode($json));
        		return;
			}
			
			$this->load->model('account/customer_group');

    		$customer_group_info = $this->model_account_customer_group->getCustomerGroup(1);
    
    		$result = $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET " . 
    		"language_id = '" . (int)$this->db->escape($data['language_id']) . 
    		"', customer_group_id = '1" . 
    		"', firstname = '" . $this->db->escape($data['firstname']) . 
    		"', lastname = '" . $this->db->escape($data['lastname']) . 
    		"', email = '" . $this->db->escape($data['email']) . 
    		"', uid = '" . $this->db->escape($data['uid']) . 
    		"', telephone = '" . $this->db->escape($data['telephone']) .
    		"', salt = '" . $this->db->escape($salt = token(9)) . 
    		"', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . 
    		"', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . 
    		"', status = '" . (int)!$customer_group_info['approval'] . 
    		"', date_added = NOW()");
    		
    		$new_id = $this->db->getLastId();
    		
    		$json['success'] = true;
		    $json['message'] = "The request is successful";
		    $json['customer_id'] = $new_id;

			$this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		$json['success'] = false;
		$json['message'] = $this->error;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		return;
	}

	private function validate() {
	    
	    if(isset($this->request->post['email'])) {
	        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
    			$this->error['email'] = $this->language->get('error_email');
    		}
	    } else {
	        $this->error['email'] = $this->language->get('error_email');
	    }
	    
	    if(isset($this->request->post['password'])) {
	        if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
    			$this->error['password'] = $this->language->get('error_password');
    		}
	    } else {
	        $this->error['password'] = $this->language->get('error_password');
	    }
		
		if(!isset($this->request->post['language_id'])) {
		    $this->error['language'] = "language not set";
		}
		
		if(!isset($this->request->post['firstname'])) {
		    $this->error['firstname'] = "firstname not set";
		}
		
		if(!isset($this->request->post['lastname'])) {
		    $this->error['lastname'] = "lastname not set";
		}
		
		if(!isset($this->request->post['uid'])) {
		    $this->error['uid'] = "uid not set";
		}
		
		if(!isset($this->request->post['telephone'])) {
		    $this->error['telephone'] = "telephone not set";
		}
		
		return !$this->error;
	}

}