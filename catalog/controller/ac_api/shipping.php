<?php 
/**
 * shipping.php
 *
 * Shipping method management 
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiShipping extends RestApiController {

	public function methods() {

    $this->auth('get');
    $this->load->language('ac_api/checkout');

		//set shipping address if not set
		if(!isset($this->session->data['shipping_address']) || (isset($this->session->data['shipping_address']) && !$this->session->data['shipping_address'])){
			$this->model('account/address');
			$address = $this->model_account_address->getAddress($this->customer->getAddressId());
			if($address){
				$this->session->data['shipping_address'] = $address;
			}else{
				$this->json['error'][] = $this->language->get('default_address_not_valid');
				$this->sendResponse();
			}
		}
		
    // Shipping Methods
    $method_data = array();
      
		if (isset($this->session->data['shipping_address'])) {

			$this->load->model('setting/extension');

			$results = $this->model_setting_extension->getExtensions('shipping');

			foreach ($results as $result) {
				if ($this->config->get('shipping_' . $result['code'] . '_status')) {
					$this->load->model('extension/shipping/' . $result['code']);

					$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

					if ($quote) {
						$method_data[$result['code']] = array(
							'title'      => $quote['title'],
							'quote'      => $quote['quote'],
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['shipping_methods'] = $method_data;
			
			$this->json['data'] = $method_data;
		
		}else{
			$this->json['error'][] = $this->language->get('shipping_address_not_found');
		}
    
    $this->sendResponse();
	}  

}