<?php 
/**
 * product.php
 *
 * Product management / Search
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiProduct extends RestApiController {

  //end-point call for offer page
  public function getOffers()
  {
    
    $this->auth('post');

    //$this->load->language('ac_api/product');
    $this->model('catalog/category');
    $this->model('catalog/product');
    $this->model('tool/image');

    $post = $this->getPost();

    if (isset($post['sort']) && $post['sort']) {
      $sort = $post['sort'];
      } else {
          $sort = 'p.sort_order';
    }

    if (isset($post['order']) && $post['order']) {
          $order = $post['order'];
      } else {
          $order = 'ASC';
    }

    if (isset($post['page']) && $post['page']) {
          $page = $post['page'];
      } else {
          $page = 1;
    }

    if (isset($post['limit']) && $post['limit']) {
          $limit = (int)$post['limit'];
      } else {
          $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
    }
    
    $filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
    );
    
    $product_total = $this->model_catalog_product->getTotalProductSpecials();
    $results = $this->model_catalog_product->getProductSpecials($filter_data);

    $products = array();
    foreach ($results as $result) {
      $product = $this->genrateProductData($result);
      if($product){
        $products[] = $product;
      }
    }
    
    $data['products'] = $products;

    //Pagination
    $total_pages = ceil($product_total / $limit);
    $next_page = $page+1;

    $data['pagination'] = array(
      'product_total' => $product_total,
      'total_pages'   => $total_pages,
      'current'       => $page,
      'next'          => ($next_page <= $total_pages)?$next_page:0,
      'limit'         => $limit
    );
    
    $this->json['data'] = $data;
    return $this->sendResponse();

  }

  //End-point get all product
  public function getProducts()
  {
    
    $this->auth('post');

    $this->load->language('ac_api/category');
    $this->model('catalog/category');
    $this->model('catalog/product');
    $this->model('tool/image');

    $data['products'] = $this->getProductData(true);
    
    $this->json['data'] = $data;
    return $this->sendResponse();

  }

  public function details(){

    $this->auth('post');

    $post = $this->getPost();
    $this->validateData(['product_id'],$post);
    
    $this->load->language('ac_api/category');
    $this->model('catalog/category');
    $this->model('catalog/product');
    $this->model('tool/image');
    $this->model('catalog/review');

    $product = array();
    $product_info = $this->model_catalog_product->getProduct($post['product_id']);

    if ($product_info) {

      $product['name'] = $product_info['name'];
      $product['product_id'] = (int)$post['product_id'];

      $product['text_minimum'] = $product_info['minimum'];
      $product['total_review'] = $product_info['reviews'];

			$product['manufacturer'] = $product_info['manufacturer'];
			$product['model'] = $product_info['model'];
			$product['reward'] = $product_info['reward'];
			$product['points'] = $product_info['points'];
			$product['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

      if ($product_info['quantity'] <= 0) {
				$product['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$product['stock'] = $product_info['quantity'];
			} else {
				$product['stock'] = $this->language->get('text_instock');
      }
      
      if ($product_info['image']) {
				$product['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$product['popup'] = '';
      }
      
      if ($product_info['image']) {
				$product['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$product['thumb'] = '';
      }
      
      $product['images'] = array();
			$results = $this->model_catalog_product->getProductImages($post['product_id']);

			foreach ($results as $result) {
				$product['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_height'))
				);
      }
      
      if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
        $product['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        $product['price_int'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'],'','');
			} else {
        $product['price'] = false;
        $product['price_int'] = false;
      }
      
      if ((float)$product_info['special']) {
        $product['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        $product['special_int'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'],'','');
			} else {
        $product['special'] = false;
        $product['special_int'] = false;
      }
      
      if ($this->config->get('config_tax')) {
        $product['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
        $product['tax_int'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency'], '', '');
			} else {
				$product['tax'] = $product['tax_int']= false;
      }
      
      //discounts
      $discounts = $this->model_catalog_product->getProductDiscounts($post['product_id']);

			$product['discounts'] = array();

			foreach ($discounts as $discount) {
				$product['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
				);
      }

      //Product options
      $product['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($post['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
              $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
              $price_int = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency'], '', '');
						} else {
							$price = $price_int = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
              'price'                   => $price,
              'price_int'               => $price_int,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$product['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
      }

      if ($product_info['minimum']) {
				$product['minimum'] = $product_info['minimum'];
			} else {
				$product['minimum'] = 1;
      }
      
      $product['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$product['review_guest'] = true;
			} else {
				$product['review_guest'] = false;
			}
      
      // if ($this->customer->isLogged()) {
			// 	$product['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			// } else {
			// 	$product['customer_name'] = 'Enter Your Name';
			// }
			// if ($this->customer->isLogged()) {
			// 	$product['customer_email'] = $this->customer->getEmail();
			// } else {
			// 	$product['customer_email'] = 'Enter Your Email';
      // }
      
      $product['reviews'] = (int)$product_info['reviews'];
      $product['rating'] = (int)$product_info['rating'];
      
      $product['share'] = $this->url->link('product/product', 'product_id=' . (int)$post['product_id']);
      
      $product['attribute_groups'] = $this->model_catalog_product->getProductAttributes($post['product_id']);
      
      $product['related'] = array();
      $results = $this->model_catalog_product->getProductRelated($post['product_id']);

      foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$product['related'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating
				);
      }
      
      $product['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$product['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
      }
      
      $product['customer_reviews'] = array();
      $results = $this->model_catalog_review->getReviewsByProductId($post['product_id'], 0, 50);
      foreach ($results as $result) {
        $product['customer_reviews'][] = array(
          'name'     => $result['author'],
          'text'       => nl2br($result['text']),
          'rating'     => (int)$result['rating'],
          'images'  => json_decode($result['images']),
          'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        );
      }

      $this->model_catalog_product->updateViewed($post['product_id']);

      $data['product'] = $product;
      
    }else{
      $data['product'] = array();

    }


    $this->json['data'] = $data;
    return $this->sendResponse();
  }

  public function addReview()
  {
    $this->auth('post');
    $this->load->language('ac_api/cart');
        
    $post = $this->getPost();
    $this->validateData(['product_id','review','rating'],$post);

    if(isset($post['images']) && $post['images']){
      $post['review_images'] = $post['images'];
      unset($post['images']);
    }
    
    $post['customer_id'] = $this->customer->getId();
    $post['author'] = $this->customer->getFirstName()." ".$this->customer->getLastName();

    $review_id = $this->model_ac_api_common->addProductReview($post);


    if($review_id){
      $this->json['data']['review_id'] = $review_id;
      $this->json['data']['review_data'] = $post;
    }else{
      $this->json['error'][] = $this->language->get('review_add_error');
    }


    return $this->sendResponse();

  }

  public function search()
  {
    
    $this->auth('post');
    $post = $this->getPost();
    $this->validateData(['search'],$post);

    $this->model('catalog/category');
    $this->model('catalog/product');
    $this->model('tool/image');

    $data['products'] = $this->getProductData();
    
    $this->json['data'] = $data;
    return $this->sendResponse();

  }
  
  public function getQuote()
  {
    
    $this->auth('post');
    $post = $this->getPost();
    $this->validateData(['name','phone','email','product_data'],$post);


    $quote_id = $this->model_ac_api_common->saveQuote($post);

    if($quote_id){
      $this->json['data']['quote_request_no'] = $quote_id;
      $this->json['data']['message'] = $this->language->get('get_quote_success');
    }else{
      $this->json['error'][] = $this->language->get('get_quote_error');
    }

    return $this->sendResponse();

  }

}
