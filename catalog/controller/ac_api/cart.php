<?php 
/**
 * cart.php
 *
 * Cart management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiCart extends RestApiController {
  
  public function list(){
    
    $this->auth('get');

    $this->load->language('ac_api/cart');

    $this->json['data']['products'] = $this->cart->getProducts();
    
    if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {

      // Stock
      if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
        $this->json['error']['stock'] = $this->language->get('error_stock');
      }

      // Products
      $json['products'] = array();

      $products = $this->cart->getProducts();

      foreach ($products as $product) {
        $product_total = 0;

        foreach ($products as $product_2) {
          if ($product_2['product_id'] == $product['product_id']) {
            $product_total += $product_2['quantity'];
          }
        }

        if ($product['minimum'] > $product_total) {
          $this->json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
        }

        $option_data = array();

        foreach ($product['option'] as $option) {
          $option_data[] = array(
            'product_option_id'       => $option['product_option_id'],
            'product_option_value_id' => $option['product_option_value_id'],
            'name'                    => $option['name'],
            'value'                   => $option['value'],
            'type'                    => $option['type']
          );
        }

        if ($product['image']) {
          $this->load->model('tool/image');
          $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
        } else {
          $image = '';
        }

        $json['products'][] = array(
          'cart_id'    => $product['cart_id'],
          'thumb'     => $image,
          'product_id' => $product['product_id'],
          'name'       => $product['name'],
          'model'      => $product['model'],
          'option'     => $option_data,
          'quantity'   => $product['quantity'],
          'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
          'shipping'   => $product['shipping'],
          'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency'],'' , false),
          'total'      => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'],
          'reward'     => $product['reward'],
          'currency'  => $this->session->data['currency'],
          'rating'    =>  $product['rating'],
          'price_with_currency'  => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency'])
        );
      }

      // Voucher
      $json['vouchers'] = array();

      if (!empty($this->session->data['vouchers'])) {
        foreach ($this->session->data['vouchers'] as $key => $voucher) {
          $json['vouchers'][] = array(
            'code'             => $voucher['code'],
            'description'      => $voucher['description'],
            'from_name'        => $voucher['from_name'],
            'from_email'       => $voucher['from_email'],
            'to_name'          => $voucher['to_name'],
            'to_email'         => $voucher['to_email'],
            'voucher_theme_id' => $voucher['voucher_theme_id'],
            'message'          => $voucher['message'],
            'price'            => $this->currency->format($voucher['amount'], $this->session->data['currency']),			
            'amount'           => $voucher['amount']
          );
        }
      }
    
      // Totals
      $this->load->model('setting/extension');

      $totals = array();
      $taxes = $this->cart->getTaxes();
      $total = 0;

      // Because __call can not keep var references so we put them into an array. 
      $total_data = array(
        'totals' => &$totals,
        'taxes'  => &$taxes,
        'total'  => &$total
      );
      
      $sort_order = array();

      $results = $this->model_setting_extension->getExtensions('total');

      foreach ($results as $key => $value) {
        $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
      }

      array_multisort($sort_order, SORT_ASC, $results);

      foreach ($results as $result) {
        if ($this->config->get('total_' . $result['code'] . '_status')) {
          $this->load->model('extension/total/' . $result['code']);
          
          // We have to put the totals in an array so that they pass by reference.
          $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
        }
      }

      $sort_order = array();

      foreach ($totals as $key => $value) {
        $sort_order[$key] = $value['sort_order'];
      }

      array_multisort($sort_order, SORT_ASC, $totals);

      $json['totals'] = array();

      foreach ($totals as $total) {
        $json['totals'][] = array(
          'title' => $total['title'],
          'text_int' => $this->currency->format($total['value'], $this->session->data['currency'],'',false),
          'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
        );
      }
    

      $this->json['data'] = $json;
      
    }else{
      $this->json['data']['products'] = array();
      $this->json['data']['message'] = $this->language->get('cart_empty');
    }
    
    return $this->sendResponse();

  }

  public function add(){
    
    $this->auth('post');
    $this->load->language('ac_api/cart');
        
    $post = $this->getPost();
    $this->validateData(['product_id','quantity'],$post);
    
    $this->load->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($post['product_id']);
    
    if($product_info){
      
      $post["option"] = html_entity_decode($post["option"]);
      if (isset($post['option']) && $post['option']) {
        $option = json_decode($post["option"],1);
      } else {
        $option = array();
      }

      $product_options = $this->model_catalog_product->getProductOptions($post['product_id']);

      foreach ($product_options as $product_option) {
        if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
          $this->json['error'][] = sprintf($this->language->get('error_required'), $product_option['name']);
        }
      }

      //TODO:recurring in phase2
      $recurring_id = 0;

      if(empty($this->json['error'])){
              
        $this->cart->add((int)$post['product_id'], (int)$post['quantity'], $option, $recurring_id);

        $this->json['data']['message'] = sprintf($this->language->get('text_add_success'),$product_info['name']);

        // Unset all shipping and payment methods
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);
        
        // Totals
        $this->load->model('setting/extension');
        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;
        
        $total_data = array(
          'totals' => &$totals,
          'taxes'  => &$taxes,
          'total'  => &$total
        );
        
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $sort_order = array();

          $results = $this->model_setting_extension->getExtensions('total');

          foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
          }

          array_multisort($sort_order, SORT_ASC, $results);

          foreach ($results as $result) {
            if ($this->config->get('total_' . $result['code'] . '_status')) {
              $this->load->model('extension/total/' . $result['code']);

              // We have to put the totals in an array so that they pass by reference.
              $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
          }

          $sort_order = array();

          foreach ($totals as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
          }

          array_multisort($sort_order, SORT_ASC, $totals);
        }
        
        $this->json['data']['total_int'] = $total;

        $this->json['data']['total'] = $this->currency->format($total, $this->session->data['currency']);
        
      }
      

    }else{
      
      $this->statusCode = 404;
      $this->json['error'][] = $this->language->get('product_not_found');
    }

    return $this->sendResponse();
  }

  public function remove(){

    $this->auth('DELETE');
    $this->load->language('ac_api/cart');

    $post = $this->getPost();
    $this->validateData(['card_id'],$post);

    if (isset($post['card_id']) && ctype_digit($post['card_id'])) {
      
      $this->cart->remove($post['card_id']);
      $this->json['data']['message'] = $this->language->get('product_removed');

    } else {

        $this->json['error'][] = $this->language->get('invalid_data');
        $this->statusCode = 400;
    }
    
    return $this->sendResponse();
  }

  public function update(){

    $this->auth('post');
    $this->load->language('ac_api/cart');

    $post = $this->getPost();

    $this->validateData(['cart_id','quantity'],$post);    

    $this->cart->update($post['cart_id'], $post['quantity']);

    $this->json['data']['success'] = $this->language->get('text_cart_update');

    unset($this->session->data['shipping_method']);
    unset($this->session->data['shipping_methods']);
    unset($this->session->data['payment_method']);
    unset($this->session->data['payment_methods']);
    unset($this->session->data['reward']);
    
    return $this->sendResponse();

  }

}
