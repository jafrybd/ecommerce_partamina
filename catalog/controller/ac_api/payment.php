<?php 
/**
 * payment.php
 *
 * Pyment method management 
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiPayment extends RestApiController {

	public function methods() {

    $this->auth('get');
    $this->load->language('ac_api/checkout');
		
		// Delete past shipping methods and method just in case there is an error
		unset($this->session->data['payment_methods']);
		unset($this->session->data['payment_method']);

		$json = array();

    //set Payment address if not set
		if(!isset($this->session->data['payment_address']) || (isset($this->session->data['payment_address']) && !$this->session->data['payment_address'])){
			$this->model('account/address');
			$address = $this->model_account_address->getAddress($this->customer->getAddressId());
			if($address){
				$this->session->data['payment_address'] = $address;
			}else{
				$this->json['error'][] = $this->language->get('default_address_not_valid');
				$this->sendResponse();
			}
    }
    
    // Payment Address
    if (!isset($this->session->data['payment_address'])) {
      $json['error'] = $this->language->get('error_payment_address');
    }
    
    if (!$json) {
      // Totals
      $totals = array();
      $taxes = $this->cart->getTaxes();
      $total = 0;

      // Because __call can not keep var references so we put them into an array. 
      $total_data = array(
        'totals' => &$totals,
        'taxes'  => &$taxes,
        'total'  => &$total
      );

      $this->load->model('setting/extension');

      $sort_order = array();

      $results = $this->model_setting_extension->getExtensions('total');

      foreach ($results as $key => $value) {
        $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
      }

      array_multisort($sort_order, SORT_ASC, $results);

      foreach ($results as $result) {
        if ($this->config->get('total_' . $result['code'] . '_status')) {
          $this->load->model('extension/total/' . $result['code']);
          
          // We have to put the totals in an array so that they pass by reference.
          $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
        }
      }

      // Payment Methods
      $json['payment_methods'] = array();

      $this->load->model('setting/extension');

      $results = $this->model_setting_extension->getExtensions('payment');

      $recurring = $this->cart->hasRecurringProducts();

      foreach ($results as $result) {
        if ($this->config->get('payment_' . $result['code'] . '_status')) {
          $this->load->model('extension/payment/' . $result['code']);

          $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

          if ($method) {
            if ($recurring) {
              if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                $json['payment_methods'][$result['code']] = $method;
              }
            } else {
              $json['payment_methods'][$result['code']] = $method;
            }
          }
        }
      }

      $sort_order = array();

      foreach ($json['payment_methods'] as $key => $value) {
        $sort_order[$key] = $value['sort_order'];
      }

      array_multisort($sort_order, SORT_ASC, $json['payment_methods']);

      if ($json['payment_methods']) {
        $this->session->data['payment_methods'] = $json['payment_methods'];
      } else {
        $json['error'] = $this->language->get('error_no_payment');
      }
    }
    
    if(isset($json['error']) && $json['error']){
      $this->json['error'][] = $json['error'];
    }else{
      $this->json['data'] = array_values($json['payment_methods']);
    }
		
    $this->sendResponse();
	}  

}