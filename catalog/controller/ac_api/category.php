<?php 
/**
 * category.php
 *
 * Category management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiCategory extends RestApiController {

  //end-point call for cateogry page
  public function getCategeories()
  {
    
    $this->auth('post');

    $this->load->language('ac_api/category');
    $this->model('catalog/category');
    $this->model('catalog/product');
    $this->model('tool/image');

    $data['category'] = $this->allCategoryData();
    $data['products'] = $this->getProductData(true);
    $data['sort_by'] = $this->getSortByData();
    
    $this->json['data'] = $data;
    return $this->sendResponse();

  }

  public function allCategoryData()
  {

    $parents = $this->model_catalog_category->getCategories();

    $categories = array();
    foreach($parents as $parent){
      $parent_category = $this->formatCategoryData($parent);
      if($parent_category){
        $category_childs = $this->model_catalog_category->getCategories($parent['category_id']);
        foreach($category_childs  as $children){
          $parent_category['child'][] = $this->formatCategoryData($children);
        }
        $categories[] = $parent_category;
      }
    }
    
    return $categories;
  }

}
