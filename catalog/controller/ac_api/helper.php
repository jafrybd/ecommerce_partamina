<?php
/**
 * helper.php
 *
 * Helper api
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link
 * @documentations
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiHelper extends RestApiController {

    public function session(){

        $this->auth('get');
        $this->load->language('ac_api/helper');

        $this->json['data']['session_id'] = $this->session->getId();

        return $this->sendResponse();
    }

    public function sessionInformation(){

        $this->auth('get');
        $this->load->language('ac_api/helper');

        $data = $this->session->data;
        $data['id'] = $this->session->getId();
        $this->json['data']['session'] = $data;

        return $this->sendResponse();
    }

    public function languages(){

        $this->auth('get');
        $this->load->language('ac_api/helper');

        $this->load->model('localisation/language');

        $data['languages'] = array();

        $results = $this->model_localisation_language->getLanguages();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['languages'][] = array(
                    'language_id' => $result['language_id'],
                    'image' => $result['image'],
                    'name' => $result['name'],
                    'code' => $result['code']
                );
            }
        }

        $this->json['data'] = $data;

        return $this->sendResponse();
    }

    public function currencies(){

        $this->auth('get');
        $this->load->language('ac_api/helper');

        $this->load->model('localisation/currency');

        $data['currencies'] = array();

        $results = $this->model_localisation_currency->getCurrencies();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['currencies'][] = array(
                    'title'        => $result['title'],
                    'code'         => $result['code'],
                    'symbol_left'  => $result['symbol_left'],
                    'symbol_right' => $result['symbol_right']
                );
            }
        }

        $this->json['data'] = $data;

        return $this->sendResponse();
    }

    public function changeLanguage(){

        //pr($this->request->server['REQUEST_METHOD'],1);
        // pr($_SERVER);
        // pr($this->request->server['REQUEST_METHOD'],1);

        $this->auth('post');
        $this->load->language('ac_api/helper');

        $post = $this->getPost();

        $this->validateData(['language_code'],$post);

        $this->load->model('localisation/language');
        $allLanguages = $this->model_localisation_language->getLanguages();

        $languages = array();

        foreach ($allLanguages as $result) {
            $languages[$result['code']] = $result;
        }

        if(count($languages) > 1){

            $oc_lang = $post['language_code'];

            //check if language code is valid
            if(isset($languages[$oc_lang])){

                $this->session->data['language'] = $oc_lang;
                $this->config->set('config_language', $oc_lang);

                $this->config->set('config_language_id', $languages[$oc_lang]['language_id']);

                if (isset($languages[$oc_lang]['directory']) && !empty($languages[$oc_lang]['directory'])) {
                    $directory = $languages[$oc_lang]['directory'];
                } else {
                    $directory = $languages[$oc_lang]['code'];
                }

                $language = new \Language($directory);
                $language->load($directory);
                $this->registry->set('language', $language);

                $this->load->language('ac_api/helper');

                $this->json['data']['message'] = $this->language->get('api_langauge_changed');

            }else{

                $this->statusCode = 404;
                $this->json['error'][] = $this->language->get('api_invalid_langauge_code');

            }

        }

        $this->sendResponse();
    }

    public function changeCurrency(){

        $this->auth('post');
        $this->load->language('ac_api/helper');

        $post = $this->getPost();

        $this->validateData(['currency_code'],$post);

        //TODO:validate currency exists
        $this->session->data['currency'] = $post['currency_code'];
        $this->json['data']['message'] = $this->language->get('api_currency_changed');

        $this->sendResponse();
    }

    public function zones()
    {
        $this->auth('post');
        $post = $this->getPost();

        $this->validateData(['country_id'],$post);

        $this->load->language('ac_api/helper');
        $this->load->model('localisation/zone');

        $this->json['data'] = $this->model_localisation_zone->getZonesByCountryId($post['country_id']);;

        return $this->sendResponse();
    }

    public function getInformationList()
    {
        $this->auth('get');

        $this->json['data'] = $this->model_ac_api_common->getInformations();

        return $this->sendResponse();
    }

    public function getInformationContent()
    {
        $this->auth('post');
        $post = $this->getPost();

        $this->validateData(['information_id'],$post);

        $information_id = $post['information_id'];
        $information_info = $this->model_ac_api_common->getInformation($information_id);

        if ($information_info) {

            $data['heading_title'] = $information_info['title'];
            $data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
//            $data['description'] = str_replace("\r\n",'',strip_tags(html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8')));

            $this->json['data'] = $data;

        }else{
            $this->statusCode = 404;
            $this->json['error'][] = $this->language->get('information_not_found');
        }

        return $this->sendResponse();
    }

    public function uplodImage()
    {
        $this->auth('post');

        $post = $this->getPost();
        $this->validateData(['image_data','prefix'],$post);

        if(!$post['prefix']){
            $post['prefix'] = "upload";
        }

        $upload_reponse = $this->uploadBase64Image($post['image_data'],$post['prefix']);
        if($upload_reponse['status']){
            $this->json['data']['image_url'] = $upload_reponse['message'];
        }else{
            $this->statusCode = 400;
            $this->json['error'][] = $upload_reponse['message'];
        }
        return $this->sendResponse();
    }

    public function customerService()
    {
        $this->auth('post');

        $post = $this->getPost();
        $this->validateData(['name','email','subject','enquiry'],$post);

        try{
            $to = $this->config->get('config_email');
            $from = $post['email'];

            $name = $post['name'];
            $subject = $post['subject'];

            $subject2 = "Copy of your form submission";

            $message = $name . " wrote the following:" . "\n\n" . $post['enquiry'];
            $message2 = "Here is a copy of your message " . $name . "\n\n" . $post['enquiry'];

            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8\r\n";
            $headers .= "From: <".$from. ">" ;

            $headers2 = "From:" . $to;

            mail($to,$subject,$message,$headers);
            mail($from,$subject2,$message2,$headers2);

            $this->json['data']['message'] = "Mail Sent. Thank you " . $name . ", we will contact you shortly.";

        }catch(\Throwable $th){

            $this->statusCode = 401;
            $this->json['error'][] = $th->getMessage();
        }

        return $this->sendResponse();
    }

}