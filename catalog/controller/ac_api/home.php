<?php 
/**
 * helper.php
 *
 * Helper api
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiHome extends RestApiController {

  public function home(){
    
    $this->auth('get','home');

    $this->model('catalog/product');
    $this->model('tool/image');

    //if cache send request
    // if($this->cache->get('home_api')){
    //   $this->json['data'] = $this->cache->get('home_api');

    //   return $this->sendResponse();
    // }

    //secion 1 - main banner 
    $home_banner_id = 9;
    $data['banner'] = $this->genrateBannerData($this->model_ac_api_common->getBanner($home_banner_id));

    //secion 2 
    $approved_by_banner_id = 8;
    $data['approved_by'] = $this->genrateBannerData($this->model_ac_api_common->getBanner($approved_by_banner_id));


    //section 3 all category section
    $this->model('catalog/category');
    $results = $this->model_catalog_category->getCategories();
    $categories = array();

    foreach($results as $result){
      if (is_file(DIR_IMAGE . $result['image'])) {
        $image = $this->model_tool_image->resize($result['image'], $this->config->get('home_category_width'), $this->config->get('home_category_hieght'));
      } else {
        $image = $this->model_tool_image->resize('no_image.png', $this->config->get('home_category_width'), $this->config->get('home_category_hieght'));
      }
      $all_product_items = $this->model_ac_api_common->countProductsByCategoryId($result['category_id']);
      $categories[] = array(
        'category_id'    => $result['category_id'],
        'name'      => $result['name'],
        'image'     => $image,
        'items'     => $all_product_items,
        'item_text' => ($all_product_items>1)?'items':'item'
      );
    }
    $data['categories'] = $categories;

    //section 4 featured section
    $data['featured'] = array(
      'label' => 'Featured',
      'products' => $this->model_ac_api_common->getFeaturedProducts()
    );

    //section 5 best seller section
    $best_seller = array();
    $results = $this->model_catalog_product->getBestSellerProducts($this->config->get('home_product_limit'));
    foreach($results as $result){
      $best_seller[] = $this->genrateProductData($result);
    }
    $data['best_seller'] = array(
      'label'   => 'Best Sellers',
      'products' => $best_seller
    );

    //section 6 Special section
    $special_products = array();
    $filter_data = array(
			'sort'  => "pd.name",
			'order' => "ASC",
			'start' => "0",
			'limit' => $this->config->get('home_product_limit')
		);
    $results = $this->model_catalog_product->getProductSpecials($filter_data);
    foreach($results as $result){
      $special_products[] = $this->genrateProductData($result);
    }
    $data['special'] = array(
      'label'   => 'Special',
      'products' => $special_products
    );

    //create cache
    $this->cache->set('home_api', $data);

    //append data
    $this->json['data'] = $data;

    return $this->sendResponse();
  }

  public function genrateBannerData($results){

    $banner_data = array();

    if($results){
      foreach($results as $result){

        if (is_file(DIR_IMAGE . $result['image'])) {
          $image = $this->model_tool_image->resize($result['image'], $this->config->get('home_banner_width'), $this->config->get('home_banner_hieght'));
        } else {
          $image = $this->model_tool_image->resize('no_image.png', $this->config->get('home_banner_width'), $this->config->get('home_banner_hieght'));
        }
        $banner_data[] = array(
          'title' => $result['title'],
          'link'  => $result['link'],
          'image' => $image
        );
      }

    }else{
      $banner_data = array();
    }

    return $banner_data;
  }

}