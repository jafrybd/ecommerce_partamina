<?php 
/**
 * wishilist.php
 *
 * Wishlist management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiWishlist extends RestApiController {

  public function add()
  {
    $this->auth('post');
    $post = $this->getPost();
    $this->validateData(['product_id'],$post);
    
    $this->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($post['product_id']);
    
    if($product_info){
      
      $this->model('account/wishlist');
      if ($this->customer->isLogged()) {
        
        $this->model_account_wishlist->addWishlist($post['product_id']);
        $json['success'] = sprintf($this->language->get('wish_list_success'), $product_info['name']);
        
        $json['total'] = $this->model_account_wishlist->getTotalWishlist();

      }else{

        if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
        }
        $this->session->data['wishlist'][] = $post['product_id'];
        $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
        $json['success'] = sprintf($this->language->get('wish_list_success'), $product_info['name']);

        $json['total'] = (isset($this->session->data['wishlist'])) ? count($this->session->data['wishlist']) : 0;
      }

      $this->json['data'] = $json;

    }else{
      $this->json['error'][] = $this->language->get('product_not_found');
    }


    return $this->sendResponse();

  }

  public function delete()
  {
    $this->auth('post');
    $post = $this->getPost();
    $this->validateData(['product_id'],$post);
    
    $this->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($post['product_id']);
    
    if($product_info){
      
      $this->model('account/wishlist');
      if ($this->customer->isLogged()) {
        
        $this->model_account_wishlist->deleteWishlist($post['product_id']);
        $this->json['data']['message'] = sprintf($this->language->get('wish_list_delete_success'), $product_info['name']);
        
        $this->json['data']['total'] = $this->model_account_wishlist->getTotalWishlist();

      }else{

        // if(isset($this->session->data['wishlist'][$post['product_id']])) {
        //   unset($this->session->data['wishlist'][$post['product_id']]);
				// 	$this->json['data']['success'] = sprintf($this->language->get('wish_list_success'), $product_info['name']);

        //   $this->json['data']['total'] = (isset($this->session->data['wishlist'])) ? count($this->session->data['wishlist']) : 0;
        // }else{

        //   $this->json['error'][] = $this->language->get('product_not_in_wishlist');
        // }
        $this->json['error'][] = $this->language->get('auth_user');

      }

    }else{
      $this->json['error'][] = $this->language->get('product_not_found');
    }


    return $this->sendResponse();

  }

  public function all(){
    
    $this->auth('post');
    $this->model('account/wishlist');
    $this->model('catalog/product');
    $this->model('tool/image');
    
    $products = array();

    if ($this->customer->isLogged()) {      
      $results = $this->model_account_wishlist->getWishlist();

      foreach($results as $result){
        $product_info = $this->model_catalog_product->getProduct($result['product_id']);
        if ($product_info) {
          $products[] = $this->genrateProductData($product_info);
        } else {
          $this->model_account_wishlist->deleteWishlist($result['product_id']);
        }
      }

      $this->json['data']['total'] = $this->model_account_wishlist->getTotalWishlist();
      $this->json['data']['product'] = $products;

    }else{

      if(isset($this->session->data['wishlist'])) {

        $results = $this->session->data['wishlist'];

        foreach($results as $key => $product_id){
          $product_info = $this->model_catalog_product->getProduct($product_id);
          if ($product_info) {
            $products[] = $this->genrateProductData($product_info);
          } else {
            unset($this->session->data['wishlist'][$key]);
          }
        }

        $this->json['data']['total'] = count($this->session->data['wishlist']);
        $this->json['data']['product'] = $products;

      }else{
        $this->json['data']['total'] = 0;
        $this->json['data']['product'] = array();
      }

    }

    return $this->sendResponse();
  }


}
