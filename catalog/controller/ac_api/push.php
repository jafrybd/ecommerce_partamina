<?php 
/**
 * push.php
 *
 * Push notification management 
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiPush extends RestApiController {

	public function updateFirebase() {

    $this->needLogin();
    $this->auth('post');
    $post = $this->getPost();

    $this->validateData(['firebase_token'],$post);

    if($this->model_ac_api_common->updateFirebaseToken($this->customer->getId(), $post['firebase_token'])){
      $this->json['data']['message'] = $this->language->get('firebase_token_update_success');
      $this->json['data']['customer_id'] = $this->customer->getId();
    }else{
      if(!$this->customer->getId()){
        $this->json['error'][] = $this->language->get('unauthorised_access');
      }else{
        $this->json['error'][] = $this->language->get('firebase_token_update_error');
      }
    }

    $this->sendResponse();
  }  
  
  public function sendPush(){
    
    $this->needLogin();
    $this->auth('post');
    $post = $this->getPost();
    $this->validateData(['title','body','custom_data'],$post);

    $fcm_token = $this->customer->getFCMToken();
    $fcm_tokens = array();
    array_push($fcm_tokens,$fcm_token);

    if($fcm_tokens){
      $fcm_notificaton = array(
        "title" => $post['title'],
        "body"  => $post['body'],
      );
      $fcm_notification_custom_data = json_decode($post['custom_data']);

      $response = $this->execute_fcm($fcm_tokens, $fcm_notificaton, $fcm_notification_custom_data);

      $this->json['data'] = $response;

      $this->sendResponse();
    }
  }

}