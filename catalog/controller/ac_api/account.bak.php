<?php
/**
 * account.php
 *
 * Account management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link
 * @documentations
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiAccount extends RestApiController {

    public function login(){

        $this->auth('POST');
        $this->load->language('ac_api/account');

        $post = $this->getPost();
        $this->validateData(['email','password'],$post);

        //TODO:other validation

        $this->load->model('account/customer');

        if (!$this->customer->login($post['email'], $post['password'])) {

            $this->json['error'][] = $this->language->get('error_login');

            $this->model_account_customer->addLoginAttempt($post['email']);

        } else {

            $this->model_account_customer->deleteLoginAttempts($post['email']);

            unset($this->session->data['guest']);

            // Default Shipping Address
            $this->load->model('account/address');

            // if ($this->config->get('config_tax_customer') == 'payment') {
            //   $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            // }

            // if ($this->config->get('config_tax_customer') == 'shipping') {
            //   $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            // }

            $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());

            // Wishlist
            if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                $this->load->model('account/wishlist');

                foreach ($this->session->data['wishlist'] as $key => $product_id) {
                    $this->model_account_wishlist->addWishlist($product_id);

                    unset($this->session->data['wishlist'][$key]);
                }
            }

            $this->json['data']['user'] = array(
                'session_id' => $this->session->getId(),
                'customer_id' => $this->customer->getId(),
                'email' => $this->customer->getEmail(),
                'first_name' => $this->customer->getFirstName(),
                'getLastName' => $this->customer->getFirstName(),
                'fcm_token'  => $this->customer->getFCMToken(),
                'profile'  => $this->customer->getProfileImg(),
                'product_in_cart' => $this->cart->countProducts()
            );
        }

        $this->sendResponse();
    }

    public function logout(){

        $this->auth('get');

        $this->load->language('ac_api/account');

        if ($this->customer->isLogged()) {
            $this->customer->logout();

            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_address']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['comment']);
            unset($this->session->data['order_id']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);

        }

        $this->sendResponse();

    }

    public function register(){

        $this->auth("post","account");

        $post = $this->getPost();
        $this->validateData(['firstname','lastname','email','telephone','password'],$post);

        //validate registeration data
        $this->registerUser($post);

        $this->sendResponse();

    }

    private function validateRegistrationData($post,$check_password=true) {
        $error = array();

        if ((utf8_strlen(trim($post['firstname'])) < 1) || (utf8_strlen(trim($post['firstname'])) > 32)) {
            $error[] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($post['lastname'])) < 1) || (utf8_strlen(trim($post['lastname'])) > 32)) {
            $error[] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($post['email']) > 96) || !filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $error[] = $this->language->get('error_email');
        }

        if($check_password){
            if ($this->model_account_customer->getTotalCustomersByEmail($post['email'])) {
                $error[] = $this->language->get('error_exists');
            }
        }else{
            if (($this->customer->getEmail() != $post['email']) && $this->model_account_customer->getTotalCustomersByEmail($post['email'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        if(utf8_strlen($post['telephone'])){
            if ((utf8_strlen($post['telephone']) < 11) || (utf8_strlen($post['telephone']) > 14)) {
                $error[] = $this->language->get('error_telephone');
            }
        }

        if($check_password){
            if ((utf8_strlen(html_entity_decode($post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
                $error[] = $this->language->get('error_password');
            }
        }

        return $error;
    }

    private function registerUser($post){

        $this->model('account/customer');
        $validate_response = $this->validateRegistrationData($post);

        if(!empty($validate_response)){
            $this->json['error'] = $validate_response;
            $this->json['error_code'] = "API-AREG03";
            $this->sendResponse();
        }

        try {

            $customer_id = $this->model_ac_api_common->addCustomer($post);

            if(!$customer_id){
                $this->json['error'][] = $this->language->get('register_login_error');
                $this->json['error_code'] = "API-AREG01";
                $this->sendResponse();
            }

            if($this->customer->login($post['email'], $post['password'])){

                $this->model_account_customer->deleteLoginAttempts($post['email']);

                unset($this->session->data['guest']);

                // Wishlist
                if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                    $this->load->model('account/wishlist');

                    foreach ($this->session->data['wishlist'] as $key => $product_id) {
                        $this->model_account_wishlist->addWishlist($product_id);

                        unset($this->session->data['wishlist'][$key]);
                    }
                }

                $this->json['data']['message'] = $this->language->get('register_success');

                $this->json['data']['user'] = array(
                    'session_id' => $this->session->getId(),
                    'customer_id' => $this->customer->getId(),
                    'email' => $this->customer->getEmail(),
                    'first_name' => $this->customer->getFirstName(),
                    'getLastName' => $this->customer->getFirstName(),
                    'product_in_cart' => $this->cart->countProducts()
                );

            }else{
                $this->json['data']['message'] = $this->language->get('register_success');
                // $this->json['error'][] = $this->language->get('register_login_error');
                // $this->json['error_code'] = "API-AREG01";
            }

        } catch (\Throwable $th) {
            $this->json['error'][] = $this->language->get('register_error');
            $this->json['error_code'] = "API-AREG02";
            $this->json['error_message'] = $th->getMessage();
        }

        return true;
    }

    public function socialLogin(){

        $this->auth('POST','account');

        $post = $this->getPost();
        $this->validateData(['firstname','lastname','email','telephone','password','social_id'],$post);
        //TODO:validate social_id

        $this->model('account/customer');
        //login if true
        if($this->model_account_customer->getTotalCustomersByEmail($post['email'])){

            if($this->customer->login($post['email'], '', true)){

                $this->model_account_customer->deleteLoginAttempts($post['email']);

                unset($this->session->data['guest']);

                // Wishlist
                if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                    $this->load->model('account/wishlist');

                    foreach ($this->session->data['wishlist'] as $key => $product_id) {
                        $this->model_account_wishlist->addWishlist($product_id);

                        unset($this->session->data['wishlist'][$key]);
                    }
                }

                $this->json['data']['message'] = $this->language->get('login_success');

                $this->json['data']['user'] = array(
                    'session_id' => $this->session->getId(),
                    'customer_id' => $this->customer->getId(),
                    'email' => $this->customer->getEmail(),
                    'first_name' => $this->customer->getFirstName(),
                    'getLastName' => $this->customer->getFirstName(),
                    'product_in_cart' => $this->cart->countProducts()
                );

            }else{
                $this->json['error'][] = $this->language->get('login_error');
                $this->json['error_code'] = "API-SL01";
            }

        }else{
            //validate registeration data
            $this->registerUser($post);
        }

        $this->sendResponse();
    }

    function addressList()
    {
        $this->auth('get');
        $this->model('account/address');

        $this->json['data'] = array_values($this->model_account_address->getAddresses());

        $this->sendResponse();
    }

    /*
    Profile detils
    */
    public function details()
    {
        $this->auth('get');
        $this->model('account/customer');

        $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
        if($customer_info){

            $this->json['data'] = array(
                "customer_id"     => $customer_info['customer_id'],
                "language_id"     => $customer_info['language_id'],
                "profile_image"   => $this->imageLink($customer_info['profile_image']),
                "firstname"       => $customer_info['firstname'],
                "lastname"        => $customer_info['lastname'],
                "email"           => $customer_info['email'],
                "telephone"       => $customer_info['telephone'],
                "newsletter"      => $customer_info['newsletter'],
                "address_id"      => $customer_info['address_id'],
                "status"          => $customer_info['status'],
                "date_added"      => $customer_info['date_added'],
            );

        }else{
            $this->json['error'][] = $this->language->get('customer_not_found');
        }


        $this->sendResponse();
    }

    public function changePassword()
    {
        $this->auth('post');

        $post = $this->getPost();
        $this->validateData(['old_password','password','confirm'],$post);

        $error = array();

        if(!$this->model_ac_api_common->confirmPassword($this->customer->getEmail(), $post['old_password'])){
            $error[] = $this->language->get('error_old_password');
        }

        if ((utf8_strlen(html_entity_decode($post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
            $error[] = $this->language->get('error_password');
        }

        if ($post['confirm'] != $post['password']) {
            $error[] = $this->language->get('error_confirm');
        }

        if($error){
            $this->json['error'] = $error;
        }else{

            $this->model('account/customer');

            $this->model_account_customer->editPassword($this->customer->getEmail(), $post['password']);

            $this->json['data']['message'] = $this->language->get('password_text_success');
        }

        $this->sendResponse();
    }

    public function updateProfile()
    {
        $this->auth('post');

        $post = $this->getPost();
        $this->validateData(['firstname','lastname','email','telephone'],$post);

        $this->model('account/customer');

        $validate_response = $this->validateRegistrationData($post,false);

        if(!empty($validate_response)){
            $this->json['error'] = $validate_response;
            $this->json['error_code'] = "API-UPN01";
            $this->sendResponse();
        }

        $this->model_account_customer->editCustomer($this->customer->getId(),$post);

        $this->json['data']['message'] = $this->language->get('profile_update_success');

        $this->sendResponse();
    }

    public function forgotPassword()
    {
        $this->auth('post');

        $post = $this->getPost();
        $this->validateData(['email'],$post);

        $this->load->language('account/forgotten');
        $this->load->model('account/customer');
        $validate_response = $this->validateForgotPassword($post);

        if(!empty($validate_response)){
            $this->json['error'] = $validate_response;
            $this->json['error_code'] = "API-FP194";
            $this->sendResponse();
        }

        $this->model_account_customer->editCode($post['email'], token(40));

        $this->json['data']['message'] =  $this->language->get('text_success');

        $this->sendResponse();
    }

    protected function validateForgotPassword($post) {

        $error = array();

        if (!isset($post['email'])) {
            $error[] = $this->language->get('error_email');
        } elseif (!$this->model_account_customer->getTotalCustomersByEmail($post['email'])) {
            $error[] = $this->language->get('error_email');
        }

        // Check if customer has been approved.
        $customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);

        if ($customer_info && !$customer_info['status']) {
            $error[] = $this->language->get('error_approved');
        }

        return $error;
    }
}