importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');

/* Please add your FCM APP credentials below */
firebase.initializeApp({
  apiKey: 'AIzaSyCIcNM6B7Kg_5P2rVNc-yLGoY1_CLdaMQ4',
  authDomain: 'https://c6346cb19029.ngrok.io',
  databaseURL: 'https://medinatech-8d215.firebaseio.com',
  projectId: 'medinatech-8d215',
  storageBucket: 'medinatech-8d215.appspot.com',
  messagingSenderId: '185209964071',
  appId: '1:185209964071:android:baa53652c81e442104b002'
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  const notificationTitle = 'Background Message from html';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});