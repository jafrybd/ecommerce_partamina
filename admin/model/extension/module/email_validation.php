<?php
class ModelExtensionModuleEmailValidation extends Model {

	public function addEmailValidation($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "email_validation SET `email` = '" . $this->db->escape($data['email']) . "', `status` = '" . $this->db->escape($data['status']) . "', `sub_status` = '" . $this->db->escape($data['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($data['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($data['domain_age_days']) . "', `customer_id` = " . (int)$data['customer_id'] . ", `date_added` = NOW()");

		return $this->db->getLastId();
	}

	public function editEmailValidation($email_validation_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "email_validation SET `status` = '" . $this->db->escape($data['status']) . "', `sub_status` = '" . $this->db->escape($data['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($data['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($data['domain_age_days']) . "', `customer_id` = " . (int)$data['customer_id'] . ", `date_modified` = NOW() WHERE `email_validation_id` = " . (int)$email_validation_id);

		$affected = $this->db->countAffected();

		return $affected > 0 ? $affected : false;
	}

	public function getEmailValidationToken($email_validation_id) {
		$token = token(32);

		$this->db->query("UPDATE `" . DB_PREFIX . "email_validation` SET token = '" . $this->db->escape($token) . "', token_date = NOW() WHERE email_validation_id = " . (int)$email_validation_id);

		return $token;
	}

	public function getEmailValidation($email_validation_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email_validation_id` = " . (int)$email_validation_id . " LIMIT 1");

		return $query->row;
	}

	public function getEmailValidationByCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `customer_id` = " . (int)$customer_id . " LIMIT 1");

		return $query->row;
	}

	public function getEmailValidationByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email` = '" . $this->db->escape($email) . "' LIMIT 1");

		return $query->row;
	}

	public function getEmailValidations($data = array()) {
		$sql = "SELECT ev.*, CONCAT(c.firstname, ' ', c.lastname) AS `name`, IF(ev.customer_id, (SELECT COUNT(DISTINCT ev2.email) FROM " . DB_PREFIX . "email_validation ev2 WHERE ev.customer_id = ev2.customer_id), 0) AS total_emails FROM " . DB_PREFIX . "email_validation ev LEFT JOIN " . DB_PREFIX . "customer c ON(ev.customer_id = c.customer_id)";

		$sort_data = array(
			'customer_id' => 'ev.customer_id',
			'customer' => 'CONCAT(c.firstname, " ", c.lastname)',
			'email' => 'ev.email',
			'status' => 'CONCAT_WS("_", IF(LENGTH(ev.status), ev.status, NULL), IF(LENGTH(ev.sub_status), ev.sub_status, NULL))',
			'date' => 'GREATEST(COALESCE(ev.date_added, 0), COALESCE(ev.date_modified, 0))'
		);

		$condition = $this->_emailValidationsCondition($data);

		if ($condition) {
			$sql .= " WHERE " . implode(" AND ", $condition);
		}

		if (isset($data['sort']) && isset($sort_data[$data['sort']])) {
			$sql .= " ORDER BY " . $sort_data[$data['sort']];
		} else {
			$sql .= " ORDER BY " . $sort_data['date'];
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalEmailValidations($data) {
		$sql = "SELECT count(ev.email_validation_id) AS total FROM " . DB_PREFIX . "email_validation ev LEFT JOIN " . DB_PREFIX . "customer c ON(ev.customer_id = c.customer_id)";

		$condition = $this->_emailValidationsCondition($data);

		if ($condition) {
			$sql .= " WHERE " . implode(" AND ", $condition);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function deleteEmailValidation($email_validation_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "email_validation WHERE `email_validation_id` = " . (int)$email_validation_id);
	}

	public function getCustomers($data = array()) {
		$sql = "SELECT customer_id, email FROM " . DB_PREFIX . "customer c";

		$condition = $this->_customerCondition($data);

		if ($condition) {
			$sql .= " WHERE " . implode(" AND ", $condition);
		}

		$sql .= " ORDER BY customer_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalCustomers($data = array()) {
		$sql = "SELECT count(customer_id) AS total FROM " . DB_PREFIX . "customer c";

		$condition = $this->_customerCondition($data);

		if ($condition) {
			$sql .= " WHERE " . implode(" AND ", $condition);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function upgrade() {
		$chk = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "email_validation'");
		if (!$chk->num_rows) {
			$this->install();
		}
	}

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "email_validation` (
  `email_validation_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) UNSIGNED NOT NULL,
  `email` varchar(96) NOT NULL,
  `token` varchar(32) NULL DEFAULT NULL,
  `token_date` DATETIME NULL DEFAULT NULL,
  `status` varchar(64) NOT NULL,
  `sub_status` varchar(64) NOT NULL,
  `domain_age_days` int(11) NOT NULL,
  `did_you_mean` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`email_validation_id`),
  UNIQUE KEY `email` (`email`),
  KEY `customer_id` (`customer_id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
	}

	private function _customerCondition($data) {
		$cond = array();

		if (isset($data['filter_validated'])) {
			if ($data['filter_validated']) {
				$cond[] = "c.email IN (SELECT ev.email FROM " . DB_PREFIX . "email_validation ev)";
			} else {
				$cond[] = "c.email NOT IN (SELECT ev.email FROM " . DB_PREFIX . "email_validation ev)";
			}
		}

		if (!empty($data['filter_status'])) {
			if ($data['filter_status'] == 1) {
				$cond[] = "c.status = 1";
			} elseif ($data['filter_status'] == 2) {
				$cond[] = "c.status = 0";
			}
		}

		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$cond[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}

		if (!empty($data['filter_date_before'])) {
			$cond[] = "DATE(c.date_added) > DATE('" . $this->db->escape($data['filter_date_before']) . "')";
		}

		return $cond;
	}

	private function _emailValidationsCondition($data) {
		$cond = array();

		if (!empty($data['filter_name'])) {
			$cond[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$cond[] = "ev.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
		}

		return $cond;
	}
}