<?php
class ModelExtensionReportGroup extends Model {
	
	public function addFAQGroup($data) {
	
		$this->db->query("INSERT INTO " . DB_PREFIX . "faq_group SET name = '" . $this->db->escape($data['name']) . "',sort_order = '" . (int)$data['sort_order'] . "'");

		return $group_id;
		}
	
	public function addFAQ($group_id,$data1) {

		if($group_id){
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "faq_support WHERE group_id = '" . (int)$group_id . "' ");
		
		}
				foreach ($data1['product_attribute'] as $count => $product_attribute_description) {
				foreach ($product_attribute_description as $count_id => $faq_attribute)
						{
							if($count_id !=0){
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "questions SET  group_id = '". (int)$group_id  ."'");
					
						}
					$que_id= $this->db->getLastId();
						
				foreach($faq_attribute as $language_id => $faq1_attribute){
						
					$this->db->query("INSERT INTO " . DB_PREFIX . "faq_support SET question = '" . $this->db->escape($faq1_attribute['name']) . "',answer = '" . $this->db->escape($faq1_attribute['text']) .  "',language_id='".(int)$language_id. "'  , que_id='".(int)$que_id."', group_id = '". (int)$group_id  ."'");
				
						}
						
						}
				
				}
	}
	


	public function editGroup($group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "faq_group SET sort_order = '" . (int)$data['sort_order'] . "', name = '" . $this->db->escape($data['name']) . "' WHERE group_id = '" . (int)$group_id . "'");
	
	}

	public function deleteGroup($group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "faq_group WHERE group_id = '" . (int)$group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "faq_support WHERE group_id = '" . (int)$group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "questions WHERE group_id = '" . (int)$group_id . "'");
	}

	public function getGroup($group_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_group WHERE group_id = '" . (int)$group_id . "'");

		return $query->rows;
	}
	public function getfaq($group_id) {
		
		
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_group WHERE group_id = '" . (int)$group_id . "' ");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_support WHERE group_id = '" . (int)$group_id . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['que_id']][$product_attribute_description['language_id']] = array('name' => $product_attribute_description['question'],
				'text' => html_entity_decode($product_attribute_description['answer'],ENT_QUOTES,'UTF-8'));
			
			}

			$product_attribute_data[] = array(
				'group_id'                  => $product_attribute['group_id'],
				'product_description_data' => $product_attribute_description_data
			);
		}
		
		return $product_attribute_data;
		
		
	}

	public function getGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "faq_group WHERE group_id !=0";

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "faq_group");

		return $query->row['total'];
	}
}