<?php
$_['heading_title'] = 'Quote List';
$_['text_list'] = 'List';
$_['text_no_results'] = 'No result';

$_['column_quote_id'] = 'Quote Id';
$_['column_full_name'] = 'Full Name';
$_['column_phone'] = 'Phone';
$_['column_email'] = 'Email';
$_['column_product'] = 'Product Name';
$_['column_date'] = 'Date Added';
$_['column_action'] = 'Action';
$_['column_qty'] = 'Qty';
