<?php

// Error
$_['error_invalid_email'] = 'Please enter a valid email address.';
$_['error_disposable_email'] = 'Please enter a non-disposable email address.';
$_['error_free_email'] = 'Please enter a non-free email address.';
$_['error_role_email'] = 'Please enter a non-role-based email address.';
$_['error_block_email_domain'] = 'This email domain has been denied access.';