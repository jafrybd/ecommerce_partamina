<?php
// Heading
$_['heading_title']    = 'Multi-language Zone';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified multilangzone module!';
$_['text_edit']        = 'Edit multilangzone Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify multilangzone module!';