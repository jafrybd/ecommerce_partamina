<?php
// Heading
$_['heading_title']    = '<p style="color:green">Advance Support FAQ</p>';
$_['heading_title1']    = 'Advance Support FAQ';
$_['heading_title50']    = 'FAQ Group List';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified account module!';
$_['text_edit']        = 'Edit Account Module';
$_['text_success']      = 'Success: You have modified attribute groups!';
$_['text_list']         = 'FAQ Group List';
$_['text_add']          = 'Add FAQ Group';
$_['text_edit']         = 'Edit Attribute Group';
$_['text_enabled']         = 'enabled';
$_['text_disabled']         = 'disabled';
$_['entry_status']         = 'status';

// Entry
$_['entry_status']     = 'Status';
$_['entry_name']        = 'FAQ Group Name';
$_['entry_attribute']        = 'Questions';
$_['entry_text']        = 'Answers';
$_['entry_sort_order']  = 'Sort Order';
$_['button_addFAQ']  = 'Add FAQ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify account module!';
$_['column_name']       = 'FAQ Group Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_group']     = 'Add Group';
$_['column_faq']     = 'Add FAQ';
