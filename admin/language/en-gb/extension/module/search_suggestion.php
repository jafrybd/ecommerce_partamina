<?php

// Heading
$_['heading_title'] = 'Search Suggestion [sv2109.com]';

// Column
$_['column_search_by'] = 'Search by';
$_['column_relevance_weight'] = 'Word weight for relevance sort';
$_['column_search_type'] = 'Search';
$_['column_search_type_sort'] = 'Sort order';
$_['column_fields'] = 'Fields';
$_['column_fields_settings'] = 'Settings';
$_['column_fields_show'] = 'Show';
$_['column_fields_show_field_name'] = 'Show field name';
$_['column_fields_css'] = 'CSS';
$_['column_fields_location'] = 'Location';
$_['column_fields_sort'] = 'Sort order (Drag & Drop)';
$_['column_fields_attributes'] = 'Attributes';
$_['column_fields_attributes_show'] = 'Show';
$_['column_fields_attributes_replace_text'] = 'Replace attributes values (separated by commas) by attribute name';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified module Search Suggestion!';
$_['text_edit'] = 'Edit Search Suggestion module';
$_['text_relevance_weight_mr'] = 'Set up in module "<a href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=11363">Search Relevance</a>"';
$_['text_order_name'] = 'By name';
$_['text_order_rating'] = 'By rate';
$_['text_order_relevance'] = 'By relevance';

$_['text_order_dir_asc'] = 'Ascending';
$_['text_order_dir_desc'] = 'Descending';

$_['text_logic_or'] = 'OR';
$_['text_logic_and'] = 'AND';

$_['text_autocomplete_module'] = 'Module';
$_['text_autocomplete_theme'] = 'Theme';

$_['text_field_location_inline'] = 'Inline';
$_['text_field_location_newline'] = 'New line';
$_['text_product_field_name'] = 'Name';
$_['text_product_field_price'] = 'Price';
$_['text_product_field_image'] = 'Image';
$_['text_field_image_width'] = 'Width';
$_['text_field_image_height'] = 'Height';
$_['text_product_field_description'] = 'Description';
$_['text_product_field_attributes'] = 'Attributes';
$_['text_product_field_manufacturer'] = 'Manufacturer';
$_['text_product_field_model'] = 'Model';
$_['text_product_field_sku'] = 'SKU';
$_['text_product_field_upc'] = 'UPC';
$_['text_product_field_ean'] = 'EAN';
$_['text_product_field_jan'] = 'JAN';
$_['text_product_field_isbn'] = 'ISBN';
$_['text_product_field_mpn'] = 'MPN';
$_['text_product_field_quantity'] = 'Quantity';
$_['text_product_field_rating'] = 'Rating';
$_['text_product_field_cart'] = 'Add to cart button';
$_['text_product_field_cart_code'] = 'Button code';
$_['text_product_field_tag'] = 'Tag';
$_['text_product_field_more'] = 'More fields';
$_['text_product_field_custom'] = 'Custom fields';

$_['text_fields_cut'] = 'Cut';
$_['text_fields_separator'] = 'Delimiter';

$_['text_fields_attributes_show'] = 'Show';
$_['text_fields_attributes_hide'] = 'Hide';
$_['text_fields_attributes_replace'] = 'Replace';

$_['text_category_field_name'] = 'Name';
$_['text_category_field_image'] = 'Image';
$_['text_category_field_description'] = 'Description';

$_['text_manufacturer_field_name'] = 'Name';
$_['text_manufacturer_field_image'] = 'Image';

$_['text_information_field_title'] = 'Title';
$_['text_information_field_description'] = 'Description';

// Tabs
$_['tab_general'] = 'General settings';
$_['tab_settings'] = 'Settings';
$_['tab_search_by'] = 'Search by';
$_['tab_fields'] = 'Show fields';
$_['tab_product'] = 'Product';
$_['tab_category'] = 'Category';
$_['tab_manufacturer'] = 'Manufacturer';
$_['tab_information'] = 'Information';
$_['tab_support'] = 'Support';

// Entry
$_['entry_status'] = 'Status';
$_['entry_order'] = 'Sort';
$_['entry_logic'] = 'Search logic';
$_['entry_title'] = 'Title';
$_['entry_limit'] = 'Limit results';
$_['entry_cache'] = 'Cache results';
$_['entry_css'] = 'CSS';
$_['entry_element'] = 'Search form';
$_['entry_width'] = 'Search block width (px, em, %)';
$_['entry_more'] = 'Show more results link';
$_['entry_search_type_order'] = 'Sort order (Drag & Drop)';
$_['entry_autocomplete'] = 'Autocomplete script';
$_['entry_fix_keyboard_layout'] = 'Fix keyboard layout';
$_['entry_fix_transliteration'] = 'Fix transliteration';

// Help
$_['help_product_order'] = 'You need module Search Relevance for sorting by relevance';
$_['help_product_logic'] = 'Not uses for sort by relevance';
$_['help_fix_keyboard_layout'] = 'Correct the wrong keyboard layout, for example: дфзещз-> laptop';
$_['help_fix_transliteration'] = 'Correct the wrong transliteration, for example: лаптоп-> laptop';
$_['help_tab_product_search_by'] = '';




// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Search Suggestion!';

// Support
$_['support_text'] = '
<br />
<b>If this module does not work or you have a problem with installing or using this module you can:</b>
<ul>
  <li>email me at <a href="mailto:sv2109@gmail.com?subject=Search Relevance module">sv2109@gmail.com</a></li>
  <li>write a comment on the module page</li>
</ul>
<br/>
<b>You can also contact me if:</b>
<ul>
  <li>you need help with your OpenCart</li>
  <li>you need to create any other module for OpenCart</li> 
</ul>
<br/>
<div style="font-size: 150%;">Other useful modules you can find <a href="http://sv2109.com/en/modules">here</a>:</div>
<br/>
<a href="http://sv2109.com/en/modules"><img src="http://sv2109.com/i/ssb.png" alt=""><img src="http://sv2109.com/i/srb.png" alt=""><img src="http://sv2109.com/i/isb.png" alt=""><img src="http://sv2109.com/i/fcsb.png" alt="">
<br/><img src="http://sv2109.com/i/acb.png" alt=""><img src="http://sv2109.com/i/asb.png" alt=""><img src="http://sv2109.com/i/iocb.png" alt=""><img src="http://sv2109.com/i/tab.png" alt=""></a>
';
