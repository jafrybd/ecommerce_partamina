<?php
// Heading
$_['heading_title']                                    = 'MailboxValidator Email Validator 1.2.0';
$_['heading_name']                                     = 'MailboxValidator Email Validator';

// Text
$_['text_edit']                                        = 'Edit MailboxValidator Module';
$_['text_success']                                     = 'Success: You have modified MailboxValidator module settings!';
$_['text_module_disabled']                             = 'Warning: Module is disabled';

// Entry
$_['entry_api_key']                                    = 'API Key';
// $_['entry_name']                                    = 'Module Name';
// $_['entry_title']                                   = 'Heading Title';
$_['entry_status']                                     = 'Status';
$_['entry_debug']                                      = 'Debug Mode';
$_['entry_remaining_credits']                          = 'Credits Available:';
$_['entry_valid_email_validator']                      = 'Block Invalid Email';
$_['entry_disposable_email_validator']                 = 'Block Disposable Email';
$_['entry_free_email_validator']                       = 'Block Free Email';
$_['entry_role_email_validator']                       = 'Block Role-Based Email';
$_['entry_custom_email_domain_validator']              = 'Block Custom Email Domains';
$_['entry_invalid_email_error_message_setting']        = 'Error Message for Invalid email address';
$_['entry_disposable_email_error_message_setting']     = 'Error Message for Disposable email address';
$_['entry_free_email_error_message_setting']           = 'Error Message for Free email address';
$_['entry_role_based_email_error_message_setting']     = 'Error Message for Role-based email address';
$_['entry_block_email_domain_error_message_setting']   = 'Error Message for Custom Email Domains';

// Help
$_['help_valid_email_validator']                       = 'Block invalid email from using your service. This option will perform a comprehensive validation, including SMTP server check.';
$_['help_disposable_email_validator']                  = 'Block disposable email from registering for your service.';
$_['help_free_email_validator']                        = 'Block free email type from registering your service.';
$_['help_role_email_validator']                        = 'Block role-based email type from registering your service.';
$_['help_custom_email_domain_email_validator']         = 'Block email domains from registering for your service. Please enter one domain name in each row.';
$_['help_invalid_email_error_message_setting']         = 'Design your custom error message for invalid email address.';
$_['help_disposable_email_error_message_setting']      = 'Design your custom error message for disposable email address.';
$_['help_free_email_error_message_setting']            = 'Design your custom error message for free email address.';
$_['help_role_based_email_error_message_setting']      = 'Design your custom error message for role-based email address.';
$_['help_block_email_domain_error_message_setting']    = 'Design your custom error message for email domains.';


// Error
$_['error_permission']                                 = 'Warning: You do not have permission to modify MailboxValidator module!';
$_['error_api_key']                                    = 'Error: You must enter a API Key.';
$_['error_invalid_error_message']                      = 'Error: You must enter an invalid error message.';
$_['error_low_credits']                                = 'Warning: Your MailboxValidator API Credits is low.';