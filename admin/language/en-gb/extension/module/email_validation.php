<?php
// Heading
$_['heading_title']    = 'Email Validation with ZeroBounce - 3.0.1.5';
$_['heading_name']     = 'Email Validation';
$_['heading_result']   = 'Validated Customers';

// Column
$_['column_customer']  = 'Customer';
$_['column_customers'] = 'Customers';
$_['column_email']     = 'Email';
$_['column_date' ]     = 'Date';
$_['column_status']    = 'Result';
$_['column_total']     = 'Total';

// Text
$_['text_all']               = 'All';
$_['text_api_key']           = 'Create an account at <a href="https://www.zerobounce.net?ref=opencart-templates" target="_blank">ZeroBounce</a> for Free Trials credits. Copy your API Key.';
$_['text_check_abuse']       = '<b>Abuse</b> - Deny emails that are people who are known to click the abuse links in emails, hence abusers or complainers.';
$_['text_check_catch_all']   = '<b>Catch All</b> - Deny emails that are impossible to validate without sending a real email and waiting for a bounce. The term Catch-all means that the email server tells you that the email is valid, whether it\'s valid or invalid.';
$_['text_check_disposable']  = '<b>Disposable (Do Not Mail)</b> - Deny temporary emails created for the sole purpose to sign up to websites without giving their real email address. These emails are short lived from 15 minutes to around 6 months';
$_['text_check_free_mail']   = '<b>Free Mail Provider</b> - Deny emails that are from a free email service provider.';
$_['text_check_invalid']     = '<b>Invalid</b> - Deny emails that we determined to be invalid. The results are 99.999% accurate.';
$_['text_check_possible_trap'] = '<b>Possible Trap (Do Not Mail)</b> - Deny emails that contain keywords that might correlate to possible spam traps like spam@ or @spamtrap.com.';
$_['text_check_role_based']  = '<b>Role Based (Do Not Mail)</b> - Deny emails that belong to a position or a group of people, like sales@ info@ and contact@. Role based emails have a strong correlation to people reporting mails sent to them as spam and abuse.';
$_['text_check_role_based_catch_all'] = '<b>Role Based Catch All (Do Not Mail)</b> - Deny emails that are role based and also belong to a catch_all domain.';
$_['text_check_toxic']       = '<b>Toxic (Do Not Mail)</b> - Deny domains that are known for abuse, spam, and bot created emails.';
$_['text_check_spamtrap']    = '<b>Spam Trap</b> - Deny emails based on internal research via a series of algorithms, ZeroBounce can detect email accounts that are connected with industry-wide blacklists.';
$_['text_check_possible_trap'] = '<b>Possible Spam Tram (Do Not Mail)</b> - Deny emails contain keywords that might correlate to possible spam traps like spam@ or @spamtrap.com.';
$_['text_check_unknown']     = '<b>Unknown</b> - Deny emails that are unable validate for one reason or another. Typical cases are "The mail server was down" or "the anti-spam system is blocking us". In most cases, 80% unknowns are invalid/bad email addresses.';
$_['text_confirm']           = 'Are you sure?';
$_['text_custom']            = 'Custom';
$_['text_customers']         = 'Customers';
$_['text_default']           = 'Default';
$_['text_edit']              = 'Edit Module';
$_['text_extension']         = 'Extensions';
$_['text_help_alert']        = 'Send admin email if credits get below';
$_['text_help_banned']       = 'e.g test.com, example.com';
$_['text_help_check']        = 'Default validation checks to validate email. Find out more about validation statuses at: <a href="https://www.zerobounce.net/docs/?ref=opencart-templates#status-codes" target="_blank">ZeroBounce</a>';
$_['text_help_domain_age']   = 'Minimum number of days domain from email has been registered for.';
$_['text_help_expire_days']  = 'Number of days to wait before re-validating email';
$_['text_help_recheck']      = 'Auto validate customers who\'ve already registered the next time they log into your store. If customer hs invalid emails will be logged out and required change their email to a valid email.';
$_['text_ip']                = 'Send IP address for extra security checks';
$_['text_missing_emailtemplate'] = 'Missing Email Template Module';
$_['text_module_disabled']   = 'Warning: Module is disabled';
$_['text_success']           = 'Success: You have modified Email Validation module!';
$_['text_success_clear']     = 'Success: You have emptied Email Validation debug log!';
$_['text_success_delete']    = 'Success: You have deleted Email Validation Customers!';
$_['text_success_empty']     = 'Success: You have emptied Email Validation cache!';
$_['text_success_enable']    = 'Success: You have manually enabled customers!';
$_['text_success_disable']   = 'Success: You have manually disabled customers!';
$_['text_success_validate']  = 'Success: You have validated %s customers. %s remaining unvalidated customers!';
$_['text_tab_result']        = 'Validation Results';
$_['text_tab_setting']       = 'General Settings';
$_['text_tab_validate']      = 'Email Settings';
$_['text_validated']         = 'Validated';
$_['text_validating']        = 'Validating...';
$_['text_unvalidated']       = 'Not Validated';

// Entry
$_['entry_alert']            = 'Low Credits Alert';
$_['entry_api_key']          = 'API Key';
$_['entry_banned']           = 'Banned Domain(s)';
$_['entry_check']            = 'Validation Check(s)';
$_['entry_customer_check']   = 'Validate Existing Customers';
$_['entry_debug']            = 'Debug';
$_['entry_domain_age']       = 'Domain Age';
$_['entry_email']            = 'Email';
$_['entry_expire_days']      = 'Days Email Valid';
$_['entry_name']             = 'Name';
$_['entry_status']           = 'Module Status';
$_['entry_read_timeout']     = 'Read Timeout (secs)';
$_['entry_request_timeout']  = 'Request Timeout (secs)';
$_['entry_valid']            = 'Validate';
$_['entry_valid_account_edit']        = 'Customer Edit Details';
$_['entry_valid_account_register']    = 'Customer Register';
$_['entry_valid_customer_customer']   = 'Admin - Customer Details';
$_['entry_valid_checkout_guest']      = 'Checkout as Guest';
$_['entry_valid_checkout_register']   = 'Checkout as Customer';
$_['entry_valid_information_contact'] = 'Contact Us';
$_['entry_valid_journal2_newsletter'] = 'Journal - Newsletter <small>(OPTIONAL)</small>';
$_['entry_valid_marketing_contact']   = 'Admin - Newsletter Mail';

//Button
$_['button_cancel']          = 'Cancel';
$_['button_clear']           = 'Clear Debug Log';
$_['button_emailtemplate']   = 'Email Template';
$_['button_result']          = 'Validation Results';
$_['button_validate']        = 'Validate Customers';

// Error
$_['error_api_key']          = 'Warning: You must enter a API Key.';
$_['error_alpha_numeric']    = 'Warning: You must enter alphanumeric text without spaces.';