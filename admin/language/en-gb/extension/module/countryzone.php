<?php
// Heading
$_['heading_title']    = 'CountryZone';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified store module!';
$_['text_edit']        = 'Edit CountryZone Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify CountryZone module!';