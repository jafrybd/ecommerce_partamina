<?php
// Heading
$_['heading_title']            = '<p style="color:green">Advance Support Tickets</p>';
$_['heading_title1']            = 'Advance Support Tickets';
$_['heading_title1']            = 'Advance Support Status';

// Text
$_['text_extension']            = 'Extension';
$_['text_success']              = 'Success: You have modified products!';
$_['text_subject']              = 'Ticked status changed';
$_['text_list']                 = 'Ticket List';
$_['text_add']                  = 'Add Product';
$_['text_yes']                  = 'Yes';
$_['text_no']                   = 'No';
$_['text_submit']               = 'Submit';
$_['text_user']                 = 'User Information';
$_['text_change_status']        = 'Change Status';
$_['text_new_comment']          = 'New comment received';
$_['text_select']               = 'Select department';
$_['text_select_status']        = 'Select status';
$_['text_support_list']        = 'Support List ';
$_['text_department']      	  = 'Department ';
$_['text_faq']       			 = 'Group ';
$_['text_status']       	 = 'Status ';
$_['text_email']        	 = 'Email ';
$_['text_ticket']       	 = 'Ticket ';
$_['support_id']        	 = 'Ticket Number ';
$_['current_status']         = 'Y request  status ';

              
$_['text_upload']            = 'Attachments';
$_['text_history']           = 'History';
$_['text_comment']           = 'Comment';
$_['text_comment_msg']       = 'You have received a new comment
';
$_['text_notify']            = 'Your ticket status has been changed';
$_['text_view']              = 'Request';
// Column
$_['column_subject']         = 'Subject';
$_['column_department']      = 'Department';
$_['column_message']         = 'Message';
$_['column_customer']        = 'Customer name';
$_['column_date_added']      = 'Date Added';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_notfiy']          = 'Notify';
$_['column_action']          = 'Action';
$_['column_date_added']          = 'Date Added';
$_['column_status']          = 'Status';

$_['column_notify']          = 'Notify';
$_['column_comment']         = 'Comment';

// Entry
$_['entry_name']             = 'Product Name';
$_['entry_subject']     	 = 'Subject';
$_['entry_department'] 	     = 'Department';
$_['entry_message'] 		 = 'Message';
$_['entry_notify'] 			 = 'Notify';
$_['entry_customer'] 		 = 'Customer Name';
$_['entry_date_added']       = 'Date Added';
$_['entry_comment']          = 'Comment';
$_['entry_status']           = 'Status';
$_['entry_telephone']        = 'Telephone';
$_['entry_email']            = 'Email';
$_['text_filter']            = 'Search';
$_['button_add']            = 'Add Status';


// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
