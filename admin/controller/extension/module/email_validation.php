<?php
class ControllerExtensionModuleEmailValidation extends Controller {
    private $error = array();
	protected $_fields = array('alert', 'api_key', 'banned', 'debug', 'check', 'customer_days', 'customer_status', 'customer_check', 'domain_age','expire_days', 'read_timeout', 'request_timeout', 'route', 'route_check', 'status');
	protected $_validation_routes = array('account_register', 'account_edit', 'checkout_register', 'checkout_guest', 'information_contact', 'customer_customer', 'marketing_contact', 'journal2_newsletter');

	public function index() {
        $this->load->language('extension/module/email_validation');
		$this->load->language('extension/module/email_validation/error');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addStyle('view/stylesheet/module/email_validation.css');
        $this->document->addScript('view/javascript/module/email_validation.js');

        $this->load->model('extension/module/email_validation');
        $this->load->model('setting/setting');

        $this->model_extension_module_email_validation->upgrade();

		if (!empty($this->request->get['action'])) {
			switch ($this->request->get['action']) {
				case 'delete':
					if (!empty($this->request->post['selected']) && is_array($this->request->post['selected'])) {
						foreach ($this->request->post['selected'] as $email_validation_id) {
							$this->model_extension_module_email_validation->deleteEmailValidation($email_validation_id);
						}

						$this->session->data['success'] = $this->language->get('text_success_delete');
					}
					break;
				case 'disable':
				case 'enable':
					if (!empty($this->request->post['selected']) && is_array($this->request->post['selected'])) {
						foreach ($this->request->post['selected'] as $email_validation_id) {
							$email_validation_info = $this->model_extension_module_email_validation->getEmailValidation($email_validation_id);

							if ($email_validation_info) {
								if ($this->request->get['action'] == 'enable') {
									$email_validation_info['status'] = 'valid';
									$email_validation_info['sub_status'] = '';
								} else {
									$email_validation_info['status'] = 'disable';
									$email_validation_info['sub_status'] = '';
								}

								$this->model_extension_module_email_validation->editEmailValidation($email_validation_id, $email_validation_info);
							}
						}

						$this->session->data['success'] = $this->language->get('text_success_' . $this->request->get['action']);
					}
					break;
				case 'clear':
					if (file_exists(DIR_LOGS . '/email_validation.log')) {
						unlink(DIR_LOGS . '/email_validation.log');
					}

					$this->session->data['success'] = $this->language->get('text_success_clear');

					break;
			}

			$this->response->redirect($this->url->link('extension/module/email_validation', 'user_token=' . $this->session->data['user_token']));
		}

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('module_email_validation', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module/email_validation', 'user_token=' . $this->session->data['user_token']));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['api_key'])) {
            $data['error_api_key'] = $this->error['api_key'];
        } else {
            $data['error_api_key'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_name'),
            'href' => $this->url->link('extension/module/email_validation', 'user_token=' . $this->session->data['user_token'])
        );

        $language_vars = array(
            'button_cancel',
            'button_clear',
            'button_validate',
            'button_emailtemplate',
            'button_result',
            'button_save',
            'column_customers',
            'column_total',
            'entry_alert',
            'entry_api_key',
            'entry_banned',
            'entry_check',
			'entry_customer_check',
			'entry_debug',
			'entry_domain_age',
			'entry_expire_days',
			'entry_read_timeout',
            'entry_request_timeout',
            'entry_status',
            'entry_valid',
            'heading_title',
            'text_all',
            'text_api_key',
            'text_check_abuse',
            'text_check_catch_all',
            'text_check_disposable',
            'text_check_free_mail',
            'text_check_invalid',
            'text_check_possible_trap',
            'text_check_role_based',
            'text_check_role_based_catch_all',
            'text_check_spamtrap',
            'text_check_toxic',
            'text_check_unknown',
            'text_confirm',
            'text_custom',
            'text_customer_validate',
            'text_disabled',
            'text_edit',
            'text_enabled',
            'text_help_alert',
            'text_help_banned',
            'text_help_check',
			'text_help_domain_age',
			'text_help_expire_days',
			'text_help_recheck',
			'text_help_validate',
			'text_module_disabled',
			'text_no',
			'text_tab_result',
			'text_tab_setting',
			'text_tab_validate',
			'text_validated',
			'text_validating',
			'text_valid_admin',
			'text_valid_checkout',
			'text_valid_contact',
			'text_valid_edit',
			'text_valid_guest',
			'text_valid_newsletter',
			'text_valid_register',
            'text_yes',
            'text_unvalidated'
        );

        foreach ($language_vars as $language_var) {
            $data[$language_var] = $this->language->get($language_var);
        }

        $data['action'] = $this->url->link('extension/module/email_validation', 'user_token=' . $this->session->data['user_token']);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module');

        foreach($this->_fields as $field) {
            if (isset($this->request->post['module_email_validation_' . $field])) {
                $data['module_email_validation_' . $field] = $this->request->post['module_email_validation_' . $field];
            } else {
				$data['module_email_validation_' . $field] = $this->config->get('module_email_validation_' . $field);
			}
        }

        if (!is_array($data['module_email_validation_route'])) {
			$data['module_email_validation_route'] = array();
		}
        if (!is_array($data['module_email_validation_route_check'])) {
			$data['module_email_validation_route_check'] = array();
		}
        if (!is_array($data['module_email_validation_check'])) {
			$data['module_email_validation_check'] = array();
		}

		$data['validation_routes'] = array();

        foreach($this->_validation_routes as $route) {
            if (!isset($data['module_email_validation_route'][$route])) {
				$data['module_email_validation_route'][$route] = 0;
			}

			$data['validation_routes'][] = $route;

			$data['entry_valid_' . $route] = $this->language->get('entry_valid_' . $route);
        }

		$filter_data = array(
			'filter_validated' => 0,
			'filter_status' => 1,
		);

		$data['unvalidated_total'] = $this->model_extension_module_email_validation->getTotalCustomers($filter_data);

		$filter_data['filter_validated'] = 1;

		$data['validated_total'] = $this->model_extension_module_email_validation->getTotalCustomers($filter_data);

		$data['customer_total'] = ($data['validated_total'] + $data['unvalidated_total']);

        if (file_exists(DIR_LOGS . '/email_validation.log')) {
            $data['email_validation_debug_log'] = file_get_contents(DIR_LOGS . '/email_validation.log');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/email_validation/form', $data));
    }

    public function result() {
		$this->load->language('extension/module/email_validation');

		$this->load->model('extension/module/email_validation');

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_name'  => $filter_name,
			'filter_email' => $filter_email,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$results = $this->model_extension_module_email_validation->getEmailValidations($filter_data);

		$validation_total = $this->model_extension_module_email_validation->getTotalEmailValidations($filter_data);

		$this->load->model('customer/customer');

		$data['validations'] = array();

		if ($results) {
			foreach ($results as $result) {
				$data['validations'][] = array(
					'email_validation_id' => $result['email_validation_id'],
					'email' => $result['email'],
					'total_emails' => $result['total_emails'],
					'date' => date($this->language->get('date_format_short'), strtotime(($result['date_modified'] && $result['date_modified'] != '0000-00-00 00:00:00' ? $result['date_modified'] : $result['date_added']))),
					'status' => $this->language->get($result['status'] . ($result['sub_status'] ? '_' . $result['sub_status'] : '')),
					'name' => $result['name'],
					'url_edit' => $result['customer_id'] ? $this->url->link('customer/customer/edit', 'customer_id=' . $result['customer_id'] . '&user_token=' . $this->session->data['user_token'], true): ''
				);
			}
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $validation_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/module/email_validation/result', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		
		$data['results'] = sprintf($this->language->get('text_pagination'), ($validation_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($validation_total - $this->config->get('config_limit_admin'))) ? $validation_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $validation_total, ceil($validation_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_email'] = $filter_email;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['action'] = $this->url->link('extension/module/email_validation', 'user_token=' . $this->session->data['user_token']);

		$data['heading_result'] = $this->language->get('heading_result');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_date'] = $this->language->get('column_date');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_status'] = $this->language->get('column_status');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_name'] = $this->language->get('entry_name');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_customer'] = $this->url->link('extension/module/email_validation/result', 'user_token=' . $this->session->data['user_token'] . '&sort=customer' . $url, true);
		$data['sort_email'] = $this->url->link('extension/module/email_validation/result', 'user_token=' . $this->session->data['user_token'] . '&sort=email' . $url, true);
		$data['sort_date'] = $this->url->link('extension/module/email_validation/result', 'user_token=' . $this->session->data['user_token'] . '&sort=date' . $url, true);
		$data['sort_status'] = $this->url->link('extension/module/email_validation/result', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);

		$this->response->setOutput($this->load->view('extension/module/email_validation/result', $data));
    }

    public function install() {
        if (!$this->user->hasPermission('modify', 'extension/module/email_validation')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->language('extension/module/email_validation');

        $this->load->model('extension/module/email_validation');

        $this->model_extension_module_email_validation->install();

        $this->load->model('setting/setting');

        $settings = $this->model_setting_setting->getSetting('module_email_validation');

        if (!is_array($settings)) {
            $settings = array();
        }

        $settings['module_email_validation_status'] = 0;
        $settings['module_email_validation_expire_days'] = 365;
        $settings['module_email_validation_suggestion'] = 1;
        $settings['module_email_validation_read_timeout'] = 200;
        $settings['module_email_validation_request_timeout'] = 150;
		$settings['module_email_validation_customer_check'] = 1;
        $settings['module_email_validation_check'] = array('invalid' => 1, 'toxic' => 1, 'spamtrap' => 1, 'possible_trap' => 1, 'disposable' => 1);
        $settings['module_email_validation_route'] = array('account_edit' => 1, 'account_register' => 1, 'checkout_guest' => 1, 'checkout_register' => 1);

        $this->model_setting_setting->editSetting('module_email_validation', $settings);
    }

    public function uninstall() {
		$this->load->model('setting/setting');

		$this->model_setting_setting->deleteSetting('module_email_validation');
    }

    public function validate_customers() {
		$filter_data = array(
			'filter_validated' => 0,
			'filter_status' => 1,
			'start' => 0,
			'limit' => 2
		);

		$this->load->library('email_validation');

		$this->load->model('extension/module/email_validation');

		$this->load->language('extension/module/email_validation');

		$customers = $this->model_extension_module_email_validation->getCustomers($filter_data);

		if ($customers) {
			foreach($customers as $customer) {
				$this->email_validation->validate($customer['email'], array('customer_id' => $customer['customer_id']));
			}
		}

		$total_customers = $this->model_extension_module_email_validation->getTotalCustomers($filter_data);

		$json = array();

		$json['total'] = count($customers);

		$json['success'] = sprintf($this->language->get('text_success_validate'), count($customers), $total_customers);

		if ($total_customers) {
			$json['next'] = str_replace('&amp;', '&', $this->url->link('extension/module/email_validation/validate_customers', 'user_token=' . $this->session->data['user_token'], true));
		} else {
			$json['next'] = '';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function check() {
		if (!$this->config->get('module_email_validation_status')) {
			return true;
		}

		$json = array();

		$this->load->language('extension/module/email_validation/error');

		if (empty($this->request->post['email'])) {
			$json['error'] = $this->language->get('error_required');
		} elseif (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$json['error'] = $this->language->get('error_email');
		}

		if (empty($json['error'])) {
			$json['success'] = true;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function valid() {
		if (!$this->config->get('module_email_validation_status')) {
			return true;
		}

		if (empty($this->request->post['email'])) {
			return false;
		}

		$this->load->language('extension/module/email_validation/error');

		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$result = $this->language->get('error_email');
		} else {
			$email_validation = new \Email_Validation($this->registry);

			$result = $email_validation->validate($this->request->post['email'], array('customer_id' => $this->customer->getId()));
		}

		return $result;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/email_validation')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['module_email_validation_api_key'])) {
			$this->error['api_key'] = $this->language->get('error_api_key');
		} elseif(!preg_match('/^[a-z0-9]+$/i', $this->request->post['module_email_validation_api_key'])) {
			$this->error['api_key'] = $this->language->get('error_alpha_numeric');
		}

		return !$this->error;
	}
}