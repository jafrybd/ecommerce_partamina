<?php
class ControllerExtensionModuleSupportStatus extends Controller {
	private $error = array();

	public function index() {
	$this->load->language('extension/module/support_status');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');
		$this->load->model('extension/report/support');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_setting_setting->editSetting('support', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			$this->session->data['error'] = $this->language->get('text_error');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
		$data['heading_title1_status'] = $this->language->get('heading_title1_status');
		$data['heading_title_status'] = $this->language->get('heading_title_status');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_status_list'] = $this->language->get('text_status_list');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_color'] = $this->language->get('entry_color');
		$data['entry_sort'] = $this->language->get('entry_sort');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true);
	
	

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['category_status'])) {
			$data['category_status'] = $this->request->post['category_status'];
		} else {
			$data['category_status'] = $this->config->get('category_status');
		}
       $this->load->language('extension/module/support_status');

		
        $this->load->model('extension/report/support');

		$this->installt();
		$this->getList();
		
		
		
	}
	
	public function installt() { 
	$this->model_extension_report_support->install();
   }
	
	public function add(){
		$this->load->model('extension/report/support');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$this->model_extension_report_support->addStatus($this->request->post);
	
			$this->response->redirect($this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true));
		
		}
		
	$this->getForm();
		 
	}
		
	public function edit(){
		$this->load->model('extension/report/support');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$this->model_extension_report_support->editStatus($this->request->post,$this->request->get['status_id']);
	
			$this->response->redirect($this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true));
		
		}
		
	$this->getForm();
		 
	}
	
   
	public function delete() {
		$this->load->language('extension/module/support_status');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/support');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $status_id) {
				
				if($status_id == 1 || $status_id == 2  || $status_id == 3)
				{
						$this->session->data['success'] = $this->language->get('text_error');
				}
			else{
				$this->model_extension_report_support->deletestatus($status_id);
					$this->session->data['success'] = $this->language->get('text_success');
				}
			}


		

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}
	
	
	protected function getList(){
		
			$this->load->language('extension/module/support_status');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		
		if(isset($this->request->get['filter_department'])){
				
				$filter_department=$this->request->get['filter_department'];
				
				}else{
				$filter_department=null;
			}
			
		if(isset($this->request->get['filter_date_added'])){
				
				$filter_date_added=$this->request->get['filter_date_added'];
				
					
		}else{
				$filter_date_added=null;
			}
		if(isset($this->request->get['filter_status'])){
				
				$filter_status=$this->request->get['filter_status'];
					
		}else{
				$filter_status=null;
			}
		
	
		
	
		
		

		$url = '';
		
		
		
		
		
		
		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

			$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		$data['delete'] = $this->url->link('extension/module/support_status/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['add'] = $this->url->link('extension/module/support_status/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
	

		$data['statusData'] = array();

		
	//	$data['departments']=$this->model_extension__report_support->getDepartmentList();
		
		
			$filter_data = array(
		
			
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);
		

		$this->load->model('extension/report/support');

		$status_total = $this->model_extension_report_support->getTotalStatus($filter_data);

		$statusData=$this->model_extension_report_support->getstatus();



		foreach ($statusData as $status) {
		
			
			$data['statusData'][] = array(
				'status_id' => $status['status_id'],
				
				'status'       => $status['name'],
				'color'     => $status['color'],
				//'notify'     =>$notifyStatus['notify'] ,
				'edit'       => $this->url->link('extension/module/support_status/edit', 'user_token=' . $this->session->data['user_token'] . '&status_id=' . $status['status_id'] . $url, true)
			);
	
		
		}



		


		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');

		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
	    $data['text_no_results'] = $this->language->get('text_no_results');
	    $data['text_select_status'] = $this->language->get('text_select_status');
	    $data['text_status_list'] = $this->language->get('text_status_list');
	    $data['text_error'] = $this->language->get('text_error');

	
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_color'] = $this->language->get('entry_color');
		$data['error_comment1'] = $this->language->get('error_comment1');
		$data['text_filter'] = $this->language->get('text_filter');

	
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		
		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['filter_department'])){
				
				$filter_status=$this->request->get['filter_department'];
			}else{
				$filter_status=null;
				
			}	
			if(isset($this->request->get['filter_date_added'])){
				
				$filter_date_added=$this->request->get['filter_date_added'];
			}else{
				$filter_date_added=null;
				
			}
		

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $status_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

	
		$data['results'] = sprintf($this->language->get('text_pagination'), ($status_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($status_total - $this->config->get('config_limit_admin'))) ? $status_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $status_total, ceil($status_total / $this->config->get('config_limit_admin')));

			
		$data['filter_department'] = $filter_department;
		$data['filter_date_added'] = $filter_date_added;
		$data['filter_status'] = $filter_status;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/support_status', $data));
	}

	

	
	
	
	
	
	
	protected function getForm() {
		
		
		$this->load->language('extension/module/support_status');
		$this->load->model('extension/report/support');
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_color'] = $this->language->get('entry_color');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
	
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		
		

		$url = '';

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
		
		
			if (!isset($this->request->get['status_id'])) {
			$data['action'] = $this->url->link('extension/module/support_status/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('extension/module/support_status/edit', 'user_token=' . $this->session->data['user_token'] . '&status_id=' . $this->request->get['status_id'] . $url, 'SSL');
		}

		
		$data['cancel'] = $this->url->link('xtension/module/support_status/', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['status_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$status_info = $this->model_extension_report_support->getStatusName($this->request->get['status_id']);
			
		}

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/language');


		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($status_info)) {
			$data['status'] = $status_info[0]['name'];
		} else {
			$data['status'] = '';
		}

		

		if (isset($this->request->post['color'])) {
			$data['color'] = $this->request->post['color'];
		} elseif (!empty($status_info)) {
			
			$data['color'] = $status_info[0]['color'];
		} else {
			$data['color'] = '';
		}

		

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/support_status_form', $data));
	}
	
	
	
	
	
	
	
			private function validate() {
				if($this->request->post['notify']!=' '){
		if ((utf8_strlen(trim($this->request->post['comment1'])) < 1)) {
			$this->error['comment1'] = $this->language->get('error_comment1');
		}
				}
		return !$this->error;
	}

	
	

}