<?php
class ControllerExtensionModuleSupport extends Controller {
	private $error = array();

	public function index() {
	$this->load->language('extension/module/support');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_setting_setting->editSetting('support', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['text_Ticked_status_changed'] = $this->language->get('text_Ticked_status_changed');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' =>$this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['category_status'])) {
			$data['category_status'] = $this->request->post['category_status'];
		} else {
			$data['category_status'] = $this->config->get('category_status');
		}
		$this->load->language('extension/module/support');

		

		$this->load->model('extension/report/support');

		$this->installt();
		
		//The name of the directory that we need to create.
$directoryName = 'attachments/';


//Check if the directory already exists.
if(!is_dir($directoryName)){
    //Directory does not exist, so lets create it.
    mkdir($directoryName, 0755, true);
}
		
		
		
		
		
		
		$this->getList();
		
		
		
	}
	
   public function installt() { 
	$this->model_extension_report_support->install();
   }
	public function delete() {
		$this->load->language('extension/module/support');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/support');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $support_id) {
				$this->model_extension_report_support->deleteticket($support_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}
	
	
	
		public function view() {
			$this->load->language('extension/module/support');
			
		$this->load->model('extension/report/support');
		$this->load->model('user/user');
		$data['users'] = array();
	$data['user_token'] = $this->session->data['user_token'];
		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
        $data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_view') : $this->language->get('text_edit');
		
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_comment_msg'] = $this->language->get('text_comment_msg');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_submit'] = $this->language->get('text_submit');
		$data['text_history'] = $this->language->get('text_history');
		$data['text_user'] = $this->language->get('text_user');
		$data['text_new_comment'] = $this->language->get('text_new_comment');
		$data['support_id'] = $this->language->get('support_id');
	
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_department'] = $this->language->get('entry_department');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_notify'] = $this->language->get('entry_notify');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_change_status'] = $this->language->get('text_change_status');

		$data['column_date_added'] = $this->language->get('column_date_added');
	
		$data['column_notify'] = $this->language->get('column_notify');
		$data['column_comment'] = $this->language->get('column_comment');
		$data['column_status'] = $this->language->get('column_status');
		

		
		$data['button_comment'] = $this->language->get('button_comment');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['tab_general'] = $this->language->get('tab_general');
	
	

		$url = '';



		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_view'),
			'href' => $this->url->link('extension/module/support/view', 'user_token=' . $this->session->data['user_token'] . '&support_id=' . $this->request->get['support_id'] . $url, true)
		);

		if (!isset($this->request->get['support_id'])) {
			$data['action'] = $this->url->link('extension/module/support/view', 'user_token=' . $this->session->data['user_token'] . $url, true);
			
		} else {
			$data['action'] = $this->url->link('extension/module/support/view', 'user_token=' . $this->session->data['user_token'] . '&support_id=' . $this->request->get['support_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url, true);

		
		
		
		
		
		if (isset($this->request->get['support_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
		
		$support_info = $this->model_extension_report_support->getTicketsview($this->request->get['support_id']);
		
				foreach($support_info as $info){
					$data['user_info'][]=array(
					'email'=>$info['email'],
					'telephone'=>$info['telephone']
			
					);
				}
		
		
		$data['support']=$support_info[0]['support_id'];
		
		
		$data['statusData']=$this->model_extension_report_support->getstatus();
		$data['logusers']=$this->user->getUsername();
		$comment_info1 = $this->model_extension_report_support->getComment($this->request->get['support_id']);
	
			foreach($comment_info1 as $comments){
				if($comments['comment']){
					$comment=$comments['comment'];
				}
			else{
					$commnet='';
			 
			 	}
		  
			$data['CommentData'][] = array(
					'comment'  => $comment,
					'name'       => $comments['name'],
					'datetime'       => $comments['date_added'],
					'status'=>$comments['status']
					);
		 
		 
		 }

	   $data['file_info'] = $this->model_extension_report_support->getSupportFile($this->request->get['support_id']); 
			 
	  }
			
		 if (isset($this->request->get['support_id']) && ($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
			$logId = $this->user->getId();	
			
			$support_info = $this->model_extension_report_support->getTicketsview($this->request->get['support_id']);
				foreach($support_info as $info){
					$data['user_info'][]=array(
					'email'=>$info['email'],
					'telephone'=>$info['telephone']
			
					);
		
				 }
				
			$data['support']=$support_info[0]['support_id'];
				
			if(!empty($this->request->post['comment'])){
				
			$username = $this->model_extension_report_support->getname($logId);
			$name=$username[0]['firstname'].''.$username[0]['lastname'];
			$comment_info = $this->model_extension_report_support->addComment($this->request->get['support_id'],$this->request->post['comment'],$name);
				}
			$comment_info1 = $this->model_extension_report_support->getComment($this->request->get['support_id']);
		
				foreach($comment_info1 as $comments){
			
			 	 	if($comments['comment']){
			 
						$comment=$comments['comment'];
						}
					else{
							$commnet='';
							}
		 
		 
				$data['CommentData'][] = array(
					'comment'  => $comment,
					'name'       => $comments['name'],
					'datetime'       => $comments['date_added'],
					'status'=>$comments['status']
					);
		 
				}
		
			$data['statusData']=$this->model_extension_report_support->getstatus();
			
			
			$support_info = $this->model_extension_report_support->getTicketsview($this->request->get['support_id']);

	
		
			$data['file_info'] = $this->model_extension_report_support->getSupportFile($this->request->get['support_id']); 
			$url = '';
	
	
			if (isset($this->request->get['support_id'])) {
	
			$this->response->redirect($this->url->link('extension/module/support/view', 'user_token=' . $this->session->data['user_token'] . '&support_id=' . $this->request->get['support_id'] . $url, true));
			
			} else {
			$this->response->redirect($this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url, true));
			}
	
	
			
		}
	
	

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		
		
		
		  if (isset($this->request->post['comment'])) {		
			$data['comment'] = $this->request->post['comment'];
		} elseif (!empty($comment_info)) {
			$data['comment'] = $comment_info['comment'];
		}  else {
			$data['comment'] = '';
		}
		  if (isset($this->request->post['firstname'])) {		
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($comment_info)) {
			$data['firstname'] = $comment_info['firstname'];
		}  else {
			$data['firstname'] = '';
		}
		
		if (isset($this->request->post['subject'])) {
			$data['subject'] = $this->request->post['subject'];
		} elseif (!empty($support_info)) {
			$data['subject'] = $support_info[0]['subject'];
		} else {
			$data['subject'] = '';
		}
		if (isset($this->request->post['date_added'])) {
			$data['date_added'] = $this->request->post['date_added'];
		} elseif (!empty($support_info)) {
			$data['date_added'] = $support_info[0]['date_added'];
		} else {
			$data['date_added'] = '';
		}
		if (isset($this->request->post['department'])) {
			$data['department'] = $this->request->post['department'];
		} elseif (!empty($support_info)) {
			$data['department']    =  $support_info[0]['department'];
		} else {
			$data['department'] = '';
		}

		if (isset($this->request->post['customer'])) {
			$data['customer'] = $this->request->post['customer'];
		} elseif (!empty($support_info)) {
			$data['customer'] = $support_info[0]['firstname'].' '.$support_info[0]['lastname'];
		} else {
			$data['customer'] = '';
		}
		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} elseif (!empty($support_info)) {
			$data['message'] = $support_info[0]['message'];
		} else {
			$data['message'] = '';
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($support_info)) {
			$data['status_id'] = $support_info[0]['status_id'];
		
		} else {
			$data['status_id'] = '';
		}

		if (isset($this->request->post['notify'])) {		
			$data['notify'] = $this->request->post['notify'];
		} elseif (!empty($notify_data)) {
			$data['notify'] = $notify_data['notify'];
		}  else {
			$data['notify'] = '';
		}
		$data['histories'] = array();
		$this->load->model('extension/report/support');
		 $results = $this->model_extension_report_support->getHistory($this->request->get['support_id']);
		
		foreach ($results as $result) {
			
			$status=$this->model_extension_report_support->getStatusName($result['status_id']);
			 if( $result['notify']=='1'){
			$notify='yes';
	 
			}else{
	 
			$notify='no';
			}
 
			
			$data['histories'][] = array(
				'notify'     => $notify ,
				'status_id'     => $status[0]['name'],
				'message1'    => nl2br($result['message']),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$this->load->model('setting/store');

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/view', $data));
		
	}

	
	
	protected function getList(){
		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		
		if(isset($this->request->get['filter_department'])){
				
				$filter_department=$this->request->get['filter_department'];
				
				}else{
				$filter_department=null;
			}
			
		if(isset($this->request->get['filter_date_added'])){
				
				$filter_date_added=$this->request->get['filter_date_added'];
				
					
		}else{
				$filter_date_added=null;
			}
		if(isset($this->request->get['filter_status'])){
				
				$filter_status=$this->request->get['filter_status'];
					
		}else{
				$filter_status=null;
			}
		
	
		
	
		
		

		$url = '';
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		
		
		
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

			$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		$data['delete'] = $this->url->link('extension/module/support/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['products'] = array();

		
		$data['departments']=$this->model_extension_report_support->getDepartmentList();
		$data['statusData']=$this->model_extension_report_support->getstatus();
		
		
			$filter_data = array(
		
			'filter_department'=>$filter_department,
			'filter_date_added'=>$filter_date_added,
			'filter_status'=>$filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);
	

		$this->load->model('extension/report/support');

		$ticket_total = $this->model_extension_report_support->getTotalTickets($filter_data);

		$results = $this->model_extension_report_support->getTickets($filter_data);



		foreach ($results as $result) {
		
			$department=$this->model_extension_report_support->getDepartment($result['department']);
			
			$data['department']=$department;
		
			$customer=$this->model_extension_report_support->getCustomer($result['customer_id']);
			
			$data['customer']=$customer;
			
			$status=$this->model_extension_report_support->getStatusName($result['status_id']);
			
			$data['status']=$status;
		
			$notifyStatus=$this->model_extension_report_support->getStatusNotify($result['support_id']);
		
			if(isset($notifyStatus['notify'])){
			
				$notifyStatus['notify']=$notifyStatus['notify'];
				}
			else{
				$notifyStatus['notify']=0;
			}

			$data['tickets'][] = array(
				'support_id' => $result['support_id'],
				
				'subject'       => $result['subject'],
				'department'      => $department[0]['department'],
				'message'      => $result['message'],
				'customer'      => $customer[0]['firstname'].$customer[0]['lastname'],
				'date_added'=> $result['date_added'],
				'status'     => $status[0]['name'],
				'color'     => $status[0]['color'],
				'notify'     =>$notifyStatus['notify'] ,
				'view'       => $this->url->link('extension/module/support/view', 'user_token=' . $this->session->data['user_token'] . '&support_id=' . $result['support_id'] . $url, true)
			);
	
		
}



		


		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
	    $data['text_no_results'] = $this->language->get('text_no_results');
	    $data['text_select_status'] = $this->language->get('text_select_status');
	    $data['support_id'] = $this->language->get('support_id');

		$data['column_date_added'] = $this->language->get('column_date_added');
	
		$data['column_notify'] = $this->language->get('column_notify');
		$data['column_comment'] = $this->language->get('column_comment');
		$data['column_image'] = $this->language->get('column_image');
		$data['column_subject'] = $this->language->get('column_subject');
		$data['column_department'] = $this->language->get('column_department');
		$data['column_message'] = $this->language->get('column_message');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_notfiy'] = $this->language->get('column_notfiy');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['error_comment1'] = $this->language->get('error_comment1');
		$data['text_filter'] = $this->language->get('text_filter');

	
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		
		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if(isset($this->request->get['filter_department'])){
				
				$filter_status=$this->request->get['filter_department'];
			}else{
				$filter_status=null;
				
			}	
			if(isset($this->request->get['filter_date_added'])){
				
				$filter_date_added=$this->request->get['filter_date_added'];
			}else{
				$filter_date_added=null;
				
			}
		

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $ticket_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/module/support', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

	
		$data['results'] = sprintf($this->language->get('text_pagination'), ($ticket_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ticket_total - $this->config->get('config_limit_admin'))) ? $ticket_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ticket_total, ceil($ticket_total / $this->config->get('config_limit_admin')));

			
		$data['filter_department'] = $filter_department;
		$data['filter_date_added'] = $filter_date_added;
		$data['filter_status'] = $filter_status;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/ticket_list', $data));
	}

	
	
		public function history() {
			$this->load->language('extension/module/support');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_notify'] = $this->language->get('text_notify');
		$data['text_Ticked_status_changed'] = $this->language->get('text_Ticked_status_changed');

		$data['column_date_added'] = $this->language->get('column_date_added');
	
		$data['column_notify'] = $this->language->get('column_notify');
		$data['column_comment'] = $this->language->get('column_comment');
		$data['error_comment1'] = $this->language->get('error_comment1');
		
		$data['user_token'] = $this->session->data['user_token'];
		

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
			if (isset($this->request->post['comment1'])) {		
			$data['comment1'] = $this->request->post['comment1'];
		} elseif (!empty($results)) {
			$data['comment1'] = $notify_data['comment1'];
		}  else {
			$data['comment1'] = '';
		}
			if (isset($this->request->post['notify'])) {		
			$data['notify'] = $this->request->post['notify'];
		} elseif (!empty($results)) {
			$data['notify'] = $notify;
		}  else {
			$data['notify'] = '';
		}
		if (isset($this->error['comment1'])) {
			$data['error_comment1'] = $this->error['comment1'];
		} else {
			$data['error_comment1'] = '';
		}
		$this->load->model('extension/report/support');
		
		 if (isset($this->request->get['support_id']) && ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()){
		
		$notify_data = $this->model_extension_report_support->addNotify($this->request->post,$this->request->get['support_id']); 
		 
		$data['histories'] = array();
		$this->load->model('extension/report/support');
		 $results = $this->model_extension_report_support->getHistory($this->request->get['support_id']);
	
		foreach ($results as $result) {
			
			$status=$this->model_extension_report_support->getStatusName($result['status_id']);
			
			$data['histories'][] = array(
				'notify'     => $result['notify'] ,
				'status_id'     => $status[0]['name'],
				'message1'    => nl2br($result['message']),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}
	
	 	$htmlf='';
		
		
	
		$htmlf .= '<div class="table-responsive">';
		$htmlf .= '<table class="table table-bordered">';
    	
		$htmlf .= '<tbody>';
			if ($results) { 
			foreach ($results as $result) {
 
					
			$status=$this->model_extension_report_support->getStatusName($result['status_id']);
 
						if( $result['notify']=='1'){
						$notify='yes';
	 
							}else{
	 
							$notify='no';
							}
 
      	$htmlf .= '<tr>';
       	$htmlf .= ' <td class="text-left">'.$result['date_added'].'</td>';
      	$htmlf .= '  <td class="text-left">'.$result['message'].'</td>';
        $htmlf .= ' <td class="text-left">'. $status[0]['name'].'</td>';
       	$htmlf .= ' <td class="text-left">'. $notify.'</td>';

     	$htmlf .= ' </tr>';
			} 
		} else { 
     	$htmlf .= ' <tr>';
        	$htmlf .= '<td class="text-center" colspan="4"></td>';
    	$htmlf .= '  </tr>';
		 } 
     	$htmlf .= '</tbody>';
	 	$htmlf .= ' </table>';
		$htmlf .= '</div>';
			
		$this->session->data['success'] = $this->language->get('text_success');
		$url = '';
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($htmlf)); 
		
		
		 }
		
		
		}
			private function validate() {
				if($this->request->post['notify']!=' '){
		if ((utf8_strlen(trim($this->request->post['comment1'])) < 1)) {
			$this->error['comment1'] = $this->language->get('error_comment1');
		}
				}
		return !$this->error;
	}

	
	

}