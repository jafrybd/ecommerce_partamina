<?php
class ControllerExtensionModuleSupportSetting extends Controller {
	private $error = array();

	public function index() {
	$this->load->language('extension/module/support_setting');

		$this->load->model('setting/module');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');
		$this->load->model('extension/report/support');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_setting_setting->editSetting('support', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
	

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_status_list'] = $this->language->get('text_status_list');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_color'] = $this->language->get('entry_color');
		$data['entry_sort'] = $this->language->get('entry_sort');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true);
	
	

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['category_status'])) {
			$data['category_status'] = $this->request->post['category_status'];
		} else {
			$data['category_status'] = $this->config->get('category_status');
		}
       $this->load->language('extension/module/support_setting');

		
       
		
		$this->installt();
		$this->getform();
		
		
		
	}
	public function installt() { 
	$this->model_extension_report_support->install();
   }
	
	public function add(){
		$this->load->model('setting/module');;
		$this->load->model('extension/report/support');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$module_info = $this->model_extension_report_support->getModuleSetting('setting');
			
			if(!empty($module_info)){
				
							$this->model_extension_report_support->editSetting('setting' ,$this->request->post);			
				
			}else{
				
				
				$this->model_setting_module->addModule('setting', $this->request->post);
			
			} 

			
			
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		
		}
		
	$this->getForm();
		 
	}
	
   protected function getForm() {
		
		
		$this->load->language('extension/module/support_setting');
		$this->load->model('extension/report/support');
		
		$data['heading_title'] = $this->language->get('heading_title');

			
		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_create'] = $this->language->get('text_create');
		$data['text_create_admin'] = $this->language->get('text_create_admin');
		$data['text_comment_admin'] = $this->language->get('text_comment_admin');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_select_status'] = $this->language->get('text_select_status');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_faq'] = $this->language->get('entry_faq');
		
		$data['tab_customer'] = $this->language->get('tab_customer');
		$data['tab_admin'] = $this->language->get('tab_admin');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
	
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		
		

		$url = '';

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support_setting', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
		
		
		
			$data['action'] = $this->url->link('extension/module/support_setting/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		

		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		if (($this->request->server['REQUEST_METHOD'] != 'POST')) {
			 $module_info = $this->model_extension_report_support->getModuleSetting('setting');

		}

		$data['user_token'] = $this->session->data['user_token'];

		$data['statusData']=$this->model_extension_report_support->getstatus();
		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		
		if (isset($this->request->post['config_faq'])) {
			$data['config_faq'] = $this->request->post['config_faq'];
		} elseif (!empty($module_info)) {
			$data['config_faq'] = $module_info['config_faq'];
		} else {
			$data['config_faq'] = '';
		}
		if (isset($this->request->post['config_status'])) {
			$data['config_status'] = $this->request->post['config_status'];
		} elseif (!empty($module_info)) {
			$data['config_status'] = $module_info['config_status'];
		} else {
			$data['config_status'] = '';
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/support_setting_form', $data));
	}
	
	
	
	
	
	
	
			private function validate() {
				if($this->request->post['notify']!=' '){
		if ((utf8_strlen(trim($this->request->post['comment1'])) < 1)) {
			$this->error['comment1'] = $this->language->get('error_comment1');
		}
				}
		return !$this->error;
	}

	
	

}