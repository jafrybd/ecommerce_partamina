<?php
class ControllerExtensionModuleMailboxValidator extends Controller {
	private $error = array();
	protected $_fields = array('api_key', 'status', 'debug', 'remaining_credits', 'valid_email_validator', 'disposable_email_validator', 'free_email_validator', 'role_email_validator', 'custom_email_domain_validator', 'invalid_email_error_message', 'disposable_email_error_message', 'free_email_error_message', 'role_based_email_error_message', 'block_email_domain_error_message');
	public function index() { 
		$this->load->language('extension/module/mailboxvalidator');
		$this->document->setTitle($this->language->get('heading_title'));
		// $this->document->addScript('view/javascript/jquery.tagsinput.min.js');
		// $this->document->addScript('admin/view/javascript/mbv.js');
		// $this->document->addScript('view/javascript/mbv.js');
		
		if(file_exists('view/javascript/jquery.tagsinput.min.js')) {
			$this->document->addScript('view/javascript/jquery.tagsinput.min.js');
		}
		if(file_exists('view/javascript/mbv.js')) {
			$this->document->addScript('view/javascript/mbv.js');
		}
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_mailboxvalidator', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/mailboxvalidator', 'user_token=' . $this->session->data['user_token'] , true));
		}
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['api_key'])) {
			$data['error_api_key'] = $this->error['api_key'];
		} else {
			$data['error_api_key'] = '';
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_edit'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_name'),
			'href' => $this->url->link('extension/module/mailboxvalidator', 'user_token=' . $this->session->data['user_token'])
		);
		
		$data['action'] = $this->url->link('extension/module/mailboxvalidator', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		foreach($this->_fields as $field) {
			if (isset($this->request->post['module_mailboxvalidator_' . $field])) {
				$data['module_mailboxvalidator_' . $field] = $this->request->post['module_mailboxvalidator_' . $field];
			} else {
				$data['module_mailboxvalidator_' . $field] = $this->config->get('module_mailboxvalidator_' . $field);
			}
		}
		
		
		try {
			$Response = $this->Http('http://api.mailboxvalidator.com/plan?' . http_build_query(array(
				'key' => $data['module_mailboxvalidator_api_key'],
				// 'format' => 'json',
				// 'source' => 'opencart',
			)));
			if (is_null( $json = json_decode($Response, true)) === FALSE) {
				$data['module_mailboxvalidator_remaining_credits'] = $json['credits_available'];
			}
		} catch( Exception $e ) {
			$data['module_mailboxvalidator_remaining_credits'] = $this->config->get('module_mailboxvalidator_remaining_credits');
		}
		
		// if ($this->config->get('module_mailboxvalidator_remaining_credits') < 100) {
		if ($data['module_mailboxvalidator_remaining_credits'] < 100) {
			$data['error_low_credits'] = $this->language->get('error_low_credits');
		} else {
			$data['error_low_credits'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('extension/module/mailboxvalidator', $data));
	}
	protected function validate() { 
		if (!$this->user->hasPermission('modify', 'extension/module/mailboxvalidator')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['module_mailboxvalidator_api_key'])) {
			$this->error['api_key'] = $this->language->get('error_api_key');
		}

		return !$this->error;
	}
	public function install() { 
		$this->load->model('setting/setting');
		$settings = $this->model_setting_setting->getSetting('module_mailboxvalidator');

		if (!is_array($settings)) {
			$settings = array();
		}
		
		$settings['module_mailboxvalidator_status'] = 0;
		$settings['module_mailboxvalidator_debug'] = 0;
		$settings['module_mailboxvalidator_remaining_credits'] = 0;
		$settings['module_mailboxvalidator_api_key'] = '';
		$settings['module_mailboxvalidator_valid_email_validator'] = 0;
		$settings['module_mailboxvalidator_disposable_email_validator'] = 0;
		$settings['module_mailboxvalidator_free_email_validator'] = 0;
		$settings['module_mailboxvalidator_role_email_validator'] = 0;
		$settings['custom_email_domain_validator'] = '';
		$settings['invalid_email_error_message'] = 'Please enter a valid email address.';
		$settings['disposable_email_error_message'] = 'Please enter a non-disposable email address.';
		$settings['free_email_error_message'] = 'Please enter a non-free email address.';
		$settings['role_based_email_error_message'] = 'Please enter a non-role-based email address.';
		$settings['block_email_domain_error_message'] = 'This email domain has been denied access.';
		
		$this->model_setting_setting->editSetting('module_mailboxvalidator', $settings);
	}
	public function uninstall() { 
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('module_mailboxvalidator');
	}
	public function valid() {
		if (!$this->config->get('module_mailboxvalidator_status')) {
			return true;
		}
		if (empty($this->request->post['email'])) {
			return false;
		}
		$this->load->library('mailboxvalidator');
		$email = $this->request->post['email'];
		$result = $this->mailboxvalidator->validate($email);
		if ($result != '') {
			return $result;
		}
		
		return false;
	}
	private function Http($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		$Response = curl_exec($ch);
		curl_close($ch);

		return $Response;
	}
}