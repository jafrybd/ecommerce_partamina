<?php
class ControllerExtensionModuleMultilangzone extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/multilangzone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$status_status = $this->config->get('module_multilangzone_status');
			$this->request->post['module_multilangzone_first'] = '1';
			$this->model_setting_setting->editSetting('module_multilangzone', $this->request->post);
					
			if (isset($this->request->post['module_multilangzone_status']) ) {
				$this->cache->delete('zone');
				$this->load->model('setting/modification');
				$row = $this->model_setting_modification->getModificationByCode('zonetranslate');
				if ($this->request->post['module_multilangzone_status'] == 0 && $status_status!=0) {
					$this->model_setting_modification->disableModification($row['modification_id']);
					$this->response->redirect($this->url->link('marketplace/modification/refresh', 'user_token=' . $this->session->data['user_token'] , true));
				} elseif ($this->request->post['module_multilangzone_status'] == 1 && $status_status!=1) {
					$this->model_setting_modification->enableModification($row['modification_id']);
					$this->response->redirect($this->url->link('marketplace/modification/refresh', 'user_token=' . $this->session->data['user_token'] , true));
				}
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/multilangzone', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/multilangzone', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_multilangzone_status'])) {
			$data['module_multilangzone_status'] = $this->request->post['module_multilangzone_status'];
		} else {
			$data['module_multilangzone_status'] = $this->config->get('module_multilangzone_status');
		}
		
		if (!(isset($this->request->post['module_multilangzone_first']) or (null !==$this->config->get('module_multilangzone_first')))) {
		    $data['refreshmod'] = $this->url->link('marketplace/modification/refresh', 'user_token=' . $this->session->data['user_token'] , true);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/multilangzone', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/multilangzone')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function install() {
		$query = $this->db->query("SET AUTOCOMMIT = 0;");
		$query = $this->db->query("START TRANSACTION;");
		$query = $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX .  "zone_description`;");
		$query = $this->db->query("CREATE TABLE `" . DB_PREFIX .  "zone_description` ( `zone_id` int(11) NOT NULL,  `language_id` int(11) NOT NULL,  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
		$query = $this->db->query("ALTER TABLE `" . DB_PREFIX .  "zone_description` ADD PRIMARY KEY (`zone_id`,`language_id`);");
		$query = $this->db->query("INSERT INTO `" . DB_PREFIX .  "zone_description`  SELECT zone_id,'1',name FROM `" . DB_PREFIX .  "zone` ORDER BY zone_id;");
		$query = $this->db->query("ALTER TABLE `" . DB_PREFIX .  "zone` ADD `sort_order` INT(3) NOT NULL AFTER `status`;");
		$query = $this->db->query("COMMIT;");
		$this->cache->delete('zone');
	}
	
	public function uninstall() {
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('module_multilangzone');
		$query = $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX .  "zone_description`;");
		$query = $this->db->query("ALTER TABLE `" . DB_PREFIX .  "zone` DROP `sort_order`;");
		$this->load->model('setting/modification');
		$row = $this->model_setting_modification->getModificationByCode('zonetranslate');
		$this->model_setting_modification->disableModification($row['modification_id']);
		$this->response->redirect($this->url->link('marketplace/modification/refresh', 'user_token=' . $this->session->data['user_token'] , true));
	}
	
}