$(document).ready(function() {
    var currentRequest = null;

    /**
     * Get param from url
     * @param string name
     */
    var getUrlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return (results==null) ? null : (decodeURIComponent(results[1]) || 0);
    }

    // Bootstrap 4.0 fix
    $('.btn-group-toggle > .btn').click(function() {
        $($(this).data('target')).collapse($(this).hasClass('active') ? 'hide' : 'show')
    });

    $('body').on('change', 'input[name=email]', function(){
        var emailField = $(this);

        currentRequest = $.ajax({
            type: 'POST',
            url: 'index.php?route=extension/module/email_validation/check&user_token=' + getUrlParam('user_token'),
            dataType: 'json',
            data: {
                'email': emailField.val(),
                'action_route': getUrlParam('route')
            },
            beforeSend : function() {
                if (currentRequest != null) {
                    currentRequest.abort();
                }

                if (emailField.parent('.input-group').length) {
                    emailField.parent('.input-group').next('.text-danger').remove();
                } else if (emailField.next('.text-danger').length) {
                    emailField.next('.text-danger').remove();
                }
            },
            success: function(json) {
                var msg;

                if (emailField.parent('.input-group').length) {
                    if (!emailField.parent('.input-group').next('.text-danger').length) {
                        emailField.parent('.input-group').after('<div class="text-danger"></div>')
                    }

                    msg = emailField.parent('.input-group').next('.text-danger')
                } else {
                    if (!emailField.next('.text-danger').length) {
                        emailField.after('<div class="text-danger"></div>')
                    }

                    msg = emailField.next('.text-danger')
                }

                if (json && json['error']) {
                    msg.html(json['error']);
                } else if (msg.length) {
                    msg.remove();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + " " + xhr.statusText + " " + xhr.responseText);
            }
        });
    });

    var validateCustomers;
    var currentValidateRequest;
    var totalValidatedCustomers = 0;

    $('#action-validate-customers').click(function() {
        var $btn = $(this);
        $btn.button('loading');

        validateCustomers = true;

        var ajaxCall = function(url) {
            if (!validateCustomers) return false;

            $('#cancel-validate-customers').removeClass('disabled').removeAttr('disabled')

            currentValidateRequest = $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: $('#form-module').find(':input').serialize(),
                beforeSend : function() {
                    if (currentValidateRequest != null) {
                        currentValidateRequest.abort();
                    }
                },
                success: function(json) {
                    if (json['total']) {
                        totalValidatedCustomers += json['total'];
                    }

                    var message = json['success'] ? json['success'].replace('%s', totalValidatedCustomers) : '';

                    if ($('#modal-customer-alert').length) {
                        $('#modal-customer-alert').html('<i class="fa fa-tick-circle"></i> ' + message);
                    } else {
                        $('#tab-customer').prepend('<div class="alert alert-success" id="modal-customer-alert"><i class="fa fa-tick-circle"></i> ' + message + '</div>');
                    }

                    if (json['next']) {
                        ajaxCall(json['next']);
                    } else {
                        $btn.button('reset');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + " " + xhr.statusText + " " + xhr.responseText);
                }
            });
        };

        ajaxCall('index.php?route=extension/module/email_validation/validate_customers&user_token=' + getUrlParam('user_token'));

        return false;
    });

    $('#cancel-validate-customers').click(function() {
        $(this).addClass('disabled').attr('disabled', 'disabled');

        validateCustomers = false;

        if (currentValidateRequest != null) {
            currentValidateRequest.abort();
        }

        $('#action-validate-customers').button('reset');
    });

    $('#submit-email-validation').click(function() {
        var $btn = $(this);
        $btn.button('loading');

        var $selected = $('input[name=\'selected[]\']:checked');

        if (!$selected.length) {
            $btn.button('reset');
            return false;
        }

        $selected.parents('tr').find('.data-email-validation').html('<i class="fa fa-spinner fa-spin fa-fw"></i>');

        $selected.each(function() {
            var $this = $(this);
            var $selected_row = $(this).parents('tr');

            $.ajax({
                type: 'POST',
                url: 'index.php?route=extension/module/email_validation/valid&user_token=' + getUrlParam('user_token'),
                dataType: 'json',
                data: {
                    'email': $selected_row.find('[data-email]').data('email')
                },
                success: function(json) {
                    $btn.button('reset');

                    if (!$selected_row.find('.data-email-validation')) {
                        $selected_row.find('[data-email]').insertBefore("<span class='data-email-validation'></span>");
                    }

                    if (json['error']) {
                        $selected_row.find('.data-email-validation').html('<span data-toggle="tooltip" title="' + json['error'] + '"><i class="fa fa-fw fa-thumbs-down text-warning"></i></span>');
                    } else if (json['reason']) {
                        $selected_row.find('.data-email-validation').html('<span data-toggle="tooltip" title="' + json['reason'] + '"><i class="fa fa-fw fa-thumbs-up text-success"></i></span>');
                    }

                    $this.prop('checked', false);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + " " + xhr.statusText + " " + xhr.responseText);
                }
            });
        });

        return false;
    });

    $('#modal-result').on('show.bs.modal', function () {
        var $tab = $(this).find('.modal-dialog');

        var modal_results_url = 'index.php?route=extension/module/email_validation/result&user_token=' + getUrlParam('user_token');

        function initLoadedHtml() {
            var loading_html = '<div class="panel-body text-center mt-4 mb-4"><i class="fa fa-spinner fa-4x fa-spin text-primary mb-2"></i></div>';

            $tab.find('#button-filter').off('click').click(function(e) {
                e.preventDefault();

                $tab.find('[data-toggle=tooltip]').tooltip('hide');

                $tab.find('.panel-body').html(loading_html);

                var url = modal_results_url;

                var filter_name = $('#input-name').val();
                if (filter_name) {
                    url += '&filter_name=' + filter_name;
                }

                var filter_email = $('#input-email').val();
                if (filter_email) {
                    url += '&filter_email=' + filter_email;
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'html',
                    success: function(html) {
                        $tab.html(html);
                        initLoadedHtml();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(thrownError + " " + xhr.statusText + " " + xhr.responseText);
                    }
                });
            });

            $tab.find('.thead-sort a').off('click').click(function(e) {
                e.preventDefault();

                var sort = getUrlParam('sort', this.getAttribute('href'));
                var order = ($(this).hasClass('asc') ? 'ASC' : 'DESC');

                $('[name=sort]').val(sort);
                $('[name=order]').val(order);

                $tab.find('[data-toggle=tooltip]').tooltip('hide');

                $tab.find('.panel-body').html(loading_html);

                modal_results_url = this.href;

                $tab.load(this.href, initLoadedHtml);
            });

            $tab.find('.pagination a').off('click').click(function(e) {
                e.preventDefault();

                $tab.find('[data-toggle=tooltip]').tooltip('hide');

                $tab.find('.panel-body').html(loading_html);

                modal_results_url = this.href;

                $tab.load(this.href, initLoadedHtml);
            });

            $tab.find('.pagination select').removeAttr('onchange').off('change').change(function(e) {
                e.preventDefault();

                $tab.find('[data-toggle=tooltip]').tooltip('hide');

                $tab.find('.panel-body').html(loading_html);

                modal_results_url = this.href;

                $tab.load(this.value, initLoadedHtml);

                return false;
            });
        }

        if ($tab.is(':empty')) {
            $.ajax({
                type: 'GET',
                url: modal_results_url,
                dataType: 'html',
                success: function (html) {
                    $tab.html(html);

                    initLoadedHtml();
                }
            });
        }
    });
});