<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {

		$temp = $this->config->get('config_language_id');
		$zone_data = $this->cache->get(
		'zone.' . (int)$country_id . '.' . (int)$temp);

		if (!$zone_data) {
			
			$query = $this->db->query("SELECT DISTINCT z.*, IFNULL(zd.name, z.name) as name FROM `" . DB_PREFIX . "zone` z LEFT JOIN " . DB_PREFIX . "zone_description zd ON  z.zone_id  = zd.zone_id AND zd.language_id = '" . (int)$this->config->get('config_language_id') . "' WHERE country_id = '" . (int)$country_id . "' and z.status = '1'  ORDER BY z.sort_order ASC, zd.name ASC");

			$zone_data = $query->rows;

			$this->cache->set(
		'zone.' . (int)$country_id . '.' . (int)$temp, $zone_data);
		}

		return $zone_data;
	}
}