<?php
class ModelLocalisationCountry extends Model {
	public function getCountry($country_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND status = '1'");

		return $query->row;
	}

	public function getCountries() {

		$temp = $this->config->get('config_language_id');
		$country_data = $this->cache->get(
		'country.catalog' . (int)$temp);

		if (!$country_data) {
			
			$query = $this->db->query("SELECT DISTINCT c.*, IFNULL(cd.name, c.name) as name FROM `" . DB_PREFIX . "country` c LEFT JOIN " . DB_PREFIX . "country_description cd ON (c.country_id  = cd.country_id ) AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' WHERE c.status = '1'  ORDER BY c.sort_order ASC, cd.name ASC");

			$country_data = $query->rows;

			$this->cache->set(
		'country.catalog' . (int)$temp, $country_data);
		}

		return $country_data;
	}
}