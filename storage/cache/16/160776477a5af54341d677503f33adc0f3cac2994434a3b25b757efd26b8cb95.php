<?php

/* __string_template__756d47ecdb9d19b061f432225b4c404fdfe9e95ec7ea335f81af5eb239db4079 */
class __TwigTemplate_af53f7e6d324ba50849046bd1a2abb71129158c81cac51dac3cea81c42c11b5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<footer style=\"box-shadow: 0px -3px 6px rgba(0,0,0,.16);\">
  <div class=\"container m-ftr-link cmn-width\" style=\"padding-top: 20px;\">
    <div class=\"row\" style=\"padding-bottom: 30px;\">
      <div class=\"col-sm-2 wid widd\">
       
        <ul class=\"list-unstyled fimg\">
          <img src=\"image/ppc.png\" height=\"150px\" class=\"ext\"/>
          
        </ul>
      </div>
      ";
        // line 13
        if ((isset($context["informations"]) ? $context["informations"] : null)) {
            // line 14
            echo "      <div class=\"col-sm-2 wid widd\">
        <h5>";
            // line 15
            echo (isset($context["text_information"]) ? $context["text_information"] : null);
            echo "</h5>
        <ul class=\"list-unstyled\">
         ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 18
                echo "          <li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\"> ";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "          
        </ul>
      </div>
      ";
        }
        // line 24
        echo "      
      
      
      <div class=\"col-sm-2 wid widd\">
        <h5>";
        // line 28
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
        // line 30
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</a></li>
           ";
        // line 31
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 32
            echo "          <li><a href=\"";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\"> ";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
           ";
        }
        // line 34
        echo "          <li><a href=\"";
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</a></li>
           <li><a href=\"";
        // line 35
        echo (isset($context["return"]) ? $context["return"] : null);
        echo "\"> ";
        echo (isset($context["text_return"]) ? $context["text_return"] : null);
        echo "</a></li>
          
        </ul>
      </div>
      <div class=\"col-sm-3 ftr-contact widTwo\">
        <h5>";
        // line 40
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</h5>
        <table><tr><td style=\"padding-right:20px;color:black;\"><i class=\"fa fa-map-marker\" style=\"font-size: 18px;\" aria-hidden=\"true\" ></i></td><td style=\"color:black;\">
          ";
        // line 42
        echo (isset($context["text_siteAddress"]) ? $context["text_siteAddress"] : null);
        echo " </td></tr></table>

\t\t<table><tr><td style=\"padding-right:10px;color:black;padding-top: 10px;padding-bottom: 15px;\"><i class=\"fa fa-envelope-o\" aria-hidden=\"true\" style=\"padding-right:10px;\"></i></td><td style=\"color:black;\">
         ";
        // line 45
        echo (isset($context["text_email"]) ? $context["text_email"] : null);
        echo "
          
\t\t</td></tr></table>
        
        ";
        // line 49
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 50
            echo "        <p >
        
        <i class=\"fa fa-comments\" aria-hidden=\"true\" style=\"padding-right:10px;\"></i>
\t\t<a href=\"https://www.medinatech.co/ecommerce/index.php?route=information/contact\">
            ";
            // line 54
            echo (isset($context["text_service"]) ? $context["text_service"] : null);
            echo "
        </a>
        
        </p>
        
        ";
        }
        // line 60
        echo "        
        
      
        
        <a href=\"\">
          <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"2em\" height=\"2em\" preserveAspectRatio=\"xMidYMid meet\" viewBox=\"0 0 448 512\" class=\"iconify\" data-icon=\"fa-brands:facebook-square\" data-inline=\"false\" style=\"transform: rotate(360deg);\"><path d=\"M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48c27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z\" fill=\"currentColor\"></path></svg>
        </a>
        
        <a href=\"mailto:cmo@pertaminabd.com\">
          <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"2.3em\" height=\"2em\" preserveAspectRatio=\"xMidYMid meet\" viewBox=\"0 0 16 16\" class=\"iconify\" data-icon=\"icomoon-free:mail\" data-inline=\"false\" style=\"transform: rotate(360deg);\"><path d=\"M13.333 0H2.667A2.675 2.675 0 0 0 0 2.667v10.666C0 14.8 1.2 16 2.667 16h10.666C14.801 16 16 14.8 16 13.333V2.667A2.674 2.674 0 0 0 13.333 0zM4 4h8c.143 0 .281.031.409.088L8 9.231L3.591 4.088A.982.982 0 0 1 4 4zm-1 7V5l.002-.063l2.932 3.421l-2.9 2.9A.967.967 0 0 1 3 11zm9 1H4c-.088 0-.175-.012-.258-.034L6.588 9.12l1.413 1.648L9.414 9.12l2.846 2.846a.967.967 0 0 1-.258.034zm1-1c0 .088-.012.175-.034.258l-2.9-2.9l2.932-3.421L13 5v6z\" fill=\"currentColor\"></path></svg>
        </a>
      </div>
    </div>      
  </div>
<!--footer social,payments,App-->
      <section class=\"footer-down\" >
      <div class=\"container\">
        <div class=\"row\">
             <div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"width:100%;\">
                <div class=\"\">
                    ";
        // line 81
        echo "                   <p class=\"power-by text-center\" style=\"color:white; font-weight:400;margin-top: -14px;\"><small>";
        echo (isset($context["text_authorizedBy"]) ? $context["text_authorizedBy"] : null);
        echo "</small></p>
                </div>
            </div>
           
        </div>
      </div>
    </section>
        <!--footer social,payments,App End--> 
</footer>
";
        // line 91
        echo "<img src=\"image/catalog/banner/banner-pt.png\" style=\"display: block;\" class=\"pertamina-logo\" />


";
        // line 94
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 95
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
</body></html>";
    }

    public function getTemplateName()
    {
        return "__string_template__756d47ecdb9d19b061f432225b4c404fdfe9e95ec7ea335f81af5eb239db4079";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 97,  187 => 95,  183 => 94,  178 => 91,  165 => 81,  143 => 60,  134 => 54,  128 => 50,  126 => 49,  119 => 45,  113 => 42,  108 => 40,  98 => 35,  91 => 34,  83 => 32,  81 => 31,  75 => 30,  70 => 28,  64 => 24,  58 => 20,  47 => 18,  43 => 17,  38 => 15,  35 => 14,  33 => 13,  19 => 1,);
    }
}
/* */
/* */
/* <footer style="box-shadow: 0px -3px 6px rgba(0,0,0,.16);">*/
/*   <div class="container m-ftr-link cmn-width" style="padding-top: 20px;">*/
/*     <div class="row" style="padding-bottom: 30px;">*/
/*       <div class="col-sm-2 wid widd">*/
/*        */
/*         <ul class="list-unstyled fimg">*/
/*           <img src="image/ppc.png" height="150px" class="ext"/>*/
/*           */
/*         </ul>*/
/*       </div>*/
/*       {% if informations %}*/
/*       <div class="col-sm-2 wid widd">*/
/*         <h5>{{ text_information }}</h5>*/
/*         <ul class="list-unstyled">*/
/*          {% for information in informations %}*/
/*           <li><a href="{{ information.href }}"> {{ information.title }}</a></li>*/
/*           {% endfor %}*/
/*           */
/*         </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*       */
/*       */
/*       */
/*       <div class="col-sm-2 wid widd">*/
/*         <h5>{{ text_account }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/*            {% if logged %}*/
/*           <li><a href="{{ order }}"> {{ text_order }}</a></li>*/
/*            {% endif %}*/
/*           <li><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>*/
/*            <li><a href="{{ return }}"> {{ text_return }}</a></li>*/
/*           */
/*         </ul>*/
/*       </div>*/
/*       <div class="col-sm-3 ftr-contact widTwo">*/
/*         <h5>{{ text_contact }}</h5>*/
/*         <table><tr><td style="padding-right:20px;color:black;"><i class="fa fa-map-marker" style="font-size: 18px;" aria-hidden="true" ></i></td><td style="color:black;">*/
/*           {{ text_siteAddress }} </td></tr></table>*/
/* */
/* 		<table><tr><td style="padding-right:10px;color:black;padding-top: 10px;padding-bottom: 15px;"><i class="fa fa-envelope-o" aria-hidden="true" style="padding-right:10px;"></i></td><td style="color:black;">*/
/*          {{ text_email }}*/
/*           */
/* 		</td></tr></table>*/
/*         */
/*         {% if logged %}*/
/*         <p >*/
/*         */
/*         <i class="fa fa-comments" aria-hidden="true" style="padding-right:10px;"></i>*/
/* 		<a href="https://www.medinatech.co/ecommerce/index.php?route=information/contact">*/
/*             {{ text_service }}*/
/*         </a>*/
/*         */
/*         </p>*/
/*         */
/*         {% endif %}*/
/*         */
/*         */
/*       */
/*         */
/*         <a href="">*/
/*           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512" class="iconify" data-icon="fa-brands:facebook-square" data-inline="false" style="transform: rotate(360deg);"><path d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48c27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z" fill="currentColor"></path></svg>*/
/*         </a>*/
/*         */
/*         <a href="mailto:cmo@pertaminabd.com">*/
/*           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2.3em" height="2em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16" class="iconify" data-icon="icomoon-free:mail" data-inline="false" style="transform: rotate(360deg);"><path d="M13.333 0H2.667A2.675 2.675 0 0 0 0 2.667v10.666C0 14.8 1.2 16 2.667 16h10.666C14.801 16 16 14.8 16 13.333V2.667A2.674 2.674 0 0 0 13.333 0zM4 4h8c.143 0 .281.031.409.088L8 9.231L3.591 4.088A.982.982 0 0 1 4 4zm-1 7V5l.002-.063l2.932 3.421l-2.9 2.9A.967.967 0 0 1 3 11zm9 1H4c-.088 0-.175-.012-.258-.034L6.588 9.12l1.413 1.648L9.414 9.12l2.846 2.846a.967.967 0 0 1-.258.034zm1-1c0 .088-.012.175-.034.258l-2.9-2.9l2.932-3.421L13 5v6z" fill="currentColor"></path></svg>*/
/*         </a>*/
/*       </div>*/
/*     </div>      */
/*   </div>*/
/* <!--footer social,payments,App-->*/
/*       <section class="footer-down" >*/
/*       <div class="container">*/
/*         <div class="row">*/
/*              <div class="col-md-4 col-sm-6 col-xs-12" style="width:100%;">*/
/*                 <div class="">*/
/*                     {#{{ powered }}#}*/
/*                    <p class="power-by text-center" style="color:white; font-weight:400;margin-top: -14px;"><small>{{ text_authorizedBy }}</small></p>*/
/*                 </div>*/
/*             </div>*/
/*            */
/*         </div>*/
/*       </div>*/
/*     </section>*/
/*         <!--footer social,payments,App End--> */
/* </footer>*/
/* {# <a href="" id="go-top" title="Back To Top" style="display: block;"><i class="fa fa-angle-double-up"></i></a> #}*/
/* <img src="image/catalog/banner/banner-pt.png" style="display: block;" class="pertamina-logo" />*/
/* */
/* */
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* <!--*/
/* OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.*/
/* Please donate via PayPal to donate@opencart.com*/
/* //-->*/
/* </body></html>*/
