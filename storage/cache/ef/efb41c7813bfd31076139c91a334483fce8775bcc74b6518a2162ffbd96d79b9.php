<?php

/* default/template/extension/module/html.twig */
class __TwigTemplate_decffcca39c275d7165428e94b4317ea369a8396fe66da8fa5f7418cfee86686 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>";
        if ((isset($context["heading_title"]) ? $context["heading_title"] : null)) {
            // line 2
            echo "  <h2>";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h2>
  ";
        }
        // line 4
        echo "  ";
        echo (isset($context["html"]) ? $context["html"] : null);
        echo "</div>
<!--static banners -->
<!-- <div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-6 col-xs-12 col-sm-6 b-effect\">
            <div class=\"img-effect\">
                <a href=\"#\">
                <img src=\"image/catalog/banner1.png\" alt=\"banner\" class=\"img img-responsive\">
                </a>
            </div>
        </div>      
    </div>
    <div class=\"row\">
        <div class=\"col-md-6 col-md-offset-6 col-sm-offset-6 col-xs-offset-6 col-xs-12 b-effect\">
            <div class=\"img-effect\">
                <a href=\"#\">
                <img src=\"image/catalog/banner2.png\" alt=\"banner\" class=\"img img-responsive\">
                </a>
            </div>
        </div>      
    </div>
    <div class=\"row\">
        <div class=\"col-md-6 col-xs-12 col-sm-6 b-effect\">
            <div class=\"img-effect\">
                <a href=\"#\">
                <img src=\"image/catalog/banner3.png\" alt=\"banner\" class=\"img img-responsive\">
                </a>
            </div>
        </div>      
    </div>
</div> -->
<!--static banners -->


<script>
\t\$(function(){
\tvar\tbtn = \$(\".slider__btn\");
\t
\tbtn.on(\"click\",function(){
\t\t\$(\".slider__item\").first().clone().appendTo(\".slider\");
\t\t\$(\".slider__image\").first().css({transform: \"rotateX(-180deg)\", opacity: 0});
\t\tsetTimeout(function(){
\t\t\t\$(\".slider__item\").first().remove();
\t\t},200);
\t});
});
</script>


<script type=\"text/javascript\"><!--
   \$(document).ready(function() {
    \$(\"#testimonial\").owlCarousel({
    itemsCustom : [
    [0, 1],
    [500, 1],
    [768, 1],
    [992, 1],
    [1200, 1],
    ],
            //autoPlay: 3000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : false,
            pagination:false
    });
    });
--></script>


<script>
\t  jQuery('.statistic-counter').counterUp({
                delay: 10,
                time: 2000
            });
</script>

<!--about slider start -->
<script>
 \$(document).ready(function() {
    \$(\"#about-slider\").owlCarousel({
    itemsCustom : [
    [0, 1]  
    ],
             autoPlay: 3000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : true,
            pagination:true
    });
    });
</script>
<!-- about slider end-->
<!--slider javascript-->
<script>
 \$(document).ready(function() {
    \$(\"#image-slider\").owlCarousel({
    itemsCustom : [
    [0, 1],
    [500, 2],
    [768, 2],
    [992, 3],
    [1200, 4],
    ],
            // autoPlay: 1000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : true,
            pagination:false
    });
    });
</script>
<!--slider javascript end-->
<script>
    \$(document).ready(function(){

    \$(\".filter-button\").click(function(){
        var value = \$(this).attr('data-filter');
        
        if(value == \"all\")
        {
            //\$('.filter').removeClass('hidden');
            \$('.filter').show('1000');
        }
        else
        {
//            \$('.filter[filter-item=\"'+value+'\"]').removeClass('hidden');
//            \$(\".filter\").not('.filter[filter-item=\"'+value+'\"]').addClass('hidden');
            \$(\".filter\").not('.'+value).hide('3000');
            \$('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if (\$(\".filter-button\").removeClass(\"active\")) {
\$(this).removeClass(\"active\");
}
\$(this).addClass(\"active\");

});
</script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 4,  22 => 2,  19 => 1,);
    }
}
/* <div>{% if heading_title %}*/
/*   <h2>{{ heading_title }}</h2>*/
/*   {% endif %}*/
/*   {{ html }}</div>*/
/* <!--static banners -->*/
/* <!-- <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-6 col-xs-12 col-sm-6 b-effect">*/
/*             <div class="img-effect">*/
/*                 <a href="#">*/
/*                 <img src="image/catalog/banner1.png" alt="banner" class="img img-responsive">*/
/*                 </a>*/
/*             </div>*/
/*         </div>      */
/*     </div>*/
/*     <div class="row">*/
/*         <div class="col-md-6 col-md-offset-6 col-sm-offset-6 col-xs-offset-6 col-xs-12 b-effect">*/
/*             <div class="img-effect">*/
/*                 <a href="#">*/
/*                 <img src="image/catalog/banner2.png" alt="banner" class="img img-responsive">*/
/*                 </a>*/
/*             </div>*/
/*         </div>      */
/*     </div>*/
/*     <div class="row">*/
/*         <div class="col-md-6 col-xs-12 col-sm-6 b-effect">*/
/*             <div class="img-effect">*/
/*                 <a href="#">*/
/*                 <img src="image/catalog/banner3.png" alt="banner" class="img img-responsive">*/
/*                 </a>*/
/*             </div>*/
/*         </div>      */
/*     </div>*/
/* </div> -->*/
/* <!--static banners -->*/
/* */
/* */
/* <script>*/
/* 	$(function(){*/
/* 	var	btn = $(".slider__btn");*/
/* 	*/
/* 	btn.on("click",function(){*/
/* 		$(".slider__item").first().clone().appendTo(".slider");*/
/* 		$(".slider__image").first().css({transform: "rotateX(-180deg)", opacity: 0});*/
/* 		setTimeout(function(){*/
/* 			$(".slider__item").first().remove();*/
/* 		},200);*/
/* 	});*/
/* });*/
/* </script>*/
/* */
/* */
/* <script type="text/javascript"><!--*/
/*    $(document).ready(function() {*/
/*     $("#testimonial").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 1],*/
/*     [500, 1],*/
/*     [768, 1],*/
/*     [992, 1],*/
/*     [1200, 1],*/
/*     ],*/
/*             //autoPlay: 3000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : false,*/
/*             pagination:false*/
/*     });*/
/*     });*/
/* --></script>*/
/* */
/* */
/* <script>*/
/* 	  jQuery('.statistic-counter').counterUp({*/
/*                 delay: 10,*/
/*                 time: 2000*/
/*             });*/
/* </script>*/
/* */
/* <!--about slider start -->*/
/* <script>*/
/*  $(document).ready(function() {*/
/*     $("#about-slider").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 1]  */
/*     ],*/
/*              autoPlay: 3000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : true,*/
/*             pagination:true*/
/*     });*/
/*     });*/
/* </script>*/
/* <!-- about slider end-->*/
/* <!--slider javascript-->*/
/* <script>*/
/*  $(document).ready(function() {*/
/*     $("#image-slider").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 1],*/
/*     [500, 2],*/
/*     [768, 2],*/
/*     [992, 3],*/
/*     [1200, 4],*/
/*     ],*/
/*             // autoPlay: 1000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : true,*/
/*             pagination:false*/
/*     });*/
/*     });*/
/* </script>*/
/* <!--slider javascript end-->*/
/* <script>*/
/*     $(document).ready(function(){*/
/* */
/*     $(".filter-button").click(function(){*/
/*         var value = $(this).attr('data-filter');*/
/*         */
/*         if(value == "all")*/
/*         {*/
/*             //$('.filter').removeClass('hidden');*/
/*             $('.filter').show('1000');*/
/*         }*/
/*         else*/
/*         {*/
/* //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');*/
/* //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');*/
/*             $(".filter").not('.'+value).hide('3000');*/
/*             $('.filter').filter('.'+value).show('3000');*/
/*             */
/*         }*/
/*     });*/
/*     */
/*     if ($(".filter-button").removeClass("active")) {*/
/* $(this).removeClass("active");*/
/* }*/
/* $(this).addClass("active");*/
/* */
/* });*/
/* </script>*/
/* */
