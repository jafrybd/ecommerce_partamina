<?php

/* __string_template__450f490f7a4858470caeeab53c9df25895a6acb507ae819bdf0c7bc01b6b72b1 */
class __TwigTemplate_ba1bfac6dde416ac0b8cd1882b71b2ef1cfa95320ae747622203a76c13f68b7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"breadcrumb-bg\">
  <div class=\"container\">
    <ul class=\"breadcrumb\">
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 6
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "  </ul>
  </div>
</div>
<div id=\"product-product\" class=\"container\"> 
  <div class=\"row\">";
        // line 12
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 13
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 14
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 15
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 16
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 17
            echo "    ";
        } else {
            // line 18
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 19
            echo "    ";
        }
        // line 20
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\"  style=\"top:0px!important;\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <div class=\"row p-p\">
       ";
        // line 22
        if (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 23
            echo "        ";
            $context["class"] = "col-sm-6";
            // line 24
            echo "        ";
        } else {
            // line 25
            echo "        ";
            $context["class"] = "col-sm-5";
            // line 26
            echo "        ";
        }
        // line 27
        echo "         <!-- SB-theme image & Additional Image Start -->
        <div class=\"";
        // line 28
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
         ";
        // line 29
        if (((isset($context["thumb"]) ? $context["thumb"] : null) || (isset($context["images"]) ? $context["images"] : null))) {
            // line 30
            echo "          <ul class=\"thumbnails col-sm-12 sb-theme-img-box\">
            ";
            // line 31
            if ((isset($context["thumb"]) ? $context["thumb"] : null)) {
                // line 32
                echo "            <li>
            <a class=\"thumbnail\" href=\"";
                // line 33
                echo (isset($context["popup"]) ? $context["popup"] : null);
                echo "\" title=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\">
            <img data-zoom-image=\"";
                // line 34
                echo (isset($context["popup"]) ? $context["popup"] : null);
                echo "\" src=\"";
                echo (isset($context["thumb"]) ? $context["thumb"] : null);
                echo "\" title=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" alt=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" id=\"zoom_03\"  class=\"img-responsive\" />
            </a>
            </li>
            ";
            }
            // line 38
            echo "            ";
            if ((isset($context["images"]) ? $context["images"] : null)) {
                // line 39
                echo "           

            <li class=\"col-sm-4 \"  id=\"sb-additional-img\" class=\"owl-carousel\">
             ";
                // line 42
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 43
                    echo "            <a data-zoom-image=\"";
                    echo $this->getAttribute($context["image"], "popup", array());
                    echo "\" data-image=\"";
                    echo $this->getAttribute($context["image"], "popup", array());
                    echo "\" href=\"";
                    echo $this->getAttribute($context["image"], "popup", array());
                    echo "\" title=\"";
                    echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                    echo "\">
             <img src=\"";
                    // line 44
                    echo $this->getAttribute($context["image"], "thumb", array());
                    echo "\" title=\"";
                    echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                    echo "\" alt=\"";
                    echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                    echo "\" class=\"img-responsive\" />
             </a>
             ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 47
                echo "             </li>
            
            ";
            }
            // line 50
            echo "          </ul>
          ";
        }
        // line 52
        echo "          </div>
           <!-- SB-theme image & Additional Image end -->                 
        ";
        // line 54
        if (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 55
            echo "        ";
            $context["class"] = "col-sm-6";
            // line 56
            echo "        ";
        } else {
            // line 57
            echo "        ";
            $context["class"] = "col-sm-7";
            // line 58
            echo "        ";
        }
        // line 59
        echo "        <div class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">

        <!-- SB-theme product Description Part Start -->
             <div class=\"col-sm-12\">
                <h3 class=\"p-p-title\">";
        // line 63
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
                <p class=\"\">";
        // line 64
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "</p>
                <ul class=\"list-unstyled p-p-detail\">
                  ";
        // line 66
        if ((isset($context["manufacturer"]) ? $context["manufacturer"] : null)) {
            // line 67
            echo "                  <li><span>";
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo "</span> <a href=\"";
            echo (isset($context["manufacturers"]) ? $context["manufacturers"] : null);
            echo "\">";
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo "</a></li>
                  ";
        }
        // line 69
        echo "                  <li><span>";
        echo (isset($context["text_model"]) ? $context["text_model"] : null);
        echo "</span> ";
        echo (isset($context["model"]) ? $context["model"] : null);
        echo "</li>
                  ";
        // line 70
        if ((isset($context["reward"]) ? $context["reward"] : null)) {
            // line 71
            echo "                  <li><span>";
            echo (isset($context["text_reward"]) ? $context["text_reward"] : null);
            echo "</span> ";
            echo (isset($context["reward"]) ? $context["reward"] : null);
            echo "</li>
                  ";
        }
        // line 73
        echo "                  <li><span>";
        echo (isset($context["text_stock"]) ? $context["text_stock"] : null);
        echo "</span> ";
        echo (isset($context["stock"]) ? $context["stock"] : null);
        echo "</li>
                </ul>

                <!--multiple option start-->
                <div id=\"product\"> ";
        // line 77
        if ((isset($context["options"]) ? $context["options"] : null)) {
            // line 78
            echo "            <hr>
                  ";
            // line 79
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 80
                echo "            <h3>";
                echo (isset($context["text_option"]) ? $context["text_option"] : null);
                echo "</h3>
            ";
                // line 81
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    // line 82
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "select")) {
                        // line 83
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 84
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <select name=\"option[";
                        // line 85
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\">
                <option value=\"\">";
                        // line 86
                        echo (isset($context["text_select"]) ? $context["text_select"] : null);
                        echo "</option>
                ";
                        // line 87
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            // line 88
                            echo "                <option value=\"";
                            echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                            echo "\">";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "
                ";
                            // line 89
                            if ($this->getAttribute($context["option_value"], "price", array())) {
                                // line 90
                                echo "                (";
                                echo $this->getAttribute($context["option_value"], "price_prefix", array());
                                echo $this->getAttribute($context["option_value"], "price", array());
                                echo ")
                ";
                            }
                            // line 91
                            echo " </option>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 93
                        echo "              </select>
            </div>
            ";
                    }
                    // line 96
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "radio")) {
                        // line 97
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\">";
                        // line 98
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <div id=\"input-option";
                        // line 99
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\"> ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            // line 100
                            echo "                <div class=\"radio\">
                  <label>
                    <input type=\"radio\" name=\"option[";
                            // line 102
                            echo $this->getAttribute($context["option"], "product_option_id", array());
                            echo "]\" value=\"";
                            echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                            echo "\" />
                    ";
                            // line 103
                            if ($this->getAttribute($context["option_value"], "image", array())) {
                                echo " <img src=\"";
                                echo $this->getAttribute($context["option_value"], "image", array());
                                echo "\" alt=\"";
                                echo $this->getAttribute($context["option_value"], "name", array());
                                echo " ";
                                if ($this->getAttribute($context["option_value"], "price", array())) {
                                    echo " ";
                                    echo $this->getAttribute($context["option_value"], "price_prefix", array());
                                    echo " ";
                                    echo $this->getAttribute($context["option_value"], "price", array());
                                    echo " ";
                                }
                                echo "\" class=\"img-thumbnail\" /> ";
                            }
                            echo "                  
                    ";
                            // line 104
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "
                    ";
                            // line 105
                            if ($this->getAttribute($context["option_value"], "price", array())) {
                                // line 106
                                echo "                    (";
                                echo $this->getAttribute($context["option_value"], "price_prefix", array());
                                echo $this->getAttribute($context["option_value"], "price", array());
                                echo ")
                    ";
                            }
                            // line 107
                            echo " </label>
                </div>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 109
                        echo " </div>
            </div>
            ";
                    }
                    // line 112
                    echo "                  
            ";
                    // line 113
                    if (($this->getAttribute($context["option"], "type", array()) == "checkbox")) {
                        // line 114
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\">";
                        // line 115
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <div id=\"input-option";
                        // line 116
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\"> ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            // line 117
                            echo "                <div class=\"checkbox\">
                  <label>
                    <input type=\"checkbox\" name=\"option[";
                            // line 119
                            echo $this->getAttribute($context["option"], "product_option_id", array());
                            echo "][]\" value=\"";
                            echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                            echo "\" />
                    ";
                            // line 120
                            if ($this->getAttribute($context["option_value"], "image", array())) {
                                echo " <img src=\"";
                                echo $this->getAttribute($context["option_value"], "image", array());
                                echo "\" alt=\"";
                                echo $this->getAttribute($context["option_value"], "name", array());
                                echo " ";
                                if ($this->getAttribute($context["option_value"], "price", array())) {
                                    echo " ";
                                    echo $this->getAttribute($context["option_value"], "price_prefix", array());
                                    echo " ";
                                    echo $this->getAttribute($context["option_value"], "price", array());
                                    echo " ";
                                }
                                echo "\" class=\"img-thumbnail\" /> ";
                            }
                            // line 121
                            echo "                    ";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "
                    ";
                            // line 122
                            if ($this->getAttribute($context["option_value"], "price", array())) {
                                // line 123
                                echo "                    (";
                                echo $this->getAttribute($context["option_value"], "price_prefix", array());
                                echo $this->getAttribute($context["option_value"], "price", array());
                                echo ")
                    ";
                            }
                            // line 124
                            echo " </label>
                </div>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 126
                        echo " </div>
            </div>
            ";
                    }
                    // line 129
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "text")) {
                        // line 130
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 131
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <input type=\"text\" name=\"option[";
                        // line 132
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "\" placeholder=\"";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\" />
            </div>
            ";
                    }
                    // line 135
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "textarea")) {
                        // line 136
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 137
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <textarea name=\"option[";
                        // line 138
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" rows=\"5\" placeholder=\"";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\">";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</textarea>
            </div>
            ";
                    }
                    // line 141
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "file")) {
                        // line 142
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\">";
                        // line 143
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <button type=\"button\" id=\"button-upload";
                        // line 144
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" data-loading-text=\"";
                        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                        echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                        echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                        echo "</button>
              <input type=\"hidden\" name=\"option[";
                        // line 145
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" />
            </div>
            ";
                    }
                    // line 148
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "date")) {
                        // line 149
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 150
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"option[";
                        // line 152
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                    }
                    // line 158
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "datetime")) {
                        // line 159
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 160
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <div class=\"input-group datetime\">
                <input type=\"text\" name=\"option[";
                        // line 162
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                    }
                    // line 168
                    echo "            ";
                    if (($this->getAttribute($context["option"], "type", array()) == "time")) {
                        // line 169
                        echo "            <div class=\"form-group";
                        if ($this->getAttribute($context["option"], "required", array())) {
                            echo " required ";
                        }
                        echo "\">
              <label class=\"control-label\" for=\"input-option";
                        // line 170
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option"], "name", array());
                        echo "</label>
              <div class=\"input-group time\">
                <input type=\"text\" name=\"option[";
                        // line 172
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                    }
                    // line 178
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 179
                echo "                  ";
            }
            // line 180
            echo "            ";
        }
        // line 181
        echo "            ";
        if ((isset($context["recurrings"]) ? $context["recurrings"] : null)) {
            // line 182
            echo "            <hr>
            <h3>";
            // line 183
            echo (isset($context["text_payment_recurring"]) ? $context["text_payment_recurring"] : null);
            echo "</h3>
            <div class=\"form-group required\">
              <select name=\"recurring_id\" class=\"form-control\">
                <option value=\"\">";
            // line 186
            echo (isset($context["text_select"]) ? $context["text_select"] : null);
            echo "</option>
                ";
            // line 187
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["recurrings"]) ? $context["recurrings"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 188
                echo "                <option value=\"";
                echo $this->getAttribute($context["recurring"], "recurring_id", array());
                echo "\">";
                echo $this->getAttribute($context["recurring"], "name", array());
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 190
            echo "              </select>
              <div class=\"help-block\" id=\"recurring-description\"></div>
            </div>
            ";
        }
        // line 193
        echo "  
             ";
        // line 194
        if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
            // line 195
            echo "             ";
            if ((isset($context["price"]) ? $context["price"] : null)) {
                // line 196
                echo "                <ul class=\"list-unstyled\">
                  ";
                // line 197
                if ( !(isset($context["special"]) ? $context["special"] : null)) {
                    // line 198
                    echo "                  <li class=\"product-prices\"><span class=\"pp-price-title\">";
                    echo (isset($context["text_price"]) ? $context["text_price"] : null);
                    echo "  : </span>
                    ";
                    // line 199
                    echo (isset($context["price"]) ? $context["price"] : null);
                    echo "
                  </li>
                  ";
                } else {
                    // line 202
                    echo "                  <li><span class=\"pp-price-title\">";
                    echo (isset($context["text_price"]) ? $context["text_price"] : null);
                    echo " : </span> <span class=\"product-prices\">";
                    echo (isset($context["special"]) ? $context["special"] : null);
                    echo "  </span><span style=\"text-decoration:line-through;\">";
                    echo (isset($context["price"]) ? $context["price"] : null);
                    echo "</span>                                    
                  </li>
                  ";
                }
                // line 205
                echo "                  ";
                if ((isset($context["tax"]) ? $context["tax"] : null)) {
                    // line 206
                    echo "                 <!--  <li>";
                    echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                    echo " ";
                    echo (isset($context["tax"]) ? $context["tax"] : null);
                    echo "</li> -->
                  ";
                }
                // line 208
                echo "                  ";
                if ((isset($context["points"]) ? $context["points"] : null)) {
                    // line 209
                    echo "                  <li><span class=\"pp-titles\">";
                    echo (isset($context["text_points"]) ? $context["text_points"] : null);
                    echo "</span> ";
                    echo (isset($context["points"]) ? $context["points"] : null);
                    echo "</li>
                  ";
                }
                // line 211
                echo "                  ";
                if ((isset($context["discounts"]) ? $context["discounts"] : null)) {
                    // line 212
                    echo "                  <li>
                    <hr>
                  </li>
                  ";
                    // line 215
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["discounts"]) ? $context["discounts"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                        // line 216
                        echo "                  <li> ";
                        echo $this->getAttribute($context["discount"], "quantity", array());
                        echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
                        echo $this->getAttribute($context["discount"], "price", array());
                        echo "</li>
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 218
                    echo "                 
                  ";
                }
                // line 220
                echo "                </ul>
                ";
            }
            // line 222
            echo "\t\t\t\t";
        }
        // line 223
        echo "             
            <div class=\"form-group\">
              <label class=\"control-label pp-price-title\" for=\"input-quantity\">";
        // line 225
        echo (isset($context["entry_qty"]) ? $context["entry_qty"] : null);
        echo " : </label>
              <input type=\"text\" name=\"quantity\" value=\"";
        // line 226
        echo (isset($context["minimum"]) ? $context["minimum"] : null);
        echo "\" class=\"p-p-qty form-control\" size=\"2\" id=\"input-quantity\" class=\"form-control\" />
              <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 227
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "\" />
              <br/>             
            </div>
            ";
        // line 230
        if (((isset($context["minimum"]) ? $context["minimum"] : null) > 1)) {
            // line 231
            echo "            <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["text_minimum"]) ? $context["text_minimum"] : null);
            echo "</div>
            ";
        }
        // line 233
        echo "            </div>
             <div class=\"btn-group\">
                ";
        // line 235
        if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
            // line 236
            echo "              <button type=\"button\" id=\"button-cart\" data-loading-text=\"";
            echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
            echo "</button>
               ";
        }
        // line 238
        echo "              <a href=\"#\" data-toggle=\"modal\" data-target=\"#exampleModal\">
\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"margin-left:5px;\">";
        // line 239
        echo (isset($context["text_reqQuote"]) ? $context["text_reqQuote"] : null);
        echo "</button>
\t\t\t  </a>
               
            <button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-primary p-p-btn\" title=\"";
        // line 242
        echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
        echo "\" onclick=\"wishlist.add('";
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "');\"><i class=\"fa fa-heart\"></i></button>
            <button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-primary p-p-btn\" title=\"";
        // line 243
        echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
        echo "\" onclick=\"compare.add('";
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "');\"><i class=\"fa fa-refresh\"></i></button>
          </div>
            <!--modal start --> 
               <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" style=\"display: none;\" aria-hidden=\"true\">
                <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"padding: 17px;\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel\">";
        // line 250
        echo (isset($context["text_bulk"]) ? $context["text_bulk"] : null);
        echo "</h5>
                <a href=\"#\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                <span aria-hidden=\"true\">×</span>
                </a>
                </div>
                
                <div class=\"col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12\" style=\"background: white;\">

                <div class=\"card\">

                <div class=\"card-body\">
                <form action=\"catalog/view/theme/default/template/product/quote_mail.php\" method=\"post\" enctype=\"multipart/form-data\" >
                <div class=\"form-group\">
                <label for=\"inputText3\" class=\"col-form-label\">";
        // line 263
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo "</label>
                <input id=\"inputText3\" type=\"text\" value=\"";
        // line 264
        echo (isset($context["customer_name"]) ? $context["customer_name"] : null);
        echo "\" class=\"form-control\" placeholder=\"";
        echo (isset($context["text_enterName"]) ? $context["text_enterName"] : null);
        echo "\" name=\"name\" required>
                <input id=\"inputText3\" type=\"hidden\" value=\"";
        // line 265
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "\" class=\"form-control\" name=\"id\" hidden>
                </div>
                <div class=\"form-group\">
                <label for=\"inputEmail\">";
        // line 268
        echo (isset($context["text_emailAddress"]) ? $context["text_emailAddress"] : null);
        echo "</label>
                <input id=\"inputEmail\" type=\"email\" value=\"";
        // line 269
        echo (isset($context["customer_email"]) ? $context["customer_email"] : null);
        echo "\" class=\"form-control\" placeholder=\"";
        echo (isset($context["text_enterEmail"]) ? $context["text_enterEmail"] : null);
        echo "\" name=\"email\" required>
                
                </div>
                <div class=\"form-group\">
                <label for=\"inputText4\" class=\"col-form-label\">";
        // line 273
        echo (isset($context["text_phone"]) ? $context["text_phone"] : null);
        echo "</label>
                <input id=\"inputText4\" type=\"text\" value=\"";
        // line 274
        echo (isset($context["entry_telephone"]) ? $context["entry_telephone"] : null);
        echo "\" class=\"form-control\" placeholder=\"";
        echo (isset($context["text_enterPhone"]) ? $context["text_enterPhone"] : null);
        echo "\" name=\"phone\" required>
                </div>
                <div class=\"form-group\">
                <label for=\"inputText3\" class=\"col-form-label\">";
        // line 277
        echo (isset($context["text_model"]) ? $context["text_model"] : null);
        echo "</label>
                <input id=\"inputText3\" type=\"text\" value=\"";
        // line 278
        echo (isset($context["model"]) ? $context["model"] : null);
        echo "\" class=\"form-control\" name=\"model\" readonly>
                </div>
                  <div class=\"form-group\">
                <label for=\"inputText3\" class=\"col-form-label\">";
        // line 281
        echo (isset($context["entry_qty"]) ? $context["entry_qty"] : null);
        echo "</label>
                <input required type=\"number\" onkeypress=\"return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 1 && event.charCode <= 100000))\" name=\"quantity\" pattern=\"[0-9]\" id=\"quantity\" value= \"";
        // line 282
        echo (isset($context["quantity"]) ? $context["quantity"] : null);
        echo "\"   class=\"form-control\">
                </div>
                
                <div class=\"modal-footer\">
                <a href=\"#\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</a>
                <input class=\"btn btn-primary\" type=\"submit\"  value=\"";
        // line 287
        echo (isset($context["text_submit"]) ? $context["text_submit"] : null);
        echo "\" name='but_upload'/>
                </div>
                </form>

                </div>
                </div>
                </div>
                 
                </div>
              
                </div>
                </div>
                </div>
               <!--modal start -->  
            <!--multiple option end -->
               <!-- rating & social buttons-->

          <div class=\"rating marginTop1\">
            <p>";
        // line 305
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 306
            echo "              ";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            echo " 
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 307
        echo " &nbsp &nbsp |  &nbsp &nbsp  <a href=\"\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
        echo (isset($context["reviews"]) ? $context["reviews"] : null);
        echo "</a> &nbsp &nbsp | &nbsp &nbsp <a href=\"\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
        echo (isset($context["text_write"]) ? $context["text_write"] : null);
        echo "</a></p>
            <hr>
            <!-- AddThis Button BEGIN -->
            <div class=\"addthis_toolbox addthis_default_style\" data-url=\"";
        // line 310
        echo (isset($context["share"]) ? $context["share"] : null);
        echo "\">
               
                </div>    
                
            <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e\"></script> 
            <!-- AddThis Button END --> 
          </div>         
                <!--rating & social buttons end -->
              </div>
        <!-- SB-theme product Description Part End -->

         
            <!-- Product description2 start-->
          <div class=\"col-sm-12  p-p-price-detail\">
             
          </div>
            <!-- Product description end -->
         
           </div>
<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fas fa-exclamation\"></i> ";
        // line 329
        echo (isset($context["text_congo"]) ? $context["text_congo"] : null);
        echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button></div>
         <!--sb theme tab start-->
      <div class=\"col-md-12 sb-theme-p-d-tab\">
         <ul class=\"nav nav-tabs\">
            <!-- <li class=\"active\"><a href=\"#tab-description\" data-toggle=\"tab\">";
        // line 333
        echo (isset($context["tab_description"]) ? $context["tab_description"] : null);
        echo "</a></li> -->
            ";
        // line 334
        if ((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null)) {
            // line 335
            echo "            <li><a href=\"#tab-specification\" data-toggle=\"tab\">";
            echo (isset($context["tab_attribute"]) ? $context["tab_attribute"] : null);
            echo "</a></li>
            ";
        }
        // line 337
        echo "            ";
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 338
            echo "            <li class=\"active\"><a href=\"#tab-review\" data-toggle=\"tab\">";
            echo (isset($context["tab_review"]) ? $context["tab_review"] : null);
            echo "</a></li>
            ";
        }
        // line 340
        echo "          </ul>
          <div class=\"tab-content\">
            <!-- <div class=\"tab-pane active\" id=\"tab-description\">";
        // line 342
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "</div> -->
            ";
        // line 343
        if ((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null)) {
            // line 344
            echo "            <div class=\"tab-pane\" id=\"tab-specification\">
              <table class=\"table table-bordered\">
                ";
            // line 346
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 347
                echo "                <thead>
                  <tr>
                    <td colspan=\"2\"><strong>";
                // line 349
                echo $this->getAttribute($context["attribute_group"], "name", array());
                echo "</strong></td>
                  </tr>
                </thead>
                <tbody>
                ";
                // line 353
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 354
                    echo "                <tr>
                  <td>";
                    // line 355
                    echo $this->getAttribute($context["attribute"], "name", array());
                    echo "</td>
                  <td>";
                    // line 356
                    echo $this->getAttribute($context["attribute"], "text", array());
                    echo "</td>
                </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 359
                echo "                  </tbody>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 361
            echo "              </table>
            </div>
            ";
        }
        // line 364
        echo "            ";
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 365
            echo "            <div class=\"tab-pane active\" id=\"tab-review\">
              <form class=\"form-horizontal\" id=\"form-review\">
                <div id=\"review\"></div>
                <h2>";
            // line 368
            echo (isset($context["text_write"]) ? $context["text_write"] : null);
            echo "</h2>
                ";
            // line 369
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 370
                echo "                <div class=\"form-group required\">
                  <div class=\"col-sm-12\">
                    <label class=\"control-label\" for=\"input-name\">";
                // line 372
                echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
                echo "</label>
                    <input type=\"text\" name=\"name\" value=\"";
                // line 373
                echo (isset($context["customer_name"]) ? $context["customer_name"] : null);
                echo "\" id=\"input-name\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group required\">
                  <div class=\"col-sm-12\">
                    <label class=\"control-label\" for=\"input-review\">";
                // line 378
                echo (isset($context["entry_review"]) ? $context["entry_review"] : null);
                echo "</label>
                    <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control\"></textarea>
                    <div class=\"help-block\">";
                // line 380
                echo (isset($context["text_note"]) ? $context["text_note"] : null);
                echo "</div>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <div class=\"col-sm-12\">
                    <label class=\"control-label\">";
                // line 385
                echo (isset($context["entry_rating"]) ? $context["entry_rating"] : null);
                echo "</label>
                    &nbsp;&nbsp;&nbsp; ";
                // line 386
                echo (isset($context["entry_bad"]) ? $context["entry_bad"] : null);
                echo "&nbsp;
                    <input type=\"radio\" name=\"rating\" value=\"1\" />
                    &nbsp;
                    <input type=\"radio\" name=\"rating\" value=\"2\" />
                    &nbsp;
                    <input type=\"radio\" name=\"rating\" value=\"3\" />
                    &nbsp;
                    <input type=\"radio\" name=\"rating\" value=\"4\" />
                    &nbsp;
                    <input type=\"radio\" name=\"rating\" value=\"5\" />
                    &nbsp;";
                // line 396
                echo (isset($context["entry_good"]) ? $context["entry_good"] : null);
                echo "</div>
                </div>
                ";
                // line 398
                echo (isset($context["captcha"]) ? $context["captcha"] : null);
                echo "
                <div class=\"buttons clearfix\">
                  <div class=\"pull-right\">
                    <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 401
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">";
                echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
                echo "</button>
                  </div>
                </div>
                ";
            } else {
                // line 405
                echo "                ";
                echo (isset($context["text_login"]) ? $context["text_login"] : null);
                echo "
                ";
            }
            // line 407
            echo "              </form>
            </div>
            ";
        }
        // line 409
        echo "</div>
</div>
<!--sb theme tab end -->
      </div>


      <br/>
      ";
        // line 416
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 417
            echo "       <div class=\"shopz-title-left row\">
        <h2 class=\"theme-title\"> ";
            // line 418
            echo (isset($context["text_related"]) ? $context["text_related"] : null);
            echo "</h2>   
       </div>
      
      <div class=\"row\"> ";
            // line 421
            $context["i"] = 0;
            // line 422
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 423
                echo "        ";
                if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
                    // line 424
                    echo "        ";
                    $context["class"] = "col-xs-8 col-sm-6";
                    // line 425
                    echo "        ";
                } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
                    // line 426
                    echo "        ";
                    $context["class"] = "col-xs-6 col-md-4";
                    // line 427
                    echo "        ";
                } else {
                    // line 428
                    echo "        ";
                    $context["class"] = "col-xs-6 col-sm-3";
                    // line 429
                    echo "        ";
                }
                // line 430
                echo "        
        <div class=\"";
                // line 431
                echo (isset($context["class"]) ? $context["class"] : null);
                echo "\">
         <div class=\"product-thumb\">
            <div class=\"image\"><a href=\"";
                // line 433
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive\" /></a></div>
            <div>
            ";
                // line 435
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 436
                    echo "              ";
                    if ($this->getAttribute($context["product"], "special", array())) {
                        // line 437
                        echo "               <div class=\"sale\"><img src=\"image/catalog/sale-sticker.png\" alt=\"sale\" class=\"sale product\"></div>
              ";
                    }
                    // line 439
                    echo "            ";
                }
                // line 440
                echo "              ";
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 441
                    echo "                ";
                    if ($this->getAttribute($context["product"], "special", array())) {
                        // line 442
                        echo "                  <div class=\"sale\"><img src=\"image/catalog/sale-sticker.png\" alt=\"sale\" class=\"sale product\"></div>
                ";
                    }
                    // line 444
                    echo "              ";
                }
                // line 445
                echo "              <div class=\"caption\">
                <h4><a href=\"";
                // line 446
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4>
                <p class=\"hidden-xs hidden-sm hidden-md hidden-lg\">";
                // line 447
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>
                ";
                // line 448
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 449
                    echo "                <p class=\"price\"> ";
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 450
                        echo "                  ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
                  ";
                    } else {
                        // line 451
                        echo " <span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span> ";
                    }
                    // line 452
                    echo "                  ";
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        echo " <span class=\"price-tax hidden-xs hidden-sm hidden-md hidden-lg\">";
                        echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span> ";
                    }
                    echo " </p>
                ";
                }
                // line 454
                echo "                
                <div class=\"rating\">
                  ";
                // line 456
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 457
                    echo "                  ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                    } else {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                    }
                    // line 458
                    echo "                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 459
                echo "                </div>
                 </div>
               <div class=\"button-group sb-btn-group\">                
                <button type=\"button\" class=\"ad-to-cart\" onclick=\"cart.add('";
                // line 462
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-shopping-basket hidden-lg hidden-md\"></i> <span class=\"hidden-xs hidden-sm\">";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button>
                <button type=\"button\" class=\"theme-btn theme-btn-w\" data-toggle=\"tooltip\" title=\"";
                // line 463
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-heart\"></i></button>                
                <button type=\"button\" class=\"theme-btn theme-btn-c\" data-toggle=\"tooltip\" title=\"";
                // line 464
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-refresh\"></i></button>
              </div>
            </div>
          </div>
        </div>

        ";
                // line 470
                if ((((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null)) && ((((isset($context["i"]) ? $context["i"] : null) + 1) % 2) == 0))) {
                    // line 471
                    echo "        <div class=\"clearfix visible-md visible-sm\"></div>
        ";
                } elseif ((                // line 472
(isset($context["column_left"]) ? $context["column_left"] : null) || ((isset($context["column_right"]) ? $context["column_right"] : null) && ((((isset($context["i"]) ? $context["i"] : null) + 1) % 3) == 0)))) {
                    // line 473
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                } elseif ((((                // line 474
(isset($context["i"]) ? $context["i"] : null) + 1) % 4) == 0)) {
                    // line 475
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                }
                // line 477
                echo "        ";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 478
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
        </div>
        ";
        }
        // line 481
        echo "        ";
        if ((isset($context["tags"]) ? $context["tags"] : null)) {
            // line 482
            echo "        <p>";
            echo (isset($context["text_tags"]) ? $context["text_tags"] : null);
            echo "
        ";
            // line 483
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null))));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 484
                echo "        ";
                if (($context["i"] < (twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null)) - 1))) {
                    echo " <a href=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                    echo "</a>,
        ";
                } else {
                    // line 485
                    echo " <a href=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                    echo "</a> ";
                }
                // line 486
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " </p>
        ";
        }
        // line 488
        echo "      ";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 489
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
<script type=\"text/javascript\"><!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=checkout/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));

\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('.breadcrumb').after('<div class=\"alert alert-success alert-dismissible\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

\t\t\t\t\$('#cart > button').html('<span id=\"cart-total\"><img src=\"image/catalog/cart-icon.png\" alt=\"cart\"> ' + json['total'] + '</span>');

\t\t\t\t\$('html, body').animate({ scrollTop: 0 }, 'slow');

\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t}
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: '";
        // line 567
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tlanguage: '";
        // line 572
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tlanguage: '";
        // line 578
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;

\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
    \tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    \$('#review').fadeOut('slow');

    \$('#review').load(this.href);

    \$('#review').fadeIn('slow');
});

\$('#review').load('index.php?route=product/product/review&product_id=";
        // line 645
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "');

\$('#button-review').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=product/product/write&product_id=";
        // line 649
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: \$(\"#form-review\").serialize(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-review').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-review').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t// \$('input[name=\\'name\\']').val('');
\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t}
\t\t}
\t});
});

\$(document).ready(function() {
\t\$('.thumbnails').magnificPopup({
\t\ttype:'image',
\t\tdelegate: 'a',
\t\tgallery: {
\t\t\tenabled: true
\t\t}
\t});
});
//--></script> 


<script>
     if (jQuery(window).width() > 980){
        //initiate the plugin and pass the id of the div containing gallery images
            \$(\"#zoom_03\").elevateZoom({gallery:'sb-additional-img', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: ''});
        //pass the images to Fancybox
            \$(\"#zoom_03\").bind(\"click\", function (e) {
            var ez = \$('#zoom_03').data('elevateZoom');
            \$.fancybox(ez.getGalleryList());
            return false;
            });
    }
</script>

<!--slider for product-->
<script type=\"text/javascript\"><!--
\$('#sb-additional-img').owlCarousel({
  itemsCustom : [
        [1, 1],  
        [360, 2],
        [380, 3],
        [500, 4],
        [768, 4],
        [991, 3],
        [1199, 4]
        ],
  autoPlay: false,
  navigation: true,
  navigationText: ['<i class=\"fa fa-angle-left fa-5x\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right fa-5x\" aria-hidden=\"true\"></i>'],
  pagination: false
});
--></script>
<!--over slider for product-->
";
        // line 721
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "__string_template__450f490f7a4858470caeeab53c9df25895a6acb507ae819bdf0c7bc01b6b72b1";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1688 => 721,  1613 => 649,  1606 => 645,  1536 => 578,  1527 => 572,  1519 => 567,  1438 => 489,  1433 => 488,  1424 => 486,  1417 => 485,  1407 => 484,  1403 => 483,  1398 => 482,  1395 => 481,  1385 => 478,  1382 => 477,  1378 => 475,  1376 => 474,  1373 => 473,  1371 => 472,  1368 => 471,  1366 => 470,  1355 => 464,  1349 => 463,  1343 => 462,  1338 => 459,  1332 => 458,  1325 => 457,  1321 => 456,  1317 => 454,  1305 => 452,  1298 => 451,  1292 => 450,  1289 => 449,  1287 => 448,  1283 => 447,  1277 => 446,  1274 => 445,  1271 => 444,  1267 => 442,  1264 => 441,  1261 => 440,  1258 => 439,  1254 => 437,  1251 => 436,  1249 => 435,  1238 => 433,  1233 => 431,  1230 => 430,  1227 => 429,  1224 => 428,  1221 => 427,  1218 => 426,  1215 => 425,  1212 => 424,  1209 => 423,  1204 => 422,  1202 => 421,  1196 => 418,  1193 => 417,  1191 => 416,  1182 => 409,  1177 => 407,  1171 => 405,  1162 => 401,  1156 => 398,  1151 => 396,  1138 => 386,  1134 => 385,  1126 => 380,  1121 => 378,  1113 => 373,  1109 => 372,  1105 => 370,  1103 => 369,  1099 => 368,  1094 => 365,  1091 => 364,  1086 => 361,  1079 => 359,  1070 => 356,  1066 => 355,  1063 => 354,  1059 => 353,  1052 => 349,  1048 => 347,  1044 => 346,  1040 => 344,  1038 => 343,  1034 => 342,  1030 => 340,  1024 => 338,  1021 => 337,  1015 => 335,  1013 => 334,  1009 => 333,  1002 => 329,  980 => 310,  971 => 307,  958 => 306,  954 => 305,  933 => 287,  925 => 282,  921 => 281,  915 => 278,  911 => 277,  903 => 274,  899 => 273,  890 => 269,  886 => 268,  880 => 265,  874 => 264,  870 => 263,  854 => 250,  842 => 243,  836 => 242,  830 => 239,  827 => 238,  819 => 236,  817 => 235,  813 => 233,  807 => 231,  805 => 230,  799 => 227,  795 => 226,  791 => 225,  787 => 223,  784 => 222,  780 => 220,  776 => 218,  765 => 216,  761 => 215,  756 => 212,  753 => 211,  745 => 209,  742 => 208,  734 => 206,  731 => 205,  720 => 202,  714 => 199,  709 => 198,  707 => 197,  704 => 196,  701 => 195,  699 => 194,  696 => 193,  690 => 190,  679 => 188,  675 => 187,  671 => 186,  665 => 183,  662 => 182,  659 => 181,  656 => 180,  653 => 179,  647 => 178,  634 => 172,  627 => 170,  620 => 169,  617 => 168,  604 => 162,  597 => 160,  590 => 159,  587 => 158,  574 => 152,  567 => 150,  560 => 149,  557 => 148,  549 => 145,  541 => 144,  537 => 143,  530 => 142,  527 => 141,  515 => 138,  509 => 137,  502 => 136,  499 => 135,  487 => 132,  481 => 131,  474 => 130,  471 => 129,  466 => 126,  458 => 124,  451 => 123,  449 => 122,  444 => 121,  428 => 120,  422 => 119,  418 => 117,  412 => 116,  408 => 115,  401 => 114,  399 => 113,  396 => 112,  391 => 109,  383 => 107,  376 => 106,  374 => 105,  370 => 104,  352 => 103,  346 => 102,  342 => 100,  336 => 99,  332 => 98,  325 => 97,  322 => 96,  317 => 93,  310 => 91,  303 => 90,  301 => 89,  294 => 88,  290 => 87,  286 => 86,  280 => 85,  274 => 84,  267 => 83,  264 => 82,  260 => 81,  255 => 80,  253 => 79,  250 => 78,  248 => 77,  238 => 73,  230 => 71,  228 => 70,  221 => 69,  211 => 67,  209 => 66,  204 => 64,  200 => 63,  192 => 59,  189 => 58,  186 => 57,  183 => 56,  180 => 55,  178 => 54,  174 => 52,  170 => 50,  165 => 47,  152 => 44,  141 => 43,  137 => 42,  132 => 39,  129 => 38,  116 => 34,  110 => 33,  107 => 32,  105 => 31,  102 => 30,  100 => 29,  96 => 28,  93 => 27,  90 => 26,  87 => 25,  84 => 24,  81 => 23,  79 => 22,  71 => 20,  68 => 19,  65 => 18,  62 => 17,  59 => 16,  56 => 15,  53 => 14,  51 => 13,  47 => 12,  41 => 8,  30 => 6,  26 => 5,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="breadcrumb-bg">*/
/*   <div class="container">*/
/*     <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   </div>*/
/* </div>*/
/* <div id="product-product" class="container"> */
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}"  style="top:0px!important;">{{ content_top }}*/
/*       <div class="row p-p">*/
/*        {% if column_left or column_right %}*/
/*         {% set class = 'col-sm-6' %}*/
/*         {% else %}*/
/*         {% set class = 'col-sm-5' %}*/
/*         {% endif %}*/
/*          <!-- SB-theme image & Additional Image Start -->*/
/*         <div class="{{ class }}">*/
/*          {% if thumb or images %}*/
/*           <ul class="thumbnails col-sm-12 sb-theme-img-box">*/
/*             {% if thumb %}*/
/*             <li>*/
/*             <a class="thumbnail" href="{{ popup }}" title="{{ heading_title }}">*/
/*             <img data-zoom-image="{{ popup }}" src="{{ thumb }}" title="{{ heading_title }}" alt="{{ heading_title }}" id="zoom_03"  class="img-responsive" />*/
/*             </a>*/
/*             </li>*/
/*             {% endif %}*/
/*             {% if images %}*/
/*            */
/* */
/*             <li class="col-sm-4 "  id="sb-additional-img" class="owl-carousel">*/
/*              {% for image in images %}*/
/*             <a data-zoom-image="{{ image.popup }}" data-image="{{ image.popup }}" href="{{ image.popup }}" title="{{ heading_title }}">*/
/*              <img src="{{ image.thumb }}" title="{{ heading_title }}" alt="{{ heading_title }}" class="img-responsive" />*/
/*              </a>*/
/*              {% endfor %}*/
/*              </li>*/
/*             */
/*             {% endif %}*/
/*           </ul>*/
/*           {% endif %}*/
/*           </div>*/
/*            <!-- SB-theme image & Additional Image end -->                 */
/*         {% if column_left or column_right %}*/
/*         {% set class = 'col-sm-6' %}*/
/*         {% else %}*/
/*         {% set class = 'col-sm-7' %}*/
/*         {% endif %}*/
/*         <div class="{{ class }}">*/
/* */
/*         <!-- SB-theme product Description Part Start -->*/
/*              <div class="col-sm-12">*/
/*                 <h3 class="p-p-title">{{ heading_title }}</h3>*/
/*                 <p class="">{{ description }}</p>*/
/*                 <ul class="list-unstyled p-p-detail">*/
/*                   {% if manufacturer %}*/
/*                   <li><span>{{ text_manufacturer }}</span> <a href="{{ manufacturers }}">{{ manufacturer }}</a></li>*/
/*                   {% endif %}*/
/*                   <li><span>{{ text_model }}</span> {{ model }}</li>*/
/*                   {% if reward %}*/
/*                   <li><span>{{ text_reward }}</span> {{ reward }}</li>*/
/*                   {% endif %}*/
/*                   <li><span>{{ text_stock }}</span> {{ stock }}</li>*/
/*                 </ul>*/
/* */
/*                 <!--multiple option start-->*/
/*                 <div id="product"> {% if options %}*/
/*             <hr>*/
/*                   {% if review_guest %}*/
/*             <h3>{{ text_option }}</h3>*/
/*             {% for option in options %}*/
/*             {% if option.type == 'select' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <select name="option[{{ option.product_option_id }}]" id="input-option{{ option.product_option_id }}" class="form-control">*/
/*                 <option value="">{{ text_select }}</option>*/
/*                 {% for option_value in option.product_option_value %}*/
/*                 <option value="{{ option_value.product_option_value_id }}">{{ option_value.name }}*/
/*                 {% if option_value.price %}*/
/*                 ({{ option_value.price_prefix }}{{ option_value.price }})*/
/*                 {% endif %} </option>*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'radio' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label">{{ option.name }}</label>*/
/*               <div id="input-option{{ option.product_option_id }}"> {% for option_value in option.product_option_value %}*/
/*                 <div class="radio">*/
/*                   <label>*/
/*                     <input type="radio" name="option[{{ option.product_option_id }}]" value="{{ option_value.product_option_value_id }}" />*/
/*                     {% if option_value.image %} <img src="{{ option_value.image }}" alt="{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}" class="img-thumbnail" /> {% endif %}                  */
/*                     {{ option_value.name }}*/
/*                     {% if option_value.price %}*/
/*                     ({{ option_value.price_prefix }}{{ option_value.price }})*/
/*                     {% endif %} </label>*/
/*                 </div>*/
/*                 {% endfor %} </div>*/
/*             </div>*/
/*             {% endif %}*/
/*                   */
/*             {% if option.type == 'checkbox' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label">{{ option.name }}</label>*/
/*               <div id="input-option{{ option.product_option_id }}"> {% for option_value in option.product_option_value %}*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     <input type="checkbox" name="option[{{ option.product_option_id }}][]" value="{{ option_value.product_option_value_id }}" />*/
/*                     {% if option_value.image %} <img src="{{ option_value.image }}" alt="{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}" class="img-thumbnail" /> {% endif %}*/
/*                     {{ option_value.name }}*/
/*                     {% if option_value.price %}*/
/*                     ({{ option_value.price_prefix }}{{ option_value.price }})*/
/*                     {% endif %} </label>*/
/*                 </div>*/
/*                 {% endfor %} </div>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'text' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'textarea' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <textarea name="option[{{ option.product_option_id }}]" rows="5" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control">{{ option.value }}</textarea>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'file' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label">{{ option.name }}</label>*/
/*               <button type="button" id="button-upload{{ option.product_option_id }}" data-loading-text="{{ text_loading }}" class="btn btn-default btn-block"><i class="fa fa-upload"></i> {{ button_upload }}</button>*/
/*               <input type="hidden" name="option[{{ option.product_option_id }}]" value="" id="input-option{{ option.product_option_id }}" />*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'date' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <div class="input-group date">*/
/*                 <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/*                 <span class="input-group-btn">*/
/*                 <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                 </span></div>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'datetime' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <div class="input-group datetime">*/
/*                 <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/*                 <span class="input-group-btn">*/
/*                 <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                 </span></div>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if option.type == 'time' %}*/
/*             <div class="form-group{% if option.required %} required {% endif %}">*/
/*               <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/*               <div class="input-group time">*/
/*                 <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/*                 <span class="input-group-btn">*/
/*                 <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                 </span></div>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% endfor %}*/
/*                   {% endif %}*/
/*             {% endif %}*/
/*             {% if recurrings %}*/
/*             <hr>*/
/*             <h3>{{ text_payment_recurring }}</h3>*/
/*             <div class="form-group required">*/
/*               <select name="recurring_id" class="form-control">*/
/*                 <option value="">{{ text_select }}</option>*/
/*                 {% for recurring in recurrings %}*/
/*                 <option value="{{ recurring.recurring_id }}">{{ recurring.name }}</option>*/
/*                 {% endfor %}*/
/*               </select>*/
/*               <div class="help-block" id="recurring-description"></div>*/
/*             </div>*/
/*             {% endif %}  */
/*              {% if review_guest %}*/
/*              {% if price %}*/
/*                 <ul class="list-unstyled">*/
/*                   {% if not special %}*/
/*                   <li class="product-prices"><span class="pp-price-title">{{ text_price }}  : </span>*/
/*                     {{ price }}*/
/*                   </li>*/
/*                   {% else %}*/
/*                   <li><span class="pp-price-title">{{ text_price }} : </span> <span class="product-prices">{{ special }}  </span><span style="text-decoration:line-through;">{{ price }}</span>                                    */
/*                   </li>*/
/*                   {% endif %}*/
/*                   {% if tax %}*/
/*                  <!--  <li>{{ text_tax }} {{ tax }}</li> -->*/
/*                   {% endif %}*/
/*                   {% if points %}*/
/*                   <li><span class="pp-titles">{{ text_points }}</span> {{ points }}</li>*/
/*                   {% endif %}*/
/*                   {% if discounts %}*/
/*                   <li>*/
/*                     <hr>*/
/*                   </li>*/
/*                   {% for discount in discounts %}*/
/*                   <li> {{ discount.quantity }}{{ text_discount }}{{ discount.price }}</li>*/
/*                   {% endfor %}*/
/*                  */
/*                   {% endif %}*/
/*                 </ul>*/
/*                 {% endif %}*/
/* 				{% endif %}*/
/*              */
/*             <div class="form-group">*/
/*               <label class="control-label pp-price-title" for="input-quantity">{{ entry_qty }} : </label>*/
/*               <input type="text" name="quantity" value="{{ minimum }}" class="p-p-qty form-control" size="2" id="input-quantity" class="form-control" />*/
/*               <input type="hidden" name="product_id" value="{{ product_id }}" />*/
/*               <br/>             */
/*             </div>*/
/*             {% if minimum > 1 %}*/
/*             <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_minimum }}</div>*/
/*             {% endif %}*/
/*             </div>*/
/*              <div class="btn-group">*/
/*                 {% if review_guest %}*/
/*               <button type="button" id="button-cart" data-loading-text="{{ text_loading }}" class="btn btn-primary">{{ button_cart }}</button>*/
/*                {% endif %}*/
/*               <a href="#" data-toggle="modal" data-target="#exampleModal">*/
/* 				<button type="button" class="btn btn-primary" style="margin-left:5px;">{{ text_reqQuote }}</button>*/
/* 			  </a>*/
/*                */
/*             <button type="button" data-toggle="tooltip" class="btn btn-primary p-p-btn" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product_id }}');"><i class="fa fa-heart"></i></button>*/
/*             <button type="button" data-toggle="tooltip" class="btn btn-primary p-p-btn" title="{{ button_compare }}" onclick="compare.add('{{ product_id }}');"><i class="fa fa-refresh"></i></button>*/
/*           </div>*/
/*             <!--modal start --> */
/*                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">*/
/*                 <div class="modal-dialog" role="document">*/
/*                 <div class="modal-content">*/
/*                 <div class="modal-header" style="padding: 17px;">*/
/*                 <h5 class="modal-title" id="exampleModalLabel">{{ text_bulk }}</h5>*/
/*                 <a href="#" class="close" data-dismiss="modal" aria-label="Close">*/
/*                 <span aria-hidden="true">×</span>*/
/*                 </a>*/
/*                 </div>*/
/*                 */
/*                 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: white;">*/
/* */
/*                 <div class="card">*/
/* */
/*                 <div class="card-body">*/
/*                 <form action="catalog/view/theme/default/template/product/quote_mail.php" method="post" enctype="multipart/form-data" >*/
/*                 <div class="form-group">*/
/*                 <label for="inputText3" class="col-form-label">{{ text_name }}</label>*/
/*                 <input id="inputText3" type="text" value="{{ customer_name }}" class="form-control" placeholder="{{ text_enterName }}" name="name" required>*/
/*                 <input id="inputText3" type="hidden" value="{{ product_id }}" class="form-control" name="id" hidden>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                 <label for="inputEmail">{{ text_emailAddress }}</label>*/
/*                 <input id="inputEmail" type="email" value="{{ customer_email }}" class="form-control" placeholder="{{ text_enterEmail }}" name="email" required>*/
/*                 */
/*                 </div>*/
/*                 <div class="form-group">*/
/*                 <label for="inputText4" class="col-form-label">{{ text_phone }}</label>*/
/*                 <input id="inputText4" type="text" value="{{ entry_telephone }}" class="form-control" placeholder="{{ text_enterPhone }}" name="phone" required>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                 <label for="inputText3" class="col-form-label">{{ text_model }}</label>*/
/*                 <input id="inputText3" type="text" value="{{ model }}" class="form-control" name="model" readonly>*/
/*                 </div>*/
/*                   <div class="form-group">*/
/*                 <label for="inputText3" class="col-form-label">{{ entry_qty }}</label>*/
/*                 <input required type="number" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 1 && event.charCode <= 100000))" name="quantity" pattern="[0-9]" id="quantity" value= "{{ quantity }}"   class="form-control">*/
/*                 </div>*/
/*                 */
/*                 <div class="modal-footer">*/
/*                 <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>*/
/*                 <input class="btn btn-primary" type="submit"  value="{{ text_submit }}" name='but_upload'/>*/
/*                 </div>*/
/*                 </form>*/
/* */
/*                 </div>*/
/*                 </div>*/
/*                 </div>*/
/*                  */
/*                 </div>*/
/*               */
/*                 </div>*/
/*                 </div>*/
/*                 </div>*/
/*                <!--modal start -->  */
/*             <!--multiple option end -->*/
/*                <!-- rating & social buttons-->*/
/* */
/*           <div class="rating marginTop1">*/
/*             <p>{% for i in 1..5 %}*/
/*               {% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %} */
/*               {% endfor %} &nbsp &nbsp |  &nbsp &nbsp  <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">{{ reviews }}</a> &nbsp &nbsp | &nbsp &nbsp <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">{{ text_write }}</a></p>*/
/*             <hr>*/
/*             <!-- AddThis Button BEGIN -->*/
/*             <div class="addthis_toolbox addthis_default_style" data-url="{{ share }}">*/
/*                */
/*                 </div>    */
/*                 */
/*             <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> */
/*             <!-- AddThis Button END --> */
/*           </div>         */
/*                 <!--rating & social buttons end -->*/
/*               </div>*/
/*         <!-- SB-theme product Description Part End -->*/
/* */
/*          */
/*             <!-- Product description2 start-->*/
/*           <div class="col-sm-12  p-p-price-detail">*/
/*              */
/*           </div>*/
/*             <!-- Product description end -->*/
/*          */
/*            </div>*/
/* <div class="alert alert-success alert-dismissible"><i class="fa fas fa-exclamation"></i> {{ text_congo }}<button type="button" class="close" data-dismiss="alert">×</button></div>*/
/*          <!--sb theme tab start-->*/
/*       <div class="col-md-12 sb-theme-p-d-tab">*/
/*          <ul class="nav nav-tabs">*/
/*             <!-- <li class="active"><a href="#tab-description" data-toggle="tab">{{ tab_description }}</a></li> -->*/
/*             {% if attribute_groups %}*/
/*             <li><a href="#tab-specification" data-toggle="tab">{{ tab_attribute }}</a></li>*/
/*             {% endif %}*/
/*             {% if review_status %}*/
/*             <li class="active"><a href="#tab-review" data-toggle="tab">{{ tab_review }}</a></li>*/
/*             {% endif %}*/
/*           </ul>*/
/*           <div class="tab-content">*/
/*             <!-- <div class="tab-pane active" id="tab-description">{{ description }}</div> -->*/
/*             {% if attribute_groups %}*/
/*             <div class="tab-pane" id="tab-specification">*/
/*               <table class="table table-bordered">*/
/*                 {% for attribute_group in attribute_groups %}*/
/*                 <thead>*/
/*                   <tr>*/
/*                     <td colspan="2"><strong>{{ attribute_group.name }}</strong></td>*/
/*                   </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for attribute in attribute_group.attribute %}*/
/*                 <tr>*/
/*                   <td>{{ attribute.name }}</td>*/
/*                   <td>{{ attribute.text }}</td>*/
/*                 </tr>*/
/*                 {% endfor %}*/
/*                   </tbody>*/
/*                 {% endfor %}*/
/*               </table>*/
/*             </div>*/
/*             {% endif %}*/
/*             {% if review_status %}*/
/*             <div class="tab-pane active" id="tab-review">*/
/*               <form class="form-horizontal" id="form-review">*/
/*                 <div id="review"></div>*/
/*                 <h2>{{ text_write }}</h2>*/
/*                 {% if review_guest %}*/
/*                 <div class="form-group required">*/
/*                   <div class="col-sm-12">*/
/*                     <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/*                     <input type="text" name="name" value="{{ customer_name }}" id="input-name" class="form-control" />*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <div class="col-sm-12">*/
/*                     <label class="control-label" for="input-review">{{ entry_review }}</label>*/
/*                     <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>*/
/*                     <div class="help-block">{{ text_note }}</div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <div class="col-sm-12">*/
/*                     <label class="control-label">{{ entry_rating }}</label>*/
/*                     &nbsp;&nbsp;&nbsp; {{ entry_bad }}&nbsp;*/
/*                     <input type="radio" name="rating" value="1" />*/
/*                     &nbsp;*/
/*                     <input type="radio" name="rating" value="2" />*/
/*                     &nbsp;*/
/*                     <input type="radio" name="rating" value="3" />*/
/*                     &nbsp;*/
/*                     <input type="radio" name="rating" value="4" />*/
/*                     &nbsp;*/
/*                     <input type="radio" name="rating" value="5" />*/
/*                     &nbsp;{{ entry_good }}</div>*/
/*                 </div>*/
/*                 {{ captcha }}*/
/*                 <div class="buttons clearfix">*/
/*                   <div class="pull-right">*/
/*                     <button type="button" id="button-review" data-loading-text="{{ text_loading }}" class="btn btn-primary">{{ button_continue }}</button>*/
/*                   </div>*/
/*                 </div>*/
/*                 {% else %}*/
/*                 {{ text_login }}*/
/*                 {% endif %}*/
/*               </form>*/
/*             </div>*/
/*             {% endif %}</div>*/
/* </div>*/
/* <!--sb theme tab end -->*/
/*       </div>*/
/* */
/* */
/*       <br/>*/
/*       {% if products %}*/
/*        <div class="shopz-title-left row">*/
/*         <h2 class="theme-title"> {{ text_related }}</h2>   */
/*        </div>*/
/*       */
/*       <div class="row"> {% set i = 0 %}*/
/*         {% for product in products %}*/
/*         {% if column_left and column_right %}*/
/*         {% set class = 'col-xs-8 col-sm-6' %}*/
/*         {% elseif column_left or column_right %}*/
/*         {% set class = 'col-xs-6 col-md-4' %}*/
/*         {% else %}*/
/*         {% set class = 'col-xs-6 col-sm-3' %}*/
/*         {% endif %}*/
/*         */
/*         <div class="{{ class }}">*/
/*          <div class="product-thumb">*/
/*             <div class="image"><a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" /></a></div>*/
/*             <div>*/
/*             {% if product.price %}*/
/*               {% if product.special %}*/
/*                <div class="sale"><img src="image/catalog/sale-sticker.png" alt="sale" class="sale product"></div>*/
/*               {% endif %}*/
/*             {% endif %}*/
/*               {% if product.price %}*/
/*                 {% if product.special %}*/
/*                   <div class="sale"><img src="image/catalog/sale-sticker.png" alt="sale" class="sale product"></div>*/
/*                 {% endif %}*/
/*               {% endif %}*/
/*               <div class="caption">*/
/*                 <h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/*                 <p class="hidden-xs hidden-sm hidden-md hidden-lg">{{ product.description }}</p>*/
/*                 {% if product.price %}*/
/*                 <p class="price"> {% if not product.special %}*/
/*                   {{ product.price }}*/
/*                   {% else %} <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span> {% endif %}*/
/*                   {% if product.tax %} <span class="price-tax hidden-xs hidden-sm hidden-md hidden-lg">{{ text_tax }} {{ product.tax }}</span> {% endif %} </p>*/
/*                 {% endif %}*/
/*                 */
/*                 <div class="rating">*/
/*                   {% for i in 1..5 %}*/
/*                   {% if product.rating < i %} <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> {% else %} <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> {% endif %}*/
/*                   {% endfor %}*/
/*                 </div>*/
/*                  </div>*/
/*                <div class="button-group sb-btn-group">                */
/*                 <button type="button" class="ad-to-cart" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-basket hidden-lg hidden-md"></i> <span class="hidden-xs hidden-sm">{{ button_cart }}</span></button>*/
/*                 <button type="button" class="theme-btn theme-btn-w" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>                */
/*                 <button type="button" class="theme-btn theme-btn-c" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-refresh"></i></button>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/* */
/*         {% if column_left and column_right and (i + 1) % 2 == 0 %}*/
/*         <div class="clearfix visible-md visible-sm"></div>*/
/*         {% elseif column_left or column_right and (i + 1) % 3 == 0 %}*/
/*         <div class="clearfix visible-md"></div>*/
/*         {% elseif (i + 1) % 4 == 0 %}*/
/*         <div class="clearfix visible-md"></div>*/
/*         {% endif %}*/
/*         {% set i = i + 1 %}*/
/*         {% endfor %} */
/*         </div>*/
/*         {% endif %}*/
/*         {% if tags %}*/
/*         <p>{{ text_tags }}*/
/*         {% for i in 0..tags|length %}*/
/*         {% if i < (tags|length - 1) %} <a href="{{ tags[i].href }}">{{ tags[i].tag }}</a>,*/
/*         {% else %} <a href="{{ tags[i].href }}">{{ tags[i].tag }}</a> {% endif %}*/
/*         {% endfor %} </p>*/
/*         {% endif %}*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/getRecurringDescription',*/
/* 		type: 'post',*/
/* 		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#recurring-description').html('');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* */
/* 			if (json['success']) {*/
/* 				$('#recurring-description').html(json['success']);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('#button-cart').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-cart').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-cart').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* */
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/* */
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* */
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* */
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('.breadcrumb').after('<div class="alert alert-success alert-dismissible">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* */
/* 				$('#cart > button').html('<span id="cart-total"><img src="image/catalog/cart-icon.png" alt="cart"> ' + json['total'] + '</span>');*/
/* */
/* 				$('html, body').animate({ scrollTop: 0 }, 'slow');*/
/* */
/* 				$('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/* 			}*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('.date').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickTime: false*/
/* });*/
/* */
/* $('.datetime').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickDate: true,*/
/* 	pickTime: true*/
/* });*/
/* */
/* $('.time').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickDate: false*/
/* });*/
/* */
/* $('button[id^=\'button-upload\']').on('click', function() {*/
/* 	var node = this;*/
/* */
/* 	$('#form-upload').remove();*/
/* */
/* 	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* */
/* 	$('#form-upload input[name=\'file\']').trigger('click');*/
/* */
/* 	if (typeof timer != 'undefined') {*/
/*     	clearInterval(timer);*/
/* 	}*/
/* */
/* 	timer = setInterval(function() {*/
/* 		if ($('#form-upload input[name=\'file\']').val() != '') {*/
/* 			clearInterval(timer);*/
/* */
/* 			$.ajax({*/
/* 				url: 'index.php?route=tool/upload',*/
/* 				type: 'post',*/
/* 				dataType: 'json',*/
/* 				data: new FormData($('#form-upload')[0]),*/
/* 				cache: false,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				beforeSend: function() {*/
/* 					$(node).button('loading');*/
/* 				},*/
/* 				complete: function() {*/
/* 					$(node).button('reset');*/
/* 				},*/
/* 				success: function(json) {*/
/* 					$('.text-danger').remove();*/
/* */
/* 					if (json['error']) {*/
/* 						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');*/
/* 					}*/
/* */
/* 					if (json['success']) {*/
/* 						alert(json['success']);*/
/* */
/* 						$(node).parent().find('input').val(json['code']);*/
/* 					}*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 		}*/
/* 	}, 500);*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('#review').delegate('.pagination a', 'click', function(e) {*/
/*     e.preventDefault();*/
/* */
/*     $('#review').fadeOut('slow');*/
/* */
/*     $('#review').load(this.href);*/
/* */
/*     $('#review').fadeIn('slow');*/
/* });*/
/* */
/* $('#review').load('index.php?route=product/product/review&product_id={{ product_id }}');*/
/* */
/* $('#button-review').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/write&product_id={{ product_id }}',*/
/* 		type: 'post',*/
/* 		dataType: 'json',*/
/* 		data: $("#form-review").serialize(),*/
/* 		beforeSend: function() {*/
/* 			$('#button-review').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-review').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible').remove();*/
/* */
/* 			if (json['error']) {*/
/* 				$('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/* 				// $('input[name=\'name\']').val('');*/
/* 				$('textarea[name=\'text\']').val('');*/
/* 				$('input[name=\'rating\']:checked').prop('checked', false);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* */
/* $(document).ready(function() {*/
/* 	$('.thumbnails').magnificPopup({*/
/* 		type:'image',*/
/* 		delegate: 'a',*/
/* 		gallery: {*/
/* 			enabled: true*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script> */
/* */
/* */
/* <script>*/
/*      if (jQuery(window).width() > 980){*/
/*         //initiate the plugin and pass the id of the div containing gallery images*/
/*             $("#zoom_03").elevateZoom({gallery:'sb-additional-img', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: ''});*/
/*         //pass the images to Fancybox*/
/*             $("#zoom_03").bind("click", function (e) {*/
/*             var ez = $('#zoom_03').data('elevateZoom');*/
/*             $.fancybox(ez.getGalleryList());*/
/*             return false;*/
/*             });*/
/*     }*/
/* </script>*/
/* */
/* <!--slider for product-->*/
/* <script type="text/javascript"><!--*/
/* $('#sb-additional-img').owlCarousel({*/
/*   itemsCustom : [*/
/*         [1, 1],  */
/*         [360, 2],*/
/*         [380, 3],*/
/*         [500, 4],*/
/*         [768, 4],*/
/*         [991, 3],*/
/*         [1199, 4]*/
/*         ],*/
/*   autoPlay: false,*/
/*   navigation: true,*/
/*   navigationText: ['<i class="fa fa-angle-left fa-5x" aria-hidden="true"></i>', '<i class="fa fa-angle-right fa-5x" aria-hidden="true"></i>'],*/
/*   pagination: false*/
/* });*/
/* --></script>*/
/* <!--over slider for product-->*/
/* {{ footer }} */
/* */
