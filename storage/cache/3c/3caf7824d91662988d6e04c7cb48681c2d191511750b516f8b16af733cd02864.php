<?php

/* default/template/common/home.twig */
class __TwigTemplate_9c55a8a6312cd8a14846269ea394128309babbb773fb758a876ef3c41ae15751 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

<div id=\"common-home\" class=\"container-fluid\">
  <div class=\"row\">";
        // line 4
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 5
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 6
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 7
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 8
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 11
            echo "    ";
        }
        // line 12
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\"><div class=\"row\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div></div>
    ";
        // line 13
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "
    
    ";
        // line 16
        echo "<script src=\"https://www.gstatic.com/firebasejs/4.6.2/firebase.js\"></script>
<script>  
  var config = {
    apiKey: \"AIzaSyCIcNM6B7Kg_5P2rVNc-yLGoY1_CLdaMQ4\",
    authDomain: \"https://c6346cb19029.ngrok.io\",
    databaseURL: \"https://medinatech-8d215.firebaseio.com\",
    storageBucket: \"medinatech-8d215.appspot.com\",
    messagingSenderId: \"185209964071\",
  };
  firebase.initializeApp(config);

  const messaging = firebase.messaging();
  messaging
    .requestPermission()
    .then(function () {
      return messaging.getToken()
    })
    .then(function(token) {
      console.log(token);
    })
    .catch(function (err) {
      console.log(\"Unable to get permission to notify.\", err);
    });

  messaging.onMessage(function(payload) {
    
    \$(document).ready(function(){
      console.log(payload);
    });
  });

  messaging.onTokenRefresh(() => {
  messaging.getToken().then((refreshedToken) => {
    console.log(refreshedToken)
  }).catch((err) => {
    console.log('Unable to retrieve refreshed token ', err);
  });
  });
</script>
";
        // line 56
        echo "
    </div>
</div>
";
        // line 59
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "

<script type=\"text/javascript\">
    /*Start Homepage header JS */
function header() {
    if (jQuery(window).width() > 992) {
        if (jQuery(this).scrollTop() > 400)
        {
            jQuery('header').addClass(\"fixed\");

        } else {
            jQuery('header').removeClass(\"fixed\");
        }
   } else {
   jQuery('header').removeClass(\"fixed\");
   }
}

\$(document).ready(function () {
    header();
});
jQuery(window).resize(function () {
    header();
});
jQuery(window).scroll(function () {
    header();
});

/*End Homepage header JS*/
</script>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 59,  103 => 56,  62 => 16,  57 => 13,  49 => 12,  46 => 11,  43 => 10,  40 => 9,  37 => 8,  34 => 7,  31 => 6,  29 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* <div id="common-home" class="container-fluid">*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}"><div class="row">{{ content_top }}{{ content_bottom }}</div></div>*/
/*     {{ column_right }}*/
/*     */
/*     {# Web Push Notification code start from here #}*/
/* <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>*/
/* <script>  */
/*   var config = {*/
/*     apiKey: "AIzaSyCIcNM6B7Kg_5P2rVNc-yLGoY1_CLdaMQ4",*/
/*     authDomain: "https://c6346cb19029.ngrok.io",*/
/*     databaseURL: "https://medinatech-8d215.firebaseio.com",*/
/*     storageBucket: "medinatech-8d215.appspot.com",*/
/*     messagingSenderId: "185209964071",*/
/*   };*/
/*   firebase.initializeApp(config);*/
/* */
/*   const messaging = firebase.messaging();*/
/*   messaging*/
/*     .requestPermission()*/
/*     .then(function () {*/
/*       return messaging.getToken()*/
/*     })*/
/*     .then(function(token) {*/
/*       console.log(token);*/
/*     })*/
/*     .catch(function (err) {*/
/*       console.log("Unable to get permission to notify.", err);*/
/*     });*/
/* */
/*   messaging.onMessage(function(payload) {*/
/*     */
/*     $(document).ready(function(){*/
/*       console.log(payload);*/
/*     });*/
/*   });*/
/* */
/*   messaging.onTokenRefresh(() => {*/
/*   messaging.getToken().then((refreshedToken) => {*/
/*     console.log(refreshedToken)*/
/*   }).catch((err) => {*/
/*     console.log('Unable to retrieve refreshed token ', err);*/
/*   });*/
/*   });*/
/* </script>*/
/* {# Web Push Notification code end here #}*/
/* */
/*     </div>*/
/* </div>*/
/* {{ footer }}*/
/* */
/* <script type="text/javascript">*/
/*     /*Start Homepage header JS *//* */
/* function header() {*/
/*     if (jQuery(window).width() > 992) {*/
/*         if (jQuery(this).scrollTop() > 400)*/
/*         {*/
/*             jQuery('header').addClass("fixed");*/
/* */
/*         } else {*/
/*             jQuery('header').removeClass("fixed");*/
/*         }*/
/*    } else {*/
/*    jQuery('header').removeClass("fixed");*/
/*    }*/
/* }*/
/* */
/* $(document).ready(function () {*/
/*     header();*/
/* });*/
/* jQuery(window).resize(function () {*/
/*     header();*/
/* });*/
/* jQuery(window).scroll(function () {*/
/*     header();*/
/* });*/
/* */
/* /*End Homepage header JS*//* */
/* </script>*/
/* */
