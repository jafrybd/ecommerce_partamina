<?php

/* __string_template__8b05ac5887f1b453aaf8741dea5337cc7529b123f6ddb83b8d94d8dd867edc7a */
class __TwigTemplate_88f968aee988a35198f32e942c66f6fcd4c372d4a238568832b15804aa122eff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 13
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 14
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 15
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
";
        }
        // line 17
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 18
            echo "<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
";
        }
        // line 20
        echo "<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i\" rel=\"stylesheet\"> 
<!-- image hover effect-->
<link href=\"catalog/view/javascript/hover-effect-image/main.css\" rel=\"stylesheet\">
<!-- image hover effect end-->
<link href=\"catalog/view/theme/default/stylesheet/stylesheet.css\" rel=\"stylesheet\">
";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 30
            echo "<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 33
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 37
            echo "<!-- product zoom start -->
<script src=\"catalog/view/javascript/zoom/jquery.elevatezoom.js\" type=\"text/javascript\"></script>
<!-- product zoom end -->
<!-- Sb Theme Custom javascript Start -->
<script src=\"catalog/view/javascript/sb-theme/sb-theme-custom.js\" type=\"text/javascript\"></script>
<!-- Sb Theme Custom javascript End -->
<!-- animation javascript Start -->
<script src=\"catalog/view/javascript/animation/custom.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/animation/animate.min.css\" rel=\"stylesheet\">
<!-- animation javascript End -->
<!--counter start -->
<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js\"></script>
<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js\"></script>
<!-- counter end -->
 <!--RTL start -->
     ";
            // line 52
            if (((isset($context["direction"]) ? $context["direction"] : null) == "rtl")) {
                // line 53
                echo "     <link href=\"catalog/view/theme/default/stylesheet/rtl.css\" rel=\"stylesheet\">
     ";
            }
            // line 55
            echo "        <!-- RTL END-->
<!--lightbox -->
    <!--    <script src=\"catalog/view/javascript/inspire/blog/lightbox-2.6.min.js\" type=\"text/javascript\"></script>
       <link href=\"catalog/view/javascript/inspire/blog/lightbox.css\" rel=\"stylesheet\" type=\"text/css\" /> -->
<!--lightbox End-->
<link href=\"";
            // line 60
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 63
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
      <div id=\"fb-root\"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v7.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class=\"fb-customerchat\"
        attribution=setup_tool
        page_id=\"100659961710614\"
  theme_color=\"#ed1c24\"
  logged_in_greeting=\"Hi! How can we help you?\"
  logged_out_greeting=\"Hi! How can we help you?\">
      </div> 
  <nav id=\"top\">
  <div class=\"container\">
      
    <p class=\"pull-left hidden-xs  sb-theme-free-ship\"> ";
        // line 96
        echo (isset($context["text_authorized"]) ? $context["text_authorized"] : null);
        echo " &nbsp; &nbsp; </p>
    
    ";
        // line 98
        echo (isset($context["language"]) ? $context["language"] : null);
        echo "
       <div id=\"top-links\" class=\"nav pull-right\">
      <ul class=\"list-inline\">
       <!--  <li><a href=\"";
        // line 101
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\"><i class=\"fa fa-phone\"></i></a> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "</span></li> -->
      
        
        <!-- <li><a href=\"";
        // line 104
        echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
        echo "\" title=\"";
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "</span></a></li> -->
        <!-- <li><a href=\"";
        // line 105
        echo (isset($context["checkout"]) ? $context["checkout"] : null);
        echo "\" title=\"";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "\"><i class=\"fa fa-share\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</span></a></li> -->
      </ul>
    </div>
  </div>
</nav>
<header>
  <div id=\"static-header\">
  <div class=\"container cmn-h-pd\">
    <div class=\"row\">
      <div class=\"col-sm-3 col-xs-5 t-logo\">
        <div id=\"logo mtop\" >";
        // line 115
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            echo "<a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive rr\" /></a>";
        } else {
            // line 116
            echo "          <h1><a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "</a></h1>
          ";
        }
        // line 117
        echo "</div>
      </div>
      <div id=\"navbar\" class=\"navbar-static-top\">
      <div class=\"col-sm-6 col-xs-12 t-search cntct-services\">
         ";
        // line 121
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "
      </div></div>
      <div class=\"col-sm-3 col-xs-6 t-search t-search-1 mtop\">
      <!--drop -->
      <ul class=\"sca\" type=\"none\">
        <li class=\"dropdown header-btns\">
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"image/catalog/search.png\"></a>
          <div class=\"dropdown-menu my-search\">
           ";
        // line 129
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "
          </div>
        </li>
        <li class=\"m-cart t-cart header-btns\">";
        // line 132
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "</li>
         <li class=\"dropdown my-account header-btns\"><a href=\"";
        // line 133
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"image/catalog/user.png\" alt=\"user\"> <!-- <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</span> <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i> --></a>
          <ul class=\"dropdown-menu\">
            ";
        // line 135
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 136
            echo "            <li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\"><i class=\"fa fa-address-book-o\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 137
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\"><i class=\"fa fa-history\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 138
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\"><i class=\"fa fa-retweet\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 139
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\"><i class=\"fa fa-download\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 140
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
            ";
        } else {
            // line 142
            echo "            <li><a href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\"><i class=\"fa fa-address-book-o\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 143
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
            ";
        }
        // line 145
        echo "          </ul>
        </li>        
      </ul>
      <!-- drop end-->
      </div>
            
    </div>
  </div>
  </div>

</header>

<script type=\"text/javascript\">  
\$(document).ready(function() {
    if (jQuery(window).width() > 767){
    \$('#static-header').affix({
        offset: {
           top: \$('header').height()
        }
    });
    }
});
</script>
";
    }

    public function getTemplateName()
    {
        return "__string_template__8b05ac5887f1b453aaf8741dea5337cc7529b123f6ddb83b8d94d8dd867edc7a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  352 => 145,  345 => 143,  338 => 142,  331 => 140,  325 => 139,  319 => 138,  313 => 137,  306 => 136,  304 => 135,  295 => 133,  291 => 132,  285 => 129,  274 => 121,  268 => 117,  260 => 116,  248 => 115,  231 => 105,  223 => 104,  215 => 101,  209 => 98,  204 => 96,  171 => 65,  163 => 63,  159 => 62,  149 => 60,  142 => 55,  138 => 53,  136 => 52,  119 => 37,  115 => 36,  112 => 35,  103 => 33,  99 => 32,  86 => 30,  82 => 29,  71 => 20,  65 => 18,  63 => 17,  57 => 15,  55 => 14,  51 => 13,  47 => 12,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if description %}*/
/* <meta name="description" content="{{ description }}" />*/
/* {% endif %}*/
/* {% if keywords %}*/
/* <meta name="keywords" content="{{ keywords }}" />*/
/* {% endif %}*/
/* <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />*/
/* <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/* <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> */
/* <!-- image hover effect-->*/
/* <link href="catalog/view/javascript/hover-effect-image/main.css" rel="stylesheet">*/
/* <!-- image hover effect end-->*/
/* <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">*/
/* {% for style in styles %}*/
/* <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* <script src="catalog/view/javascript/common.js" type="text/javascript"></script>*/
/* {% for link in links %}*/
/* <!-- product zoom start -->*/
/* <script src="catalog/view/javascript/zoom/jquery.elevatezoom.js" type="text/javascript"></script>*/
/* <!-- product zoom end -->*/
/* <!-- Sb Theme Custom javascript Start -->*/
/* <script src="catalog/view/javascript/sb-theme/sb-theme-custom.js" type="text/javascript"></script>*/
/* <!-- Sb Theme Custom javascript End -->*/
/* <!-- animation javascript Start -->*/
/* <script src="catalog/view/javascript/animation/custom.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/animation/animate.min.css" rel="stylesheet">*/
/* <!-- animation javascript End -->*/
/* <!--counter start -->*/
/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>*/
/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>*/
/* <!-- counter end -->*/
/*  <!--RTL start -->*/
/*      {% if direction == 'rtl' %}*/
/*      <link href="catalog/view/theme/default/stylesheet/rtl.css" rel="stylesheet">*/
/*      {% endif %}*/
/*         <!-- RTL END-->*/
/* <!--lightbox -->*/
/*     <!--    <script src="catalog/view/javascript/inspire/blog/lightbox-2.6.min.js" type="text/javascript"></script>*/
/*        <link href="catalog/view/javascript/inspire/blog/lightbox.css" rel="stylesheet" type="text/css" /> -->*/
/* <!--lightbox End-->*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endfor %}*/
/* {% for analytic in analytics %}*/
/* {{ analytic }}*/
/* {% endfor %}*/
/* </head>*/
/* <body>*/
/* <!-- Load Facebook SDK for JavaScript -->*/
/*       <div id="fb-root"></div>*/
/*       <script>*/
/*         window.fbAsyncInit = function() {*/
/*           FB.init({*/
/*             xfbml            : true,*/
/*             version          : 'v7.0'*/
/*           });*/
/*         };*/
/* */
/*         (function(d, s, id) {*/
/*         var js, fjs = d.getElementsByTagName(s)[0];*/
/*         if (d.getElementById(id)) return;*/
/*         js = d.createElement(s); js.id = id;*/
/*         js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';*/
/*         fjs.parentNode.insertBefore(js, fjs);*/
/*       }(document, 'script', 'facebook-jssdk'));</script>*/
/* */
/*       <!-- Your Chat Plugin code -->*/
/*       <div class="fb-customerchat"*/
/*         attribution=setup_tool*/
/*         page_id="100659961710614"*/
/*   theme_color="#ed1c24"*/
/*   logged_in_greeting="Hi! How can we help you?"*/
/*   logged_out_greeting="Hi! How can we help you?">*/
/*       </div> */
/*   <nav id="top">*/
/*   <div class="container">*/
/*       */
/*     <p class="pull-left hidden-xs  sb-theme-free-ship"> {{ text_authorized }} &nbsp; &nbsp; </p>*/
/*     */
/*     {{ language }}*/
/*        <div id="top-links" class="nav pull-right">*/
/*       <ul class="list-inline">*/
/*        <!--  <li><a href="{{ contact }}"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md">{{ telephone }}</span></li> -->*/
/*       */
/*         */
/*         <!-- <li><a href="{{ shopping_cart }}" title="{{ text_shopping_cart }}"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_shopping_cart }}</span></a></li> -->*/
/*         <!-- <li><a href="{{ checkout }}" title="{{ text_checkout }}"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_checkout }}</span></a></li> -->*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/* </nav>*/
/* <header>*/
/*   <div id="static-header">*/
/*   <div class="container cmn-h-pd">*/
/*     <div class="row">*/
/*       <div class="col-sm-3 col-xs-5 t-logo">*/
/*         <div id="logo mtop" >{% if logo %}<a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" class="img-responsive rr" /></a>{% else %}*/
/*           <h1><a href="{{ home }}">{{ name }}</a></h1>*/
/*           {% endif %}</div>*/
/*       </div>*/
/*       <div id="navbar" class="navbar-static-top">*/
/*       <div class="col-sm-6 col-xs-12 t-search cntct-services">*/
/*          {{ menu }}*/
/*       </div></div>*/
/*       <div class="col-sm-3 col-xs-6 t-search t-search-1 mtop">*/
/*       <!--drop -->*/
/*       <ul class="sca" type="none">*/
/*         <li class="dropdown header-btns">*/
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="image/catalog/search.png"></a>*/
/*           <div class="dropdown-menu my-search">*/
/*            {{ search }}*/
/*           </div>*/
/*         </li>*/
/*         <li class="m-cart t-cart header-btns">{{ cart }}</li>*/
/*          <li class="dropdown my-account header-btns"><a href="{{ account }}" title="{{ text_account }}" class="dropdown-toggle" data-toggle="dropdown"><img src="image/catalog/user.png" alt="user"> <!-- <span class="hidden-xs hidden-sm hidden-md">{{ text_account }}</span> <i class="fa fa-angle-down" aria-hidden="true"></i> --></a>*/
/*           <ul class="dropdown-menu">*/
/*             {% if logged %}*/
/*             <li><a href="{{ account }}"><i class="fa fa-address-book-o" aria-hidden="true"></i> {{ text_account }}</a></li>*/
/*             <li><a href="{{ order }}"><i class="fa fa-history" aria-hidden="true"></i> {{ text_order }}</a></li>*/
/*             <li><a href="{{ transaction }}"><i class="fa fa-retweet" aria-hidden="true"></i> {{ text_transaction }}</a></li>*/
/*             <li><a href="{{ download }}"><i class="fa fa-download" aria-hidden="true"></i> {{ text_download }}</a></li>*/
/*             <li><a href="{{ logout }}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{ text_logout }}</a></li>*/
/*             {% else %}*/
/*             <li><a href="{{ register }}"><i class="fa fa-address-book-o" aria-hidden="true"></i> {{ text_register }}</a></li>*/
/*             <li><a href="{{ login }}"><i class="fa fa-sign-in" aria-hidden="true"></i> {{ text_login }}</a></li>*/
/*             {% endif %}*/
/*           </ul>*/
/*         </li>        */
/*       </ul>*/
/*       <!-- drop end-->*/
/*       </div>*/
/*             */
/*     </div>*/
/*   </div>*/
/*   </div>*/
/* */
/* </header>*/
/* */
/* <script type="text/javascript">  */
/* $(document).ready(function() {*/
/*     if (jQuery(window).width() > 767){*/
/*     $('#static-header').affix({*/
/*         offset: {*/
/*            top: $('header').height()*/
/*         }*/
/*     });*/
/*     }*/
/* });*/
/* </script>*/
/* */
