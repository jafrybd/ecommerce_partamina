<?php

/* __string_template__4c97a7dae69d46f5192168a845eb50e068c27710a99dcf13e78653d68d09cf51 */
class __TwigTemplate_83fd7c606aae5d9dedf9305fb8e4c938a04d1959af826ccada6dd4c09688a027 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"tab-pane\" id=\"best\">
<div class=\"container\">
<div id=\"bestseller-product\" class=\"owl-carousel owl-theme arrow-top\">
 ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 5
            echo "   <div class=\"product-layout col-lg-12 col-md-12 col-sm-12 col-xs-12\">
    <div class=\"product-thumb transition\">
      <div class=\"image\">
      <a href=\"";
            // line 8
            echo $this->getAttribute($context["product"], "href", array());
            echo "\"><img src=\"";
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" class=\"img-responsive\" /></a></div>
        ";
            // line 9
            if ($this->getAttribute($context["product"], "price", array())) {
                // line 10
                echo "          ";
                if ($this->getAttribute($context["product"], "special", array())) {
                    // line 11
                    echo "               <div class=\"sale\"><img src=\"image/catalog/sale-sticker.png\" alt=\"sale\" class=\"sale product\"></div>
          ";
                }
                // line 13
                echo "      ";
            }
            // line 14
            echo "      <div class=\"caption\">
        <h4><a href=\"";
            // line 15
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a></h4>
        <p class=\"hidden-xs hidden-sm hidden-md hidden-lg\">";
            // line 16
            echo $this->getAttribute($context["product"], "description", array());
            echo "</p>
       
        <div class=\"rating\">";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 19
                echo "          ";
                if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                    echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                } else {
                    echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                }
                // line 20
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</div>
        ";
            // line 21
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 22
                echo "        ";
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 23
                    echo "        <p class=\"price\"> ";
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 24
                        echo "          ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
          ";
                    } else {
                        // line 25
                        echo " <span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span> ";
                    }
                    // line 26
                    echo "          ";
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        echo " <span class=\"price-tax hidden-xs hidden-sm hidden-md hidden-lg\">";
                        echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span> ";
                    }
                    echo " </p>
        ";
                }
                // line 27
                echo " 
        ";
            }
            // line 28
            echo " 
      </div>
        <div class=\"button-group sb-btn-group\">                
             ";
            // line 31
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                echo "    <button type=\"button\" class=\"ad-to-cart\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-shopping-basket hidden-lg hidden-md\"></i> <span class=\"hidden-xs hidden-sm\">";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button>";
            }
            // line 32
            echo "                <button type=\"button\" class=\"theme-btn theme-btn-w\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
            echo "\" onclick=\"wishlist.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-heart\"></i></button>                
                <button type=\"button\" class=\"theme-btn theme-btn-c\" data-toggle=\"tooltip\" title=\"";
            // line 33
            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
            echo "\" onclick=\"compare.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-refresh\"></i></button>
              </div>
    </div>
  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "</div>
</div>
</div>
</div>
</div>
<script type=\"text/javascript\">
    \$(document).ready(function() {
    \$(\"#bestseller-product\").owlCarousel({
    itemsCustom : [
    [0, 1],
    [500, 2],
    [768, 2],
    [992, 3],
    [1200, 4],
    ],
            // autoPlay: 1000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : true,
            pagination:false
    });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "__string_template__4c97a7dae69d46f5192168a845eb50e068c27710a99dcf13e78653d68d09cf51";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 38,  145 => 33,  138 => 32,  130 => 31,  125 => 28,  121 => 27,  109 => 26,  102 => 25,  96 => 24,  93 => 23,  90 => 22,  88 => 21,  80 => 20,  73 => 19,  69 => 18,  64 => 16,  58 => 15,  55 => 14,  52 => 13,  48 => 11,  45 => 10,  43 => 9,  33 => 8,  28 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="tab-pane" id="best">*/
/* <div class="container">*/
/* <div id="bestseller-product" class="owl-carousel owl-theme arrow-top">*/
/*  {% for product in products %}*/
/*    <div class="product-layout col-lg-12 col-md-12 col-sm-12 col-xs-12">*/
/*     <div class="product-thumb transition">*/
/*       <div class="image">*/
/*       <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" /></a></div>*/
/*         {% if product.price %}*/
/*           {% if product.special %}*/
/*                <div class="sale"><img src="image/catalog/sale-sticker.png" alt="sale" class="sale product"></div>*/
/*           {% endif %}*/
/*       {% endif %}*/
/*       <div class="caption">*/
/*         <h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/*         <p class="hidden-xs hidden-sm hidden-md hidden-lg">{{ product.description }}</p>*/
/*        */
/*         <div class="rating">{% for i in 1..5 %}*/
/*           {% if product.rating < i %} <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> {% else %} <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> {% endif %}*/
/*           {% endfor %}</div>*/
/*         {% if review_guest %}*/
/*         {% if product.price %}*/
/*         <p class="price"> {% if not product.special %}*/
/*           {{ product.price }}*/
/*           {% else %} <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span> {% endif %}*/
/*           {% if product.tax %} <span class="price-tax hidden-xs hidden-sm hidden-md hidden-lg">{{ text_tax }} {{ product.tax }}</span> {% endif %} </p>*/
/*         {% endif %} */
/*         {% endif %} */
/*       </div>*/
/*         <div class="button-group sb-btn-group">                */
/*              {% if review_guest %}    <button type="button" class="ad-to-cart" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-basket hidden-lg hidden-md"></i> <span class="hidden-xs hidden-sm">{{ button_cart }}</span></button>{% endif %}*/
/*                 <button type="button" class="theme-btn theme-btn-w" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>                */
/*                 <button type="button" class="theme-btn theme-btn-c" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-refresh"></i></button>*/
/*               </div>*/
/*     </div>*/
/*   </div>*/
/*   {% endfor %}*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript">*/
/*     $(document).ready(function() {*/
/*     $("#bestseller-product").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 1],*/
/*     [500, 2],*/
/*     [768, 2],*/
/*     [992, 3],*/
/*     [1200, 4],*/
/*     ],*/
/*             // autoPlay: 1000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : true,*/
/*             pagination:false*/
/*     });*/
/*     });*/
/* </script>*/
/* */
