<?php

/* __string_template__658563fabd6635ddf73d584e4ffec865baa9e93f242fe01199828827a908187b */
class __TwigTemplate_a1445e7f5da540cd367bb0dd4275e6fee1e96b716953e45788729df33f44c7ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"breadcrumb-bg\">
  <div class=\"container\">
    <ul class=\"breadcrumb\">
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 6
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "  </ul>
  </div>
</div>
<div id=\"product-category\" class=\"container sb-theme-cmn-border\">  
  <h2 class=\"theme-title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h2>
  <div class=\"row\">";
        // line 13
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 14
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 15
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 16
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 17
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 18
            echo "    ";
        } else {
            // line 19
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 20
            echo "    ";
        }
        // line 21
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\" style=\"top:0px!important;\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "      
     <!--  ";
        // line 22
        if (((isset($context["thumb"]) ? $context["thumb"] : null) || (isset($context["description"]) ? $context["description"] : null))) {
            // line 23
            echo "      <div class=\"row\"> ";
            if ((isset($context["thumb"]) ? $context["thumb"] : null)) {
                // line 24
                echo "        <div class=\"col-sm-12 sb-theme-p-0 sb-theme-images-effect\"><a href=\"#\"><img src=\"";
                echo (isset($context["thumb"]) ? $context["thumb"] : null);
                echo "\" alt=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" title=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" class=\"img-thumbnail\" /></a></div>
        ";
            }
            // line 25
            echo " 
        ";
            // line 26
            if ((isset($context["description"]) ? $context["description"] : null)) {
                // line 27
                echo "        <div class=\"col-sm-12 sb-theme-cat-desc\">";
                echo (isset($context["description"]) ? $context["description"] : null);
                echo "</div>
        ";
            }
            // line 28
            echo "</div>
    
      ";
        }
        // line 30
        echo " -->
      ";
        // line 31
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 32
            echo "      <h3>";
            echo (isset($context["text_refine"]) ? $context["text_refine"] : null);
            echo "</h3>
      ";
            // line 33
            if ((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) <= 5)) {
                // line 34
                echo "      <div class=\"row\">
        <div class=\"col-sm-12 sb-theme-cat-list\">
          <ul>
            ";
                // line 37
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 38
                    echo "            <li><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 40
                echo "          </ul>
        </div>
      </div>
      ";
            } else {
                // line 44
                echo "      <div class=\"row\">";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch((isset($context["categories"]) ? $context["categories"] : null), twig_round((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) / 4), 1, "ceil")));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 45
                    echo "        <div class=\"col-sm-3\">
          <ul>
            ";
                    // line 47
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["category"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 48
                        echo "            <li><a href=\"";
                        echo $this->getAttribute($context["child"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["child"], "name", array());
                        echo "</a></li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 50
                    echo "          </ul>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "</div>
      <br />
      ";
            }
            // line 55
            echo "      ";
        }
        // line 56
        echo "      ";
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 57
            echo "       <br/>
      <div class=\"row\">
        <div class=\"col-md-2 col-sm-6\">
          <div class=\"sb-theme-list-grid\">
            <button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 61
            echo (isset($context["button_list"]) ? $context["button_list"] : null);
            echo "\"><img src=\"image/catalog/view-list.png\" alt=\"list view\"></button>
            <button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 62
            echo (isset($context["button_grid"]) ? $context["button_grid"] : null);
            echo "\"><img src=\"image/catalog/view-grid.png\" alt=\"Grid view\"></button>
          </div>
        </div>
        <div class=\"col-md-3 col-sm-6\">
          <div class=\"form-group sb-theme-cat-cmpr\"><a href=\"";
            // line 66
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\" id=\"compare-total\" class=\"btn btn-link\">";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo "</a></div>
        </div>
        <div class=\"col-md-4 col-xs-6\">
          <div class=\"form-group input-group\">
            <label class=\"input-group-addon srt-by\" for=\"input-sort\">";
            // line 70
            echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
            echo "</label>
            <select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              ";
            // line 75
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 76
                echo "              ";
                if (($this->getAttribute($context["sorts"], "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
                    // line 77
                    echo "              
              
              
              <option value=\"";
                    // line 80
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
              
              
              
              ";
                } else {
                    // line 85
                    echo "              
              
              
              <option value=\"";
                    // line 88
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
              
              
              
              ";
                }
                // line 93
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "            
            
            
            </select>
          </div>
        </div>
        <div class=\"col-md-3 col-xs-6\">
          <div class=\"form-group input-group \">
            <label class=\"input-group-addon srt-by\" for=\"input-limit\">";
            // line 102
            echo (isset($context["text_limit"]) ? $context["text_limit"] : null);
            echo "</label>
            <select id=\"input-limit\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              ";
            // line 107
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 108
                echo "              ";
                if (($this->getAttribute($context["limits"], "value", array()) == (isset($context["limit"]) ? $context["limit"] : null))) {
                    // line 109
                    echo "              
              
              
              <option value=\"";
                    // line 112
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
              
              
              
              ";
                } else {
                    // line 117
                    echo "              
              
              
              <option value=\"";
                    // line 120
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
              
              
              
              ";
                }
                // line 125
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 126
            echo "            
            
            
            </select>
          </div>
        </div>
      </div>
      <div class=\"row category-products\"> ";
            // line 133
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 134
                echo "        <div class=\"product-layout product-list col-xs-12 \">
          <div class=\"product-thumb\">
            <div class=\"image\">
              <a href=\"";
                // line 137
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">
                <img src=\"";
                // line 138
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive\" />
              </a>
            </div>
            <div>
             
                ";
                // line 143
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 144
                    echo "              ";
                    if ($this->getAttribute($context["product"], "special", array())) {
                        // line 145
                        echo "                 <div class=\"sale\"><img src=\"image/catalog/sale-sticker.png\" alt=\"sale\" class=\"sale product\"></div>
              ";
                    }
                    // line 147
                    echo "                ";
                }
                // line 148
                echo "              <div class=\"caption\">
                <h4><a href=\"";
                // line 149
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4>
                <p class=\"sb-theme-p-desc\">";
                // line 150
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>
                ";
                // line 151
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 152
                    echo "                ";
                    if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                        // line 153
                        echo "                <p class=\"price\"> ";
                        if ( !$this->getAttribute($context["product"], "special", array())) {
                            // line 154
                            echo "                  ";
                            echo $this->getAttribute($context["product"], "price", array());
                            echo "
                  ";
                        } else {
                            // line 155
                            echo " <span class=\"price-new\">";
                            echo $this->getAttribute($context["product"], "special", array());
                            echo "</span> <span class=\"price-old\">";
                            echo $this->getAttribute($context["product"], "price", array());
                            echo "</span> ";
                        }
                        // line 156
                        echo "                  ";
                        if ($this->getAttribute($context["product"], "tax", array())) {
                            echo " <span class=\"price-tax  hidden-xs hidden-sm hidden-md hidden-lg\">";
                            echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                            echo " ";
                            echo $this->getAttribute($context["product"], "tax", array());
                            echo "</span> ";
                        }
                        echo " </p>
                ";
                    }
                    // line 158
                    echo "               ";
                }
                // line 159
                echo "                <div class=\"rating\"> ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 160
                    echo "                  ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                    } else {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
                    }
                    // line 161
                    echo "                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " </div>
               
                </div>
              <div class=\"button-group sb-btn-group\">  
                ";
                // line 165
                if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                    // line 166
                    echo "                <button type=\"button\" class=\"ad-to-cart\" onclick=\"cart.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-shopping-basket hidden-lg hidden-md\"></i> <span class=\"hidden-xs hidden-sm\">";
                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                    echo "</span></button>
                
                <button type=\"button\" class=\"theme-btn theme-btn-w\" data-toggle=\"tooltip\" title=\"";
                    // line 168
                    echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                    echo "\" onclick=\"wishlist.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-heart\"></i></button>                
                <button type=\"button\" class=\"theme-btn theme-btn-c\" data-toggle=\"tooltip\" title=\"";
                    // line 169
                    echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                    echo "\" onclick=\"compare.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-refresh\"></i></button>
                   ";
                }
                // line 171
                echo "              </div>
            </div>
          </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo " </div>
      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">";
            // line 177
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "</div>
        <div class=\"col-sm-6 text-right\">";
            // line 178
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</div>
      </div>
      ";
        }
        // line 181
        echo "      ";
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 182
            echo "      <p>Sorry, Product List for this Category is Empty !</p>
      ";
            // line 184
            echo "      ";
            // line 185
            echo "      ";
            // line 186
            echo "      ";
            // line 187
            echo "      ";
        }
        // line 188
        echo "      ";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 189
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 191
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "__string_template__658563fabd6635ddf73d584e4ffec865baa9e93f242fe01199828827a908187b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  522 => 191,  517 => 189,  512 => 188,  509 => 187,  507 => 186,  505 => 185,  503 => 184,  500 => 182,  497 => 181,  491 => 178,  487 => 177,  483 => 175,  473 => 171,  466 => 169,  460 => 168,  452 => 166,  450 => 165,  439 => 161,  432 => 160,  427 => 159,  424 => 158,  412 => 156,  405 => 155,  399 => 154,  396 => 153,  393 => 152,  391 => 151,  387 => 150,  381 => 149,  378 => 148,  375 => 147,  371 => 145,  368 => 144,  366 => 143,  354 => 138,  350 => 137,  345 => 134,  341 => 133,  332 => 126,  326 => 125,  316 => 120,  311 => 117,  301 => 112,  296 => 109,  293 => 108,  289 => 107,  281 => 102,  271 => 94,  265 => 93,  255 => 88,  250 => 85,  240 => 80,  235 => 77,  232 => 76,  228 => 75,  220 => 70,  211 => 66,  204 => 62,  200 => 61,  194 => 57,  191 => 56,  188 => 55,  183 => 52,  175 => 50,  164 => 48,  160 => 47,  156 => 45,  151 => 44,  145 => 40,  134 => 38,  130 => 37,  125 => 34,  123 => 33,  118 => 32,  116 => 31,  113 => 30,  108 => 28,  102 => 27,  100 => 26,  97 => 25,  87 => 24,  84 => 23,  82 => 22,  75 => 21,  72 => 20,  69 => 19,  66 => 18,  63 => 17,  60 => 16,  57 => 15,  55 => 14,  51 => 13,  47 => 12,  41 => 8,  30 => 6,  26 => 5,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="breadcrumb-bg">*/
/*   <div class="container">*/
/*     <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   </div>*/
/* </div>*/
/* <div id="product-category" class="container sb-theme-cmn-border">  */
/*   <h2 class="theme-title">{{ heading_title }}</h2>*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}" style="top:0px!important;">{{ content_top }}      */
/*      <!--  {% if thumb or description %}*/
/*       <div class="row"> {% if thumb %}*/
/*         <div class="col-sm-12 sb-theme-p-0 sb-theme-images-effect"><a href="#"><img src="{{ thumb }}" alt="{{ heading_title }}" title="{{ heading_title }}" class="img-thumbnail" /></a></div>*/
/*         {% endif %} */
/*         {% if description %}*/
/*         <div class="col-sm-12 sb-theme-cat-desc">{{ description }}</div>*/
/*         {% endif %}</div>*/
/*     */
/*       {% endif %} -->*/
/*       {% if categories %}*/
/*       <h3>{{ text_refine }}</h3>*/
/*       {% if categories|length <= 5 %}*/
/*       <div class="row">*/
/*         <div class="col-sm-12 sb-theme-cat-list">*/
/*           <ul>*/
/*             {% for category in categories %}*/
/*             <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*         </div>*/
/*       </div>*/
/*       {% else %}*/
/*       <div class="row">{% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}*/
/*         <div class="col-sm-3">*/
/*           <ul>*/
/*             {% for child in category %}*/
/*             <li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*         </div>*/
/*         {% endfor %}</div>*/
/*       <br />*/
/*       {% endif %}*/
/*       {% endif %}*/
/*       {% if products %}*/
/*        <br/>*/
/*       <div class="row">*/
/*         <div class="col-md-2 col-sm-6">*/
/*           <div class="sb-theme-list-grid">*/
/*             <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_list }}"><img src="image/catalog/view-list.png" alt="list view"></button>*/
/*             <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_grid }}"><img src="image/catalog/view-grid.png" alt="Grid view"></button>*/
/*           </div>*/
/*         </div>*/
/*         <div class="col-md-3 col-sm-6">*/
/*           <div class="form-group sb-theme-cat-cmpr"><a href="{{ compare }}" id="compare-total" class="btn btn-link">{{ text_compare }}</a></div>*/
/*         </div>*/
/*         <div class="col-md-4 col-xs-6">*/
/*           <div class="form-group input-group">*/
/*             <label class="input-group-addon srt-by" for="input-sort">{{ text_sort }}</label>*/
/*             <select id="input-sort" class="form-control" onchange="location = this.value;">*/
/*               */
/*               */
/*               */
/*               {% for sorts in sorts %}*/
/*               {% if sorts.value == '%s-%s'|format(sort, order) %}*/
/*               */
/*               */
/*               */
/*               <option value="{{ sorts.href }}" selected="selected">{{ sorts.text }}</option>*/
/*               */
/*               */
/*               */
/*               {% else %}*/
/*               */
/*               */
/*               */
/*               <option value="{{ sorts.href }}">{{ sorts.text }}</option>*/
/*               */
/*               */
/*               */
/*               {% endif %}*/
/*               {% endfor %}*/
/*             */
/*             */
/*             */
/*             </select>*/
/*           </div>*/
/*         </div>*/
/*         <div class="col-md-3 col-xs-6">*/
/*           <div class="form-group input-group ">*/
/*             <label class="input-group-addon srt-by" for="input-limit">{{ text_limit }}</label>*/
/*             <select id="input-limit" class="form-control" onchange="location = this.value;">*/
/*               */
/*               */
/*               */
/*               {% for limits in limits %}*/
/*               {% if limits.value == limit %}*/
/*               */
/*               */
/*               */
/*               <option value="{{ limits.href }}" selected="selected">{{ limits.text }}</option>*/
/*               */
/*               */
/*               */
/*               {% else %}*/
/*               */
/*               */
/*               */
/*               <option value="{{ limits.href }}">{{ limits.text }}</option>*/
/*               */
/*               */
/*               */
/*               {% endif %}*/
/*               {% endfor %}*/
/*             */
/*             */
/*             */
/*             </select>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="row category-products"> {% for product in products %}*/
/*         <div class="product-layout product-list col-xs-12 ">*/
/*           <div class="product-thumb">*/
/*             <div class="image">*/
/*               <a href="{{ product.href }}">*/
/*                 <img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/*               </a>*/
/*             </div>*/
/*             <div>*/
/*              */
/*                 {% if product.price %}*/
/*               {% if product.special %}*/
/*                  <div class="sale"><img src="image/catalog/sale-sticker.png" alt="sale" class="sale product"></div>*/
/*               {% endif %}*/
/*                 {% endif %}*/
/*               <div class="caption">*/
/*                 <h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/*                 <p class="sb-theme-p-desc">{{ product.description }}</p>*/
/*                 {% if product.price %}*/
/*                 {% if review_guest %}*/
/*                 <p class="price"> {% if not product.special %}*/
/*                   {{ product.price }}*/
/*                   {% else %} <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span> {% endif %}*/
/*                   {% if product.tax %} <span class="price-tax  hidden-xs hidden-sm hidden-md hidden-lg">{{ text_tax }} {{ product.tax }}</span> {% endif %} </p>*/
/*                 {% endif %}*/
/*                {% endif %}*/
/*                 <div class="rating"> {% for i in 1..5 %}*/
/*                   {% if product.rating < i %} <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> {% else %} <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>{% endif %}*/
/*                   {% endfor %} </div>*/
/*                */
/*                 </div>*/
/*               <div class="button-group sb-btn-group">  */
/*                 {% if review_guest %}*/
/*                 <button type="button" class="ad-to-cart" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-basket hidden-lg hidden-md"></i> <span class="hidden-xs hidden-sm">{{ button_cart }}</span></button>*/
/*                 */
/*                 <button type="button" class="theme-btn theme-btn-w" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>                */
/*                 <button type="button" class="theme-btn theme-btn-c" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-refresh"></i></button>*/
/*                    {% endif %}*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         {% endfor %} </div>*/
/*       <div class="row">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {% if not categories and not products %}*/
/*       <p>Sorry, Product List for this Category is Empty !</p>*/
/*       {#<p>{{ text_empty }}</p>#}*/
/*       {#<div class="buttons">#}*/
/*       {#  <div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>#}*/
/*       {#</div>#}*/
/*       {% endif %}*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }} */
/* */
