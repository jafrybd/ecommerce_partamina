<?php

/* default/template/product/compare.twig */
class __TwigTemplate_ce017a19415a3e15a1f04c45bd2a6138bbfdff7e9c386646ed64e2521577937e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"breadcrumb-bg\">
  <div class=\"container\">
    <ul class=\"breadcrumb\">
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 6
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "  </ul>
  </div>
</div>
<div id=\"product-compare\" class=\"container\">  
  ";
        // line 12
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 13
            echo "  <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 17
        echo "  <div class=\"row\">";
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 18
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 19
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 20
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 21
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 22
            echo "    ";
        } else {
            // line 23
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 24
            echo "    ";
        }
        // line 25
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h1>";
        // line 26
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      ";
        // line 27
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 28
            echo "      <table class=\"table table-bordered\">
        <thead>
          <tr>
            <td colspan=\"";
            // line 31
            echo (twig_length_filter($this->env, (isset($context["products"]) ? $context["products"] : null)) + 1);
            echo "\"><strong>";
            echo (isset($context["text_product"]) ? $context["text_product"] : null);
            echo "</strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>";
            // line 36
            echo (isset($context["text_name"]) ? $context["text_name"] : null);
            echo "</td>
            ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 38
                echo "            <td><a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><strong>";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</strong></a></td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo " </tr>
          <tr>
            <td>";
            // line 41
            echo (isset($context["text_image"]) ? $context["text_image"] : null);
            echo "</td>
            ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 43
                echo "            <td class=\"text-center\">";
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    echo " <img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-thumbnail\" /> ";
                }
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo " </tr>
          <tr>
            <td>";
            // line 46
            echo (isset($context["text_price"]) ? $context["text_price"] : null);
            echo "</td>
            ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 48
                echo "            <td>";
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 49
                    echo "              ";
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 50
                        echo "              ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
              ";
                    } else {
                        // line 51
                        echo " <strike>";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</strike> ";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "
              ";
                    }
                    // line 53
                    echo "              ";
                }
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo " </tr>
          <tr>
            <td>";
            // line 56
            echo (isset($context["text_model"]) ? $context["text_model"] : null);
            echo "</td>
            ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 58
                echo "            <td>";
                echo $this->getAttribute($context["product"], "model", array());
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo " </tr>
          <tr>
            <td>";
            // line 61
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo "</td>
            ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 63
                echo "            <td>";
                echo $this->getAttribute($context["product"], "manufacturer", array());
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo " </tr>
          <tr>
            <td>";
            // line 66
            echo (isset($context["text_availability"]) ? $context["text_availability"] : null);
            echo "</td>
            ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 68
                echo "            <td>";
                echo $this->getAttribute($context["product"], "availability", array());
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo " </tr>
        ";
            // line 70
            if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
                // line 71
                echo "        <tr>
          <td>";
                // line 72
                echo (isset($context["text_rating"]) ? $context["text_rating"] : null);
                echo "</td>
          ";
                // line 73
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    // line 74
                    echo "          <td class=\"rating\"> ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 75
                        echo "            ";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                            echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                        } else {
                            echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                        }
                        // line 76
                        echo "            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo " <br />
            ";
                    // line 77
                    echo $this->getAttribute($context["product"], "reviews", array());
                    echo "</td>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo " </tr>
        ";
            }
            // line 80
            echo "        <tr>
          <td>";
            // line 81
            echo (isset($context["text_summary"]) ? $context["text_summary"] : null);
            echo "</td>
          ";
            // line 82
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 83
                echo "          <td class=\"description\">";
                echo $this->getAttribute($context["product"], "description", array());
                echo "</td>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo " </tr>
        <tr>
          <td>";
            // line 86
            echo (isset($context["text_weight"]) ? $context["text_weight"] : null);
            echo "</td>
          ";
            // line 87
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 88
                echo "          <td>";
                echo $this->getAttribute($context["product"], "weight", array());
                echo "</td>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo " </tr>
        <tr>
          <td>";
            // line 91
            echo (isset($context["text_dimension"]) ? $context["text_dimension"] : null);
            echo "</td>
          ";
            // line 92
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 93
                echo "          <td>";
                echo $this->getAttribute($context["product"], "length", array());
                echo " x ";
                echo $this->getAttribute($context["product"], "width", array());
                echo " x ";
                echo $this->getAttribute($context["product"], "height", array());
                echo "</td>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo " </tr>
          </tbody>
        
        ";
            // line 97
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 98
                echo "        <thead>
          <tr>
            <td colspan=\"";
                // line 100
                echo (twig_length_filter($this->env, (isset($context["products"]) ? $context["products"] : null)) + 1);
                echo "\"><strong>";
                echo $this->getAttribute($context["attribute_group"], "name", array());
                echo "</strong></td>
          </tr>
        </thead>
        ";
                // line 103
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
                foreach ($context['_seq'] as $context["key"] => $context["attribute"]) {
                    // line 104
                    echo "        <tbody>
          <tr>
            <td>";
                    // line 106
                    echo $this->getAttribute($context["attribute"], "name", array());
                    echo "</td>
            ";
                    // line 107
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                        // line 108
                        echo "            ";
                        if ($this->getAttribute($this->getAttribute($context["product"], "attribute", array()), $context["key"], array(), "array")) {
                            // line 109
                            echo "            <td> ";
                            echo $this->getAttribute($this->getAttribute($context["product"], "attribute", array()), $context["key"], array(), "array");
                            echo "</td>
            ";
                        } else {
                            // line 111
                            echo "            <td></td>
            ";
                        }
                        // line 113
                        echo "            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 114
                    echo "          </tr>
        </tbody>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 117
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "        <tr>
          <td></td>
          ";
            // line 120
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 121
                echo "          <td><input type=\"button\" value=\"";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "\" class=\"btn btn-primary btn-block\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "');\" />
            <a href=\"";
                // line 122
                echo $this->getAttribute($context["product"], "remove", array());
                echo "\" class=\"btn btn-danger btn-block\">";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "</a></td>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo " </tr>
      </table>
      ";
        } else {
            // line 126
            echo "      <p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
      <div class=\"buttons\">
        <div class=\"pull-right\"><a href=\"";
            // line 128
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-default\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
      </div>
      ";
        }
        // line 131
        echo "      ";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 132
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 134
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/product/compare.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  502 => 134,  497 => 132,  492 => 131,  484 => 128,  478 => 126,  473 => 123,  463 => 122,  454 => 121,  450 => 120,  446 => 118,  440 => 117,  432 => 114,  426 => 113,  422 => 111,  416 => 109,  413 => 108,  409 => 107,  405 => 106,  401 => 104,  397 => 103,  389 => 100,  385 => 98,  381 => 97,  376 => 94,  363 => 93,  359 => 92,  355 => 91,  351 => 89,  342 => 88,  338 => 87,  334 => 86,  330 => 84,  321 => 83,  317 => 82,  313 => 81,  310 => 80,  306 => 78,  298 => 77,  290 => 76,  283 => 75,  278 => 74,  274 => 73,  270 => 72,  267 => 71,  265 => 70,  262 => 69,  253 => 68,  249 => 67,  245 => 66,  241 => 64,  232 => 63,  228 => 62,  224 => 61,  220 => 59,  211 => 58,  207 => 57,  203 => 56,  199 => 54,  190 => 53,  182 => 51,  176 => 50,  173 => 49,  170 => 48,  166 => 47,  162 => 46,  158 => 44,  141 => 43,  137 => 42,  133 => 41,  129 => 39,  118 => 38,  114 => 37,  110 => 36,  100 => 31,  95 => 28,  93 => 27,  89 => 26,  82 => 25,  79 => 24,  76 => 23,  73 => 22,  70 => 21,  67 => 20,  64 => 19,  62 => 18,  57 => 17,  49 => 13,  47 => 12,  41 => 8,  30 => 6,  26 => 5,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="breadcrumb-bg">*/
/*   <div class="container">*/
/*     <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   </div>*/
/* </div>*/
/* <div id="product-compare" class="container">  */
/*   {% if success %}*/
/*   <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h1>{{ heading_title }}</h1>*/
/*       {% if products %}*/
/*       <table class="table table-bordered">*/
/*         <thead>*/
/*           <tr>*/
/*             <td colspan="{{ products|length + 1 }}"><strong>{{ text_product }}</strong></td>*/
/*           </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*           <tr>*/
/*             <td>{{ text_name }}</td>*/
/*             {% for product in products %}*/
/*             <td><a href="{{ product.href }}"><strong>{{ product.name }}</strong></a></td>*/
/*             {% endfor %} </tr>*/
/*           <tr>*/
/*             <td>{{ text_image }}</td>*/
/*             {% for product in products %}*/
/*             <td class="text-center">{% if product.thumb %} <img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /> {% endif %}</td>*/
/*             {% endfor %} </tr>*/
/*           <tr>*/
/*             <td>{{ text_price }}</td>*/
/*             {% for product in products %}*/
/*             <td>{% if product.price %}*/
/*               {% if not product.special %}*/
/*               {{ product.price }}*/
/*               {% else %} <strike>{{ product.price }}</strike> {{ product.special }}*/
/*               {% endif %}*/
/*               {% endif %}</td>*/
/*             {% endfor %} </tr>*/
/*           <tr>*/
/*             <td>{{ text_model }}</td>*/
/*             {% for product in products %}*/
/*             <td>{{ product.model }}</td>*/
/*             {% endfor %} </tr>*/
/*           <tr>*/
/*             <td>{{ text_manufacturer }}</td>*/
/*             {% for product in products %}*/
/*             <td>{{ product.manufacturer }}</td>*/
/*             {% endfor %} </tr>*/
/*           <tr>*/
/*             <td>{{ text_availability }}</td>*/
/*             {% for product in products %}*/
/*             <td>{{ product.availability }}</td>*/
/*             {% endfor %} </tr>*/
/*         {% if review_status %}*/
/*         <tr>*/
/*           <td>{{ text_rating }}</td>*/
/*           {% for product in products %}*/
/*           <td class="rating"> {% for i in 1..5 %}*/
/*             {% if product.rating < i %} <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> {% else %} <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> {% endif %}*/
/*             {% endfor %} <br />*/
/*             {{ product.reviews }}</td>*/
/*           {% endfor %} </tr>*/
/*         {% endif %}*/
/*         <tr>*/
/*           <td>{{ text_summary }}</td>*/
/*           {% for product in products %}*/
/*           <td class="description">{{ product.description }}</td>*/
/*           {% endfor %} </tr>*/
/*         <tr>*/
/*           <td>{{ text_weight }}</td>*/
/*           {% for product in products %}*/
/*           <td>{{ product.weight }}</td>*/
/*           {% endfor %} </tr>*/
/*         <tr>*/
/*           <td>{{ text_dimension }}</td>*/
/*           {% for product in products %}*/
/*           <td>{{ product.length }} x {{ product.width }} x {{ product.height }}</td>*/
/*           {% endfor %} </tr>*/
/*           </tbody>*/
/*         */
/*         {% for attribute_group in attribute_groups %}*/
/*         <thead>*/
/*           <tr>*/
/*             <td colspan="{{ products|length + 1 }}"><strong>{{ attribute_group.name }}</strong></td>*/
/*           </tr>*/
/*         </thead>*/
/*         {% for key, attribute in attribute_group.attribute %}*/
/*         <tbody>*/
/*           <tr>*/
/*             <td>{{ attribute.name }}</td>*/
/*             {% for product in products %}*/
/*             {% if product.attribute[key] %}*/
/*             <td> {{ product.attribute[key] }}</td>*/
/*             {% else %}*/
/*             <td></td>*/
/*             {% endif %}*/
/*             {% endfor %}*/
/*           </tr>*/
/*         </tbody>*/
/*         {% endfor %}*/
/*         {% endfor %}*/
/*         <tr>*/
/*           <td></td>*/
/*           {% for product in products %}*/
/*           <td><input type="button" value="{{ button_cart }}" class="btn btn-primary btn-block" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}');" />*/
/*             <a href="{{ product.remove }}" class="btn btn-danger btn-block">{{ button_remove }}</a></td>*/
/*           {% endfor %} </tr>*/
/*       </table>*/
/*       {% else %}*/
/*       <p>{{ text_empty }}</p>*/
/*       <div class="buttons">*/
/*         <div class="pull-right"><a href="{{ continue }}" class="btn btn-default">{{ button_continue }}</a></div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }} */
